<?php

namespace chargebee\ChargeBee;

class Result
{
    private $_response;
	
    private $_responseObj;

    function __construct($_response)
    {
            $this->_response = $_response;
            $this->_responseObj = array();
    }

    function subscription() 
    {
        $subscription = $this->_get('subscription', 'chargebee\ChargeBee\Models\Subscription', 
        array('addons' => 'chargebee\ChargeBee\Models\SubscriptionAddon', 'coupons' => 'chargebee\ChargeBee\Models\SubscriptionCoupon', 'shipping_address' => 'chargebee\ChargeBee\Models\SubscriptionShippingAddress'));
        return $subscription;
    }

    function customer() 
    {
        $customer = $this->_get('customer', 'chargebee\ChargeBee\Models\Customer', 
        array('billing_address' => 'chargebee\ChargeBee\Models\CustomerBillingAddress', 'contacts' => 'chargebee\ChargeBee\Models\CustomerContact', 'payment_method' => 'chargebee\ChargeBee\Models\CustomerPaymentMethod'));
        return $customer;
    }

    function card() 
    {
        $card = $this->_get('card', 'chargebee\ChargeBee\Models\Card');
        return $card;
    }

    function thirdPartyPaymentMethod() 
    {
        $third_party_payment_method = $this->_get('third_party_payment_method', 'chargebee\ChargeBee\Models\ThirdPartyPaymentMethod');
        return $third_party_payment_method;
    }

    function invoice() 
    {
        $invoice = $this->_get('invoice', 'chargebee\ChargeBee\Models\Invoice', 
        array('line_items' => 'chargebee\ChargeBee\Models\InvoiceLineItem', 'discounts' => 'chargebee\ChargeBee\Models\InvoiceDiscount', 'taxes' => 'chargebee\ChargeBee\Models\InvoiceTax', 'line_item_taxes' => 'chargebee\ChargeBee\Models\InvoiceLineItemTax', 'linked_payments' => 'chargebee\ChargeBee\Models\InvoiceLinkedPayment', 'applied_credits' => 'chargebee\ChargeBee\Models\InvoiceAppliedCredit', 'adjustment_credit_notes' => 'chargebee\ChargeBee\Models\InvoiceAdjustmentCreditNote', 'issued_credit_notes' => 'chargebee\ChargeBee\Models\InvoiceIssuedCreditNote', 'linked_orders' => 'chargebee\ChargeBee\Models\InvoiceLinkedOrder', 'notes' => 'chargebee\ChargeBee\Models\InvoiceNote', 'shipping_address' => 'chargebee\ChargeBee\Models\InvoiceShippingAddress', 'billing_address' => 'chargebee\ChargeBee\Models\InvoiceBillingAddress'));
        return $invoice;
    }

    function creditNote() 
    {
        $credit_note = $this->_get('credit_note', 'chargebee\ChargeBee\Models\CreditNote', 
        array('line_items' => 'chargebee\ChargeBee\Models\CreditNoteLineItem', 'discounts' => 'chargebee\ChargeBee\Models\CreditNoteDiscount', 'taxes' => 'chargebee\ChargeBee\Models\CreditNoteTax', 'line_item_taxes' => 'chargebee\ChargeBee\Models\CreditNoteLineItemTax', 'linked_refunds' => 'chargebee\ChargeBee\Models\CreditNoteLinkedRefund', 'allocations' => 'chargebee\ChargeBee\Models\CreditNoteAllocation'));
        return $credit_note;
    }

    function order() 
    {
        $order = $this->_get('order', 'chargebee\ChargeBee\Models\Order');
        return $order;
    }

    function transaction() 
    {
        $transaction = $this->_get('transaction', 'chargebee\ChargeBee\Models\Transaction', 
        array('linked_invoices' => 'chargebee\ChargeBee\Models\TransactionLinkedInvoice', 'linked_credit_notes' => 'chargebee\ChargeBee\Models\TransactionLinkedCreditNote', 'linked_refunds' => 'chargebee\ChargeBee\Models\TransactionLinkedRefund'));
        return $transaction;
    }

    function hostedPage() 
    {
        $hosted_page = $this->_get('hosted_page', 'chargebee\ChargeBee\Models\HostedPage');
        return $hosted_page;
    }

    function estimate() 
    {
        $estimate = $this->_get('estimate', 'chargebee\ChargeBee\Models\Estimate', array(),
        array('subscription_estimate' => 'chargebee\ChargeBee\Models\SubscriptionEstimate', 'invoice_estimate' => 'chargebee\ChargeBee\Models\InvoiceEstimate', 'next_invoice_estimate' => 'chargebee\ChargeBee\Models\InvoiceEstimate', 'credit_note_estimates' => 'chargebee\ChargeBee\Models\CreditNoteEstimate'));
        $estimate->_initDependant($this->_response['estimate'], 'subscription_estimate', 
        array('shipping_address' => 'chargebee\ChargeBee\Models\SubscriptionEstimateShippingAddress'));
        $estimate->_initDependant($this->_response['estimate'], 'invoice_estimate', 
        array('line_items' => 'chargebee\ChargeBee\Models\InvoiceEstimateLineItem', 'discounts' => 'chargebee\ChargeBee\Models\InvoiceEstimateDiscount', 'taxes' => 'chargebee\ChargeBee\Models\InvoiceEstimateTax', 'line_item_taxes' => 'chargebee\ChargeBee\Models\InvoiceEstimateLineItemTax'));
        $estimate->_initDependant($this->_response['estimate'], 'next_invoice_estimate', 
        array('line_items' => 'chargebee\ChargeBee\Models\InvoiceEstimateLineItem', 'discounts' => 'chargebee\ChargeBee\Models\InvoiceEstimateDiscount', 'taxes' => 'chargebee\ChargeBee\Models\InvoiceEstimateTax', 'line_item_taxes' => 'chargebee\ChargeBee\Models\InvoiceEstimateLineItemTax'));
        $estimate->_initDependantList($this->_response['estimate'], 'credit_note_estimates', 
        array('line_items' => 'chargebee\ChargeBee\Models\CreditNoteEstimateLineItem', 'discounts' => 'chargebee\ChargeBee\Models\CreditNoteEstimateDiscount', 'taxes' => 'chargebee\ChargeBee\Models\CreditNoteEstimateTax', 'line_item_taxes' => 'chargebee\ChargeBee\Models\CreditNoteEstimateLineItemTax'));
        return $estimate;
    }

    function plan() 
    {
        $plan = $this->_get('plan', 'chargebee\ChargeBee\Models\Plan');
        return $plan;
    }

    function addon() 
    {
        $addon = $this->_get('addon', 'chargebee\ChargeBee\Models\Addon');
        return $addon;
    }

    function coupon() 
    {
        $coupon = $this->_get('coupon', 'chargebee\ChargeBee\Models\Coupon');
        return $coupon;
    }

    function couponCode() 
    {
        $coupon_code = $this->_get('coupon_code', 'chargebee\ChargeBee\Models\CouponCode');
        return $coupon_code;
    }

    function address() 
    {
        $address = $this->_get('address', 'chargebee\ChargeBee\Models\Address');
        return $address;
    }

    function event() 
    {
        $event = $this->_get('event', 'chargebee\ChargeBee\Models\Event', 
        array('webhooks' => 'chargebee\ChargeBee\Models\EventWebhook'));
        return $event;
    }

    function comment() 
    {
        $comment = $this->_get('comment', 'chargebee\ChargeBee\Models\Comment');
        return $comment;
    }

    function download() 
    {
        $download = $this->_get('download', 'chargebee\ChargeBee\Models\Download');
        return $download;
    }

    function portalSession() 
    {
        $portal_session = $this->_get('portal_session', 'chargebee\ChargeBee\Models\PortalSession', 
        array('linked_customers' => 'chargebee\ChargeBee\Models\PortalSessionLinkedCustomer'));
        return $portal_session;
    }


    function creditNotes() 
    {
        $credit_notes = $this->_getList('credit_notes', 'chargebee\ChargeBee\Models\CreditNote',
        array('line_items' => 'chargebee\ChargeBee\Models\CreditNoteLineItem', 'discounts' => 'chargebee\ChargeBee\Models\CreditNoteDiscount', 'taxes' => 'chargebee\ChargeBee\Models\CreditNoteTax', 'line_item_taxes' => 'chargebee\ChargeBee\Models\CreditNoteLineItemTax', 'linked_refunds' => 'chargebee\ChargeBee\Models\CreditNoteLinkedRefund', 'allocations' => 'chargebee\ChargeBee\Models\CreditNoteAllocation'));
        return $credit_notes;
    }
    
    
    private function _getList($type, $class, $subTypes = array(), $dependantTypes = array(),  $dependantSubTypes = array())
    {
        if(!array_key_exists($type, $this->_response))
        {
            return null;
        }
        if(!array_key_exists($type, $this->_responseObj))
        {
            $setVal = array();
            foreach($this->_response[$type] as $stV)
            {
                $obj = new $class($stV, $subTypes, $dependantTypes);
                foreach($dependantSubTypes as $k => $v)
                {
                    $obj->_initDependant($stV, $k, $v);
                }
                array_push($setVal, $obj);
            }
            $this->_responseObj[$type] = $setVal;
        }
        return $this->_responseObj[$type];        
    }
    
    private function _get($type, $class, $subTypes = array(), $dependantTypes = array())
    {
        if(!array_key_exists($type, $this->_response))
        {
                return null;
        }
        if(!array_key_exists($type, $this->_responseObj))
        {
                $this->_responseObj[$type] = new $class($this->_response[$type], $subTypes, $dependantTypes);
        }
        return $this->_responseObj[$type];
    }

}

?>