<?php
namespace chargebee\ChargeBee\Exceptions;

use chargebee\ChargeBee\Exceptions\APIError;

class InvalidRequestException extends APIError
{
	function __construct($httpStatusCode,$jsonObject)
	{
		parent::__construct($httpStatusCode,$jsonObject);
    }
}
?>
