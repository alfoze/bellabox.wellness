<?php
namespace chargebee\ChargeBee\Exceptions;

use yii\console\Exception;

class IOException extends Exception {

    private $errorNo;

    function __construct($message, $errorNo) {
        parent::__construct($message);
        $this->errorNo = $errorNo;
    }

    public function getCurlErrorCode() {
        return $this->errorNo;
    }

}

?>
