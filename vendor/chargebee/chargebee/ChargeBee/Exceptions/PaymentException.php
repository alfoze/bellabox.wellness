<?php
namespace chargebee\ChargeBee\Exceptions;

use chargebee\ChargeBee\Exceptions\APIError;

class PaymentException extends APIError
{

	function __construct($httpStatusCode,$jsonObject)
	{
		parent::__construct($httpStatusCode,$jsonObject);
    }
}
?>
