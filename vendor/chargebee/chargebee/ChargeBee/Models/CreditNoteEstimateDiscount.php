<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class CreditNoteEstimateDiscount extends Model
{
  protected $allowed = array('amount', 'description', 'entity_type', 'entity_id');

}

?>