<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;
use chargebee\ChargeBee\Request;
use chargebee\ChargeBee\Util;

class ChargeBee_TransactionLinkedRefund extends ChargeBee_Model
{
  protected $allowed = array('txn_id', 'txn_status', 'txn_date', 'txn_amount');

}

?>