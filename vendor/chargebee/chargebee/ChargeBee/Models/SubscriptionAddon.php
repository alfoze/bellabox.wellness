<?php

namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class SubscriptionAddon extends Model
{
  protected $allowed = array('id', 'quantity');

}

?>