<?php

namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class InvoiceAppliedCredit extends Model
{
  protected $allowed = array('cn_id', 'applied_amount', 'applied_at', 'cn_reason_code', 'cn_date', 'cn_status');

}

?>