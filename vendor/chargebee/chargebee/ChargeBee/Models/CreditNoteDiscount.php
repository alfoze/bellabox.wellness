<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class CreditNoteDiscount extends Model
{
  protected $allowed = array('amount', 'description', 'entity_type', 'entity_id');

}

?>