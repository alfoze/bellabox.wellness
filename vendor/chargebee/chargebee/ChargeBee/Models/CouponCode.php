<?php

namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;
use chargebee\ChargeBee\Request;
use chargebee\ChargeBee\Util;

class CouponCode extends Model
{

  protected $allowed = array('code', 'status', 'couponId', 'couponSetName'
);



  # OPERATIONS
  #-----------

  public static function create($params, $env = null, $headers = array())
  {
    return Request::send(Request::POST, Util::encodeURIPath("coupon_codes"), $params, $env, $headers);
  }

  public static function retrieve($id, $env = null, $headers = array())
  {
    return Request::send(Request::GET, Util::encodeURIPath("coupon_codes",$id), array(), $env, $headers);
  }

  public static function archive($id, $env = null, $headers = array())
  {
    return Request::send(Request::POST, Util::encodeURIPath("coupon_codes",$id,"archive"), array(), $env, $headers);
  }

 }

?>