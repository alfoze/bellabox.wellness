<?php

namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;
use chargebee\ChargeBee\Request;
use chargebee\ChargeBee\Util;

class Transaction extends Model
{

  protected $allowed = array('id', 'customerId', 'subscriptionId', 'paymentMethod', 'referenceNumber', 'gateway',
'type', 'date', 'currencyCode', 'amount', 'idAtGateway', 'status', 'errorCode', 'errorText','voidedAt', 'resourceVersion', 'updatedAt', 'amountUnused', 'maskedCardNumber', 'referenceTransactionId','refundedTxnId', 'reversalTransactionId', 'linkedInvoices', 'linkedCreditNotes', 'linkedRefunds','deleted');



  # OPERATIONS
  #-----------

  public static function all($params = array(), $env = null, $headers = array())
  {
    return Request::sendListRequest(Request::GET, Util::encodeURIPath("transactions"), $params, $env, $headers);
  }

  public static function transactionsForCustomer($id, $params = array(), $env = null, $headers = array())
  {
    return Request::send(Request::GET, Util::encodeURIPath("customers",$id,"transactions"), $params, $env, $headers);
  }

  public static function transactionsForSubscription($id, $params = array(), $env = null, $headers = array())
  {
    return Request::send(Request::GET, Util::encodeURIPath("subscriptions",$id,"transactions"), $params, $env, $headers);
  }

  public static function paymentsForInvoice($id, $params = array(), $env = null, $headers = array())
  {
    return Request::send(Request::GET, Util::encodeURIPath("invoices",$id,"payments"), $params, $env, $headers);
  }

  public static function retrieve($id, $env = null, $headers = array())
  {
    return Request::send(Request::GET, Util::encodeURIPath("transactions",$id), array(), $env, $headers);
  }

 }

?>