<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class InvoiceShippingAddress extends Model
{
  protected $allowed = array('first_name', 'last_name', 'email', 'company', 'phone', 'line1', 'line2', 'line3', 'city', 'state_code', 'state', 'country', 'zip', 'validation_status');

}

?>