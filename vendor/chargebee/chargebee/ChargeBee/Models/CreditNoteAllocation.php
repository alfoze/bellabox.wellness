<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class CreditNoteAllocation extends Model
{
  protected $allowed = array('invoice_id', 'allocated_amount', 'allocated_at', 'invoice_date', 'invoice_status');

}

?>