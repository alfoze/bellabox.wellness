<?php

namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class InvoiceLinkedOrder extends Model
{
  protected $allowed = array('id', 'status', 'reference_id', 'fulfillment_status', 'batch_id', 'created_at');

}

?>