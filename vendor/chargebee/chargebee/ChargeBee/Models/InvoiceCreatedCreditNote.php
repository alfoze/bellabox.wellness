<?php

namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class InvoiceCreatedCreditNote extends Model
{
  protected $allowed = array('cn_id', 'cn_type', 'cn_reason_code', 'cn_date', 'cn_total', 'cn_status');

}

?>