<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class InvoiceEstimateDiscount extends Model
{
  protected $allowed = array('amount', 'description', 'entity_type', 'entity_id');

}

?>