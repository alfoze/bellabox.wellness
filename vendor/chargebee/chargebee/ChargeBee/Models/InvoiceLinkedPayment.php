<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class InvoiceLinkedPayment extends Model
{
  protected $allowed = array('txn_id', 'applied_amount', 'applied_at', 'txn_status', 'txn_date', 'txn_amount');

}

?>