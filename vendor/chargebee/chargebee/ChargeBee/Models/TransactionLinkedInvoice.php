<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class TransactionLinkedInvoice extends Model
{
  protected $allowed = array('invoice_id', 'applied_amount', 'applied_at', 'invoice_date', 'invoice_total', 'invoice_status');

}

?>