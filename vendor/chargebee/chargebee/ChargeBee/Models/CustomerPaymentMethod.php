<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class CustomerPaymentMethod extends Model
{
  protected $allowed = array('type', 'gateway', 'status', 'reference_id');

}

?>