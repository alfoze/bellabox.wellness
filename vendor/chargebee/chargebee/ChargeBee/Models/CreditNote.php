<?php

namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;
use chargebee\ChargeBee\Request;
use chargebee\ChargeBee\Util;

class CreditNote extends Model
{

  protected $allowed = array('id', 'customerId', 'subscriptionId', 'referenceInvoiceId', 'type', 'reasonCode',
'status', 'vatNumber', 'date', 'priceType', 'currencyCode', 'total', 'amountAllocated', 'amountRefunded','amountAvailable', 'refundedAt', 'resourceVersion', 'updatedAt', 'subTotal', 'lineItems', 'discounts','taxes', 'lineItemTaxes', 'linkedRefunds', 'allocations', 'deleted');



  # OPERATIONS
  #-----------

  public static function create($params, $env = null, $headers = array())
  {
    return Request::send(Request::POST, Util::encodeURIPath("credit_notes"), $params, $env, $headers);
  }

  public static function retrieve($id, $env = null, $headers = array())
  {
    return Request::send(Request::GET, Util::encodeURIPath("credit_notes",$id), array(), $env, $headers);
  }

  public static function pdf($id, $env = null, $headers = array())
  {
    return Request::send(Request::POST, Util::encodeURIPath("credit_notes",$id,"pdf"), array(), $env, $headers);
  }

  public static function all($params = array(), $env = null, $headers = array())
  {
    return Request::sendListRequest(Request::GET, Util::encodeURIPath("credit_notes"), $params, $env, $headers);
  }

  public static function creditNotesForCustomer($id, $params = array(), $env = null, $headers = array())
  {
    return Request::send(Request::GET, Util::encodeURIPath("customers",$id,"credit_notes"), $params, $env, $headers);
  }

 }

?>