<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;
use chargebee\ChargeBee\Request;
use chargebee\ChargeBee\Util;

class Estimate extends Model
{

  protected $allowed = array('createdAt', 'subscriptionEstimate', 'invoiceEstimate', 'nextInvoiceEstimate',
'creditNoteEstimates');



  # OPERATIONS
  #-----------

  public static function createSubscription($params, $env = null, $headers = array())
  {
    return Request::send(Request::POST, Util::encodeURIPath("estimates","create_subscription"), $params, $env, $headers);
  }

  public static function updateSubscription($params, $env = null, $headers = array())
  {
    return Request::send(Request::POST, Util::encodeURIPath("estimates","update_subscription"), $params, $env, $headers);
  }

  public static function renewalEstimate($id, $params = array(), $env = null, $headers = array())
  {
    return Request::send(Request::GET, Util::encodeURIPath("subscriptions",$id,"renewal_estimate"), $params, $env, $headers);
  }

 }

?>