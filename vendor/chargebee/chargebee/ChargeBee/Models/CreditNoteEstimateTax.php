<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class CreditNoteEstimateTax extends Model
{
  protected $allowed = array('name', 'amount', 'description');

}

?>