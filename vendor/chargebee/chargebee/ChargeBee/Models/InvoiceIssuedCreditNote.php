<?php
namespace chargebee\ChargeBee\Models;

use chargebee\ChargeBee\Model;

class InvoiceIssuedCreditNote extends Model
{
  protected $allowed = array('cn_id', 'cn_reason_code', 'cn_date', 'cn_total', 'cn_status');

}

?>