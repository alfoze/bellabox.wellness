<?php

namespace chargebee;

function checkExtentions() {
    $extensions = array('curl', 'json');
    foreach ($extensions AS $e) {
        if (!extension_loaded($e)) {
            throw new Exception('ChargeBee requires the ' . $e . ' extension.');
        }
    }
}

checkExtentions();

abstract class ChargeBee
{

  public static $verifyCaCerts = true;

  public static function getVerifyCaCerts() {
    return self::$verifyCaCerts;
  }

  public static function setVerifyCaCerts($verify) {
    self::$verifyCaCerts = $verify;
  }

	public static function getCaCertPath() {
		return dirname(__FILE__) . "/ssl/ca-certs.crt";
	}

}
