<?php

namespace backend\models;

use common\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class Allocationselection extends Model {

  public $dateRange;
  public $fromDate;
  public $toDate;
  public $boxType;
  public $batchId;
  public $new = '1';
  public $processed = '0';
  public $completed = '0';
  
  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        [
        'class' => \kartik\daterange\DateRangeBehavior::className(),
        'attribute' => 'dateRange',
        'dateStartAttribute' => 'fromDate',
        'dateEndAttribute' => 'toDate',
      ]
    ];
  }

  public function rules() {
    return [
      // name, email, subject and body are required
        [['dateRange', 'boxType', 'batchId'], 'required'],
        [['batchId', 'boxType'], 'string', 'max' => 50],
        [['new', 'processed', 'completed'], 'string', 'max' => 1],
        [['dateRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'dateRange' => Yii::t('app', 'Order Date Range'),
      'fromDate' => Yii::t('app', 'Start Date'),
      'toDate' => Yii::t('app', 'End Date'),
      'boxType' => Yii::t('app', 'Box Type'),
      'batchId' => Yii::t('app', 'Batch ID'),
      'new' => 'Include New',
      'processed' => 'Include Processed',
      'completed' => 'Include Completed',
      
    ];
  }

}
