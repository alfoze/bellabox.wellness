<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "divs".
 *
 * @property integer $id
 * @property string $name
 * @property integer $weight
 * @property integer $page_id
 * @property integer $landingpage_id
 * @property string $status
 *
 * @property Landingpages $landingpage
 * @property Pages $page
 */
class Divs extends \common\config\ActiveRecord2 {

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'divs';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['name', 'weight', 'page_id', 'status'], 'required'],
        [['weight', 'page_id', 'landingpage_id'], 'integer'],
        [['status'], 'string'],
        [['name'], 'string', 'max' => 100],
        [['landingpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Landingpages::className(), 'targetAttribute' => ['landingpage_id' => 'id']],
        [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pages::className(), 'targetAttribute' => ['page_id' => 'id']],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'name' => 'Name',
      'weight' => 'Weight',
      'page_id' => 'Page Content',
      'landingpage_id' => 'Page Layout',
      'status' => 'Status',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getLandingpage() {
    return $this->hasOne(Landingpages::className(), ['id' => 'landingpage_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPage() {
    return $this->hasOne(Pages::className(), ['id' => 'page_id']);
  }

}
