<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Landingpages;

/**
 * LandingpagesSearch represents the model behind the search form about `backend\models\Landingpages`.
 */
class LandingpagesSearch extends Landingpages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by','can_delete'], 'integer'],
            [['name', 'header','add_standalonecss', 'created_on', 'updated_on', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Landingpages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'can_delete' => $this->can_delete,
            'add_standalonecss'=> $this->add_standalonecss,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'header', $this->header])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
