<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $name
 * @property string $header
 * @property string $html
 * @property string $status
 * @property string $can_delete
 * @property Divs[] $divs
 */
class Pages extends \common\config\ActiveRecord2 {

  public $PID = 103;
  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'pages';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['name', 'header', 'status'], 'required'],
        [['html', 'status'], 'string'],
        [['name', 'header'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'name' => 'Name',
      'header' => 'Header',
      'html' => 'Html',
      'status' => 'Status',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDivs() {
    return $this->hasMany(Divs::className(), ['page_id' => 'id']);
  }

}
