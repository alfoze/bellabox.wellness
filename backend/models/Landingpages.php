<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "landingpages".
 *
 * @property integer $id
 * @property string $name
 * @property string $header
 * @property string $created_on
 * @property integer $created_by
 * @property string $updated_on
 * @property string $status
 * @property string $can_delete
 * @property string $add_standalonecss
 * @property Divs[] $divs
 */
class Landingpages extends \common\config\ActiveRecord2 {

  public $PID = 102;

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'landingpages';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['name', 'header', 'status'], 'required'],
        [['created_on', 'updated_on'], 'safe'],
        [['created_by', 'can_delete'], 'integer'],
        [['status'], 'string'],
        [['add_standalonecss'], 'string', 'max' => 1],
        [['name', 'header'], 'string', 'max' => 100],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'name' => 'Name',
      'header' => 'Header',
      'created_on' => 'Created On',
      'created_by' => 'Created By',
      'updated_on' => 'Updated On',
      'can_delete' => 'Can Delete',
      'status' => 'Status',
      'add_standalonecss' => 'Add BellaBox.au Style CSS'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDivs() {
    return $this->hasMany(Divs::className(), ['landingpage_id' => 'id'])->orderBy(['weight' => SORT_ASC]);
  }

  public function getUrl() {
    $urll = \yii\helpers\Url::to(['/viewpage', 'id' => $this->id]);
    $base = \yii\helpers\Url::base();
    $frontEnd = Yii::$app->params['frontendaddress'];
    return str_replace($base . "/", $frontEnd, $urll);
  }

  public function getCreatedby() {
    $user = null;
    if ($this->created_by != null) {
      $userObj = \common\models\User::findOne($this->created_by);
      if ($userObj != null) {
        $user = $userObj->fullname;
      }
    }
    return $user;
  }

}
