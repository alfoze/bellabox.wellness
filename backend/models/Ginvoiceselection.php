<?php

namespace backend\models;

use common\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class Ginvoiceselection extends Model {

  public $fromdate;
  public $timedifference;
  public $noofinvoices;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      // name, email, subject and body are required
        [['fromdate', 'timedifference', 'noofinvoices'], 'required'],
        [['timedifference','noofinvoices'], 'integer'],
        [['fromdate'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'fromdate' => Yii::t('app', 'Start Date'),
      'timedifference' => Yii::t('app', 'Time (Hours)'),
      'noofinvoices' => Yii::t('app', 'No of Invoices'),
    ];
  }
}
