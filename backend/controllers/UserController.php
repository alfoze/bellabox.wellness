<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use common\config\Controller3;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UnamesController implements the CRUD actions for Unames model.
 */
class UserController extends Controller3 {

    public $mainMenu = 700;
    public $submenu = 702;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['userlist', 'index', 'index2', 'update', 'updatepass', 'delete', 'export', 'view', 'creatcbcustomer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //  'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionCreatcbcustomer() {
        $model = new \common\models\User();

        if ($model->load(Yii::$app->request->post())) {
            \frontend\controllers\JoinController::createEnvironment();
            $sumodel = new \frontend\models\SignupForm();
            $tmpPass = $this->generateRandomString();
            $uData = Yii::$app->request->post('User');

            $sumodel->email = isset($uData['email']) ? $uData['email'] : "";
            $sumodel->f_name = isset($uData['f_name']) ? $uData['f_name'] : "";
            $sumodel->l_name = isset($uData['l_name']) ? $uData['l_name'] : "";
            $sumodel->password = $tmpPass;
            $sumodel->tmppass = $tmpPass;
            $sumodel->mobile = isset($uData['mobile']) ? $uData['mobile'] : "";
            $model = $sumodel->signup2();
            $result = \chargebee\ChargeBee\Models\Customer::create(
                            array("email" => $sumodel->email, "firstName" => $sumodel->f_name, "lastName" => $sumodel->l_name, 'cfTemporaryPassword' => $tmpPass, 'cfFromShop' => true)
            );
            if ($model != null) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', ['model' => $model,]);
    }

    public function actionUserlist($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $query->select('id, username AS text')
                    ->from('es_user')
                    ->where(['like', 'username', $q])
                    ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => User::find($id)->name];
        }
        return $out;
    }

    private function export($query = null) {
        $fields = array('index', 'f_name', 'l_name', 'email', 'mobile', 'cb_customer_id', 'creationdate', 'is_admin', 'tmp_pass');
        $data = array();
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search($query, false);
        $data = $dataProvider->getModels();
        $this->sendAsXLS(\common\config\Options::getOptionName(Controller3::USERS) . '_file', $data, '', true, $fields);
    }

    public function actionExport() {
        $fields = array('index', 'f_name', 'l_name', 'email', 'mobile', 'cb_customer_id', 'creationdate', 'is_admin', 'tmp_pass');
        $data = \common\models\User::find()->all();
        $this->sendAsXLS(\common\config\Options::getOptionName(Controller3::USERS) . '_file', $data, '', true, $fields);
    }

    protected function uploadImage($model, $oldImage, $colname) {

        if (\yii\web\UploadedFile::getInstance($model, $colname)) {
            $fileObj = \yii\web\UploadedFile::getInstance($model, $colname);
            $filename = "";
            $dirpath = realpath(dirname(getcwd()));
            $res = $dirpath; //str_replace("backend", "frontend", $dirpath);
            $filename = 'uploads/' . uniqid() . $colname . $fileObj->name;
            $uploaddir = $res;
            $fileObj->saveAs($uploaddir . "/" . $filename);
            return $filename;
        } else {
            return $oldImage;
        }
    }

    /**
     * Lists all Unames models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UserSearch();
        if (isset(Yii::$app->request->queryParams['ExportCSV'])) {
            $this->export(Yii::$app->request->queryParams);
            return;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/user/index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'title' => 'All Users'
        ]);
    }

    public function actionIndex2() {
        $this->submenu = 706;
        $searchModel = new UserSearch();
        if (isset(Yii::$app->request->queryParams['ExportCSV'])) {
            $this->export(Yii::$app->request->queryParams);
            return;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true, true);

        return $this->render('/user/index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'title' => 'Subscription Users'
        ]);
    }

    /**
     * Displays a single Unames model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $type = 'O', $pop = 0) {
        $view = 'view';
        $model = $this->findModel($id);

        $beautyProfile = array();
        $allocations = array();
        $rewardPoints = array();
        if ($model->cb_customer_id != null) {
            $questions = \common\models\Beautyprofilequestion::find()->where("active = '1'")->asArray()->all();
            foreach ($questions as $value) {
                $qs[] = $value['id'];
                $beautyProfile[$value['id']]['id'] = $value['id'];
                $beautyProfile[$value['id']]['qname'] = $value['name'];
            }

            $canswers = \common\models\Beautyprofile::find()->select(['answer', 'answer_text', 'answer_date', 'question_id'])->distinct()->where("customer_id = '$model->cb_customer_id'")->asArray()->all();
            foreach ($canswers as $canswer) {
                $bbanswer = '';
                $bbanswer = isset($ans[$canswer['answer']]) ? $ans[$canswer['answer']] : ($canswer['answer_text'] == null ? $canswer['answer_date'] : $canswer['answer_text']);
                if (isset($beautyProfile[$canswer['question_id']]['answer'])) {
                    $beautyProfile[$canswer['question_id']]['answer'] .= ' / ' . $bbanswer;
                } else {
                    $beautyProfile[$canswer['question_id']]['answer'] = $bbanswer;
                }
            }

            $allocations = \common\models\Allocation::findAll(['customer_id' => $model->cb_customer_id]);
            $rewardPoints = \common\models\Rewardpoint::findAll(['customer_id' => $model->cb_customer_id]);
        }
        return $this->render('/user/' . $view, ['model' => $model, 'beautyProfile' => $beautyProfile, 'allocations' => $allocations, 'rewardPoints' => $rewardPoints]);
    }

    /**
     * Updates an existing Unames model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $uData = Yii::$app->request->post('User');
            $model->email = isset($uData['email']) ? $uData['email'] : "";
            $model->f_name = isset($uData['f_name']) ? $uData['f_name'] : "";
            $model->l_name = isset($uData['l_name']) ? $uData['l_name'] : "";
            $model->is_admin = isset($uData['is_admin']) ? $uData['is_admin'] : "0";
            $model->is_active = isset($uData['is_active']) ? $uData['is_active'] : "1";
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', ['model' => $model,]);
    }

    public function actionUpdatepass($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $uData = Yii::$app->request->post('User');
            $pass1 = isset($uData['password1']) ? $uData['password1'] : null;
            $pass2 = isset($uData['password2']) ? $uData['password2'] : null;
            $valid = true;
            if ($pass1 == null) {
                $model->addError('password1', 'Password is required.');
                $valid = false;
            }

            if ($pass2 == null) {
                $model->addError('password2', 'Confirm Password is required.');
                $valid = false;
            }
            if ($pass2 !== $pass1) {
                $model->addError('password2', 'Confirm Password does not match password.');
                $valid = false;
            }

            if ($valid == true) {
                $model->setPassword($pass1);
                if ($model->save()) {
                    \Yii::$app->getSession()->setFlash('s_mesage', 'Password has been changed successfuly. ');
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }
        return $this->render('changepass', ['model' => $model,]);
    }

    public function actionDelete($id) {
        $model = User::findOne(['id' => $id]);
        $model->delete();
        \Yii::$app->getSession()->setFlash('s_mesage', 'User deleted successfully. ');
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Unames model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unames the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
