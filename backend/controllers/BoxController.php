<?php

namespace backend\controllers;

use Yii;
use common\models\Box;
use common\models\BoxSearch;
use common\config\Controller3;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * BoxController implements the CRUD actions for Box model.
 */
class BoxController extends Controller3 {

  public $mainMenu = 600;
  public $submenu = 603;

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
              
              [
                  'actions' => ['view','index','update', 'delete','create'],
                  'allow' => true,
                  'roles' => ['@'],
              ],
          ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  private function validateSample($samples)
  {
    $error = '';
    $arrsamples = array();
    $arrsamples = explode("\n",$samples);
    foreach($arrsamples as $sample)
    {
      $sample = trim($sample);
      if($sample == ''){continue;}
      $sampleSku = \common\models\Product::find()->where(" sku ='$sample'")->one();
      if($sampleSku == NULL)
      {
        $error .= 'Invalid SKU: '.$sample.'. ';
      }
    }
    return $error;
  }
  protected function uploadImage($model, $colname) {

    if (\yii\web\UploadedFile::getInstance($model, $colname)) {
      $fileObj = \yii\web\UploadedFile::getInstance($model, $colname);
      $filename = "";
      $dirpath = realpath(dirname(getcwd()));
      $res = $dirpath; // str_replace("backend", "frontend", $dirpath);
      $filename = "img/boxes/" . uniqid() . $fileObj->name;
      $uploaddir = $res;
      $fileObj->saveAs($uploaddir . "/" . $filename);
      return $filename;
    }
    else {
      return $model->oldImage;
    }
  }

  /**
   * Lists all Box models.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new BoxSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Box model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    return $this->render('view', [
          'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Box model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Box();

    if ($model->load(Yii::$app->request->post())) {
      $sampleError = $this->validateSample($model->samples);
      if ($sampleError == '')
      {
        $model->image = $this->uploadImage($model, 'image');
        if ($model->save()) {
          return $this->redirect(['view', 'id' => $model->id]);
        }
      }
      else{
        $model->addError('samples', $sampleError);
      }
      
    }

    return $this->render('create', ['model' => $model,]);
  }

  /**
   * Updates an existing Box model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);
    
    if ($model->load(Yii::$app->request->post())) {
      $sampleError = $this->validateSample($model->samples);
      if ($sampleError == '')
      {
        $model->image = $this->uploadImage($model, 'image');
        if ($model->save()) {
          return $this->redirect(['view', 'id' => $model->id]);
        }
      }
      else{
        $model->addError('samples', $sampleError);
      }
      
    }
    return $this->render('update', ['model' => $model]);
  }

  /**
   * Deletes an existing Box model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Box model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Box the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Box::findOne($id)) !== null) {
      return $model;
    }
    else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
