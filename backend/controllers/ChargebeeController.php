<?php

namespace backend\controllers;

use Yii;
use common\models\Beautyprofilequestion;
use common\models\BeautyprofilequestionSearch;
use common\config\Controller3;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * BeautyprofilequestionController implements the CRUD actions for Beautyprofilequestion model.
 */
class ChargebeeController extends Controller3 {

    public $mainMenu = 200;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['setallocationlist', 'getshipmentlist', 'getinvoices', 'updateaddressescb', 'getorder', 'getplans', 'getorders'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    private function getPlans($planList, $nextOffset = null) {
        $plantFilter = [];
        if ($nextOffset != null) {
            $plantFilter['offset'] = $nextOffset;
        }
        $plans = \chargebee\ChargeBee\Models\Plan::all($plantFilter);
        foreach ($plans as $plan) {
            $plan2 = $plan->plan();
            if ($plan2->cfBoxType == 'Women' && $plan2->cfPlanType !== 'Send Gift Subscription') {
                $planList[$plan2->id] = $plan2->name;
            }
        }

        if ($plans->nextOffset() !== null) {
            $planList = $this->getPlans($planList, $plans->nextOffset());
        }
        return $planList;
    }

    public function actionGetplans() {
        self::createEnvironment();
        $plans = [];
        $plans = $this->getPlans($plans);
        print_r($plans);
    }

    public function actionShowsub($subscriptionId) {
        self::createEnvironment();
        $result = \chargebee\ChargeBee\Models\Subscription::retrieve($subscriptionId);
        $subscription = isset($result->subscription()->shippingAddress) ?: null;
        echo date('Y-m-d H:i:s', $result->subscription()->updatedAt);

//    $shippingAddress = [
//        "company" => "Alfoze Technologies",
//        "line1" =>"House 9 , Bolan Road, B Block Soan Gardens",
//        "line2" => "",
//        "city" => "Islamabad",
//        "state" => "Islamabad",
//        "zip" => "55000",
//        "country" => "PK",
//        "validation_status" => "valid"
//      ];
//      $result = \chargebee\ChargeBee\Models\Subscription::update($subscriptionId, array('shippingAddress' => $shippingAddress));
//      
//    var_dump($result);
    }

    public function actionUpdateaddressescb() {
        $this->submenu = 203;
        $model = new \common\models\FileForm2();
        $error = "";
        if ($model->load(Yii::$app->request->post())) {
            set_time_limit(3000);
            ini_set('auto_detect_line_endings', true);
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
            $filePath = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $model->file->baseName . "." . $model->file->extension;
            $model->file->saveAs($filePath);

            $file = fopen($filePath, 'r');
            $count = 0;
            $rowno = 1;
            $rset = 0;
            $listDate = strtotime($model->listdate);

            self::createEnvironment();
            while (($line = fgetcsv($file)) !== FALSE) {
                if ($count > 0) {
                    if (isset($line[0]) && isset($line[1])) {

                        $subscriptionId = $line[1];
                        $subscription = null;
                        try {
                            $result = \chargebee\ChargeBee\Models\Subscription::retrieve($subscriptionId);
                            $subscription = $result->subscription();
                        } catch (\yii\console\Exception $e) {
                            $error .= "Invalid subscription id <b>$subscriptionId</b> given in CSV file on line $rowno .(1)<br>";
                        }

                        if ($subscription != null) {
                            $lastUpdated = $result->subscription()->updatedAt;
                            if ($listDate > $lastUpdated) {
                                $companyName = $line[6];
                                $line1 = $line[9];
                                $line2 = $line[10];
                                $city = $line[11];
                                $state = $line[12];
                                $zip = $line[14];
                                $country = "NZ";
                                $status = "valid";
                                try {
                                    $shippingAddress = [
                                        "company" => $companyName,
                                        "line1" => $line1,
                                        "line2" => $line2,
                                        "city" => $city,
                                        "state" => $state,
                                        "zip" => $zip,
                                        "country" => $country,
                                        "validation_status" => $status
                                    ];
                                    $result = \chargebee\ChargeBee\Models\Subscription::update($subscriptionId, array('shippingAddress' => $shippingAddress));
                                    $rset++;
                                } catch (\yii\console\Exception $e) {
                                    $error .= "Invalid subscription id <b>$subscriptionId</b> given in CSV file on line $rowno.(2)<br>";
                                }
                            } else {
                                $showDate = date('Y-m-d H:i:s', $lastUpdated);
                                $error .= "Subscription <b>$subscriptionId</b> given in CSV file on line $rowno was updated on $showDate.<br>";
                            }
                        }
                    }
                }
                $count++;
                $rowno++;
            }
            fclose($file);
            $count--;
            \Yii::$app->getSession()->setFlash("info", "$rset out of $count address(es) were updated on chargebee.");
        }
        return $this->render('updateaddressescb', ['model' => $model, 'errors' => $error]);
    }

    public function actionGenerateinvoice() {
        $this->submenu = 201;
        $model = new \backend\models\Ginvoiceselection();
        $model->noofinvoices = 10;
        $model->timedifference = 8;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $fromDate = strtotime($model->fromdate);
            self::createEnvironment();
            $count = 0;
            for ($i = 1; $i <= $model->noofinvoices; $i++) {
                $result = \chargebee\ChargeBee\Models\Invoice::create(array(
                            "customerId" => "2u9uCkzNQaT0bztV4W",
                            "date" => $fromDate,
                            'currency_code' => 'NZD',
                            "charges" => array(array(
                                    "amount" => 99,
                                    "description" => "Test Charges $fromDate"
                ))));
                $invoice = $result->invoice();
                $count++;
                $fromDate = $fromDate - ($model->timedifference * 60 * 60);
            }
            \Yii::$app->getSession()->setFlash("info", "$count invoices created.");
        }
        return $this->render('ginvoice', ['model' => $model]);
    }

    public function actionSetallocationlist() {
        $this->submenu = 202;
        $model = new \common\models\FileForm();
        if ($model->load(Yii::$app->request->post())) {
            set_time_limit(3000);
            ini_set('auto_detect_line_endings', true);
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
            $filePath = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $model->file->baseName . "." . $model->file->extension;
            $model->file->saveAs($filePath);
            $error = "";
            $file = fopen($filePath, 'r');
            $count = 0;
            $rowno = 1;
            $rset = 0;
            $completeName = \Yii::$app->params['completeName'];
            self::createEnvironment();
            while (($line = fgetcsv($file)) !== FALSE) {
                if ($count > 0) {
                    if (isset($line[0]) && isset($line[1])) {
                        $orderId = $line[0];
                        $boxVariant = $line[1];
                        $trackindId = $line[2];
                        $customerId = isset($line[3]) ? $line[3] : '';
                        $box = \common\models\Box::find()->where("variant_name = '$boxVariant'")->one();
                        $orderUpdate = ["status" => $completeName, 'referenceId' => $boxVariant, 'trackingId' => $trackindId, 'fulfillmentStatus' => 'Shipped', 'shippedAt' => time()];
                        if ($box != null) {
                            $boxId = $box->id;
                            try {
                                $allocation = \common\models\Allocation::find()->where("order_id = '$orderId'")->one();
                                $customerId = $customerId;
                                $subId = '-';
                                if ($model->for_shop == '0') {
                                    $allocation = \common\models\Allocation::find()->where("order_id = '$orderId'")->one();
                                    $Order = \chargebee\ChargeBee\Models\Order::update($orderId, $orderUpdate);
                                   // $Invoice = \chargebee\ChargeBee\Models\Invoice::retrieve($Order->order()->invoiceId);
                                    $customerId = $Order->order()->customerId; // $Invoice->invoice()->customerId;
                                    $subId = $Order->order()->subscriptionId; //$Invoice->invoice()->subscriptionId;
                                } else {
                                    $allocation = \common\models\Allocation::find()->where("customer_id = '$customerId' and  box_name = '$boxVariant' ")->one();
                                    $orderId  = '0';
                                }

                                if ($allocation == null) {
                                    $allocation = new \common\models\Allocation();
                                    $allocation->customer_id = $customerId;
                                    $allocation->subscription_id = $subId;
                                    $allocation->order_id = $orderId;
                                    $allocation->box_name = $boxVariant;
                                    $allocation->mail_ref = $trackindId;
                                    $allocation->batch_id = 0;
                                    $allocation->box_id = $boxId;
                                    $allocation->save();
                                } else {
                                    $allocation->customer_id = $customerId;
                                    $allocation->subscription_id = $subId;
                                    $allocation->order_id = $orderId;
                                    $allocation->box_name = $boxVariant;
                                    $allocation->mail_ref = $trackindId;
                                    $allocation->batch_id = 0;
                                    $allocation->box_id = $boxId;
                                    $allocation->save();
                                }
                                $rset++;
                            } catch (\yii\console\Exception $e) {
                                $error .= "Invalid order no. $orderId given in CSV file on line $rowno. <br>";
                            }
                        } else {
                            $error .= "Invalid box variant name $boxVariant given in CSV file on line $rowno.<br>";
                        }
                    }
                }
                $count++;
                $rowno++;
            }
            fclose($file);
            $count--;
            \Yii::$app->getSession()->setFlash("info", "$rset out of $count order(s) were updated on chargebee.");
            if ($error != "") {
                \Yii::$app->getSession()->setFlash("danger", $error);
            }
        }
        return $this->render('setallocation', ['model' => $model]);
    }

    public function actionGetinvoices($fdate, $tdate) {
        self::createEnvironment();
        $fromDate = strtotime($fdate);
        $toDate = strtotime($tdate);
        $invoiceList = [];
        $invoiceList = $this->getInvoices($fromDate, $toDate, $invoiceList);
        foreach ($invoiceList as $invoice) {
            if (count($invoice->lineItems) > 0) {
                foreach ($invoice->lineItems as $lineItem) {
                    var_dump($lineItem->subscriptionId);
                    echo "<br>================================================================<br>";
                }
            }
        }
    }

    public function actionGetorder($id) {
        self::createEnvironment();
        $order = $this->getOrder($id);
        $newName = \Yii::$app->params['newName'];
        $processingName = \Yii::$app->params['processingName'];
        $voidName = \Yii::$app->params['voidName'];
        $completeName = \Yii::$app->params['completeName'];

        if ($order->orderDate > time()) {
            echo "Order should not come in shipment list .. order date is in future " . date('Y-m-d', $order->orderDate);
            if ($order->status == $processingName) {
                $orderUpdate = ["status" => $newName, 'referenceId' => '', 'trackingId' => '', 'fulfillmentStatus' => ''];
                $Order = \chargebee\ChargeBee\Models\Order::update($id, $orderUpdate);
                echo "<br>Order corrected in chargebee....";
            }
        } else {
            echo "Order should come in shipment list ..";
        }
        return;
    }

    private function getOrder($orderId) {
        $Order = \chargebee\ChargeBee\Models\Order::retrieve($orderId);
        return $Order->order();
    }

    public function actionGetorders($fdate, $tdate,$isNew=true,$isProcess=true , $isComplete=false ) {
        self::createEnvironment();
        $orders = [];
        $fdate = strtotime($fdate);
        $tdate = strtotime($tdate);
        $orders = [];
        $orders = $this->getOrders($fdate, $tdate, $orders, true,$isProcess , $isComplete);
        var_dump($orders);
        return;
    }

    private function getOrders($fromDate, $toDate, $orderList, $isNew = true, $isProcess = false, $isComplete = false, $offset = null) {
        $orderStatus = [];
        if ($isNew) {
            $orderStatus[] = 'queued';
        }
        if ($isProcess) {
            $orderStatus[] = 'awaiting_shipment';
        }
        if ($isComplete) {
            $orderStatus[] = 'complete';
        }

        $orderFilter = ["orderDate[between]" => [$fromDate, $toDate], "paymentStatus[is]" => 'paid', "status[in]" => $orderStatus];
        if ($offset != null) {
            $orderFilter['offset'] = $offset;
        }
        $result = \chargebee\ChargeBee\Models\Order::all($orderFilter);
        $i = 0;
        foreach ($result as $entry) {
            $orderList[] = $entry->order();
        }
        if ($result->nextOffset() !== null) {
            $orderList = $this->getOrders($fromDate, $toDate, $orderList, $isNew, $isProcess, $isComplete, $result->nextOffset());
        }

        return $orderList;
    }

    private function getInvoices($fromDate, $toDate, $invoiceList, $offset = null, $paidAt = true, $excludeWithCN = true) {

        $colname = 'paidAt';
        if ($paidAt == false) {
            $colname = 'date';
        }
        $InvoiceFilter = ["status[is]" => 'paid', $colname . "[between]" => "[$fromDate, $toDate]"];
        if ($offset != null) {
            $InvoiceFilter['offset'] = $offset;
        }
        $result = \chargebee\ChargeBee\Models\Invoice::all($InvoiceFilter);
        $i = 0;
        foreach ($result as $entry) {
            $invoice = $entry->invoice();
            if (is_array($invoice->issuedCreditNotes)) {
                if ($excludeWithCN == false) {
                    $invoiceList[] = $invoice;
                }
            } else {
                $invoiceList[] = $invoice;
            }
        }
        if ($result->nextOffset() !== null) {
            $invoiceList = $this->getInvoices($fromDate, $toDate, $invoiceList, $result->nextOffset(), $paidAt, $excludeWithCN);
        }

        return $invoiceList;
    }

    public function actionGetshipmentlist($update = true) {
        $this->submenu = 201;
        $model = new \backend\models\Allocationselection();
        $newName = \Yii::$app->params['newName'];
        $processingName = \Yii::$app->params['processingName'];
        $voidName = \Yii::$app->params['voidName'];
        $completeName = \Yii::$app->params['completeName'];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            set_time_limit(0);
            $fromDate = $model->fromDate;
            $toDate = $model->toDate;
            $showNew = $model->new == '1' ? true : false;
            $showProcessed = $model->processed == '1' ? true : false;
            $showCompleted = $model->completed == '1' ? true : false;
            $orderList = [];
            self::createEnvironment();
            $plans = [];
            $plans = $this->getPlans($plans);
            //handel add on orders created manually
            $plans['orders-addon'] ="Orders AddOn";
            
            $orderList = $this->getOrders($fromDate, $toDate, $orderList, $showNew, $showProcessed, $showCompleted);
            $header = ['Batch Id', 'Status', 'Customer Id', 'Subscription Id', 'Invoice Id', 'Order Id', 'Order Date', 'Box Type', 'Company Name', 'Name', 'Email', 'Address 1', 'Address 2', 'City', 'State', 'Country', 'Zip', 'Is Validated'];
            $qs = [];
            $questions = Beautyprofilequestion::find()->where("active = '1'")->asArray()->all();
            foreach ($questions as $value) {
                $header[] = $value['name'];
                $qs[] = $value['id'];
            }
            $header[] = 'Age';
            $today = new \DateTime(date('Y-m-d'));
            $ans = [];
            $answers = \common\models\Beautyprofileanswer::find()->asArray()->all();
            foreach ($answers as $value) {
                $ans[$value['id']] = $value['name'];
            }

            $CsvData = array();
            $CsvData[] = $header;
            $OrderObjs = array();
            foreach ($orderList as $order) {
                $addInList = false;
                $shippingAddress = isset($order->shippingAddress)? $order->shippingAddress : [];
                $invoiceDate = '-';
                $status = $order->status;
                $orderId = $order->id;
                $subscriptionId = isset($order->subscriptionId)?$order->subscriptionId:'N/A';
                $batchId = $order->batchId == '' ? trim($model->batchId) : $order->batchId;
                if ($update === true) {
                    if ($status == $newName) {
                        $orderUpdate = ["status" => $processingName, 'fulfillmentStatus' => 'Preparing', 'batchId' => $batchId, 'note' => $subscriptionId];
                        $ord = \chargebee\ChargeBee\Models\Order::update($orderId, $orderUpdate);
                    }
                    if ($status == $processingName) {
                        if (isset($order->batchId)&& trim($order->batchId) !== '') {
                            $batchId = trim($order->batchId);
                        } else {
                            $orderUpdate = ["status" => $processingName, 'fulfillmentStatus' => 'Preparing', 'batchId' => $model->batchId, 'note' => $subscriptionId];
                            $ord = \chargebee\ChargeBee\Models\Order::update($orderId, $orderUpdate);
                            $batchId = $model->batchId;
                        }
                    }
                }
                $orderDate = date('Y-m-d', $order->orderDate);
                $companyName = isset($shippingAddress['company']) ? $shippingAddress['company'] : '';
                $customerName = (isset($shippingAddress['first_name']) ? $shippingAddress['first_name'] : '') . ' ' . (isset($shippingAddress['last_name']) ? $shippingAddress['last_name'] : '');
                $email = isset($shippingAddress['email']) ? $shippingAddress['email'] : '';
                $line1 = isset($shippingAddress['line1']) ? $shippingAddress['line1'] : '';
                $line2 = isset($shippingAddress['line2']) ? $shippingAddress['line2'] : '';
                $city = isset($shippingAddress['city']) ? $shippingAddress['city'] : '';
                $state = isset($shippingAddress['state']) ? $shippingAddress['state'] : '';
                $country = isset($shippingAddress['country']) ? $shippingAddress['country'] : '';
                $zip = isset($shippingAddress['zip']) ? $shippingAddress['zip'] : '';
                $validStatus = isset($shippingAddress['validationStatus']) ? $shippingAddress['validationStatus'] : '';
                $customerName = trim($customerName);
                $cuser = \common\models\User::find()->where(" cb_customer_id ='$order->customerId'")->one();
                if ($cuser != null) {
                    if ($customerName == '') {
                        $customerName = $cuser->f_name . " " . $cuser->l_name;
                    }
                    if ($email == '') {
                        $email = $cuser->email;
                    }
                }

                foreach ($order->orderLineItems as $orderItem) {
                    $planId = isset($orderItem['entity_id']) ? $orderItem['entity_id'] : null;
                    if ($planId != null) {
                        if (isset($plans[$planId])) {
                            $datum = [$batchId,
                                $status,
                                $order->customerId,
                                $subscriptionId,
                                $order->invoiceId,
                                $order->id,
                                $orderDate,
                                $planId,
                                $companyName,
                                $customerName,
                                $email,
                                $line1,
                                $line2,
                                $city,
                                $state,
                                $country,
                                $zip,
                                $validStatus
                            ];
                            $canswers = \common\models\Beautyprofile::find()->select(['answer', 'answer_text', 'answer_date', 'question_id'])->distinct()->where("customer_id = '$order->customerId'")->asArray()->all();
                            $cans = array();
                            foreach ($canswers as $canswer) {
                                $bbanswer = '';
                                $bbanswer = isset($ans[$canswer['answer']]) ? $ans[$canswer['answer']] : ($canswer['answer_text'] == null ? $canswer['answer_date'] : $canswer['answer_text']);
                                if (isset($cans[$canswer['question_id']])) {
                                    $cans[$canswer['question_id']] .= ' / ' . $bbanswer;
                                } else {
                                    $cans[$canswer['question_id']] = $bbanswer;
                                }
                            }
                            $dob = null;
                            foreach ($qs as $q) {
                                $datum[] = isset($cans[$q]) ? $cans[$q] : '';
                                if ($q == 16) {
                                    $dob = isset($cans[$q]) ? $cans[$q] : null;
                                }
                            }
                            $age = '';
                            if ($dob != null) {
                                $dobA = new \DateTime($dob);
                                $age = $today->diff($dobA)->format('%y');
                            }
                            $datum[] = $age;
                            $CsvData[] = $datum;
                        }
                    }
                }
            }
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/downloads/customer_list_$model->batchId.csv";
            $file = fopen($filePath, "w+");
            foreach ($CsvData as $line) {
                fputcsv($file, $line);
            }
            fclose($file);
            if (file_exists($filePath)) {
                Yii::$app->response->sendFile($filePath);
            } else {
                echo "error while accessing file";
            }
        }
        return $this->render('allocations', ['model' => $model,]);
    }

    public function actionGetshipmentlistDepreciated($update = true) {
        $this->submenu = 201;
        $model = new \backend\models\Allocationselection();
        $newName = \Yii::$app->params['newName'];
        $processingName = \Yii::$app->params['processingName'];
        $voidName = \Yii::$app->params['voidName'];
        $completeName = \Yii::$app->params['completeName'];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            set_time_limit(0);
            $planType = array();
            $planPType = array();
            $fromDate = $model->fromDate;
            $toDate = $model->toDate;
            $invoiceList = [];
            self::createEnvironment();
            $plans = [];
            $plans = $this->getPlans($plans);
            foreach ($plans as $plan) {
                $planType[$plan->plan()->id] = $plan->plan()->cfBoxType;
                $planPType[$plan->plan()->id] = $plan->plan()->cfPlanType;
            }

            $invoiceList = $this->getInvoices($fromDate, $toDate, $invoiceList);
            //$result = \chargebee\ChargeBee\Models\Invoice::all(array("status[is]" => 'paid', "date[between]" => "[$fromDate, $toDate]"));
            $header = ['Batch Id', 'Status', 'Customer Id', 'Subscription Id', 'Invoice Id', 'Order Id', 'Invoice Date', 'Order Date', 'Box Type', 'Company Name', 'Name', 'Email', 'Address 1', 'Address 2', 'City', 'State', 'Country', 'Zip', 'Is Validated'];
            $qs = [];
            $questions = Beautyprofilequestion::find()->where("active = '1'")->asArray()->all();
            foreach ($questions as $value) {
                $header[] = $value['name'];
                $qs[] = $value['id'];
            }
            $header[] = 'Age';
            $today = new \DateTime(date('Y-m-d'));
            $ans = [];
            $answers = \common\models\Beautyprofileanswer::find()->asArray()->all();
            foreach ($answers as $value) {
                $ans[$value['id']] = $value['name'];
            }

            $CsvData = array();
            $CsvData[] = $header;
            $showNew = $model->new == '1' ? true : false;
            $showProcessed = $model->processed == '1' ? true : false;
            $showCompleted = $model->completed == '1' ? true : false;
            $OrderObjs = array();
            foreach ($invoiceList as $invoice) {

                $planArr = array();
                foreach ($invoice->lineItems as $lineItem) {
                    if (isset($lineItem->entityId)) {
                        $planArr[$lineItem->subscriptionId] = $lineItem->entityId;
                    } else {
                        if (isset($lineItem->subscriptionId)) {
                            $result = \chargebee\ChargeBee\Models\Subscription::retrieve($lineItem->subscriptionId);
                            $subscription = $result->subscription() !== null ? $result->subscription() : null;
                            if ($subscription != null) {
                                if ($subscription->planUnitPrice == $lineItem->unitAmount) {
                                    $planArr[$lineItem->subscriptionId] = $subscription->planId;
                                }
                            }
                        }
                    }
                }
                if (count($planArr) > 0) {
                    foreach ($planArr as $subscriptionId => $plan) {
                        if ($plan != '') {
                            if (isset($planPType[$plan]) && $planPType[$plan] == 'Send Gift Subscription') {
                                continue;
                            }
                            if (isset($planType[$plan]) && $planType[$plan] == $model->boxType) {
                                $orderCreated = false;
                                $orderId = '';
                                $addInList = true;
                                $status = 'New';
                                $batchId = null;
                                $orderDate = $invoice->date;
                                //Invoice will only have linked orders if created else a new order has to be created
                                foreach ($invoice->linkedOrders as $order) {
                                    if ($order->status == $voidName) {
                                        continue;
                                    }
                                    $orderSubscriptionId = $invoice->subscriptionId;

                                    if (!isset($invoice->subscriptionId)) {
                                        if (!isset($OrderObjs[$order->id])) {
                                            $orderObj = $this->getOrder($order->id);
                                            $OrderObjs[$order->id] = $orderObj;
                                        } else {
                                            $orderObj = $OrderObjs[$order->id];
                                        }
                                        if (isset($OrderObjs[$order->id]->note)) {
                                            $orderSubscriptionId = trim($orderObj->note);
                                        }
                                        //if order is of future do not add it in list
                                        if ($orderObj->orderDate > time()) {
                                            continue;
                                        }
                                        $orderDate = $orderObj->orderDate;
                                    } else {
                                        $orderObj = $this->getOrder($order->id);
                                        if ($orderObj->orderDate > time()) {
                                            continue;
                                        }
                                        $orderDate = $orderObj->orderDate;
                                    }
                                    if ($orderSubscriptionId == $subscriptionId) {
                                        $orderCreated = true;
                                        $orderId = $order->id;
                                        if ($order->status == $processingName && $showProcessed == true) {
                                            $status = 'Processing';
                                            $addInList = true;
                                            if (isset($order->batchId)) {
                                                $batchId = $order->batchId;
                                            } else {
                                                if ($update === true) {
                                                    $orderUpdate = ["status" => $processingName, 'fulfillmentStatus' => 'Preparing', 'batchId' => $model->batchId, 'note' => $subscriptionId];
                                                    $order = \chargebee\ChargeBee\Models\Order::update($orderId, $orderUpdate);
                                                }
                                                $batchId = $model->batchId;
                                            }
                                        } elseif ($order->status == $newName && $showNew == true) {
                                            $status = 'New';
                                            if ($update === true) {
                                                $orderUpdate = ["status" => $processingName, 'fulfillmentStatus' => 'Preparing', 'batchId' => $model->batchId, 'note' => $subscriptionId];
                                                $order = \chargebee\ChargeBee\Models\Order::update($orderId, $orderUpdate);
                                            }
                                            $batchId = $model->batchId;
                                            $addInList = true;
                                        } elseif ($order->status == $completeName && $showCompleted == true) {
                                            $status = 'Complete';
                                            $addInList = true;
                                            if (isset($order->batchId)) {
                                                $batchId = $order->batchId;
                                            }
                                        } else {
                                            $addInList = false;
                                        }
                                    }
                                }

                                if ($orderCreated == true) {
                                    if ($batchId == null) {
                                        $addInList = false;
                                    }
                                }
                                if ($addInList == true && $orderCreated == true) {
                                    $shippingAddress = $invoice->shippingAddress;
                                    $invoiceDate = date('Y-m-d', $invoice->date);
                                    $orderDate = date('Y-m-d', $orderDate);
                                    $companyName = isset($shippingAddress->company) ? $shippingAddress->company : '';
                                    $customerName = (isset($shippingAddress->first_name) ? $shippingAddress->first_name : '') . ' ' . (isset($shippingAddress->last_name) ? $shippingAddress->last_name : '');
                                    $email = isset($shippingAddress->email) ? $shippingAddress->email : '';
                                    $line1 = isset($shippingAddress->line1) ? $shippingAddress->line1 : '';
                                    $line2 = isset($shippingAddress->line2) ? $shippingAddress->line2 : '';
                                    $city = isset($shippingAddress->city) ? $shippingAddress->city : '';
                                    $state = isset($shippingAddress->state) ? $shippingAddress->state : '';
                                    $country = isset($shippingAddress->country) ? $shippingAddress->country : '';
                                    $zip = isset($shippingAddress->zip) ? $shippingAddress->zip : '';
                                    $validStatus = isset($shippingAddress->validationStatus) ? $shippingAddress->validationStatus : '';

                                    $customerName = trim($customerName);
                                    $cuser = \common\models\User::find()->where(" cb_customer_id ='$invoice->customerId'")->one();
                                    if ($cuser != null) {
                                        if ($customerName == '') {
                                            $customerName = $cuser->f_name . " " . $cuser->l_name;
                                        }
                                        if ($email == '') {
                                            $email = $cuser->email;
                                        }
                                    }
                                    $datum = [$batchId,
                                        $status,
                                        $invoice->customerId,
                                        $subscriptionId,
                                        $invoice->id,
                                        $orderId,
                                        $invoiceDate,
                                        $orderDate,
                                        $plan,
                                        $companyName,
                                        $customerName,
                                        $email,
                                        $line1,
                                        $line2,
                                        $city,
                                        $state,
                                        $country,
                                        $zip,
                                        $validStatus
                                    ];
                                    $canswers = \common\models\Beautyprofile::find()->select(['answer', 'answer_text', 'answer_date', 'question_id'])->distinct()->where("customer_id = '$invoice->customerId'")->asArray()->all();
                                    $cans = array();
                                    foreach ($canswers as $canswer) {
                                        $bbanswer = '';
                                        $bbanswer = isset($ans[$canswer['answer']]) ? $ans[$canswer['answer']] : ($canswer['answer_text'] == null ? $canswer['answer_date'] : $canswer['answer_text']);
                                        if (isset($cans[$canswer['question_id']])) {
                                            $cans[$canswer['question_id']] .= ' / ' . $bbanswer;
                                        } else {
                                            $cans[$canswer['question_id']] = $bbanswer;
                                        }
                                    }
                                    $dob = null;
                                    foreach ($qs as $q) {
                                        $datum[] = isset($cans[$q]) ? $cans[$q] : '';
                                        if ($q == 16) {
                                            $dob = isset($cans[$q]) ? $cans[$q] : null;
                                        }
                                    }
                                    $age = '';
                                    if ($dob != null) {
                                        $dobA = new \DateTime($dob);
                                        $age = $today->diff($dobA)->format('%y');
                                    }
                                    $datum[] = $age;
                                    $CsvData[] = $datum;
                                }
                            }
                        }
                    }
                }
            }
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/downloads/customer_list_$model->batchId.csv";

            $file = fopen($filePath, "w+");
            foreach ($CsvData as $line) {
                fputcsv($file, $line);
            }
            fclose($file);
            if (file_exists($filePath)) {
                Yii::$app->response->sendFile($filePath);
            } else {
                echo "error while accessing file";
            }
        }
        return $this->render('allocations', ['model' => $model,]);
    }

    public static function createEnvironment() {
        $settings = \common\models\Settings::findOne(1);
        \chargebee\ChargeBee\Environment::configure($settings->cb_name, $settings->cb_api);
    }

}
