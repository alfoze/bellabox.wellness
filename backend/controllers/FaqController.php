<?php

namespace backend\controllers;

use Yii;
use common\models\Faq;
use common\models\FaqSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\config\Controller3;
use yii\filters\AccessControl;
/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends \common\config\Controller3 {

  public $mainMenu = 700;
  public $submenu = 703;

  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
              
              [
                  'actions' => ['view','index','update', 'delete','create'],
                  'allow' => true,
                  'roles' => ['@'],
              ],
          ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Faq models.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new FaqSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Faq model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    return $this->render('view', [
          'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Faq model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Faq();
    $lmodel = new \common\models\Sourcemessage;
    $lmodel2 = new \common\models\Sourcemessage;

    if ($model->load(Yii::$app->request->post())) {
      if ($model->save()) {
        if (isset($_POST['Sourcemessage']['question'])) {
          $lmodel->tmessage = $_POST['Sourcemessage']['question']['tmessage'];
          $lmodel->message = $model->question;
          $lmodel->save();
        }
        if (isset($_POST['Sourcemessage']['answer'])) {
          $lmodel2->tmessage = $_POST['Sourcemessage']['answer']['tmessage'];
          $lmodel2->message = $model->answer;
          $lmodel2->save();
        }

        \Yii::$app->getSession()->setFlash('success', \common\config\Options::getUNames('createSuccess'));
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    else {
      return $this->render('create', [
            'model' => $model, 'lmodel' => $lmodel, 'lmodel2' => $lmodel2
      ]);
    }
  }

  /**
   * Updates an existing Faq model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $lmodel = SourcemessageController::findModel2($model->question);
    $lmodel2 = SourcemessageController::findModel2($model->answer);

    if ($model->load(Yii::$app->request->post())) {

      if ($model->save()) {

        if (isset($_POST['Sourcemessage']['question'])) {
          $lmodel->tmessage = $_POST['Sourcemessage']['question']['tmessage'];
          $lmodel->message = $model->question;
          $lmodel->save();
        }
        if (isset($_POST['Sourcemessage']['answer'])) {
          $lmodel2->tmessage = $_POST['Sourcemessage']['answer']['tmessage'];
          $lmodel2->message = $model->answer;
          $lmodel2->save();
        }

        \Yii::$app->getSession()->setFlash('success', \common\config\Options::getUNames('updateSuccess'));
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    else {
      return $this->render('update', [
            'model' => $model, 'lmodel' => $lmodel, 'lmodel2' => $lmodel2
      ]);
    }
  }

  /**
   * Deletes an existing Faq model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Faq model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Faq the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Faq::findOne($id)) !== null) {
      return $model;
    }
    else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
