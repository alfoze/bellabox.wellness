<?php

namespace backend\controllers;

use Yii;
use common\models\Newsletteremails;
use common\models\NewsletteremailsSearch;
use common\config\Controller3;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

/**
 * NewsletteremailsController implements the CRUD actions for Newsletteremails model.
 */
class NewsletteremailsController extends Controller3 {

  public $mainMenu = 700;
  public $submenu = 705;

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
            [
            'actions' => ['view', 'index', 'update', 'delete', 'create', 'export'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  private function export($query = null) {
    $fields = array( 'name', 'last_name','email','ctimestamp');
    $data = array();
    $searchModel = new NewsletteremailsSearch();
    $dataProvider = $searchModel->search($query, false);
    $data = $dataProvider->getModels();
    $this->sendAsXLS( 'Newsletter_file', $data, '', true, $fields);
  }
  /**
   * Lists all Newsletteremails models.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new NewsletteremailsSearch();
    if (isset(Yii::$app->request->queryParams['ExportCSV'])) {
      $this->export(Yii::$app->request->queryParams);
      return;
    }
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
    ]);
  }

  public function actionExport() {
    $fields = array('email');
    $data = \common\models\Newsletteremails::find()->all();
    $this->sendAsXLS(\common\config\Options::getOptionName(Controller3::NEWSLETTER_EMAILS) . '_file', $data, '', true, $fields);
  }

  /**
   * Displays a single Newsletteremails model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    return $this->render('view', [
          'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Newsletteremails model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Newsletteremails();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    }
    else {
      return $this->render('create', [
            'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Newsletteremails model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    }
    else {
      return $this->render('update', [
            'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Newsletteremails model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Newsletteremails model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Newsletteremails the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Newsletteremails::findOne($id)) !== null) {
      return $model;
    }
    else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
