<?php

namespace backend\controllers;

use Yii;
use common\models\Optin;
use common\models\OptinSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OptinController implements the CRUD actions for Optin model.
 */
class OptinController extends \common\config\Controller3 {

  public $mainMenu = 700;
  public $submenu = 706;

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
       'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => [
            [
            'actions' => [ 'update', 'index'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
   * Lists all Optin models.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new OptinSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Updates an existing Optin model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param string $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    else {
      return $this->render('update', [
            'model' => $model,
      ]);
    }
  }

  /**
   * Finds the Optin model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param string $id
   * @return Optin the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Optin::findOne($id)) !== null) {
      return $model;
    }
    else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
