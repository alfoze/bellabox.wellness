<?php

namespace backend\controllers;

use Yii;
use common\models\Contents;
use common\models\ContentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * ContentsController implements the CRUD actions for Contents model.
 */
class ContentsController extends \common\config\Controller3 {

  public $mainMenu = 600;
  public $submenu = 607;

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
              
              [
                  'actions' => ['view','index','update', 'delete','create'],
                  'allow' => true,
                  'roles' => ['@'],
              ],
          ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  protected function uploadImage($model, $colname) {

    if (\yii\web\UploadedFile::getInstance($model, $colname)) {
      $fileObj = \yii\web\UploadedFile::getInstance($model, $colname);
      $filename = "";
      $dirpath = realpath(dirname(getcwd()));
      $res = $dirpath; // str_replace("backend", "frontend", $dirpath);
      $filename = "img/contents/" . uniqid() . $fileObj->name;
      $uploaddir = $res;
      $fileObj->saveAs($uploaddir . "/" . $filename);
      return $filename;
    }
    else {
      return $model->oldImage;
    }
  }

  /**
   * Lists all Contents models.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new ContentsSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Contents model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    return $this->render('view', [
          'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Contents model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Contents();

    if ($model->load(Yii::$app->request->post())) {
      $model->path = $this->uploadImage($model, 'path');
      if ($model->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
      }
      else {
        var_dump($model->getErrors());
      }
    }

    return $this->render('create', ['model' => $model,]);
  }

  /**
   * Updates an existing Contents model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      $model->path = $this->uploadImage($model, 'path');
      if ($model->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    return $this->render('update', ['model' => $model]);
  }

  /**
   * Deletes an existing Contents model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $model=$this->findModel($id);
    @unlink($model->path);
    $model->delete();
    return $this->redirect(['index']);
  }

  /**
   * Finds the Contents model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Contents the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Contents::findOne($id)) !== null) {
      return $model;
    }
    else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
