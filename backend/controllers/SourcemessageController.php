<?php

namespace backend\controllers;

use Yii;
use common\models\Sourcemessage;
use common\models\SourcemessageSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * SourcemessageController implements the CRUD actions for Sourcemessage model.
 */
class SourcemessageController extends \common\config\Controller3 {

    public $mainMenu = 600;
    public $submenu = 606;
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                      
                      [
                          'actions' => ['view','index','update', 'delete','create'],
                          'allow' => true,
                          'roles' => ['@'],
                      ],
                  ],
              ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sourcemessage models.
     * @return mixed
     */
    protected $target_language = 'ar';
    protected $category = 'app';


    public function actionExport() {
        $fields = array('index', 'message', 'translation');
        $data = \common\models\Sourcemessage::find()->all();
        $this->sendAsXLS('Translation_file', $data, '', true, $fields, false, 50);
    }
    public function actionIndex() {
        $searchModel = new SourcemessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sourcemessage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sourcemessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Sourcemessage();
        $languages = \common\models\Languages::findAll(['active' => '1', 'base' => '0']);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                        'model' => $model, 'languages' => $languages
            ]);
        }
    }

    /**
     * Updates an existing Sourcemessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public static function findModel2($msg){
        $model = Sourcemessage::findOne(['message'=>$msg]);
        if($model== null){
            return new Sourcemessage();
        }
        $languages = \common\models\Languages::findAll(['active' => '1', 'base' => '0']);
        return self::setTranslation($model,$languages);
    }
    public static function setTranslation($model,$languages){
        foreach ($languages as $language) {
            $tm = \common\models\Message::findOne(['language' => $language->id, 'id' => $model->id]);
            if ($tm != null) {
                $model->tmessage[$language->id] = $tm->translation;
            }
        }
        return $model;
    }
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $languages = \common\models\Languages::findAll(['active' => '1', 'base' => '0']);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }
        $model = self::setTranslation($model, $languages);
        return $this->render('update', [
                    'model' => $model, 'languages' => $languages
        ]);
    }

    /**
     * Deletes an existing Sourcemessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sourcemessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sourcemessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Sourcemessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
