<?php

namespace backend\controllers;

use Yii;
use common\models\Beautyprofilequestion;
use common\models\BeautyprofilequestionSearch;
use common\models\Beautyprofileanswer;
use backend\models\Model;
use common\config\Controller3;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\helpers\ArrayHelper;
use \yii\httpclient\Response;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
/**
 * BeautyprofilequestionController implements the CRUD actions for Beautyprofilequestion model.
 */
class BeautyprofilequestionController extends Controller3 {

  public $mainMenu = 600;
  public $submenu = 606;

  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
              
              [
                  'actions' => ['view','index','update', 'delete','create'],
                  'allow' => true,
                  'roles' => ['@'],
              ],
          ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Beautyprofilequestion models.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new BeautyprofilequestionSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Beautyprofilequestion model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    $searchModel = new \common\models\BeautyprofileanswerSearch();
    $searchModel->question_id = $id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('view', [
          'model' => $this->findModel($id),
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Creates a new Beautyprofilequestion model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Beautyprofilequestion();
    $modelsDiv = [new Beautyprofileanswer];

    if ($model->load(Yii::$app->request->post())) {

      $modelsDiv = Model::createMultiple(Beautyprofileanswer::classname());
      Model::loadMultiple($modelsDiv, Yii::$app->request->post());

      foreach ($modelsDiv as $index => $modelDiv) {
        $modelDiv->image = $this->uploadImage($modelDiv, "[{$index}]image");
      }
      // ajax validation
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ArrayHelper::merge(
                ActiveForm::validateMultiple($modelsDiv), ActiveForm::validate($model)
        );
      }

      // validate all models
      $valid = $model->validate();
      $valid = Model::validateMultiple($modelsDiv) && $valid;

      if ($valid) {
        $transaction = \Yii::$app->db->beginTransaction();
        try {

          if ($flag = $model->save(false)) {
            foreach ($modelsDiv as $modelDiv) {
              $modelDiv->question_id = $model->id;
              if (!($flag = $modelDiv->save(false))) {
                $transaction->rollBack();
                break;
              }
            }
          }

          if ($flag) {
            $transaction->commit();
            return $this->redirect(['view', 'id' => $model->id]);
          }
        }
        catch (Exception $e) {
          $transaction->rollBack();
        }
      }
    }
    return $this->render('create', [
          'model' => $model,
          'modelsDiv' => (empty($modelsDiv)) ? [new Beautyprofileanswer] : $modelsDiv
    ]);
  }

  /**
   * Updates an existing Beautyprofilequestion model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  protected function uploadImage($model, $colname) {

    if (\yii\web\UploadedFile::getInstance($model, $colname)) {
      $fileObj = \yii\web\UploadedFile::getInstance($model, $colname);
      $filename = "";
      $dirpath = realpath(dirname(getcwd()));
      $res = $dirpath; // str_replace("backend", "frontend", $dirpath);
      $filename = "img/beautyprofile/" . uniqid() . $fileObj->name;
      $uploaddir = $res;
      $fileObj->saveAs($uploaddir . "/" . $filename);
      return $filename;
    }
    else {
      return $model->oldImage;
    }
  }

  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $modelsDiv = $model->answers;

    if ($model->load(Yii::$app->request->post())) {

      $oldIDs = ArrayHelper::map($modelsDiv, 'id', 'id');
      $modelsDiv = Model::createMultiple(Beautyprofileanswer::classname(), $modelsDiv);
      Model::loadMultiple($modelsDiv, Yii::$app->request->post());
      $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsDiv, 'id', 'id')));

      foreach ($modelsDiv as $index => $modelDiv) {
        $modelDiv->image = $this->uploadImage($modelDiv, "[{$index}]image");
        ;
      }

      // ajax validation
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ArrayHelper::merge(ActiveForm::validateMultiple($modelsDiv), ActiveForm::validate($model));
      }

      // validate all models
      $valid = $model->validate();
      $valid = Model::validateMultiple($modelsDiv) && $valid;

      if ($valid) {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
          if ($flag = $model->save(false)) {
            if (!empty($deletedIDs)) {
              Beautyprofileanswer::deleteAll(['id' => $deletedIDs]);
            }
            foreach ($modelsDiv as $modelDiv) {
              $modelDiv->question_id = $model->id;
              if (!($flag = $modelDiv->save(false))) {
                $transaction->rollBack();
                break;
              }
            }
          }
          if ($flag) {
            $transaction->commit();
            return $this->redirect(['view', 'id' => $model->id]);
          }
        }
        catch (Exception $e) {
          $transaction->rollBack();
        }
      }
    }

    return $this->render('update', [
          'model' => $model,
          'modelsDiv' => (empty($modelsDiv)) ? [new Divs] : $modelsDiv
    ]);
  }

  /**
   * Deletes an existing Beautyprofilequestion model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Beautyprofilequestion model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Beautyprofilequestion the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Beautyprofilequestion::findOne($id)) !== null) {
      return $model;
    }
    else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
