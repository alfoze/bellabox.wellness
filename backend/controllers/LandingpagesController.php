<?php

namespace backend\controllers;

use Yii;
use backend\models\Landingpages;
use backend\models\LandingpagesSearch;
use backend\models\Divs;
use backend\models\Model;
use common\config\Controller3;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
/**
 * LandingpagesController implements the CRUD actions for Landingpages model.
 */
class LandingpagesController extends Controller3 {

  public $mainMenu = 600;
  public $submenu = 602;

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
              
              [
                  'actions' => ['view','index','update', 'delete','create'],
                  'allow' => true,
                  'roles' => ['@'],
              ],
          ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
   * Lists all Landingpages models.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new LandingpagesSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Landingpages model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    $model = $this->findModel($id);
    $modelsDiv = $model->divs;


    return $this->render('view', [
          'model' => $model,
          'modelsDiv' => $modelsDiv,
    ]);
  }

  /**
   * Creates a new Landingpages model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Landingpages();
    $modelsDiv = [new Divs];
    if ($model->load(Yii::$app->request->post())) {

      $modelsDiv = Model::createMultiple(Divs::classname());
      Model::loadMultiple($modelsDiv, Yii::$app->request->post());

      // ajax validation
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ArrayHelper::merge(
                ActiveForm::validateMultiple($modelsDiv), ActiveForm::validate($model)
        );
      }

      // validate all models
      $valid = $model->validate();
      $valid = Model::validateMultiple($modelsDiv) && $valid;

      if ($valid) {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
          $model->created_on = date('y-m-d');
          $model->created_by = Yii::$app->user->identity->id;
          $model->can_delete = 1;
          if ($flag = $model->save(false)) {
            foreach ($modelsDiv as $modelDiv) {
              $modelDiv->landingpage_id = $model->id;
              if (!($flag = $modelDiv->save(false))) {
                $transaction->rollBack();
                break;
              }
            }
          }
          if ($flag) {
            $transaction->commit();
            return $this->redirect(['view', 'id' => $model->id]);
          }
        }
        catch (Exception $e) {
          $transaction->rollBack();
        }
      }
    }

    return $this->render('create', [
          'model' => $model,
          'modelsDiv' => (empty($modelsDiv)) ? [new Divs] : $modelsDiv
    ]);
  }

  /**
   * Updates an existing Landingpages model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {

    $model = $this->findModel($id);
    $modelsDiv = $model->divs;

    if ($model->load(Yii::$app->request->post())) {

      $oldIDs = ArrayHelper::map($modelsDiv, 'id', 'id');
      $modelsDiv = Model::createMultiple(Divs::classname(), $modelsDiv);
      Model::loadMultiple($modelsDiv, Yii::$app->request->post());
      $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsDiv, 'id', 'id')));

      // ajax validation
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ArrayHelper::merge(
                ActiveForm::validateMultiple($modelsDiv), ActiveForm::validate($model)
        );
      }

      // validate all models
      $valid = $model->validate();
      $valid = Model::validateMultiple($modelsDiv) && $valid;

      if ($valid) {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
          $model->updated_on = date('y-m-d');
          if ($flag = $model->save(false)) {
            if (!empty($deletedIDs)) {
              Divs::deleteAll(['id' => $deletedIDs]);
            }
            foreach ($modelsDiv as $modelDiv) {
              $modelDiv->landingpage_id = $model->id;
              if (!($flag = $modelDiv->save(false))) {
                $transaction->rollBack();
                break;
              }
            }
          }
          if ($flag) {
            $transaction->commit();
            return $this->redirect(['view', 'id' => $model->id]);
          }
        }
        catch (Exception $e) {
          $transaction->rollBack();
        }
      }
    }

    return $this->render('update', [
          'model' => $model,
          'modelsDiv' => (empty($modelsDiv)) ? [new Divs] : $modelsDiv
    ]);
  }

  /**
   * Deletes an existing Landingpages model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $model = $this->findModel($id);
    if ($model->can_delete == '1') {
      $model->delete();
    }
    else {
      \Yii::$app->getSession()->setFlash('danger', "This page layout can not be deleted!");
    }
    return $this->redirect(['index']);
  }

  /**
   * Finds the Landingpages model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Landingpages the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Landingpages::findOne($id)) !== null) {
      return $model;
    }
    else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
