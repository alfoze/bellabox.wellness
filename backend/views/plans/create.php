<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Plans */

$this->title = Yii::t('app', 'Create Plan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plans-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
