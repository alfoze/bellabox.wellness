<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Plans */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plans-view">

    <p>
        <?= Html::a('List', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
          ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
      'model' => $model,
      'attributes' => [
        'name',
        'description',
        'cb_name',
        'cb_url:url',
          [
          'attribute' => 'active',
          'value' => common\config\Options::getStatusId($model->active)
        ],
        [
          'attribute' => 'gift',
          'value' => common\config\Options::getYesNoById($model->gift)
        ],
      ],
    ])
    ?>

</div>
