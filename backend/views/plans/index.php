<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\PlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Plans');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plans-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Plan'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
        'name',
        'description',
        'cb_name',
        'cb_url:url',
        [
          'attribute' => 'active',
          'value' => function($model, $index, $dataColumn) {
            return common\config\Options::getStatusId($model->active);
          },
          'filter' => Html::activeDropDownList($searchModel, 'active', common\config\Options::getStatus(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Status')]),
        ],
              [
          'attribute' => 'gift',
          'value' => function($model, $index, $dataColumn) {
            return common\config\Options::getYesNoById($model->gift);
          },
          'filter' => Html::activeDropDownList($searchModel, 'gift', common\config\Options::getYesNo(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Is Gift')]),
        ],
        
          ['class' => 'yii\grid\ActionColumn'],
      ],
    ]);
    ?>

</div>
