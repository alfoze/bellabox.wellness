<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?= $this->render('/sourcemessage/_embeded', ['model' => $lmodel, 'form' => $form,'colName'=>$model->getAttributeLabel('question'), 'pre'=>'question']) ?>
    <?= $form->field($model, 'answer')->textarea(['rows' => 4]) ?>
    <?= $this->render('/sourcemessage/_embeded', ['model' => $lmodel2, 'form' => $form,'colName'=>$model->getAttributeLabel('answer'), 'pre'=>'answer']) ?>
    



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
