<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */

$this->title = $model->question;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-view">



    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>
    
    <?php
    $decs = '';
    $decs2 = '';
    $lm = \common\models\Sourcemessage::findOne(['message' => $model->question]);
    if ($lm != null) {
        $tmodel = \common\models\Message::findAll($lm->id);
        foreach ($tmodel as $value) {
            $decs .= "( <b>" . $value->language . "</b> : " . $value->translation . " ) ";
        }
    }
    $lm = \common\models\Sourcemessage::findOne(['message' => $model->answer]);
    if ($lm != null) {
        $tmodel = \common\models\Message::findAll($lm->id);
        foreach ($tmodel as $value) {
            $decs2 .= "( <b>" . $value->language . "</b> : " . $value->translation . " ) ";
        }
    }
    ?>
    

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            [
                'attribute' => 'question',
                'value' => $model->question . '<br>' . $decs,
                'format' => 'html',
            ],
            [
                'attribute' => 'answer',
                'value' => $model->answer . '<br>' . $decs2,
                'format' => 'html',
            ],            
            
        ],
    ])
    ?>

</div>
