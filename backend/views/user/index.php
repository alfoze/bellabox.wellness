<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unames-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Shop Customer'), ['creatcbcustomer'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['style' => 'table-layout: fixed;', 'class' => 'table table-striped table-bordered'],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'options' => ['style' => 'width:50px']],
            'f_name',
            'l_name',
            'email',
            'c_timestamp',
            'cb_customer_id',
            [
                'attribute' => 'is_admin',
                'value' => function($model, $index, $dataColumn) {
                    return \common\config\Options::getYesNoById($model->is_admin);
                },
                'filter' => Html::activeDropDownList($searchModel, 'is_admin', \common\config\Options ::getYesNo(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Type')]),
            ],
            ['class' => 'yii\grid\ActionColumn',
                'header' => Html::a(Yii::t('app', 'Export'), $_SERVER['REQUEST_URI'] . '&ExportCSV=true', ['class' => 'btn btn-success btn-sm']),
                'options' => ['style' => 'width:90px'],
                'template' => '{view} {update} {pass} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {

                        return $model->is_admin == '1' ? '' : Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'data' => [
                                        'confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                                        'method' => 'post',
                                    ],
                                    'title' => Yii::t('app', 'Delete')
                        ]);
                    },
                    'pass' => function ($url, $model) {
                        $url = \yii\helpers\Url::to(['/user/updatepass', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-lock"></span>', $url, ['title' => Yii::t('app', 'Reset Password')
                        ]);
                    }
                ],
            ],
        ],
    ]);
    ?>

</div>

<style>
    td{
        word-wrap:break-word
    }

</style>