<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Countries;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unames-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'password1')->passwordInput(['maxlength' => true, 'autocomplete'=>"off"]) ?>
        </div>        
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'password2')->passwordInput(['maxlength' => true, 'autocomplete'=>"off"  ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Update Password'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
