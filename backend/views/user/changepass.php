<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */

$this->title = Yii::t('app', 'Update {modelClass}: ', []) . ' ' . $model->username;
$this->title = Yii::t('app', 'Update {modelClass}: ', [ 'modelClass' => 'User',]) . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<p>
</p>

<div class="unames-update">
    <h1><?= $model->fullname?></h1>
    <?= $this->render('_formpass', [ 'model' => $model,])?>

</div>
