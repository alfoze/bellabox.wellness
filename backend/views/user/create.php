<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */

$this->title = Yii::t('app', 'Create Shop Customer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unames-create">
    <p> Please only use this option to create customer for shopify</p>
    <?=
    $this->render('_form2', [
        'model' => $model,
    ])
    ?>

</div>
