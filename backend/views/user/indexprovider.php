<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Service Providers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unames-index">


    <?php
    // echo $this->render('_search', ['model' => $searchModel]); 
    $colname = Yii::$app->language == 'ar' ? 'name_ar' : 'name';
    ?>
    <p>
        <?= Html::a(Yii::t('app', 'Export'), ['export2'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['style' => 'table-layout: fixed;', 'class' => 'table table-striped table-bordered'],
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'options' => ['style' => 'width:50px']
            ],
            'username',
            'f_name',
            'email',
            [
                'attribute' => 'package',
                'value' => 'packagename',
                'filter' => Html::activeDropDownList($searchModel, 'package', ArrayHelper::map(\common\models\Package::find()->asArray()->all(), 'id', $colname), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Package')]),
            ],
            [
                'attribute' => 'city',
                'value' => 'cityname',
                'filter' => Html::activeDropDownList($searchModel, 'city', ArrayHelper::map(\common\models\Cities::find()->where(['active' => '1'])->asArray()->all(), 'id', $colname), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select City')]),
            ],
            ['attribute' => 'c_timestamp',
                'options' => ['style' => 'width:120px']
            ],
            [
                'header' => 'Rating',
                'value' => 'rating2',
                'filter' => Html::activeDropDownList($searchModel, 'rate', ArrayHelper::map(\common\config\Options ::getRatingSearch(), 'id', 'name'), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Rating')]),
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'options' => ['style' => 'width:30px'],
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['/user/view', 'id' => $model->id, 'type' => 'P']);
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => 'View']);
                    },
                        ],
                    ],
                ],
            ]);
            ?>

</div>
<style>
    td{
        word-wrap:break-word
    }

</style>