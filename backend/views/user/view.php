<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if (!isset($pop))
    $pop = false;
?>

<div class="unames-view">
    <?php if ($pop == false) { ?>
        <p>
            <?= Html::a(Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Change Password'), ['updatepass', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php } ?>



    <div class="row">
        <div class="col-md-6">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'username',
                    'f_name',
                    'l_name',
                    'email',
                    'company_name',
                ],
            ])
            ?>
        </div>
        <div class="col-md-6">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'mobile',
                    'c_timestamp',
                    [
                        'attribute' => 'is_admin',
                        'value' => $model->is_admin == "1" ? "Yes" : "No"
                    ],
                    'cb_customer_id',
                    'tmp_pass'
                ],
            ])
            ?>
        </div>
    </div>
    <hr>
    <h2>Beauty Profile</h2>  
    <?php
    if (count($beautyProfile) == 0) {
        echo "<h4>Beauty profile is not filled yet!</h4>";
    }
    ?>
    <div class="row">
        <?php
        foreach ($beautyProfile as $value) {
            if (isset($value['id'])) {
                ?>
                <div class="col-md-4"><b> Q.<?= $value['id'] . " : </b>" . $value['qname'] ?><br><b>Answer :</b> <?= isset($value['answer']) ?: 'N/A' ?></div>
            <?php }
        } ?>
    </div>
    <hr>
    <h2>Allocations</h2>  
    <?php
    if (count($allocations) == 0) {
        echo "<h4>No allocation have been done yet!</h4>";
    } else {
        ?>
        <table class="table">
            <tr>
                <th>Date</th>
                <th>Subscription ID</th>
                <th>Order ID</th>
                <th>Box Name</th>
                <th>Tracking No.</th>
            </tr>    
            <?php foreach ($allocations as $value) { ?>
                <tr>
                    <td><?= $value->created_when ?></td>
                    <td><?= $value->subscription_id ?></td>
                    <td><?= $value->order_id ?></td>
                    <td><?= $value->box_name ?></td>
                    <td><?= $value->mail_ref ?></td>
                </tr>
            <?php } ?>
        </table>
    <?php } ?>
    <hr>

    <?php
    $totalReward = 0;
    foreach ($rewardPoints as $value) {
        $totalReward += $value->points;
    }
    ?>
    <h2>Reward Points (<?= $totalReward ?>)</h2>  
    <?php
    if (count($rewardPoints) == 0) {
        echo "<h4>No reward points have been earned yet!</h4>";
    } else {
        ?>
        <table class="table">
            <tr>
                <th>Date</th>
                <th>Expiry Date</th>
                <th>Reason</th>
                <th>Points</th>
                <th>Expired</th>
            </tr>    
            <?php foreach ($rewardPoints as $value) { ?>
                <tr>
                    <td><?= $value->create_date ?></td>
                    <td><?= $value->expiry_date ?></td>
                    <td><?= $value->reason ?></td>
                    <td><?= $value->points ?></td>
                    <td><?= strtotime($value->expiry_date) > time() ? "Not Expired" : "Expired" ?></td>
                </tr>
            <?php } ?>
        </table>
    <?php } ?>

</div>