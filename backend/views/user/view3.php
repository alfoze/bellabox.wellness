<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
Modal::begin([
    'header' => '<h4>User Package</h4>',
    'id' => 'modal',
    'size' => 'modal-md',
]);
echo "<div id='modal-body'></div>";
Modal::end();

?>

<div class="unames-view">
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php
    $bImage = $model->b_img == "" ? "images/backbanar.jpg" : $model->b_img;
    $cImage = $model->pic == "" ? "images/user.png" : $model->pic;
    ?>
    <div id='cover_container' style="background:url('<?= common\config\Options::getFrontendAddress() . $bImage; ?>');">
        <div id='info_box'>
            <div id="profile_img"><img src="<?= common\config\Options::getFrontendAddress() . $cImage; ?>" class='avatar_img'/></div>
            <div id="info-box">
                <div id="info-name"><h3><?= $model->fullname ?></h3></div> 
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'username',
                    'f_name',
                    'l_name',
                    'email',
                    'address',
                    'phone',
                    'c_timestamp'
                ],
            ])
            ?>
        </div>
        <div class="col-md-6">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'package',
                        'value' => \common\models\Package::findOne($model->package)->name." ".($model->account_type =='0'?'': Html::a(Yii::t('app', 'Update Package'),['packagerequest/create', 'userid' => $model->id] ,[ 'class' => 'btn btn-u btn-success pull-right']) ),
                        'format'=>'raw'
                    ],
                    [
                        'attribute' => 'account_type',
                        'value' => common\config\Options::getAccountTypeById($model->account_type)
                    ],
                    [
                        'attribute' => 'city',
                        'value' => $model->city == "" ? null : \common\models\Cities::findOne($model->city)->name
                    ],
                    [
                        'attribute' => 'country',
                        'value' => $model->country == "" ? null : \common\models\Countries::findOne($model->country)->name
                    ],
                    [
                        'attribute' => 'is_active',
                        'value' => common\config\Options::getYesNoById($model->is_active)
                    ],
                    'device',
                    [
                        'attribute' => 'is_admin',
                        'value' => $model->is_admin == "1" ? "Yes" : "No"
                    ],
                ],
            ])
            ?>
        </div>
    </div>

    <?php if ($model->account_type == '1') { ?>
        <div class="row">
            <div class="col-md-3">
                <h3>
                    <?= common\config\Options::getOptionPName(common\config\Controller2::CATEGORY) . ' ' . Yii::t('app', 'Associated') ?>
                </h3>
                <div class="category-index">

                    <?=
                    yii\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'category_id',
                                'value' => function($model, $index, $dataColumn) {
                                    $decs = "";
                                    $city = \common\models\Category::findOne($model->category_id);
                                    if ($city !== null) {
                                        $decs = $city->name;
                                    }
                                    return $decs;
                                }
                            ],
                    ]]);
                    ?>

                </div>
            </div>
            <div class="col-md-9">
                <h3>
                    <?= common\config\Options::getOptionPName(common\config\Controller2::BUSINESS) . ' ' . Yii::t('app', 'Associated') ?>
                </h3>
                <div class="category-index">

                    <?=
                    yii\grid\GridView::widget([
                        'dataProvider' => $dataProvider1,
                        'filterModel' => $searchModel1,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [ 'attribute' => 'add_title',
                                'value' => function($model, $index, $dataColumn) {
                                    return Html::a($model->add_title, Yii::$app->urlManager->createUrl(["/site/businessview", 'id' => $model->id]));
                                },
                                        'format' => 'raw'
                                    ],
                                    [
                                        'attribute' => 'description',
                                        'value' => function($model, $index, $dataColumn) {
                                            $rtn = $model->description;
                                            if (strlen($rtn) > 40) {
                                                $rtn = substr($model->description, 0, 40) . " ... ";
                                            }
                                            return $rtn;
                                        },
                                        'format' => 'raw'
                                    ],
                                    'c_timestampt',
                                    'price',
                                    [
                                        'attribute' => 'category_id',
                                        'value' => function($model, $index, $dataColumn) {
                                            return $model->category;
                                        },
                                        'filter' => Html::activeDropDownList($searchModel1, 'category_id', common\config\Options::getCategories(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Category')]),
                                    ],
                                    'orders',
                                    'rating',
                                    'reveiws'
                                ]
                            ]);
                            ?>

                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="row">
                <div class="col-md-12">
                    <h3>
                        <?= common\config\Options::getOptionPName(common\config\Controller2::ORDER) ?>
                    </h3>
                    <div class="order-index">

                        <?=
                        yii\grid\GridView::widget([
                            'dataProvider' => $dataProvider2,
                            'filterModel' => $searchModel2,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute' => 'category_id',
                                    'value' => function($model, $index, $dataColumn) {
                                        $cat = \common\models\Category::findOne($model->category_id);
                                        if ($cat != null)
                                            return $cat->name;
                                    },
                                    'filter' => Html::activeDropDownList($searchModel2, 'category_id', common\config\Options::getCategories(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Category')]),
                                ],
                                [
                                    'attribute' => 'user_id',
                                    'value' => 'user',
                                    'filter' => Html::activeDropDownList($searchModel2, 'user_id', common\config\Options::getUsers(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select User')]),
                                ],
                                'subject',
                                'description',
                                'price',
                                'c_timestamp',
                                'status',
                                'feedback',
                                ['class' => 'yii\grid\ActionColumn',
                                    'template' => '{view}',
                                    'buttons' => [
                                        'view' => function ($url, $model, $key) {
                                            if ($model->type == 'O')
                                                $url = \yii\helpers\Url::to(['/order/view', 'id' => $model->id]);
                                            else
                                                $url = \yii\helpers\Url::to(['/request/view', 'id' => $model->id]);

                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => 'View']);
                                        },
                                            ],
                                        ],
                                    ]
                                ]);
                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <?= common\config\Options::getOptionPName(common\config\Controller2::FEEDBACK) ?>
                            </h3>
                            <div class="feedback-index">

                                <?=
                                yii\grid\GridView::widget([
                                    'dataProvider' => $dataProvider4,
                                    'filterModel' => $searchModel4,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'attribute' => 'business_id',
                                            'value' => function($model, $index, $dataColumn) {
                                                $cat = \common\models\Business::findOne($model->business_id);
                                                if ($cat != null)
                                                    return $cat->add_title;
                                            },
                                            'filter' => Html::activeDropDownList($searchModel4, 'business_id', common\config\Options::getBusinesses(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select ' . common\config\Options::getOptionName(common\config\Controller2::BUSINESS))]),
                                        ],
                                        'order_id',
                                        'rate',
                                        'c_date',
                                        [
                                            'attribute' => 'user_id',
                                            'value' => 'user',
                                            'filter' => Html::activeDropDownList($searchModel4, 'provider_id', common\config\Options::getEndUsers(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select End User')]),
                                        ],
                                        'comments'
                                    ]
                                ]);
                                ?>

            </div>
        </div>
    </div>
</div>


<style>
    #container{
        margin:0 auto;
        width:100%;
        height: 100px;
    }

    #container-content{
        border:solid 1px #cccccc;
        padding:20px;
        width:100%;

        color:#cccccc;
    }

    #info_box{
        padding:60px 50px 0px 10px;
        border:solid 1px #dedede;
        width:538px;
        background-color:#fcfcfc;
        margin-top:88px;
    }

    #profile_img{
        background-color: #FFFFFF;
        border: 1px solid #DEDEDE;
        margin-left: 10px;
        margin-top: -100px;
        position: absolute;
        width: 86px;
        height: 86px;
        float:left;
    }

    .avatar_img{
        padding:3px;
        width: 85px;
        height: 85px;
    }

    #cover_container{
        overflow: auto;
        width: 100%;
        height:150px;

    }

    #info-box{
        margin-left:115px;
        margin-top: -50px;
        position: absolute;
    }

    #info-name{
        float:left;
        overflow:hidden;
        word-wrap: break-word;
        width:400px;
    }

    #info-content{
        margin-left:170px;
        width:290px;
    }

    #info-photos{
        text-align:center;
        font-size:11px;
        padding:5px;
        float:left;
        width:80px;
        border:solid 1px #eeeeee;
        -moz-border-radius: 3px;-webkit-border-radius: 3px;border-radius: 3px;
    }

    #info-friends{
        text-align:center;
        font-size:11px;
        padding:5px;
        margin-left:100px;
        border:solid 1px #eeeeee;
        width:80px;
        -moz-border-radius: 3px;-webkit-border-radius: 3px;border-radius: 3px;
    }


</style>