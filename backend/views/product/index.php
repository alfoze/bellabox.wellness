<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = common\config\Options::getOptionPName($searchModel->PID);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create ' . common\config\Options::getOptionName($searchModel->PID), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          [
          'attribute' => 'image',
          'value' => function($model, $index, $dataColumn) {
            return Html::img(common\config\Options::getFrontendAddress() . $model->image, ['width' => '100']);
          },
          'format' => 'html',
          'filter' => false
        ],
        'name',
        'fullname',
          [
          'attribute' => 'type',
          'value' => function($model, $index, $dataColumn) {
            return common\config\Options::getProductTypeByID($model->type);
          },
          'filter' => Html::activeDropDownList($searchModel, 'type', common\config\Options::getProductTypes(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Type')]),
        ],
        'sku',
          [
          'attribute' => 'status',
          'value' => function($model, $index, $dataColumn) {
            return common\config\Options::getStatusId($model->status);
          },
          'filter' => Html::activeDropDownList($searchModel, 'status', common\config\Options::getStatus(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Status')]),
        ],
        // 'price',
        // 'org_price',
        // 'status',
        // 'image',
        // 'color',
        // 'size',
        // 'brand',
        // 'typeform_url:url',
        // 'typeform_code',
        ['class' => 'yii\grid\ActionColumn'],
      ],
    ]);
    ?>
</div>
