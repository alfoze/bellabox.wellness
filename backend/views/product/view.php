<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => common\config\Options::getOptionPName($model->PID), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">


    <p>
        <?= Html::a('List', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
          ],
        ])
        ?>
    </p>

    <div class="row">
        <div class="col-md-4">
            <img  class="img-responsive" src ="<?= common\config\Options::getFrontendAddress() . $model->image ?>" alt="<?= $model->name ?>"/>
        </div>
        <div class="col-md-8">
            <?=
            DetailView::widget([
              'model' => $model,
              'attributes' => [
                'id',
                'name',
                'fullname',
                  [
                  'attribute' => 'type',
                  'value' => common\config\Options::getProductTypeByID($model->type)
                ],
                'sku',
                'price',
                'org_price',
                  [
                  'attribute' => 'status',
                  'value' => common\config\Options::getStatusId($model->status)
                ],
                  [
                  'attribute' => 'brand',
                  'value' => common\config\Options::getBrandNameById($model->brand)
                ],
                  [
                  'attribute' => 'color',
                  'value' => common\config\Options::getColorById($model->color)
                ],
                'size',
                'typeform_url:url',
                'typeform_code',
              ],
            ])
            ?>
        </div>
    </div>
