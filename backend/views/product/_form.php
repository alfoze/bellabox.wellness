<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Colors;
use common\models\Brand;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id' => 'product-form']]); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

            <?=
            $form->field($model, 'color')->dropDownList(
                ArrayHelper::map(colors::find()->all(), 'id', 'name'), ['prompt' => 'Select Color',
            ]);
            ?>

            <?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'typeform_url')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'typeform_code')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'org_price')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList(common\config\Options::getStatus()) ?>

            <?=
            $form->field($model, 'brand')->dropDownList( ArrayHelper::map(brand::find()->all(), 'id', 'name'), ['prompt' => 'Select Brand' ]);?>
            
            
            <?= $form->field($model, 'type')->dropDownList(common\config\Options::getProductTypes(), ['prompt' => 'Select Type']) ?>
            <?= $form->field($model, 'image')->fileInput(['maxlength' => true]) ?>

        </div>
        <div class="form-group col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
