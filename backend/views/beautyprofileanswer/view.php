<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Beautyprofileanswer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => common\config\Options::getOptionPName($model->PID), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beautyprofileanswer-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'question_id',
            'name',
            'image',
            'description',
        ],
    ]) ?>

</div>
