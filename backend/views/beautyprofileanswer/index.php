<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BeautyprofileanswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = common\config\Options::getOptionPName($searchModel->PID);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beautyprofileanswer-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create '.common\config\Options::getOptionName($searchModel->PID), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'question_id',
            'name',
            'image',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
