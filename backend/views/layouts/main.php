<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\assets\AdminLTEAsset;
use frontend\widgets\Alert;
use common\config\View2;
use yii\widgets\Pjax;
use common\config\Options;
use common\config\Controller2;

/* @var $this yii\web\View */
/* @var $content string */

AdminLTEAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(\Yii::$app->params['name'] . ' - ' . $this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition sidebar-mini skin-purple-light ">
        <style>
            .kv-container-from{
                padding: 0px;
                border: 0px #ffffff none;
            }
            .kv-container-to{
                padding: 0px;
                border: 0px #ffffff none;
            }
        </style>
        <?php $this->beginBody() ?>
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b><?= \Yii::$app->params['name']; ?></b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><?= \Yii::$app->params['name']; ?></b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <!--<div class="pull-left">
                            <h4 style="color: white; font-size: 20px"><b><?= Yii::$app->params['name'] ?></b></h4>
                        </div>-->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <?php if (!Yii::$app->user->isGuest) { ?>
                              <li class="dropdown user user-menu">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                      <?php
                                      $user = \common\models\User::findOne(Yii::$app->user->id);
                                      $image = "images/user.png";
                                      $name = \Yii::$app->user->identity->username;
                                      ?>
                                      <span class="hidden-xs"><?= $name; ?></span>
                                  </a>
                                  <ul class="dropdown-menu">
                                      <!-- Menu Footer-->
                                      <li class="user-footer">
                                          <div class="pull-left">
                                              <?=
                                              $url = Html::a('Reset Password', ['/site/resetPassword'], ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']);
                                              ?>
                                          </div>
                                          <div class="pull-right">
                                              <?=
                                              $url = Html::a('Sign out', ['/site/logout'], ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']);
                                              ?>
                                          </div>
                                      </li>
                                  </ul>
                              </li>
                            <?php } ?>
                            <!-- Control Sidebar Toggle Button -->
                        </ul>
                    </div>
                </nav>
            </header>

            <?php
            $sideBar = [
                [
                'id' => 0, 'url' => '', 'class' => "header", 'name' => 'MAIN NAVIGATION', 'visible' => true
              ],
                [
                'id' => 100, 'url' => \Yii::$app->urlManager->createUrl("/site/index"), 'class' => "fa fa-dashboard", 'name' => 'Dashboard', 'visible' => true
              ],
                [
                'id' => 200, 'url' => '#', 'class' => "fa fa-fw fa-gears", 'name' => Yii::t('app', 'Manage Operations'), 'visible' => true, 'SubMenu' => [
                    ['id' => 201, 'url' => \Yii::$app->urlManager->createUrl("/chargebee/getshipmentlist"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::GENERATE_ALLOCATION), 'visible' => true],
                    ['id' => 202, 'url' => \Yii::$app->urlManager->createUrl("/chargebee/setallocationlist"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::UPDATE_SHIPMENT), 'visible' => true],
                  ['id' => 203, 'url' => \Yii::$app->urlManager->createUrl("/chargebee/updateaddressescb"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::UPDATE_ADDRESS_CB), 'visible' => true],
                ]
              ],
                [
                'id' => 600, 'url' => '#', 'class' => "fa fa-fw fa-gears", 'name' => Yii::t('app', 'Manage Contents'), 'visible' => true, 'SubMenu' => [
                    ['id' => 601, 'url' => \Yii::$app->urlManager->createUrl("/pages/index"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::PAGE_CONTENTS), 'visible' => true],
                    ['id' => 602, 'url' => \Yii::$app->urlManager->createUrl("/landingpages/index"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::PAGE_LAYOUT), 'visible' => true],
                    ['id' => 603, 'url' => \Yii::$app->urlManager->createUrl("/box/index"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::BOXS), 'visible' => true],
                    ['id' => 604, 'url' => \Yii::$app->urlManager->createUrl("/product/index"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::PRODUCTS), 'visible' => true],
                    ['id' => 605, 'url' => \Yii::$app->urlManager->createUrl("/beautyprofilepage/index"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::BEAUTYPROFILE_PAGE), 'visible' => true],
                    ['id' => 606, 'url' => \Yii::$app->urlManager->createUrl("/beautyprofilequestion/index"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::BEAUTYPROFILE_QUESTION), 'visible' => true],
                    ['id' => 607, 'url' => \Yii::$app->urlManager->createUrl("/contents/index"), 'class' => "glyphicon glyphicon-list", 'name' => 'Upload Contents', 'visible' => true],
                ]
              ],
                [
                'id' => 700, 'url' => '#', 'class' => "fa fa-fw fa-gears", 'name' => Yii::t('app', 'Settings'), 'visible' => true, 'SubMenu' => [
                    ['id' => 701, 'url' => \Yii::$app->urlManager->createUrl(["/settings/view", 'id' => '1']), 'class' => "fa fa-fw fa-gears", 'name' => 'General Settings', 'visible' => true],
                    ['id' => 702, 'url' => \Yii::$app->urlManager->createUrl("/user/index"), 'class' => "glyphicon glyphicon-list", 'name' => Yii::t('app', 'All Users'), 'visible' => true],
                    ['id' => 706, 'url' => \Yii::$app->urlManager->createUrl("/user/index2"), 'class' => "glyphicon glyphicon-list", 'name' => Yii::t('app', 'Subscription Users'), 'visible' => true],
                    ['id' => 703, 'url' => \Yii::$app->urlManager->createUrl("/faq/index"), 'class' => "glyphicon glyphicon-list", 'name' => Yii::t('app', 'FAQ'), 'visible' => true],
                    ['id' => 704, 'url' => \Yii::$app->urlManager->createUrl("/plans/index"), 'class' => "glyphicon glyphicon-list", 'name' => Yii::t('app', 'Plans'), 'visible' => true],
                    ['id' => 706, 'url' => \Yii::$app->urlManager->createUrl("/optin/index"), 'class' => "glyphicon glyphicon-list", 'name' => Yii::t('app', 'Optin'), 'visible' => true],
                    ['id' => 705, 'url' => \Yii::$app->urlManager->createUrl("/newsletteremails/index"), 'class' => "glyphicon glyphicon-list", 'name' => Options::getOptionPName(Controller2::NEWSLETTER_EMAILS), 'visible' => true],
                ]
              ],
            ];
            ?>
            <?php $isAdmin = false; ?>
            <?php
            if (!Yii::$app->user->isGuest) {
              $user = \common\models\User::findOne(Yii::$app->user->id);
              $isAdmin = $user->is_admin == "1" ? true : false;
              ?>

              <?php if ($isAdmin) { ?>
                <!-- ---------------  Side menu  --------------------->
                <aside class="main-sidebar">
                    <section class="sidebar">
                        <ul class="sidebar-menu">
                            <?php
                            foreach ($sideBar as $value) {
                              $active = '';
                              if (isset($this->context->mainMenu)) {
                                if ($this->context->mainMenu == $value['id']) {
                                  $active = ' active ';
                                }
                              }
                              if ($value['visible'] == true) {
                                if ($value['url'] == '') {
                                  echo '<li class="' . $value['class'] . '">' . $value['name'] . '</li>';
                                }
                                else {
                                  $mainClass = "";
                                  if ($value['url'] == '#') {
                                    $mainClass = 'treeview';
                                  }

                                  echo"<li class='$mainClass $active'>";
                                  echo '<a href="' . $value['url'] . '">';
                                  echo '<i class="' . $value['class'] . '"></i><span>' . $value['name'] . "</span>";
                                  if ($value['url'] == '#') {
                                    echo '<i class="fa fa-angle-left pull-right"></i>';
                                  }
                                  echo '</a>';

                                  if ($value['url'] == '#') {
                                    $subMenu = $value['SubMenu'];
                                    if (count($subMenu) > 0) {
                                      echo '<ul class="treeview-menu">';

                                      foreach ($subMenu as $value2) {
                                        $subActive = '';
                                        if (isset($this->context->submenu)) {
                                          if ($this->context->submenu == $value2['id']) {
                                            $subActive = ' active ';
                                          }
                                        }
                                        if ($value2['visible'] == true) {
                                          echo"<li class='$subActive'>";
                                          echo '<a href="' . $value2['url'] . '">';
                                          echo '<i class="' . $value2['class'] . '"></i><span>' . $value2['name'] . "</span>";
                                          echo '</a>';
                                          echo '</li>';
                                        }
                                      }
                                      echo '</ul>';
                                    }
                                  }
                                  echo'</li >';
                                }
                              }
                            }
                            ?>
                        </ul>
                    </section>
                    <!-- /.sidebar -->
                </aside>
              <?php } ?>
            <?php } ?>
            <?php if ($isAdmin || Yii::$app->user->isGuest) { ?>
              <div class="content-wrapper">
                  <!-- Content Header (Page header) -->
                  <section class="content-header">
                      <h2><?= $this->title; ?></h2>
                      <?=
                      Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                      ])
                      ?>
                  </section>

                  <?php if (Yii::$app->session->getFlash('s_mesage')) { ?>
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?= Yii::$app->session->getFlash('s_mesage'); ?>
                    </div>
                  <?php } ?>
                  <?php if (Yii::$app->session->getFlash('s_error')) { ?>
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?= Yii::$app->session->getFlash('s_error'); ?>
                    </div>
                  <?php } ?>
                  <?= Alert::widget() ?>
                  <section class="content">
                      <?php //Pjax::begin(['timeout' => 10000]);     ?>
                      <?php ?>
                      <?php
                      //echo Html::a("Refresh", Yii::$app->urlManager->createUrl(array_merge([Yii::$app->requestedRoute])) , ['class' => 'btn btn-sm btn-primary pull-right', 'id' => 'refreshButton']) 
                      ?>

                      <?= $content ?>

                      <?php //Pjax::end();   ?>

                  </section>
              </div>
            <?php
            }
            else {
              echo '<div class="content-wrapper">
                                <!-- Content Header (Page header) -->
                                <section class="content-header">
                                <h2>You are not authorized for this action.</h2>
                                </section>
                                </div>';
            }
            ?>
        </div>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <div class="tab-content">
                <div class="tab-pane" id="control-sidebar-home-tab">
                </div>
            </div>
        </aside>

        <div class="control-sidebar-bg"></div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; <?= Yii::$app->params['name'] ?> <?= date('Y') ?></p>

            </div>
        </footer>

<?php $this->endBody() ?>
    </body>
    <script>
      $(function () {
          tinymce.init({
              //selector: 'textarea',
              mode: "textareas",
              editor_selector: "mceEditor",
              editor_deselector: "mceNoEditor",
              height: 500,
              plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table contextmenu paste code'
              ],
              toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
              content_css: 'js/tinymce/skins/lightgray/codepen.min.css'});
      });
    </script>

</html>
<?php $this->endPage() ?>
