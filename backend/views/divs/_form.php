<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Pages;
/* @var $this yii\web\View */
/* @var $model backend\models\Divs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="divs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'page_id')->dropDownList(
        ArrayHelper::map(Pages::find()->all(), 'id', 'name'),
             ['prompt'=>'Select Page',              
            ]); ?> 

    <?= $form->field($model, 'landingpage_id')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'Inactive' => 'Inactive', 'Active' => 'Active', ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
