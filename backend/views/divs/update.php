<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Divs */

$this->title = 'Update Divs: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Divs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="divs-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
