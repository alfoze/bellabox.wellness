<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Divs */

$this->title = 'Create Divs';
$this->params['breadcrumbs'][] = ['label' => 'Divs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="divs-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
