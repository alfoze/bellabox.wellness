<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Landingpages */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => common\config\Options::getOptionPName($model->PID), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landingpages-view">


    <p>
        <?= Html::a('List', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
          ],
        ])
        ?>
    </p>
    
    <?=
    DetailView::widget([
      'model' => $model,
      'attributes' => [
        'id',
        'name',
        'header',
        'created_on',
        'createdby',
        'updated_on',
        'status',
          [
          'attribute' => 'add_standalonecss',
          'value' => $model->add_standalonecss == '1' ? 'Yes' : 'No'
        ]
      ],
    ])
    ?>

</div>
