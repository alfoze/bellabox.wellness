<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Pages;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\Landingpages */
/* @var $form yii\widgets\ActiveForm */
$url = \Yii::$app->urlManager->createUrl(["/settings/update", 'id' => '1'])
?>

<div class="landingpages-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'status')->dropDownList(\common\config\Options::getStatus2()) ?>
        </div> 
        <div class="col-sm-6">
            <?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'add_standalonecss')->checkbox() ?>
            <?= \kartik\helpers\Html::a('Update BellaBox.au Stlye CSS', $url) ?>
        </div>   
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i>Page Sections</h4></div>
        <div class="panel-body">
            <?php
            DynamicFormWidget::begin([
              'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
              'widgetBody' => '.container-items', // required: css class selector
              'widgetItem' => '.item', // required: css class
              'limit' => 20, // the maximum times, an element can be cloned (default 999)
              'min' => 1, // 0 or 1 (default 1)
              'insertButton' => '.add-item', // css class
              'deleteButton' => '.remove-item', // css class
              'model' => $modelsDiv[0],
              'formId' => 'dynamic-form',
              'formFields' => [
                'name',
                'weight',
                'page_id',
                'status',
              ],
            ]);
            ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($modelsDiv as $i => $modelDiv): ?>
                  <div class="item panel panel-default"><!-- widgetBody -->
                      <div class="panel-heading">
                          <h3 class="panel-title pull-left">Page Section</h3>
                          <div class="pull-right">
                              <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                              <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                      <div class="panel-body">
                          <?php
                          // necessary for update action.
                          if (!$modelDiv->isNewRecord) {
                            echo Html::activeHiddenInput($modelDiv, "[{$i}]id");
                          }
                          ?>

                          <div class="row">
                              <div class="col-sm-3">
                                  <?= $form->field($modelDiv, "[{$i}]name")->textInput(['maxlength' => true]) ?>
                              </div>
                              <div class="col-sm-3">
                                  <?= $form->field($modelDiv, "[{$i}]weight")->textInput(['maxlength' => true]) ?>
                              </div>
                              <div class="col-sm-3">
                                  <?=
                                  $form->field($modelDiv, "[{$i}]page_id")->dropDownList(
                                      ArrayHelper::map(Pages::find()->all(), 'id', 'name'), ['prompt' => 'Select Page',
                                  ]);
                                  ?> 
                              </div>
                              <div class="col-sm-3">
                                  <?= $form->field($modelDiv, "[{$i}]status")->dropDownList(\common\config\Options::getStatus2()) ?>
                              </div>

                          </div><!-- .row -->                      </div>
                  </div>
                <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>