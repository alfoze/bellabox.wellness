<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LandingpagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = common\config\Options::getOptionPName($searchModel->PID);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landingpages-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create '.common\config\Options::getOptionName($searchModel->PID), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
        //'id',
        'name',
        'header',
        'created_on',
        'url:url',
        'status',
          ['class' => 'yii\grid\ActionColumn'],
      ],
    ]);
    ?>
</div>
