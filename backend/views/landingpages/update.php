<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Landingpages */

$this->title = 'Update ' . common\config\Options::getOptionName($model->PID).': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => common\config\Options::getOptionPName($model->PID),'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="landingpages-update">


    <?= $this->render('_form', [
        'model' => $model,
        'modelsDiv' => $modelsDiv,
    ]) ?>

</div>
