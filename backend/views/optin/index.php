<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OptinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Optins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="optin-index">

    <?=
    GridView::widget([
      'dataProvider' => $dataProvider,
      //'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
        'id',
        'text_to_show',
          [
          'attribute' => 'active',
          'value' => 'activated'
        ],
        'type_form_code',
        'text_chosen',
          ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
      ],
    ]);
    ?>
</div>
