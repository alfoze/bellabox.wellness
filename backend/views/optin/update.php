<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Optin */

$this->title = 'Update Optin: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Optins', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="optin-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
