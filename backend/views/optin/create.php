<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Optin */

$this->title = 'Create Optin';
$this->params['breadcrumbs'][] = ['label' => 'Optins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="optin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
