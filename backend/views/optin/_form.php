<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Optin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="optin-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col col-md-6">
            <?= $form->field($model, 'text_to_show')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col col-md-6">
            <?= $form->field($model, 'active')->dropDownList(\common\config\Options::getStatus()) ?>
        </div>
    </div>
    <div class="row">
        <div class="col col-md-6">
            <?= $form->field($model, 'type_form_code')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col col-md-6">
            <?= $form->field($model, 'text_chosen')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
