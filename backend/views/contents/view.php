<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Contents */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'List of Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contents-view">
    <p>
        <?= Html::a('List of Content', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update Uploaded Content', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
          ],
        ])
        ?>
    </p>
    <div class="row">
        <div class="col-sm-4">
            <img src ="<?= common\config\Options::getFrontendAddress() . $model->oldImage ?>" altr="No Image" />
        </div>
        <div class="col-sm-6">
            <?=
            DetailView::widget([
              'model' => $model,
              'attributes' => [
                'name',
                'path',
                'url:url',
              ],
            ])
            ?>

        </div>
    </div>
