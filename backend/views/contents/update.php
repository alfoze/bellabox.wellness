<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Contents */

$this->title = 'Update Uploaded Content';
$this->params['breadcrumbs'][] = ['label' => 'List of Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update Uploaded Content';
?>
<div class="contents-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
