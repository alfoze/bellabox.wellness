<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List of Contents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contents-index">

    <p>
        <?= Html::a('Upload Content', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          [
          'attribute' => 'path',
          'value' => function($model) {
            return Html::img($model->url, ['width' => '100']);
          },
          'format' => 'html',
          'filter' => false
        ],
        'name',
        'path',
        'url:url',
          ['class' => 'yii\grid\ActionColumn'],
      ],
    ]);
    ?>
</div>
