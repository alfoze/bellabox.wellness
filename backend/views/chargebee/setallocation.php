<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model frontend\models\Trip */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Update Shipment Information';
?>
<div class="trip-create">
    <div class="margin-bottom-20"></div>
    <p class="margin-bottom-30"> You can download the CSV template for the uploading purpose from <a href="<?= common\config\Options::getFrontendAddress()."template/allocation.csv"?>"  >here</>
            
    <div class="trip-form">

        <?php
        $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'options' => ['enctype' => 'multipart/form-data']]);
        ?>
        <?=
        Form::widget([
          'model' => $model,
          'form' => $form,
          'columns' => 1,
          'attributes' => [
            'file' => [
              'type' => Form::INPUT_FILE,
            ]
          ]
            ]
        );
        ?>
         <?= $form->field($model, 'for_shop')->checkbox() ?>
        <div class="form-group">
            <?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>