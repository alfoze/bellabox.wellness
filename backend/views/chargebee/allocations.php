<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Divs */
/* @var $form yii\widgets\ActiveForm */
$boxTypes = [['id' => 'Women'], ['id' => 'Men'], ['id' => 'Baby']];
$this->title = "Generate Customer List";
?>

<div class="allocation-form">

    <?php $form = ActiveForm::begin(); ?>

    <label class="control-label" for="allocationselection-dateRange"><?= $model->getAttributeLabel('dateRange'); ?></label>

    <?=
    DateRangePicker::widget([
      'model' => $model,
      'attribute' => 'dateRange',
      'convertFormat' => true,
      'pluginOptions' => [
        'timePicker' => true,
        'timePickerIncrement' => 30,
        'locale' => [
          'format' => 'Y-m-d H:i'
        ]
      ]
    ]);
    ?>
    <div class="help-block"></div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'boxType')->dropDownList(ArrayHelper::map($boxTypes, 'id', 'id')); ?> 
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'batchId')->textInput() ?>
        </div>
        <div class="col-md-4">
            <br>
            <?= $form->field($model, 'new')->checkbox() ?>
            <?= $form->field($model, 'processed')->checkbox() ?>
            <?= $form->field($model, 'completed')->checkbox() ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton('Export', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
