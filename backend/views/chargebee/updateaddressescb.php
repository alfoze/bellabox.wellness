<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Trip */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Update Customer Shipment Addresses on ChargeBee';
?>
<div class="row">
    <div class="col col-md-6 col-sm-6">
        <div class="trip-create">
            <div class="margin-bottom-20"></div>
            <div class="trip-form">        
                <?php
                $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'options' => ['enctype' => 'multipart/form-data']]);
                ?>

                <div class="row">
                    <div class="col col-md-6 col-sm-6">
                        <label class="control-label" ><?= $model->getAttributeLabel('listdate'); ?></label>
                        <?=
                        DatePicker::widget([
                          'model' => $model,
                          'attribute' => 'listdate',
                          'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                          'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                          ]
                        ]);
                        ?>
                    </div>
                </div>
                <?=
                Form::widget([
                  'model' => $model,
                  'form' => $form,
                  'columns' => 1,
                  'attributes' => [
                    'file' => [
                      'type' => Form::INPUT_FILE,
                    ]
                  ]
                    ]
                );
                ?>
                <div class="form-group">
                    <?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <p class="margin-bottom-30"> You can download the CSV template for the uploading purpose from <a href="<?= common\config\Options::getFrontendAddress() . "template/addresses.csv" ?>"  >here</p>
    </div>
    <div class="col col-md-6 col-sm-6">
        <div style="height: 500px;overflow: scroll;">
            <?= $errors ?>
        </div>
    </div>
    
</div>