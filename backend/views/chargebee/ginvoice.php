<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Divs */
/* @var $form yii\widgets\ActiveForm */
$boxTypes = [['id' => 'Women'], ['id' => 'Men'], ['id' => 'Baby']];
$this->title = "Generate Automated Invoice";
?>

<div class="allocation-form">
    <p>This option is only to create automated invoices in chargebee-test account not on live site</p>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'fromdate')->widget(\kartik\date\DatePicker::classname(), [
              'options' => ['placeholder' => 'Enter Start'],
              'pluginOptions' => [
                'autoclose' => true
              ]
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'noofinvoices')->textInput(['type' => 'number']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'timedifference')->textInput(['type' => 'number']) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Generate', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
