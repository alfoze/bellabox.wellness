<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Beautyprofilequestion */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', common\config\Options::getOptionPName($model->PID)), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beautyprofilequestion-view">


    <p>
        <?= Html::a('List', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
          ],
        ])
        ?>
    </p>

    <div class="row">
        <div class="col-md-6">
            <?=
            DetailView::widget([
              'model' => $model,
              'attributes' => [
                  [
                  'attribute' => 'page_id',
                  'value' => common\config\Options::getBeautyPageById($model->page_id)
                ],
                'name',
                'weight',
              ],
            ])
            ?>      
        </div>
        <div class="col-md-6">
            <?=
            DetailView::widget([
              'model' => $model,
              'attributes' => [
                  [
                  'attribute' => 'active',
                  'value' => common\config\Options::getStatusId($model->active)
                ],
                  [
                  'attribute' => 'type',
                  'value' => common\config\Options::getFieldTypeByID($model->type)
                  ],
                  [
                    'attribute' => 'required',
                    'value' => common\config\Options::getQTypeByID($model->required)
                  ]
              ],
            ])
            ?>      
        </div>

    </div>
    <h3>Answer Options</h3>
    <?=
    \yii\grid\GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => false,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          [
          'attribute' => 'image',
          'value' => function($model) {
            return Html::img(common\config\Options::getFrontendAddress() . $model->image, ['width' => '100']);
          },
          'format' => 'html',
          'filter' => false
        ],
        'name',
        'description'
      ],
    ]);
    ?>

</div>
