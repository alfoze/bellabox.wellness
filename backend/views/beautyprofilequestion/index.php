<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BeautyprofilequestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', common\config\Options::getOptionPName($searchModel->PID));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beautyprofilequestion-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create ' . common\config\Options::getOptionName($searchModel->PID)), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          [
          'attribute' => 'page_id',
          'value' => function($model, $index, $dataColumn) {
            return common\config\Options::getBeautyPageById($model->page_id);
          },
          'filter' => Html::activeDropDownList($searchModel, 'page_id', common\config\Options::getBeautyPages(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Page')]),
        ],
        'name',
        'weight',
          [
          'attribute' => 'active',
          'value' => function($model, $index, $dataColumn) {
            return common\config\Options::getStatusId($model->active);
          },
          'filter' => Html::activeDropDownList($searchModel, 'active', common\config\Options::getStatus(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Status')]),
        ],
          [
          'attribute' => 'type',
          'value' => function($model, $index, $dataColumn) {
            return common\config\Options::getFieldTypeByID($model->type);
          },
          'filter' => Html::activeDropDownList($searchModel, 'type', common\config\Options::getFieldTypes(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Type')]),
        ],
        [
          'attribute' => 'required',
          'value' => function($model, $index, $dataColumn) {
            return common\config\Options::getQTypeByID($model->required);
          },
          'filter' => Html::activeDropDownList($searchModel, 'required', common\config\Options::getQType(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Question Type')]),
        ],
          ['class' => 'yii\grid\ActionColumn'],
      ],
    ]);
    ?>

</div>
