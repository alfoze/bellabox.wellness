<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Beautyprofilepage;
use common\config\Controller2;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Beautyprofilequestion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="beautyprofilequestion-form">

    <?php
    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id' => 'dynamic-form']]);
    ?>

    <div class="row">
        <div class="col-md-6">
            <?=
            $form->field($model, 'page_id')->dropDownList(
                common\config\Options::getBeautyPages(), ['prompt' => 'Select ' . common\config\Options::getOptionPName(Controller2::BEAUTYPROFILE_PAGE),
            ]);
            ?> 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'weight')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'active')->dropDownList(common\config\Options::getStatus()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'type')->dropDownList(common\config\Options::getFieldTypes()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'required')->dropDownList(common\config\Options::getQType()) ?>
        </div>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-book"></i>Answers Options</h4></div>
        <div class="panel-body">
            <?php
            DynamicFormWidget::begin([
              'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
              'widgetBody' => '.container-items', // required: css class selector
              'widgetItem' => '.item', // required: css class
              'limit' => 20, // the maximum times, an element can be cloned (default 999)
              'min' => 1, // 0 or 1 (default 1)
              'insertButton' => '.add-item', // css class
              'deleteButton' => '.remove-item', // css class
              'model' => $modelsDiv[0],
              'formId' => 'dynamic-form',
              'formFields' => [
                'name',
                'description'
              ],
            ]);
            ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($modelsDiv as $i => $modelDiv): ?>
                  <div class="item panel panel-default"><!-- widgetBody -->
                      <div class="panel-heading">
                          <h3 class="panel-title pull-left">Answer Option</h3>
                          <div class="pull-right">
                              <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                              <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                      <div class="panel-body">
                          <?php
                          // necessary for update action.
                          if (!$modelDiv->isNewRecord) {
                            echo Html::activeHiddenInput($modelDiv, "[{$i}]id");
                            echo Html::activeHiddenInput($modelDiv, "[{$i}]oldImage"); 
                          }
                          ?>

                          <div class="row">
                              <div class="col-sm-3">
                                  <?= $form->field($modelDiv, "[{$i}]name")->textInput(['maxlength' => true]) ?>
                              </div>
                              <div class="col-sm-5">
                                  <?= $form->field($modelDiv, "[{$i}]description")->textInput(['maxlength' => true]) ?>
                              </div>
                              <div class="col-sm-2">
                                  <img src="<?= common\config\Options::getFrontendAddress(). $modelDiv->image?>" alt="No image selected previously" style="width:80px"/>
                              </div>
                              <div class="col-sm-2">
                                  <?= $form->field($modelDiv, "[{$i}]image")->fileInput(['maxlength' => true]) ?>
                              </div>
                          </div><!-- .row -->
                      </div>
                  </div>
                <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
