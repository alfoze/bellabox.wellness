<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Beautyprofilequestion */

$this->title = Yii::t('app', 'Create '.common\config\Options::getOptionName($model->PID));
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', common\config\Options::getOptionPName($model->PID)), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beautyprofilequestion-create">


    <?= $this->render('_form', [
        'model' => $model,
      'modelsDiv' => $modelsDiv,
    ]) ?>

</div>
