<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Messages */
/* @var $form yii\widgets\ActiveForm */
if (isset($reply)){
    $label = Yii::t('app', 'Reply Back');
    $blable = Yii::t('app', 'Reply');
}
else {
    $label = $model->getAttributeLabel('message');
    $blable = Yii::t('app', 'Send');
}
?>

<div class="messages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'from_user')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'to_user')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'message')->textarea()->label($label) ?>
    <?= $form->field($model, 'parent_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'option_type')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'option_id')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($blable, ['class' =>  'btn-u btn-u-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
