<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Contactus */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app','Contact Us'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contact Us'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="contactus-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
