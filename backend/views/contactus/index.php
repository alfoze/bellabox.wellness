<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContactusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contact Us');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactus-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <!-- <p>
        <?php //Html::a(Yii::t('app', 'Create Contactus'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'email:email',
            'subject',
            'message:ntext',
            // 'c_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
