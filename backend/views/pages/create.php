<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Pages */

$this->title = 'Create '.common\config\Options::getOptionName($model->PID);
$this->params['breadcrumbs'][] = ['label' => common\config\Options::getOptionPName($model->PID), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
