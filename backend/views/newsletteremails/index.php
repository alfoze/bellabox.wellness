<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsletteremailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = common\config\Options::getOptionPName($searchModel->PID);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletteremails-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          'name',
          'last_name',
        'email:email',
        'ctimestamp',
          ['class' => 'yii\grid\ActionColumn',
          'header' => Html::a(Yii::t('app', 'Export'), $_SERVER['REQUEST_URI'] . '&ExportCSV=true', ['class' => 'btn btn-success btn-sm']),
        ],
      ],
    ]);
    ?>
</div>
