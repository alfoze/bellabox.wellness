<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Newsletteremails */

$this->title = 'Update ' . common\config\Options::getOptionName($model->PID).': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => common\config\Options::getOptionPName($model->PID),'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="newsletteremails-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
