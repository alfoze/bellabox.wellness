<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Beautyprofilepage */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => common\config\Options::getOptionName($model->PID),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', common\config\Options::getOptionPName($model->PID)), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="beautyprofilepage-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
