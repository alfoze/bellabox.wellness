<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Beautyprofilepage */

$this->title = Yii::t('app', 'Create '.common\config\Options::getOptionName($model->PID));
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', common\config\Options::getOptionPName($model->PID)), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beautyprofilepage-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
