<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BeautyprofilepageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', common\config\Options::getOptionPName($searchModel->PID));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beautyprofilepage-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create ' . common\config\Options::getOptionName($searchModel->PID)), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
        'name',
        'weight',
          [
          'attribute' => 'active',
          'value' => function($model, $index, $dataColumn) {
            return common\config\Options::getStatusId($model->active);
          },
          'filter' => Html::activeDropDownList($searchModel, 'active', common\config\Options::getStatus(), ['class' => 'form-control', 'prompt' => Yii::t('app', 'Select Status')]),
        ],
          ['class' => 'yii\grid\ActionColumn'],
      ],
    ]);
    ?>

</div>
