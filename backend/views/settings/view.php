<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = ['label' => 'Settings'];
?>
<div class="settings-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <div class="row">
        <div class="col col-md-6">
            <br>
            <?=
            DetailView::widget([
              'model' => $model,
              'attributes' => [
                'name',
                'cb_name',
                'cb_api',
                'billing_day',
                  [
                  'attribute' => 'header_id',
                  'value' => common\config\Options::getPageContentById($model->header_id)
                ],
                  [
                  'attribute' => 'footer_id',
                  'value' => common\config\Options::getPageContentById($model->footer_id)
                ],
                  [
                  'attribute' => 'homepage',
                  'value' => common\config\Options::getPageLayoutById($model->homepage)
                ],
                  [
                  'attribute' => 'giftpage',
                  'value' => common\config\Options::getPageLayoutById($model->giftpage)
                ],
              ],
            ])
            ?>

        </div>    
        <div class="col col-md-6">
            <strong>BelleBox.au Style CSS</strong>
            <br>
            <textarea rows="18" readonly="" style="width: 100%">
                <?= $model->standalone_css ?>
            </textarea>           
        </div>
    </div>


</div>
