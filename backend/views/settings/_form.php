<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col col-md-3">
            <?= $form->field($model, 'cb_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col col-md-3">
            <?= $form->field($model, 'cb_api')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col col-md-3">
            <?= $form->field($model, 'billing_day')->textInput(['maxlength' => true, 'type' => 'number', 'min' => 1, 'max' => 31]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col col-md-3">
            <?= $form->field($model, "header_id")->dropDownList(\yii\helpers\ArrayHelper::map(\backend\models\Pages::find()->all(), 'id', 'name'), ['prompt' => 'Select Page Contents',]); ?>
        </div>
        <div class="col col-md-3">
            <?= $form->field($model, "footer_id")->dropDownList(\yii\helpers\ArrayHelper::map(\backend\models\Pages::find()->all(), 'id', 'name'), ['prompt' => 'Select Page Contents',]); ?>
        </div>
        <div class="col col-md-3">
            <?= $form->field($model, "homepage")->dropDownList(\yii\helpers\ArrayHelper::map(\backend\models\Landingpages::find()->all(), 'id', 'name'), ['prompt' => 'Select Page',]); ?>
        </div>
        <div class="col col-md-3">
            <?= $form->field($model, "giftpage")->dropDownList(\yii\helpers\ArrayHelper::map(\backend\models\Landingpages::find()->all(), 'id', 'name'), ['prompt' => 'Select Page',]); ?>
        </div>
    </div>
    <?= $form->field($model, 'standalone_css')->textarea(['maxlength' => true, 'rows' => 5]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
