<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Sourcemessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sourcemessage-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'message')->textarea(['rows' => 2]) ?>
    <?php
    foreach ($languages as $language) {
        ?>
        <?= $form->field($model, "tmessage[$language->id]")->textarea(['rows' => 2])->label($language->name . " Message") ?>
    <?php } ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
