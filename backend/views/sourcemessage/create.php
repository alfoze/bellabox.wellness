<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Sourcemessage */

$this->title = Yii::t('app', 'Create Translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Translation'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sourcemessage-create">

    

    <?= $this->render('_form', [
        'model' => $model,'languages'=>$languages
    ]) ?>

</div>
