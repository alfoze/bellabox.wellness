<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SourcemessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Translation');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sourcemessage-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Translation'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Export') , ['export'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'message:ntext',
            [
                'attribute' => 'tmessage',
                'format'=>'html',
                'value' => function($model, $index, $dataColumn) {
                    $decs = '';
                    $tmodel = \common\models\Message::findAll($model->id);
                    foreach ($tmodel as $value) {
                        $decs .= "<b>" . $value->language . "</b> : " . $value->translation . "<br>";
                    }
                    return $decs;
                },
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',],
        ],
    ]);
    ?>

</div>
