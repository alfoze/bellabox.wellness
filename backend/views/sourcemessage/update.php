<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sourcemessage */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Translation',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Translation'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sourcemessage-update">

    

    <?= $this->render('_form', [
        'model' => $model,'languages'=>$languages
    ]) ?>

</div>
