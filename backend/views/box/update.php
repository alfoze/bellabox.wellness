<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Box */

$this->title = 'Update ' . common\config\Options::getOptionName($model->PID).': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => common\config\Options::getOptionPName($model->PID), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
