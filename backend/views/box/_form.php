<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Box */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id' => 'box-form']]); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'variant_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type')->dropDownList(common\config\Options::getBoxTypes(), ['prompt' => 'Select Box Type']) ?>

            <?= $form->field($model, 'month')->dropDownList(common\config\Options::getMonthNames(), ['prompt' => 'Select Month']) ?>

            <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList(common\config\Options::getStatus()) ?>
        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'samples')->textarea(['maxlength' => true, 'rows'=>9]) ?>
            <?= $form->field($model, 'typeform_url')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'typeform_code')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'image')->fileInput(['maxlength' => true]) ?>
        </div>
        <div class="form-group col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
