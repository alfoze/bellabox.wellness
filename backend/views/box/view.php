<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Box */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => common\config\Options::getOptionPName($model->PID), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-view">


    <p>
        <?= Html::a('List', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
          ],
        ])
        ?>
    </p>
    <div class="row">
        <div class="col-md-4">
            <img  class="img-responsive" src ="<?= common\config\Options::getFrontendAddress(). $model->image?>" alt="<?= $model->name?>"/>
        </div>
        <div class="col-md-8">
            <?=
            DetailView::widget([
              'model' => $model,
              'attributes' => [
                'id',
                'name',
                'variant_name',
                  [
                  'attribute' => 'type',
                  'value' => common\config\Options::getBoxTypeByID($model->type)
                ],
                'year',
                  [
                  'attribute' => 'month',
                  'value' => common\config\Options::getMonthNameById($model->month)
                ],
                'samples',
                  [
                  'attribute' => 'status',
                  'value' => common\config\Options::getStatusId($model->status)
                ],
                'typeform_url:url',
                'typeform_code',
              ],
            ])
            ?>

        </div>

    </div>
</div>
