<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Box */

$this->title = 'Create ' . common\config\Options::getOptionName($model->PID);
$this->params['breadcrumbs'][] = ['label' => common\config\Options::getOptionPName($model->PID), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
