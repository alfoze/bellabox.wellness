<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BoxSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'variant_name') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'month') ?>

    <?php // echo $form->field($model, 'year') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'samples') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'typeform_url') ?>

    <?php // echo $form->field($model, 'typeform_code') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
