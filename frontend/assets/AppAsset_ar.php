<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset_ar extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $sourcePath = '@web/unify/';
    public $css = [
        'plugins/bootstrap/css/bootstrap-rtl.min.css',
        'css/css-rtl/headers/header-default-rtl.css',
        'css/css-rtl/footers/footer-v4-rtl.css',
        'plugins/animate.css',
        'plugins/line-icons/line-icons.css',
        'plugins/font-awesome/css/font-awesome.min.css',
        'plugins/flexslider/flexslider.css',
        'plugins/fancybox/source/jquery.fancybox.css',
        'plugins/revolution-slider/rs-plugin/css/settings.css',
        'plugins/owl-carousel2/assets/owl.carousel.css',
        "plugins/layer-slider/layerslider/css/layerslider.css",
        'plugins/sky-forms-pro/skyforms/css/sky-forms.css',
        'plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css',
        'css/css-rtl/pages/page_search-rtl.css',
        'css/shop.style.css',
        'css/css-rtl/style-rtl.css',
        'css/css-rtl/pages/shortcode_timeline1-rtl.css',
        'css/css-rtl/rtl.css',
        'css/css-rtl/theme-colors/blue.css',
        'css/css-rtl/custom-rtl.css'
        
    ];
    public $js = [
        'plugins/jquery/jquery-migrate.min.js',
        'js/owl.carousel.min.js',
        'plugins/flexslider/jquery.flexslider-min.js',
        'plugins/back-to-top.js',
        'plugins/smoothScroll.js',
        'plugins/layer-slider/layerslider/js/greensock.js',
        'plugins/layer-slider/layerslider/js/layerslider.transitions.js',
        'plugins/layer-slider/layerslider/js/layerslider.kreaturamedia.jquery.js',
        'js/plugins/layer-slider.js',
        "plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js",
        "plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js",
        'plugins/jquery.parallax.js',
        'plugins/counter/waypoints.min.js',
        'plugins/counter/jquery.counterup.min.js',
        'plugins/owl-carousel2/owl.carousel.min.js',
        'plugins/fancybox/source/jquery.fancybox.pack.js',
        'js/custom.js',
        'js/app.js',
        'js/plugins/fancy-box.js',
        "js/plugins/revolution-slider.js",
        'js/plugins/owl-carousel-rtl.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
