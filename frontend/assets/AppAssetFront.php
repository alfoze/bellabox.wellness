<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetFront extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $sourcePath = '@web/unify/';
    public $css = [
        'plugins/bootstrap/css/bootstrap.min.css',
        'css/headers/header-v3.min.css',
        'css/footers/footer-v4.min.css',
        'plugins/font-awesome/css/font-awesome.min.css',
        "https://fonts.googleapis.com/css?family=Playfair+Display",
        'css/style.css',
        'css/theme-colors/blue.min.css',
        'css/app.css',
        'css/custom.css?v3'
    ];
    public $js = [
        'plugins/jquery/jquery-migrate.min.js',
        'plugins/back-to-top.min.js',
        'https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js',
        'js/custom.js',
        'js/app.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
