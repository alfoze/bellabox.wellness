<?php

namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model {

  public $email;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        ['email', 'filter', 'filter' => 'trim'],
        ['email', 'required'],
        ['email', 'email'],
        ['email', 'exist',
        'targetClass' => '\common\models\User',
        'filter' => ['status' => User::STATUS_ACTIVE],
        'message' => \Yii::t('app', "We don't have an account with this email.")
      ],
    ];
  }

  public function attributeLabels() {
    return [
      'email' => \Yii::t('app', 'Email'),
    ];
  }

  /**
   * Sends an email with a link, for resetting the password.
   *
   * @return boolean whether the email was send
   */
  public function sendEmail() {
    /* @var $user User */
    $user = User::findOne([
          // 'status' => User::STATUS_ACTIVE,
          'email' => $this->email,
    ]);

    if ($user) {
      if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
        $user->generatePasswordResetToken();
      }
      if ($user->save()) {
        $to = $this->email;
        $link  = Yii::$app->urlManager->createAbsoluteUrl(['/site/reset-password', 'token'=>$user['password_reset_token']]);
        $subject = 'Replacement login information for '.$user->fullname.' at ' . Yii::$app->params['name'];
        $message = "<html>
            <head>
				<title>" . \Yii::t('app', "Password Reset") . '</title>
				</head>
				<body style = "font-size:16px;" >
				Dear '.$user->f_name.', <br><br> '.
        "Oops! Forgot your password? Don't worry - it happens. <br><br>
        To reset your password, just click this link: ".$link." <br><br>
        Yours sincerely,<br>
        The bellabox team
        </body>
				</html>";
        $from = Yii::$app->params['name']."<". \Yii::$app->params['supportEmail'].">";
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: ' . $from . "\r\n";
        mail($to, $subject, $message, $headers, "-f " . $from);
        return '1';
      }
    }

    return false;
  }

}
