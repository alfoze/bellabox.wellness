<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Cancelsub extends Model {

  public $subscriptionid;
  public $reason;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      // name, email, subject and body are required
        [['reason'], 'required'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'reason' => Yii::t('app', 'Why are you deciding to cancel?')
    ];
  }

}
