<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Changesub extends Model {

  public $fromsubscriptionid;
  public $fromsubscription;
  public $tosubscriptionid;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      // name, email, subject and body are required
        [['tosubscriptionid'], 'required'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'tosubscriptionid' => Yii::t('app', 'Plan you want to switch to?')
    ];
  }

}
