<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Giftreceiver extends Model {

  public $fname;
  public $lname;
  public $email;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      // name, email, subject and body are required
        [['fname', 'lname', 'email'], 'required'],
      [['fname','lname'], 'string', 'max' => 150],
      [['email'], 'string', 'max' => 70],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'fname' => Yii::t('app', 'First Name'),
      'lname' => Yii::t('app', 'Last Name'),
      'email' => Yii::t('app', 'Email'),
    ];
  }

}
