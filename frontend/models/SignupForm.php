<?php

namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model {

  public $username;
  public $email;
  public $password;
  public $f_name;
  public $l_name;
  public $address;
  public $city;
  public $country;
  public $pic;
  public $sp;
  public $phone;
  public $mobile;
  public $tmppass;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        ['username', 'filter', 'filter' => 'trim'],
        ['username', 'required'],
        ['f_name', 'required'],
        ['l_name', 'required'],
        ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This username has already been taken.')],
        ['username', 'string', 'min' => 2, 'max' => 255],
        ['pic', 'file'],
      //['pic', 'required'],
      //['mobile', 'required'],
      [['phone', 'mobile'], 'string', 'max' => 15],
        [['tmppass'], 'string', 'max' => 100],
        ['email', 'filter', 'filter' => 'trim'],
        ['email', 'required'],
        ['email', 'email'],
        ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This email address has already been taken.')],
        ['password', 'required'],
        ['password', 'string', 'min' => 4],
    ];
  }

  public function attributeLabels() {

    return [
      'username' => Yii::t('app', 'Username'),
      'password' => Yii::t('app', 'Password'),
      'email' => Yii::t('app', 'Email'),
      'f_name' => Yii::t('app', 'First Name'),
      'l_name' => Yii::t('app', 'Last Name'),
      'address' => Yii::t('app', 'Address'),
      'city' => Yii::t('app', 'City'),
      'country' => Yii::t('app', 'Country'),
      'pic' => Yii::t('app', 'Logo/Pic'),
      'sp' => Yii::t('app', 'Service Provider'),
      'phone' => Yii::t('app', 'Phone'),
      'mobile' => Yii::t('app', 'Contact Number'),
      'tmppass' => Yii::t('app', 'Temporary Password'),
    ];
  }

  /**
   * Signs user up.
   *
   * @return User|null the saved model or null if saving fails
   */
  public function signup() {
    $this->username = $this->username != '' ?: $this->email;
    if ($this->validate()) {
      $user = new User();
      $sData = Yii::$app->request->post('SignupForm');
      $user->username = $this->username;
      $user->email = $this->email;
      $user->setPassword($this->password);
      $user->f_name = $sData['f_name'];
      $user->l_name = $sData['l_name'];
      $user->mobile = $sData['mobile'];

      $user->generateAuthKey();
      if ($user->save()) {
        return $user;
      }
    }

    return null;
  }

  public function signup2() {
    $this->username = $this->username != '' ?: $this->email;
    if ($this->validate()) {
      $user = new User();
      $user->username = $this->username;
      $user->email = $this->email;
      $user->setPassword($this->password);
      $user->f_name = $this->f_name;
      $user->l_name = $this->l_name;
      $user->mobile = $this->mobile;
      $user->tmp_pass = $this->tmppass;
      $user->generateAuthKey();
      if ($user->save()) {
        return $user;
      }
      return $user->getErrors();
    }

    return null;
  }

}
