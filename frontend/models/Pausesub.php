<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Pausesub extends Model {

    public $subscriptionid;
    public $months;
    public $reason;
    
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // name, email, subject and body are required
            [['months', 'reason'], 'required'],
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'months' => Yii::t('app', 'How long would you like to pause your subscription for?'),
          'reason' => Yii::t('app', 'Why are you deciding to pause?')
        ];
    }

}
