<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Redemesub extends Model {

    public $subscriptionid;
    public $customerId;
    public $company;
    public $address1;
    public $address2;
    public $city;
    public $zip;
    public $country;
    public $state;
    
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // name, email, subject and body are required
            [['address1', 'city', 'country', 'state'], 'required'],
          [['address1'], 'string', 'max' => 150],
          [['city'], 'string', 'max' => 50],
          [['country'], 'string', 'max' => 50],
          [['state'], 'string', 'max' => 50],
          [['zip'], 'string', 'max' => 20],
          [['address1'], 'string', 'max' => 150],
          [['company'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
           'address1' => Yii::t('app', 'Address Line 1'),
          'address2' => Yii::t('app', 'Address Line 2'),
          'company' => Yii::t('app', 'Company'),
          'city' => Yii::t('app', 'City'),
          'zip' => Yii::t('app', 'Postcode'),
          'country' => Yii::t('app', 'Country'),
          'state' => Yii::t('app', 'State'),
          
        ];
    }

}
