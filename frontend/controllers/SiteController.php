<?php

namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use \common\config\Options;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use common\models\Faq;
use common\models\Newsletteremails;
use common\models\Needhelp;
use common\models\NeedhelpSearch;
use common\models\Settings;
use yii\data\Pagination;
use common\config\Controller2;
use common\models\User;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller2 {

    public $loginSuccessfull = "Login successful, welcome to Portal";
    public $signupSuccessfull = "Thank you for Signing Up with Portal";
    public $logoutsuccess = "You have successfully logout of Portal";

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
// 'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionFileupload($id) {
        $c_id = Yii::$app->getRequest()->getQueryParam('c_type');

        $dirpath = realpath(dirname(getcwd()));
        $uploaddir = $dirpath . '/web/uploads';
        foreach ($_FILES["image_path"]["error"] as $key => $error) {
            if ($error == UPLOAD_ERR_OK) {
                $tmp_name = $_FILES["image_path"]["tmp_name"][$key];
                $name = uniqid() . $_FILES["image_path"]["name"][$key];
                $name_db[] = $name;
                move_uploaded_file($tmp_name, "$uploaddir/$name");
            }
        }

        $connection = \Yii::$app->db;
        foreach ($name_db as $rs) {
            $file_name = 'uploads/' . $rs;
            $connection->createCommand()
                    ->insert('es_gallery', [
                        'image_path' => $file_name,
                        'option_id' => $id,
                        'option_type' => $c_id,
                    ])->execute();
        }
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }

    /**
     * This function will be triggered when user is successfuly authenticated using some oAuth client.
     *
     * @param yii\authclient\ClientInterface $client
     * @return boolean|yii\web\Response
     */
    public function createAccount($userName, $email, $fName, $pic) {

        $user = new User();
        $user->username = $userName;
        $user->email = $email;
        $pass = rand(10, 1000000);
        $user->setPassword($pass);
        $user->f_name = $fName;
        $user->l_name = '';
        $user->address = '';
        $user->city = '';
        $user->country = '';
        $user->mobile = '-';
        $user->pic = $pic;
        $user->generateAuthKey();
        if ($user->save()) {
            $this->login($user);
        } else {
            \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', 'Error while creating account'));
        }
    }

    private function login($user) {

        if (Yii::$app->user->login($user, 0)) {
            \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', $this->loginSuccessfull));
        } else {
            \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', 'Error while signin process'));
        }
    }

    public function oAuthSuccess($client) {


// get user data from client
        $userModel = new \common\models\User();
        $userAttributes = $client->getUserAttributes();
        $isNewUser = false;

        if (isset($userAttributes['name'])) {
            if (isset($userAttributes['email']) && isset($userAttributes['name'])) {

                $checkuser = \common\models\User::find()->where(['username' => $userAttributes['email']])->one();
                if ($checkuser != null) {
                    $this->login($checkuser);
                } else {
                    $checkuser1s = \common\models\User::find()->where(['email' => $userAttributes['email']])->one();
                    if ($checkuser1s != null) {
                        $this->login($checkuser1s);
                    } else {
                        $user = $this->createAccount($userAttributes['email'], $userAttributes['email'], $userAttributes['name'], '');
                        $isNewUser = true;
                    }
                }
            } else {
                $checkuser = \common\models\User::find()->where(['username' => $userAttributes['emails'][0]['value']])->one();
                if ($checkuser != null) {
                    $this->login($checkuser);
                } else {

                    $checkuser2s = \common\models\User::find()->where(['email' => $userAttributes['emails'][0]['value']])->one();
                    if ($checkuser2s != null) {
                        $this->login($checkuser2s);
                    } else {
                        $user = $this->createAccount($userAttributes['emails'][0]['value'], $userAttributes['emails'][0]['value'], $userAttributes['displayName'], $userAttributes['cover']['coverPhoto']['url']);
                        $isNewUser = true;
                    }
                }
            }
        }
        if (\Yii::$app->user->isGuest)
            return Yii::$app->getResponse()->redirect(Yii::$app->urlManager->createUrl("site/index"));
        else {
            if ($isNewUser == false)
                return Yii::$app->getResponse()->redirect(Yii::$app->urlManager->createUrl("site/dashboard"));
            else
                return Yii::$app->getResponse()->redirect(Yii::$app->urlManager->createUrl("user/viewown"));
        }
// do some thing with user data. for example with $userAttributes['email']
    }

    public function actionCp() {
        $setting = Settings::findOne(1);
        \chargebee\ChargeBee\Environment::configure($setting->cb_name, $setting->cb_api);

        $return_url = \yii\helpers\Url::to(['site/index']);
        $result = \chargebee\ChargeBee\Models\PortalSession::create(array("redirectUrl" => $return_url,
                    "customer" => array("id" => 'Hr551BIQX9frnY5YxB')));
        $account_url = $result->portalSession()->accessUrl;
        $this->redirect($account_url);
    }

    public function actionIndex() {

        $model = new \frontend\models\Boxchoice();
        if ($model->load(Yii::$app->request->post())) {
            $this->redirect(["join/index/", 'id' => $model->name]);
        }
        $newsmodel = new Newsletteremails();
        $model->name = 'monthly';
        return $this->render('index', ['model' => $model, 'newsmodel' => $newsmodel]);
    }

    public function actionGift() {
        return $this->render('gift');
    }

    public function actionFaq() {
        $this->mainMenu = 700;
        $faqModel = Faq::find()->orderBy('id')->all();
        return $this->render('faq', [
                    'faq' => $faqModel,
        ]);
    }

    public function actionCheck_user() {
        if (Yii::$app->user->isGuest) {
            echo '1';
        } else {
            echo '2';
        }
    }

    public function actionFaqdetail($id) {
        $faqM = Faq::findOne(['id' => $id]);

        return $this->render('faq_detail', ['f_detail' => $faqM,]);
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $session = Yii::$app->session;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if ($session->get('subscribe') == null) {
                return $this->redirect(Yii::$app->request->referrer);
            } else {
                $plan = $session->get('plan');
                $coupon = $session->get('coupon');
                $utm_campaign = $session->get('utm_campaign');
                $utm_source = $session->get('utm_source');
                $utm_medium = $session->get('utm_medium');
                $utm_content = $session->get('utm_content');
                $session->remove('subscribe');
                $session->remove('plan');
                $session->remove('coupon');
                $session->remove('addon');
                $session->remove('utm_campaign');
                $session->remove('utm_source');
                $session->remove('utm_medium');
                $session->remove('utm_content');

                return Yii::$app->getResponse()->redirect(Yii::$app->urlManager->createUrl(["join/index", 'id' => $plan,
                                    'coupon' => $coupon, 'utm_campaign' => $utm_campaign, 'utm_source' => $utm_source,
                                    'utm_medium' => $utm_medium, 'utm_content' => $utm_content]));
            }
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCheck_login() {
        $model = new LoginForm();
        $data = Yii::$app->request->post();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', $this->loginSuccessfull));
            $data = '1';
        } else {
            $data = '2';
        }
        echo $data;
    }

    public function actionLoginpopup() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', $this->loginSuccessfull));
            if ($session->get('subscribe') == null) {
                return $this->redirect(Yii::$app->request->referrer);
            } else {
                $plan = $session->get('plan');
                $coupon = $session->get('coupon');
                $utm_campaign = $session->get('utm_campaign');
                $utm_source = $session->get('utm_source');
                $utm_medium = $session->get('utm_medium');
                $utm_content = $session->get('utm_content');
                $session->remove('subscribe');
                $session->remove('plan');
                $session->remove('coupon');
                $session->remove('addon');
                $session->remove('utm_campaign');
                $session->remove('utm_source');
                $session->remove('utm_medium');
                $session->remove('utm_content');

                return Yii::$app->getResponse()->redirect(Yii::$app->urlManager->createUrl(["join/index", 'id' => $plan,
                                    'coupon' => $coupon, 'utm_campaign' => $utm_campaign, 'utm_source' => $utm_source,
                                    'utm_medium' => $utm_medium, 'utm_content' => $utm_content]));
                
            }
        } else {
            echo \Yii::$app->view->renderFile('@app/views/site/login_1.php', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        $session = Yii::$app->session;
        $setting = Settings::findOne(1);
        \chargebee\ChargeBee\Environment::configure($setting->cb_name, $setting->cb_api);

        if ($session->get('cb_portal') !== null) {

            $result = \chargebee\ChargeBee\Models\PortalSession::logout($session->get('cb_sessionId'));
            $session->set('cb_portal', null);
            $session->set('cb_token', null);
            $session->set('cb_accessUrl', null);
            $session->set('cb_sessionId', null);
        }
        Yii::$app->user->logout();
        \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', $this->logoutsuccess));
        return $this->goHome();
    }

    public function actionCberror() {
        return $this->render('cberror');
    }

    public function actionDashboard($sort = null) {
        if (Yii::$app->user->isGuest) {
            throw new \yii\web\HttpException(403, Yii::t('app', 'You are unauthorized to perform this action.'));
        }
        $this->mainMenu = 200;
        $this->submenu = 210;
        $account_url = null;
        $accessToken = null;

        if (JoinController::createCBSession()) {
            $session = Yii::$app->session;
            $account_url = $session->get('cb_accessUrl');
            $accessToken = $session->get('cb_token');
        }

        if ($account_url != null) {
            return $this->render('dashboard', ['cbAccessUrl' => $account_url, 'accessToken' => $accessToken]);
        } else {
            return $this->redirect(["site/index"]);
        }
    }

    public function actionContact() {
        $this->mainMenu = 650;
        $model = new ContactForm();
        $cmodel = new \common\models\Contactus;
        $cs_model = Settings::findOne(['id' => 1]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {


                $cData = Yii::$app->request->post('ContactForm');
                $cmodel->name = $cData['name'];
                $cmodel->email = $cData['email'];
                $cmodel->subject = $cData['subject'];
                $cmodel->message = $cData['body'];
                $cmodel->save();

                Yii::$app->session->setFlash('success', Yii::t('app', 'Thank you for contacting us. We will respond to you as soon as possible.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'There was an error sending email.'));
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
                        'cs_model' => $cs_model,
            ]);
        }
    }

    public function actionSubsnewsletter() {

        $model = new Newsletteremails();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {

            $model->save();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Thank you for subscribing bellabox Newsletter.'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'This email already exist.'));
        }

        $this->redirect(["site/index/"]);
    }

    public function actionNeedhelp() {

        $query = Needhelp::find();
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->render('about', [
                    'models' => $models,
                    'pages' => $pages,
        ]);
    }

    public function actionView($id) {
        $n_model = Needhelp::findOne(['id' => $id]);
        return $this->render('about_detail', ['model' => $n_model]);
    }

    public function actionViewall() {
        $all_cat = \common\models\Category::find()->orderBy('name ASC')->all();
        return $this->render('categories', ['models' => $all_cat]);
    }

    public function actionAbout() {
        return $this->render('about');
    }

    public function actionSignup() {

        if (!Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(Yii::$app->urlManager->createUrl("site/index"));
        }
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            $user1 = new \common\models\User;

            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', $this->signupSuccessfull));
                    $session = Yii::$app->session;
                    if ($session->get('subscribe') == null) {
                        return $this->goHome();
                    } else {
                        $plan = $session->get('plan');
                        $coupon = $session->get('coupon');
                        $addon = $session->get('addon');
                        $utm_campaign = $session->get('utm_campaign');
                        $utm_source = $session->get('utm_source');
                        $utm_medium = $session->get('utm_medium');
                        $utm_content = $session->get('utm_content');

                        $session->remove('subscribe');
                        $session->remove('plan');
                        $session->remove('coupon');
                        $session->remove('addon');
                        $session->remove('utm_campaign');
                        $session->remove('utm_source');
                        $session->remove('utm_medium');
                        $session->remove('utm_content');

                        return Yii::$app->getResponse()->redirect(Yii::$app->urlManager->createUrl(["join/index", 'id' => $plan,
                                            'coupon' => $coupon, 'addon' => $addon,
                                            'utm_campaign' => $utm_campaign, 'utm_source' => $utm_source,
                                            'utm_medium' => $utm_medium, 'utm_content' => $utm_content]));
                    }
                }
            }
        }

        return $this->render('signup', [
                    'model' => $model,
        ]);
    }

    public function actionTerms() {
        return $this->render('terms');
    }

    public function actionPolicy() {
        return $this->render('policy');
    }

    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', 'Request for password submitted successfully. Please check your email.'));
                // return $this->redirect(Yii::$app->request->referrer);
                return $this->goHome();
            } else {
                \Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Sorry, we are unable to reset password for email provided.'));
            }
        }
        return $this->render('requestPasswordResetToken', ['model' => $model,]);
    }

    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'New password was saved.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

}
