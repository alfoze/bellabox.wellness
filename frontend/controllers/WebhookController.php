<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\data\Pagination;
use common\config\Controller2;
use common\models\User;
use common\models\Beautyprofile;
use common\models\Rewardpoint;
use common\models\Feedback;
use common\models\Allocation;

/**
 * Site controller
 */
class WebhookController extends Controller2 {

    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
// 'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionTypeformwebhook($id) {
        $current = 'Initiated ----';
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $resonce = file_get_contents('php://input');

        $resonce = json_decode($resonce, true);

        if ($resonce['event_type'] == 'form_response') {
            $formData = $resonce['form_response'];
            $formId = $formData['form_id'];
            $formHidden = $formData['hidden'];
            $customerEmail = $formHidden['email'];
            $customerid = $formHidden['cs_id'];

            JoinController::createFeedBack($formId, $customerid, $customerEmail, 0);
            JoinController::createRewardPoint($customerId, JoinController::REVIEWED);
        }
    }

    public function actionProcesswebhook() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $json = file_get_contents('php://input');
        $action = $json;
        $jsonData = json_decode($json, true);
        $eventType = isset($jsonData['event_type']) ? $jsonData['event_type'] : null;

        try {
            $content = file_get_contents('php://input');
            $webhook_content = \chargebee\ChargeBee\Models\Event::deserialize($content);
            $this->operateOnWebHook($jsonData, $webhook_content);
            echo "Webhook from Chargebee processed";
        } catch (Exception $e) {
            echo "Message :" . $e->getMessage();
            echo "\nError in processing webhook";
        }
        die();
    }

    private function operateOnWebHook($jsonData, $webhook_content) {
        if ($jsonData['event_type'] == 'customer_created') {
            echo "in customer_created event.";
            $customer = $webhook_content->content()->customer();
            $user = User::find()->where("email = '$customer->email'")->one();
            if ($user != null) {
                echo "User exists , no need to create the user." . $customer->id;
                $user->cb_customer_id = $customer->id;
                echo ($user->update()) ? "User saved." : "Error saving user.";
            } else {
                echo "unable to find user " . $customer->email;
            }
            die();
        }
        if ($jsonData['event_type'] == 'subscription_created') {
            echo "in subscription_created event.";
            $customer = $webhook_content->content()->customer();
            $subscription = $webhook_content->content()->subscription();
            $planId = $subscription->planId;
            JoinController::createEnvironment();
            $result = \chargebee\ChargeBee\Models\Plan::retrieve($planId);
            $plan = $result->plan();
            $user = User::find()->where("email = '$customer->email'")->one();

            $userFName = '';
            $userLName = '';
            $userEmail = '';
            if ($user != null) {
                echo "User exists , no need to create the user." . $customer->id;
                $user->cb_customer_id = $customer->id;
                echo ($user->update()) ? "User saved." : "Error saving user.";
                $userFName = $user->f_name;
                $userLName = $user->l_name;
                $userEmail = $user->email;

                JoinController::createRewardPoint($customer->id, JoinController::PURCHASED, $subscription->planUnitPrice / 100);
            } else {
                echo "unable to find user " . $customer->email;
            }
            echo "<br>";
            if ($plan->cfPlanType == 'Send Gift Subscription') {
                $linkedPlan = $plan->cfLinkedPlan;
                $receiverFName = $subscription->cfRecipientFirstName;
                $receiverLName = $subscription->cfRecipientLastName;
                $receiverEmail = $subscription->cfRecipientEmailAddress;
                $giftuser = User::find()->where("email = '$receiverEmail'")->one();
                $cbCustomerId = null;
                $tmpPass = $this->generateRandomString();
                if ($giftuser == null) {
                    //Create a user in the portal
                    $sumodel = new \frontend\models\SignupForm();
                    $sumodel->email = $receiverEmail;
                    $sumodel->f_name = $receiverFName;
                    $sumodel->l_name = $receiverLName;
                    $sumodel->password = $tmpPass;
                    $sumodel->tmppass = $tmpPass;
                    $sumodel->mobile = '-';
                    $sumodel->signup2();
                } else {
                    $cbCustomerId = $giftuser->cb_customer_id;
                }
                $oneMonthDate = strtotime("+1 month", strtotime(date("Y-m-d")));
                if ($cbCustomerId == null) {
                    $result = \chargebee\ChargeBee\Models\Subscription::create(
                                    array("planId" => $linkedPlan, 'cfLinkedSubscription' => $subscription->id, 'startDate' => $oneMonthDate,
                                        'cfRecipientFirstName' => $userFName, 'cfRecipientLastName' => $userLName, 'cfRecipientEmailAddress' => $userEmail,
                                        "customer" => array("email" => $receiverEmail, "firstName" => $receiverFName, "lastName" => $receiverLName, 'cfTemporaryPassword' => $tmpPass)
                    ));
                } else {
                    $result = \chargebee\ChargeBee\Models\Subscription::createForCustomer($cbCustomerId, array("planId" => $linkedPlan, 'cfLinkedSubscription' => $subscription->id, 'startDate' => $oneMonthDate,
                                'cfRecipientFirstName' => $userFName, 'cfRecipientLastName' => $userLName, 'cfRecipientEmailAddress' => $userEmail));
                }
                $giftSubId = $result->subscription()->id;
                \chargebee\ChargeBee\Models\Subscription::update($subscription->id, ['cfLinkedSubscription' => $giftSubId]);
            } elseif ($plan->cfPlanType == 'Receive Gift Subscription') {
                
            }
            die();
        }
        if ($jsonData['event_type'] == 'subscription_deleted' || $jsonData['event_type'] == 'subscription_cancelled') {
            echo "in subscription_delete event.";
            $customer = $webhook_content->content()->customer();
            $subscription = $webhook_content->content()->subscription();
            $user = User::find()->where("email = '$customer->email'")->one();

            if ($user != null) {
                JoinController::createRewardPoint($customer->id, JoinController::PURCHASE_EXPIRED, $subscription->planUnitPrice / 100, 'Points reversed due to cancellation of subscription');
            } else {
                echo "unable to find user " . $customer->email;
            }
            die();
        }
        if ($jsonData['event_type'] == 'customer_deleted') {
            echo "in customer_deleted event.";
            $customer = $webhook_content->content()->customer();

            $user = User::find()->where("cb_customer_id = '$customer->id'")->one();
            if ($user != null && $user->is_admin == false) {
                $beautyprofile = Beautyprofile::deleteAll("customer_id = '$customer->id'");
                $rewardpoint = Rewardpoint::deleteAll("customer_id = '$customer->id'");
                $feedback = Feedback::deleteAll("customer_id = '$customer->id'");
                $allocation = Allocation::deleteAll("customer_id = '$customer->id'");
                $user = User::deleteAll("cb_customer_id = '$customer->id'");

                echo "Customer's data is deleted!";
            } else {
                echo "unable to find user " . $customer->email;
            }
            die();
        }
        if ($jsonData['event_type'] == 'customer_changed') {
            echo "in customer_changed event.";
            $customer = $webhook_content->content()->customer();

            $user = User::find()->where("cb_customer_id = '$customer->id'")->one();
            if ($user != null) {
                $user->f_name = $customer->firstName;
                $user->l_name = $customer->lastName;
                if ($user->save()) {
                    echo "Customer's data is updated!";
                }
            } else {
                echo "unable to find user " . $customer->email;
            }

            die();
        }
        if ($jsonData['event_type'] == 'invoice_generated') {
            //create order on invoice creation as the payment suceed webhook will not be called for gift receive subscription 
            echo "in invoice generated.";
            $customer = $webhook_content->content();
            $invoice = $customer->invoice();
            if ($invoice->total == 0) {
                $customerId = $invoice->customerId;
                $InvoiceId = $invoice->id;
                JoinController::createEnvironment();
                foreach ($invoice->lineItems as $lineItem) {
                    // $order = \chargebee\ChargeBee\Models\Order::create(array("invoiceId" => $InvoiceId, 'note' => $lineItem->subscriptionId));
                }
            } else {
                echo 'amount greater than zero. doing nothing.';
            }
            die();
        }
        if ($jsonData['event_type'] == 'payment_succeeded') {
            echo "in payment succeeded.";
            echo "no need to do any thing";
            die();
            $customer = $webhook_content->content();
            $invoice = $customer->invoice();
            $subscription = $customer->subscription();
            if (!isset($subscription->planId)) {
                echo "no need to create order, there is no planid in subscription.";
                die();
            }
            JoinController::createEnvironment();
            $result = \chargebee\ChargeBee\Models\Plan::retrieve($subscription->planId);
            $plan = $result->plan();
            if (!isset($plan->cfPlanType)) {
                echo "no need to create order, there is no plan type.";
                die();
            }
            if ($plan->cfPlanType == 'Send Gift Subscription') {
                echo "no need to create order. it is send gift sub type";
                die();
            }

            $customerId = $invoice->customerId;
            $InvoiceId = $invoice->id;
            $isRecurring = $invoice->recurring;
            $status = $invoice->status;
            $date = $invoice->date;
            $qty = 0;
            foreach ($invoice->lineItems as $lineItem) {
//        $order = \chargebee\ChargeBee\Models\Order::create(array("invoiceId" => $InvoiceId, 'note' => $lineItem->subscriptionId));
            }
            //var_dump($order);
            die();
        }
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
