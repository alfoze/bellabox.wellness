<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use yii\base\Model;
use common\config\Controller2;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\UploadedFile;

/**
 * BusinessController implements the CRUD actions for Business model.
 */
class AppuserController extends Controller2 {

    public $mainMenu = 200;
    public $left_menu = 130;
    public $submenu = 131;

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing Business model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate() {
        $id = Yii::$app->user->identity->id;

        //$model = new User;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $uData = Yii::$app->request->post('User');
            $model->email = isset($uData['email']) ? $uData['email'] : "";
            $model->f_name = isset($uData['f_name']) ? $uData['f_name'] : "";
            $model->l_name = isset($uData['l_name']) ? $uData['l_name'] : "";
            $model->address = isset($uData['city']) ? $uData['city'] : "";
            $model->country = isset($uData['country']) ? $uData['country'] : "";
            $model->city = isset($uData['city']) ? $uData['city'] : "";
            ;

            if ($_FILES['User']['name']['pic'] != '') {
                $user1 = new \common\models\User;
                $model->pic = UploadedFile::getInstance($model, 'pic');
                $image_name = uniqid() . $model->username;
                $model->pic->saveAs('uploads/' . $image_name . '.' . $model->pic->extension);
                $model->pic = 'uploads/' . $image_name . '.' . $model->pic->extension;
            }
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', 'Your Profile have been Updated Successfully.'));
                return $this->redirect(array('site/dashboard'));
            }
        }
        if ($model->country == '') {
            $model->country = \common\config\Options::getDefaultCounty();
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
