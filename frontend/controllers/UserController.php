<?php

namespace frontend\controllers;

use yii\data\Pagination;
use Yii;
use common\models\User;
use common\models\UserSearch;
use common\config\Controller2;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

Yii::setAlias('budyaga', '@vendor/budyaga');

/**
 * UnamesController implements the CRUD actions for Unames model.
 */
class UserController extends Controller2 {

    public $mainMenu = 500;
    public $submenu = 501;
    public $city = "";
    public $category = "";
    public $keyword = "";
    public $subcategory = "";
    public $dkeyword = "";
    public $pricesort = "";
    public $citysort = "";
    public $datesort = "";
    public $country = "";
    public $namesort = "";
    public $ratesort = "";

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['updatelocation', 'update', 'viewown', 'updateimage', 'updatepimage', 'serviceprovider', 'addfavorite'],
                'rules' => [
                    [
                        'actions' => ['updatelocation', 'update', 'viewown', 'updateimage', 'updatepimage', 'serviceprovider', 'addfavorite'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //  'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {

        return [
            'uploadPhoto' => [
                'class' => 'budyaga\cropper\actions\UploadAction',
                'url' => \common\config\Options::getFrontendAddress() . '/uploads',
                'path' => 'uploads',
            ]
        ];
    }

    public function actionAddfavorite($id) {

        $model = \common\models\Favoriteadd::findOne(['option_id' => $id, 'option_type' => Controller2::USERS, 'user_id' => Yii::$app->user->id]);
        if ($model == null) {
            $model = new \common\models\Favoriteadd();
            $model->option_id = $id;
            $model->option_type = Controller2::USERS;
            $model->user_id = Yii::$app->user->id;
            $model->save();
            \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', 'Added to favourites'));
        } else {
            $model->delete();
            \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', 'Removed from favourites'));
        }
        $this->redirect(array('user/view', 'id' => $id));
    }

    public function actionSearchprovider() {
        $city = Yii::$app->request->post('city') ? Yii::$app->request->post('city') : null;
        $country = Yii::$app->request->post('country') ? Yii::$app->request->post('country') : null;
        $keyword = Yii::$app->request->post('keyword') ? Yii::$app->request->post('keyword') : null;
        $category = Yii::$app->request->post('category') ? Yii::$app->request->post('category') : null;
        $subcategory = Yii::$app->request->post('sub_category') ? Yii::$app->request->post('sub_category') : null;

        return $this->redirect(['allsellers', 'city' => $city, 'category' => $category, 'keyword' => $keyword, 'subcategory' => $subcategory, 'country' => $country]);
    }

    public function actionAllsellers($country = null, $city = null, $category = null, $subcategory = null, $keyword = null, $dkeyword = null, $datesort = null, $pricesort = null, $ratesort = null, $namesort = null, $citysort = null, $operationType = 'search') {

        if (isset($_POST['button'])) {
            $operationType = $_POST['button'];
        }
        $city = Yii::$app->request->post('city') ? Yii::$app->request->post('city') : $city;
        $country = Yii::$app->request->post('country') ? Yii::$app->request->post('country') : $country;
        $keyword = Yii::$app->request->post('keyword') ? Yii::$app->request->post('keyword') : $keyword;
        $category = Yii::$app->request->post('category') ? Yii::$app->request->post('category') : $category;
        $subcategory = Yii::$app->request->post('sub_category') ? Yii::$app->request->post('sub_category') : $subcategory;

        $this->city = $city;
        $this->country = $country;
        $this->keyword = $keyword;
        $this->category = $category;
        $this->subcategory = $subcategory;
        $this->dkeyword = $dkeyword;
        $this->datesort = $datesort;
        $this->pricesort = $pricesort;
        $this->citysort = $citysort;
        $this->namesort = $namesort;
        $this->ratesort = $ratesort;


        $where = "status = '2'";
        if ($category != null) {
            $where.=" and category_id='" . $category . "'";
        }
        if ($subcategory != null) {
            $where.=" and sub_category_id='" . $subcategory . "'";
        }
        if ($country != null) {
            $where.=" and country='" . $country . "'";
        }
        if ($city != null) {
            $where.=" and city='" . $city . "'";
        }
        $where = "id in (select distinct user_id from es_business where " . $where . ")";

        if ($keyword != null) {
            $where.=" and (notes LIKE '%" . $keyword . "%' )";
        }

        $where.= "and account_type = '1'";
        $query = \common\models\User::find();
        $query = $query->where($where);

        $countQuery = clone $query;

        if ($citysort != null) {
            $query->orderBy('city ' . $citysort);
        }
        if ($namesort != null) {
            $query->orderBy('business_name ' . $namesort);
        }
        if ($ratesort != null) {
            $query->joinWith([ 'feedback']);
            $query->orderBy(' v_feedback_provider.rate ' . $ratesort);
        }
        if ($pricesort != null) {
            $query->joinWith([ 'feedback']);
            $query->orderBy(' v_feedback_provider.reviews ' . $pricesort);
        }

        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        if ($models == null) {
            $cs_model = \common\models\Settings::findOne(['id' => 1]);
            \Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'No Record Found. Try With Different One.'));
            $this->redirect(Yii::$app->request->referrer);
        } else {

            return $this->render('allsellers', [
                        'models' => $models,
                        'pages' => $pages,
            ]);
        }
    }

    public function actionServiceprovider($id) {
        $model = $this->findModel($id);
        if ($model->id != Yii::$app->user->id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model->account_type = '1';
        if ($model->save()) {
            \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', 'User have become service provider successfully.'));
        }
        return $this->redirect(['viewown']);
    }

    public function actionUpdatelocation($id) {
        $model = $this->findModel($id);

        if ($model->id != Yii::$app->user->id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $this->mainMenu = 150;
        $this->left_menu = 300;
        $this->submenu = 302;

        if ($model->load(Yii::$app->request->post())) {

            $model->lang = isset($uData['lang']) ? $uData['lang'] : "en";

            if ($model->save()) {
                return $this->redirect(['viewown', 'id' => $model->id]);
            }
        }
        return $this->render('updatlocation', ['model' => $model,]);
    }

    protected function uploadImage($model, $oldImage, $colname) {

        if (\yii\web\UploadedFile::getInstance($model, $colname)) {
            $fileObj = \yii\web\UploadedFile::getInstance($model, $colname);
            $filename = "";
            $dirpath = realpath(dirname(getcwd()));
            $filename = 'uploads/' . uniqid() . $colname . $fileObj->name;
            $fileObj->saveAs($dirpath . "/web/" . $filename);
            return $filename;
        } else {
            return $oldImage;
        }
    }

    /**
     * Updates an existing Unames model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->id != Yii::$app->user->id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->mainMenu = 150;
        $this->left_menu = 300;
        $this->submenu = 302;

        if ($model->load(Yii::$app->request->post())) {

            $uData = Yii::$app->request->post('User');
            $model->email = isset($uData['email']) ? $uData['email'] : "";
            $model->f_name = isset($uData['f_name']) ? $uData['f_name'] : "";
            $model->l_name = isset($uData['l_name']) ? $uData['l_name'] : "";
            $model->address = isset($uData['address']) ? $uData['address'] : "";
            $model->country = isset($uData['country']) ? $uData['country'] : "";
            $model->city = isset($uData['city']) ? $uData['city'] : "";
            $model->is_active = isset($uData['is_active']) ? $uData['is_active'] : "1";
            $model->account_type = isset($uData['account_type']) ? $uData['account_type'] : "0";
            $model->lang = isset($uData['lang']) ? $uData['lang'] : "en";

            if ($model->save()) {
                return $this->redirect(['viewown', 'id' => $model->id]);
            }
        }
        return $this->render('update', ['model' => $model,]);
    }

    public function actionUpdatepimage($id) {
        $model = $this->findModel($id);

        if ($model->id != Yii::$app->user->id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $this->mainMenu = 150;
        $this->left_menu = 300;
        $this->submenu = 302;

        $gallerymodel = \common\models\Gallery::findAll(['option_type' => Controller2::USERS, 'option_id' => $id]);

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['viewown', 'id' => $model->id]);
        }

        return $this->render('updatepimage', [
                    'model' => $model,
                    'g_model' => $gallerymodel,
        ]);
    }

    public function actionUpdateimage($id) {
        $model = $this->findModel($id);

        if ($model->id != Yii::$app->user->id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $this->mainMenu = 150;
        $this->left_menu = 300;
        $this->submenu = 302;

        $pic = $model->pic;
        $bImage = $model->b_img;
        if ($model->load(Yii::$app->request->post())) {
            $uData = Yii::$app->request->post('User');
            if (isset($uData['pic'])) {
                if ($uData['pic'] != '')
                    $model->pic = str_replace(\common\config\Options::getFrontendAddress(), '', $uData['pic']);
                else
                    $model->pic = $pic;
            }
            $model->b_img = $this->uploadImage($model, $bImage, 'b_img');


            if ($model->save()) {
                return $this->redirect(['viewown', 'id' => $model->id]);
            }
        }
        $model->b_img = \common\config\Options::getFrontendAddress() . $model->b_img;
        $model->pic = \common\config\Options::getFrontendAddress() . $model->pic;
        $gallerymodel = \common\models\Gallery::findAll(['option_type' => Controller2::USERS, 'option_id' => $id]);
        return $this->render('updateimage', ['model' => $model, 'g_model' => $gallerymodel]);
    }

    public function actionViewown($id = null) {
        $this->mainMenu = 150;
        $this->left_menu = 300;
        $this->submenu = 302;

        if ($id == null) {
            $id = \Yii::$app->user->id;
        }
        $model = $this->findModel($id);
        if ($model->lat == 0) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $ip = ip2long($ip);

            $iplocation = \common\models\Ip2location::find()->where("'$ip' between ip_from and ip_to")->one();
            if ($iplocation != null) {
                $model->lat = $iplocation->latitude;
                $model->lng = $iplocation->longitude;
            } else {
                $model->lat = Yii::$app->params['lat'];
                $model->lng = Yii::$app->params['lng'];
            }
            $model->save();
        }

        return $this->render('viewown', [
                    'model' => $model,
        ]);
    }

    public function actionSubscription($id) {
        $this->mainMenu = 150;
        $this->left_menu = 300;
        $this->submenu = 303;

        $model = $this->findModel($id);

        return $this->render('viewown', [
                    'model' => $model,
        ]);
    }

    public function actionView($id) {
        $this->mainMenu = 150;
        $this->left_menu = 300;
        $this->submenu = 302;

        $model = $this->findModel($id);
        $business = \common\models\Business::find()->where(['user_id' => $id])->orderBy('id DESC')->all();
        return $this->render('view', [
                    'model' => $model,
                    'business' => $business
        ]);
    }

    /**
     * Finds the Unames model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unames the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
