<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\data\Pagination;
use common\config\Controller2;
use common\models\User;

/**
 * Site controller
 */
class JoinController extends Controller2 {

    const PURCHASED = 'P';
    const REVIEWED = 'R';
    const PURCHASE_EXPIRED = 'E';
    const REVIEW_EXPIRED = 'D';

    public $mainMenu = 100;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
// 'logout' => ['post'],
                ],
            ],
        ];
    }

    public static function createFeedBack($surveyId, $customerId, $email = '', $boxid = 0) {

        $fb = new \common\models\Feedback();
        $fb->survey_id = $surveyId;
        $fb->customer_email = '-';
        $fb->customer_id = $customerId;
        $fb->box_id = $boxid;
        return $fb->save();
    }

    public static function createRewardPoint($customerId, $reasonType, $amount = 0, $expire_reason = '') {
        $saved = false;
        $reason = '';
        $points = 0;
        if ($reasonType == self::PURCHASED) {
            $points = floor($amount);
            $reason = 'Points awarded for purchasing.';
        } elseif ($reasonType == self::REVIEWED) {
            $points = 5;
            $reason = 'Points awarded for review.';
        } elseif ($reasonType == self::PURCHASE_EXPIRED) {
            $points = -1 * floor($amount);
            $reason = $expire_reason == '' ? 'Points expired after a year.' : $expire_reason;
        } elseif ($reasonType == self::REVIEW_EXPIRED) {
            $points = -5;
            $reason = $expire_reason == '' ? 'Points expired after a year.' : $expire_reason;
        }

        if ($points < 0) {
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("SELECT ifnull(SUM(points),0) AS total_points  FROM reward_point WHERE  customer_id = :customer_id", [':customer_id' => $customerId]);
            $result = $command->queryOne();
            $totalPoints = $result['total_points'];
            if ($totalPoints + $points < 0) {
                $points = $totalPoints * -1;
            }
        }

        if ($reason != "" && $points != 0) {
            $rp = new \common\models\Rewardpoint();
            $rp->reason = $reason;
            $rp->reason_code = $reasonType;
            $rp->points = $points;
            $rp->status = 'A';
            $rp->expired = '0';
            $rp->expiry_date = date('Y-m-d', strtotime('+1 year'));
            $rp->customer_id = $customerId;
            $saved = $rp->save();
        }
        return $saved;
    }

    public function actionOptin($userid, $subid, $survey) {

        $rp = new \common\models\Optinhistory();
        $rp->sub_id = $subid;
        $rp->typeform_id = $survey;
        $saved = $rp->save();
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        }
        return $this->redirect(['join/subscription']);
    }

    public function actionIndex($id = 'monthly', $coupon = '', $giftfname = null, $giftlname = null, $giftemail = null, $addon = null, $utm_campaign = null, $utm_source = null, $utm_medium = null, $utm_content = null) {

        $session = Yii::$app->session;
        if (Yii::$app->user->isGuest) {
            $session->set('subscribe', true);
            $session->set('plan', $id);
            $session->set('coupon', $coupon);
            $session->set('addon', $addon);
            $session->set('utm_campaign', $utm_campaign);
            $session->set('utm_source', $utm_source);
            $session->set('utm_medium', $utm_medium);
            $session->set('utm_content', $utm_content);
            $this->redirect(['site/signup']);
            return;
        }

        $plan = \common\models\Plans::find()->where("name = '$id'")->one();
        if ($plan == null) {
            \Yii::$app->getSession()->setFlash('s_mesage', Yii::t('app', "No such subscription plan exists!"));
            return $this->redirect(['site/index']);
        }
        if ($plan->gift == '1' && $giftfname == null) {
            return $this->redirect(['join/fillgiftinfo', 'id' => $id, 'coupon' => $coupon,
                        'utm_campaign' => $utm_campaign, 'utm_source' => $utm_source, 'utm_medium' => $utm_medium, 'utm_content' => $utm_content]);
        }
        self::createEnvironment();
        $result = \chargebee\ChargeBee\Models\Plan::retrieve($plan->cb_name);
        $planCB = $result->plan();

        $period = ($planCB->period == '1' ? '' : $planCB->period . ' ') . $planCB->periodUnit;
        return $this->render('/join/subscribe', ['planName' => $plan->description, 'planId' => $plan->cb_name,
                    'coupon' => $coupon, 'giftfname' => $giftfname, 'giftlname' => $giftlname, 'giftemail' => $giftemail,
                    'plancbName' => $planCB->name, 'period' => $period, 'price' => $planCB->price / 100,
                    'plantype' => $planCB->cfPlanType, 'currency' => 'AUD', 'addon' => $addon,
                    'utm_campaign' => $utm_campaign, 'utm_source' => $utm_source, 'utm_medium' => $utm_medium, 'utm_content' => $utm_content]);
    }

    public function actionFillgiftinfo($id, $coupon, $addon = null, $utm_campaign = null, $utm_source = null, $utm_medium = null, $utm_content = null) {
        $plan = \common\models\Plans::find()->where("name = '$id'")->one();
        if ($plan->gift == '0') {
            $this->redirect(['join/index', 'id' => $id, 'coupon' => $coupon, 'addon' => $addon,
                'utm_campain' => $utm_campaign, 'utm_source' => $utm_source, 'utm_medium' => $utm_medium, 'utm_content' => $utm_content]);
            return;
        }
        $model = new \frontend\models\Giftreceiver();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->redirect(['join/index', 'id' => $id, 'coupon' => $coupon, 'giftfname' => $model->fname, 'giftlname' => $model->lname,
                'giftemail' => $model->email, 'addon' => $addon,
                'utm_campain' => $utm_campaign, 'utm_source' => $utm_source, 'utm_medium' => $utm_medium, 'utm_content' => $utm_content]);
        }

        return $this->render('gift_receiver_info', ['model' => $model]);
    }

    public function actionChangesub($subscriptionId, $planid, $plantype) {
        $this->left_menu = 210;
        $setting = \common\models\Settings::findOne(1);
        $plansArr = array();
        $model = new \frontend\models\Changesub();
        $model->fromsubscriptionid = $planid;
        self::createEnvironment();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $result = \chargebee\ChargeBee\Models\Plan::retrieve($model->tosubscriptionid);
            $period = $result->plan()->period;
            $currentBillDate = strtotime(date("Y-m-$setting->billing_day"));
            if ($currentBillDate > time()) {
                $period -= 1;
            }
            $periodUnit = $result->plan()->periodUnit;
            $currentBillDate = strtotime("+$period $periodUnit", $currentBillDate);
            if ($currentBillDate < time()) {
                $currentBillDate = strtotime("+1 $periodUnit", $currentBillDate);
            }

            $result = \chargebee\ChargeBee\Models\Subscription::update($subscriptionId, array("planId" => $model->tosubscriptionid, 'trialEnd' => $currentBillDate));
            $this->redirect(['join/subscription']);
        }

        $plans = \chargebee\ChargeBee\Models\Plan::all();
        foreach ($plans as $plan) {
            if ($plan->plan()->id == $planid) {
                $model->fromsubscription = $plan->plan()->name;
            } else {
                if ($plan->plan()->cfBoxType == $plantype && $plan->plan()->cfIsGift != 'True')
                    $plansArr[] = ['id' => $plan->plan()->id, 'name' => $plan->plan()->name];
            }
        }


        return $this->render('changesub', ['model' => $model, 'palnList' => $plansArr]);
    }

    public function actionCancelsub($subscriptionId, $pname) {

        $this->left_menu = 210;
        $model = new \frontend\models\Cancelsub();
        $model->subscriptionid = $subscriptionId;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            self::createEnvironment();
            $dd = time();
            $result = \chargebee\ChargeBee\Models\Subscription::update($subscriptionId, array("cf_cb_cancel_reason" => $model->reason, 'cf_cb_pause_cancel_date' => $dd));
            $result = \chargebee\ChargeBee\Models\Subscription::cancel($subscriptionId);

            $this->redirect(['join/subscription']);
        }
        return $this->render('cancelsub', ['model' => $model, 'pname' => $pname]);
    }

    public function actionRedemesub($subscriptionId) {

        $this->left_menu = 210;

        $model = new \frontend\models\Redemesub();
        $model->subscriptionid = $subscriptionId;
        self::createEnvironment();
        $result = \chargebee\ChargeBee\Models\Subscription::retrieve($subscriptionId);
        $subscription = isset($result->subscription()->shippingAddress) ?: null;

        if ($subscription != null) {
            $model->company = isset($subscription->company) ? $subscription->company : '';
            $model->address1 = isset($subscription->line1) ? $subscription->line1 : '';
            $model->address2 = isset($subscription->line2) ? $subscription->line2 : '';
            $model->city = isset($subscription->city) ? $subscription->city : '';
            $model->state = isset($subscription->state) ? $subscription->state : '';
            $model->zip = isset($subscription->zip) ? $subscription->zip : '';
            $model->country = isset($subscription->country) ? $subscription->country : '';
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $shippingAddress = [
                "company" => $model->company,
                "line1" => $model->address1,
                "line2" => $model->address2,
                "city" => $model->city,
                "state" => $model->state,
                "zip" => $model->zip,
                "country" => $model->country
            ];
            $result = \chargebee\ChargeBee\Models\Subscription::update($subscriptionId, array('startDate' => 0, 'shippingAddress' => $shippingAddress));
            $this->redirect(['join/subscription']);
        }
        return $this->render('redemesub', ['model' => $model]);
    }

    public function actionPausesub($subscriptionId, $pname) {

        $this->left_menu = 210;

        $model = new \frontend\models\Pausesub();
        $model->subscriptionid = $subscriptionId;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            self::createEnvironment();
            $dd = time();
            $month = $model->months;
            $result = \chargebee\ChargeBee\Models\Subscription::update($subscriptionId, array('cf_cb_is_pause' => 'True', "cf_cb_pause_reason" => $model->reason, 'cf_cb_pause_cancel_date' => $dd, 'cf_pause_months' => $month));
            $currentBillDate = $result->subscription()->nextBillingAt;
            $currentBillDate = strtotime("+$month month", $currentBillDate);
            $result = \chargebee\ChargeBee\Models\Subscription::changeTermEnd($subscriptionId, array("termEndsAt" => $currentBillDate));

            $this->redirect(['join/subscription']);
        }
        return $this->render('pausesub', ['model' => $model, 'pname' => $pname]);
    }

    public function actionResumesub($subscriptionId) {

        $setting = \common\models\Settings::findOne(1);

        $this->left_menu = 210;
        self::createEnvironment();
        $dd = time();
        $result = \chargebee\ChargeBee\Models\Subscription::update($subscriptionId, array('cf_cb_is_pause' => 'False', "cf_cb_pause_reason" => '', 'cf_cb_pause_cancel_date' => $dd));

        $currentBillDate = strtotime(date("Y-m-$setting->billing_day"));
        if ($currentBillDate < time()) {
            $currentBillDate = strtotime("+1 month", $currentBillDate);
        }
        $result = \chargebee\ChargeBee\Models\Subscription::changeTermEnd($subscriptionId, array("termEndsAt" => $currentBillDate));
        $this->redirect(['join/subscription']);
    }

    public function actionReactivatesub($subscriptionId) {
        $this->left_menu = 210;
        $setting = \common\models\Settings::findOne(1);
        self::createEnvironment();
        $currentBillDate = strtotime(date("Y-m-$setting->billing_day"));
        if ($currentBillDate < time()) {
            $currentBillDate = strtotime("+1 month", $currentBillDate);
        }
        $dd = time();
        $result = \chargebee\ChargeBee\Models\Subscription::update($subscriptionId, array('cf_cb_pause_cancel_date' => $dd, 'cf_cb_is_pause' => 'False', "cf_cb_pause_reason" => '', 'cf_cb_cancel_reason' => ''));
        $result = \chargebee\ChargeBee\Models\Subscription::reactivate($subscriptionId, array('trialEnd' => $currentBillDate, 'invoiceImmediately' => false));
        $this->redirect(['join/subscription']);
    }

    public function actionInv() {
        $subs = [];
        $cb_customer_id = '2smoc982QZEqiZYoFN';
        self::createEnvironment();
        $subs = $this->getSubscriptions($cb_customer_id, $subs);
        var_dump($subs);
    }

    private function getSubscriptions($cb_customer_id, $subscriptionList, $offset = null) {
        $subFilter = ["customer_id[is]" => $cb_customer_id];
        if ($offset != null) {
            $subFilter['offset'] = $offset;
        }
        $result = \chargebee\ChargeBee\Models\Subscription::all($subFilter);
        $i = 0;
        foreach ($result as $entry) {
            $subscription = $entry->subscription();
            $subscriptionList[] = $subscription;
        }
        if ($result->nextOffset() !== null) {
            $subscriptionList = $this->getSubscriptions($cb_customer_id, $subscriptionList, $result->nextOffset());
        }
        return $subscriptionList;
    }

    public function actionSubscription() {

        if (Yii::$app->user->isGuest) {
            $this->redirect(['site/login']);
            return;
        }
        $this->mainMenu = 200;
        $this->submenu = 220;
        $account_url = null;
        $user = Yii::$app->user->identity;
        $errorText = "";
        $result = null;
        $subscriptions = array();
        $errorText = "There is no subscription.";
        if ($user->cb_customer_id != null || $user->cb_customer_id != '') {
            self::createEnvironment();
            $subscriptionsList = [];
            $subscriptionsList = $this->getSubscriptions($user->cb_customer_id, $subscriptionsList);

            foreach ($subscriptionsList as $subscription) {
                $allowChange = false;
                $undemed = false;
                $boxType = '';
                $planType = 'Normal Subscription';
                $planName = " - ";
                if ($subscription->planId !== null) {
                    $plan = \chargebee\ChargeBee\Models\Plan::retrieve($subscription->planId);
                    if ($plan != null) {
                        $planName = $plan->plan()->name;
                        $allowChange = $plan->plan()->cfIsGift == 'True' ? false : true;
                        $boxType = $plan->plan()->cfBoxType;
                        $planType = $plan->plan()->cfPlanType;
                    }
                    $paused = $subscription->cfCbIsPause;
                    $paused = $paused == null ? false : ($paused == 'True' ? true : false);
                    $status = $subscription->status;
                    ;
                    $linkedsubStatus = null;
                    if ($planType == 'Send Gift Subscription') {
                        if ($subscription->cfLinkedSubscription !== null) {
                            try {
                                $linkedSub = \chargebee\ChargeBee\Models\Subscription::retrieve($subscription->cfLinkedSubscription);
                                if ($linkedSub !== null) {
                                    $linkedsubStatus = $linkedSub->subscription()->status;
                                }
                            } catch (\chargebee\ChargeBee\Exceptions\InvalidRequestException $e) {
                                
                            }
                        }
                        $linkedsubStatus = $linkedsubStatus == 'future' ? 'Not Redeemed Yet' : 'Redeemed';
                        if ($subscription->status != 'future') {
                            $status = 'non_renewing';
                        }
                    }

                    if ($status != 'cancelled') {
                        $status = $paused == true ? 'paused' : $status;
                    }
                    if ($subscription->status == 'future') {
                        $status = 'redeem';
                    }
                    if ($subscription->status == 'in_trial') {
                        $status = 'active';
                    }

                    $rFName = isset($subscription->cfRecipientFirstName) ? $subscription->cfRecipientFirstName : '';
                    $rLName = isset($subscription->cfRecipientLastName) ? $subscription->cfRecipientLastName : '';
                    $rEmail = isset($subscription->cfRecipientEmailAddress) ? $subscription->cfRecipientEmailAddress : '';
                    $rName = $rFName . ' ' . $rLName;
                    $nextBillAt = isset($subscription->nextBillingAt) ? gmdate("d/m/Y", $subscription->nextBillingAt) : "-Not available-";
                    $subscriptions[] = ['id' => $subscription->id, 'name' => $planName, 'planid' => $subscription->planId, 'planType' => $planType,
                        'boxtype' => $boxType, 'status' => $status, 'currency' => $subscription->currencyCode, 'linkedstatus' => $linkedsubStatus,
                        'price' => $subscription->planUnitPrice, 'renewed' => $subscription->billingPeriodUnit,
                        'renewesMonths' => $subscription->billingPeriod, 'nextBill' => $nextBillAt, 'allowChange' => $allowChange,
                        'ReceipientName' => trim($rName), 'ReceipientEmail' => $rEmail];
                }
            }
        }

        $rs = $this->generateRandomString();
        return $this->render('subscription', ['errorText' => $errorText, 'result' => $subscriptions, 'rs' => $rs]);
    }

    public function actionDonebp() {
        return $this->render('done');
    }

    public function actionBeautyprofile() {
        $this->mainMenu = 200;
        $this->submenu = 230;
        $user = Yii::$app->user->identity;

        $mainData = array();
        if ($user->cb_customer_id != "") {
            $beautyPages = \common\models\Beautyprofilepage::find()->where("active = '1'")->orderBy(['weight' => SORT_ASC])->all();
            foreach ($beautyPages as $beautyPage) {

                $bbquestionsList = \common\models\Beautyprofilequestion::find()->where("active = '1' and page_id = '$beautyPage->id'")->orderBy(['weight' => SORT_ASC])->all();
                $connection = Yii::$app->getDb();
                $qa = array();
                foreach ($bbquestionsList as $bbquestionsItem) {
                    $haveImage = false;
                    $answer = array();
                    if ($bbquestionsItem->type == 'T') {
                        $bbans = \common\models\Beautyprofile::find()->where(" question_id = '$bbquestionsItem->id' and customer_id = '$user->cb_customer_id'")->one();
                        if ($bbans != null) {
                            $answer[] = ['name' => $bbans->answer_text, 'image' => ''];
                        }
                    } elseif ($bbquestionsItem->type == 'D') {
                        $bbans = \common\models\Beautyprofile::find()->where(" question_id = '$bbquestionsItem->id' and customer_id = '$user->cb_customer_id'")->one();
                        if ($bbans != null) {
                            $answer[] = ['name' => $bbans->answer_date, 'image' => ''];
                        }
                    } else {
                        $sql = "SELECT distinct b.id, b.name, b.image FROM beautyprofile a, beautyprofileanswer b "
                                . " WHERE a.answer = b.id AND a.question_id =  '$bbquestionsItem->id' AND a.customer_id =  '$user->cb_customer_id'";
                        $command = $connection->createCommand($sql);
                        $answerList = $command->queryAll();

                        foreach ($answerList as $value) {
                            $answer[] = ['name' => $value['name'], 'image' => $value['image']];
                            $haveImage = $value['image'] == '' ? false : true;
                        }
                    }


                    if (count($answer) > 0) {
                        $qa[] = ['question' => $bbquestionsItem->name, 'answer' => $answer, 'haveImage' => $haveImage];
                    }
                }
                if (count($bbquestionsList) > 0) {
                    $complete = count($qa) == count($bbquestionsList) ? true : false;
                    $page = ['id' => $beautyPage->id, 'name' => $beautyPage->name, 'div_size' => $beautyPage->div_size, 'questions' => $qa, 'complete' => $complete];
                    $mainData[] = $page;
                }
            }
        }
        return $this->render('beautyprofile', ['beautyData' => $mainData]);
    }

    public function actionFeedbackdetail($boxid) {

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        }

        $this->mainMenu = 200;
        $this->submenu = 240;
        $user = Yii::$app->user->identity;
        $allocations = array();
        if ($user->cb_customer_id != "") {
            $allocationDatas = \common\models\Allocation::find()->where("customer_id = '$user->cb_customer_id' and box_id = '$boxid'")->orderBy(['created_when' => SORT_DESC])->one();
            if ($allocationDatas != null) {
                $box = \common\models\Box::findOne($boxid);
                $dateObj = \DateTime::createFromFormat('!m', $box->month);
                $monthName = $dateObj->format('F');
                $name = $box->year . " " . $monthName . ": " . $box->name;
                if ($box != null) {
                    $rated = false;
                    $fb = \common\models\Feedback::find()->where("survey_id = '$box->typeform_code' and customer_id = '$user->cb_customer_id'  and box_id = '$boxid'")->one();
                    if ($fb != null) {
                        $rated = true;
                    }
                    if (trim($box->typeform_code) != '') {
                        $boxurl = $box->typeform_url . '?cs_id=' . $user->cb_customer_id . '&boxid=' . $box->id . "&name=" . $user->fullname;
                        $ratedText = $rated == true ? "BOX IS RATED" : 'GIVE FEEDBACK >';
                        $allocations[] = ['name' => $name, 'image' => $box->image, 'rated' => $rated, 'rate_url' => $boxurl, 'main' => true, 'ratedText' => $ratedText];
                    }
                    $samples = explode("\n", $box->samples);
                    foreach ($samples as $sample) {
                        $sample = trim($sample);
                        $product = \common\models\Product::find()->where("sku = '$sample'")->one();
                        if ($product != null) {
                            $rated = false;
                            $fb = \common\models\Feedback::find()->where("survey_id = '$product->typeform_code' and customer_id = '$user->cb_customer_id'  and box_id = '$boxid'")->one();
                            if ($fb != null) {
                                $rated = true;
                            }
                            if ($product != null) {
                                if (trim($product->typeform_url) != '') {
                                    $boxurl = $product->typeform_url . '?cs_id=' . $user->cb_customer_id . '&boxid=' . $box->id . "&name=" . $user->fullname;
                                    $ratedText = $rated == true ? "SAMPLE IS RATED" : 'GIVE FEEDBACK >';
                                    $allocations[] = ['name' => $product->name, 'image' => $product->image, 'rated' => $rated, 'rate_url' => $boxurl, 'main' => false, 'ratedText' => $ratedText];
                                }
                            }
                        }
                    }
                }
            }
        }
        return $this->render('feedbackdetail', ['allocations' => $allocations]);
    }

    public function actionFeedback($cs_id = '', $survey_id = '', $email = '', $boxid = '') {

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        }
        if ($cs_id != '' && $survey_id != '') {
            JoinController::createFeedBack($survey_id, $cs_id, $email, $boxid);
            JoinController::createRewardPoint($cs_id, JoinController::REVIEWED);

            $rated = $this->isBoxRated($boxid);
            if ($rated == true) {
                return $this->redirect(['join/feedback']);
            }
            $box = \common\models\Box::findOne($boxid);
            if ($box != null) {
                return $this->redirect(['join/feedbackdetail', 'boxid' => $boxid]);
            } else {
                return $this->redirect(['join/feedback']);
            }
        }

        $this->mainMenu = 200;
        $this->submenu = 240;
        $user = Yii::$app->user->identity;
        $allocations = array();
        if ($user->cb_customer_id != "") {
            $allocationDatas = \common\models\Allocation::find()->where("customer_id = '$user->cb_customer_id' ")->orderBy(['created_when' => SORT_DESC])->limit(1)->all();
            foreach ($allocationDatas as $allocationData) {
                $box = \common\models\Box::findOne($allocationData->box_id);
                if ($box == null) {
                    continue;
                }
                $dateObj = \DateTime::createFromFormat('!m', $box->month);
                $monthName = $dateObj->format('F');

                $name = $box->year . " " . $monthName . ": " . $box->name;
                if ($box != null) {

                    $rated = $this->isBoxRated($box->id);
                    $boxurl = \yii\helpers\Url::to(['/join/feedbackdetail', 'boxid' => $box->id]);
                    $allocations[] = ['name' => $name, 'image' => $box->image, 'rated' => $rated, 'rate_url' => $boxurl];
                }
            }
        }
        return $this->render('feedback', ['allocations' => $allocations]);
    }

    private function isBoxRated($boxid) {
        $box = \common\models\Box::findOne($boxid);
        $user = Yii::$app->user->identity;
        $rated = false;
        if ($box != null) {
            $fb = \common\models\Feedback::find()->where("survey_id = '$box->typeform_code' and customer_id = '$user->cb_customer_id' and box_id = '$boxid'")->one();
            if ($fb != null) {
                $rated = true;
            }
            $sampleRate = true;
            $samples = explode("\n", $box->samples);
            foreach ($samples as $sample) {
                $sample = trim($sample);
                $product = \common\models\Product::find()->where("sku = '$sample'")->one();
                if ($product != null) {
                    if (trim($product->typeform_code) != "") {
                        $fb = \common\models\Feedback::find()->where("survey_id = '$product->typeform_code' and customer_id = '$user->cb_customer_id'  and box_id = '$boxid'")->one();
                        if ($fb == null) {
                            $sampleRate = false;
                            break;
                        }
                    }
                }
            }
            if ($sampleRate == true && $rated == true) {
                $rated = true;
            } else {
                $rated = false;
            }
        }
        return $rated;
    }

    public function actionRewardpoint() {
        $this->mainMenu = 200;
        $this->submenu = 250;
        $user = Yii::$app->user->identity;
        $rewardPoints = array();
        $totalPoint = 0;
        if ($user->cb_customer_id != "") {
            $rewardPoints = \common\models\Rewardpoint::find()->where("customer_id = '$user->cb_customer_id'")->orderBy(['create_date' => SORT_DESC])->all();

            foreach ($rewardPoints as $rewardData) {
                $totalPoint += $rewardData->points;
            }
        }
        return $this->render('rewardpoint', ['rewardPoints' => $rewardPoints, 'totalpoint' => $totalPoint]);
    }

    public function actionGethpp($planId, $coupon = '', $giftfname = null, $giftlname = null, $giftemail = null, $addon = null, 
            $utm_campaign = null, $utm_source = null, $utm_medium = null, $utm_content = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = array();
        $customerArr = array();
        $user = Yii::$app->user->identity;
        if ($user->cb_customer_id != null || $user->cb_customer_id != '') {
            $customerArr['id'] = $user->cb_customer_id;
        } else {
            $customerArr = ['first_name' => $user->f_name, 'last_name' => $user->l_name, 'email' => $user->email, 'phone' => $user->mobile];
        }
        $subscriptionArr = ["planId" => $planId, 'coupon' => $coupon];
        if ($giftfname != null) {
            $subscriptionArr['cf_recipient_first_name'] = $giftfname;
        }
        if ($giftlname != null) {
            $subscriptionArr['cf_recipient_last_name'] = $giftlname;
        }
        if ($giftemail != null) {
            $subscriptionArr['cf_recipient_email_address'] = $giftemail;
        }

        if ($utm_campaign != null) {
            $subscriptionArr['cf_utm_campaign'] = $utm_campaign;
        }
        if ($utm_source != null) {
            $subscriptionArr['cf_utm_source'] = $utm_source;
        }
        if ($utm_medium != null) {
            $subscriptionArr['cf_utm_medium'] = $utm_medium;
        }
        if ($utm_content != null) {
            $subscriptionArr['cf_utm_content'] = $utm_content;
        }
        $addOnsArr = null;
        if ($addon != null) {
            $addons = explode(",", $addon);
            foreach ($addons as $value) {
                $addOnsArr[] = ['id' => $value];
            }
        }
        self::createEnvironment();
        $result2 = \chargebee\ChargeBee\Models\HostedPage::CheckoutNew(
                        array("subscription" => $subscriptionArr,
                            "customer" => $customerArr,
                            "addons" => $addOnsArr,
                            "embed" => "true",
                            "iframeMessaging" => "true"
        ));
        $settings = \common\models\Settings::findOne(1);
        $siteName = $settings->cb_name;
        $rp = $result2->hostedPage()->url;
        $result = array("url" => $rp,
            "hosted_page_id" => $result2->hostedPage()->id,
            "site_name" => $siteName);

        return $result;
    }

    public function actionGetcbsession() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = array();
        $user = Yii::$app->user->identity;
        if ($user->cb_customer_id != null || $user->cb_customer_id != '') {
            self::createEnvironment();
            $return_url = \yii\helpers\Url::to(['site/index'], true);
            $result2 = \chargebee\ChargeBee\Models\PortalSession::create(array("redirectUrl" => $return_url, "customer" => array("id" => $user->cb_customer_id)));
            $result['access_url'] = $result2->portalSession()->accessUrl;
            $result['token'] = $result2->portalSession()->token;
        }
        return $result;
    }

    public static function createEnvironment() {
        $settings = \common\models\Settings::findOne(1);
        $siteName = $settings->cb_name;
        $siteApi = $settings->cb_api;
        \chargebee\ChargeBee\Environment::configure($siteName, $siteApi);
    }

    public static function createCBSession() {
        $session = Yii::$app->session;
        $user = Yii::$app->user->identity;

        if ($user->cb_customer_id != null || $user->cb_customer_id != '') {
            self::createEnvironment();

            $cbLogin = true;
            if ($cbLogin == true) {
                $return_url = \yii\helpers\Url::to(['site/index'], true);
                $result = \chargebee\ChargeBee\Models\PortalSession::create(array("redirectUrl" => $return_url, "customer" => array("id" => $user->cb_customer_id)));

                $session->set('cb_portal', true);
                $session->set('cb_token', $result->portalSession()->token);
                $session->set('cb_accessUrl', $result->portalSession()->accessUrl);
                $session->set('cb_sessionId', $result->portalSession()->id);
            }
            return true;
        } else {
            return false;
        }
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
