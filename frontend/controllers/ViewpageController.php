<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\data\Pagination;
use common\config\Controller2;
use common\models\User;

/**
 * Site controller
 */
class ViewpageController extends Controller2 {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
        ],
      ],
    ];
  }

  public function actionIndex($id,$layout='main_empty') {
    $landignPage = \backend\models\Landingpages::findOne($id);
    if ($landignPage == null) {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
    if ($landignPage->status == 'inactive') {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
    $this->layout = $layout;
    $css ='';
    
    if($landignPage->add_standalonecss == '1'){
      $settings = \common\models\Settings::findOne(1);
      $css = $settings->standalone_css;
    }
    $divs = \backend\models\Divs::find()->where("landingpage_id = '$id' and status = 'active'")->orderBy(['weight'=>SORT_ASC])->all();
    return $this->render('index', ['title' => $landignPage->header, 'divs' => $divs, 'css' => $css]);
  }
  public function actionIndex2($id)
  {
    return $this->actionIndex($id,'main');
  }

}
