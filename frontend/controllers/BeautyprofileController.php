<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\data\Pagination;
use common\config\Controller2;
use common\models\User;
use common\models\Beautyprofile;
use common\models\Beautyprofileanswer;
use common\models\Beautyprofilequestion;
use common\models\Beautyprofilepage;

/**
 * Site controller
 */
class BeautyprofileController extends Controller2 {

  public $mainMenu = 100;

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
// 'logout' => ['post'],
        ],
      ],
    ];
  }

  private function createBP($pageId, $customerId, $questionId, $answerId, $answerText=null, $answerDate=null) {
    if (is_array($answerId)) {
      foreach ($answerId as $ans) {
        $this->createBP($pageId, $customerId, $questionId, $ans);
      }
    }
    else {
      
      $bp = new Beautyprofile();
      $bp->page_id = $pageId;
      $bp->customer_id = $customerId;
      $bp->question_id = $questionId;
      $bp->answer = $answerId;
      $bp->answer_text = $answerText;
      $bp->answer_date = $answerDate;
      $bp->save();
    }
  }

  public function actionThankyou2() {
    return $this->redirect(['/beautyprofile/thankyou2']);
  }
  
  public function actionThankyou($price='',$currency='',$plantype='',$planname='', $hostedpageid ='') {
    $bp = Beautyprofilepage::find()->where("active = '1'")->orderBy(['weight' => SORT_ASC])->limit(1)->one();
    return $this->redirect(['/beautyprofile/update', 'pageid' => $bp->id, 'thankyoupage' => true,'price'=>$price, 'currency'=>$currency, 'plantype'=>$plantype,'planname'=>$planname, 'hostedpageid'=>$hostedpageid]);
  }

  public function actionUpdate($pageid, $thankyoupage = false,$price='',$currency='',$plantype='',$planname='', $hostedpageid ='', $firstpage = false) {
    $this->left_menu = 220;
    if(Yii::$app->user->isGuest){
      return $this->redirect(['/site/index']);
    }
    $customerId = Yii::$app->user->identity->cb_customer_id;
    $previous = 0;
    $next = 0;
    $pages = Beautyprofilepage::find()->where("active = '1'")->orderBy(['weight' => SORT_ASC])->all();

    $back = 0;
    $currentChoosen = false;
    foreach ($pages as $page) {
      if ($currentChoosen == true) {
        $next = $page->id;
        break;
      }
      if ($page->id == $pageid) {
        $currentChoosen = true;
        $previous = $back;
      }
      $back = $page->id;
    }
    $questions = Beautyprofilequestion::find()->where("page_id = '$pageid'")->orderBy(['weight' => SORT_ASC])->all();
    if ($next > 0) {
      $nquestions = Beautyprofilequestion::find()->where("page_id = '$next'")->count();
      if ($nquestions == 0) {
        $next = 0;
      }
    }
    $questionAnswers = Beautyprofile::findAll("page_id = '$pageid' and customer_id = '$customerId' ");

    if (Yii::$app->request->post()) {
      $responce = Yii::$app->request->post();
      if (isset($responce['Beautyprofile'])) {
        $answers = $responce['Beautyprofile'];
        if (count($answers) > 0) {
          Beautyprofile::deleteAll("page_id = '$pageid' and customer_id = '$customerId' ");
          foreach ($answers as $key => $answer) {
            if ($answer['type'] == 'T') {
              $this->createBP($pageid, $customerId, $key, null, $answer['answer_text'], null);
            }
            elseif ($answer['type'] == 'D') {
              $this->createBP($pageid, $customerId, $key, null, null, $answer['answer_date']);
            }
            else {
              $this->createBP($pageid, $customerId, $key, $answer['answer'], null, null);
            }
          }
        }
      }
      $pageid = $responce['type'] == 'c' ? $next : $previous;
      if ($pageid != 0) {
        return $this->redirect(['/beautyprofile/update', 'pageid' => $pageid]);
      }
      else {
        return $this->redirect(['/join/donebp']);
      }
    }
    $hpResult = null;
        //get new subscription info from chargebee // for analysis like rekutin , fb pixels
        if ($firstpage == true) {
            if ($hostedpageid != '') {
                try {
                    JoinController::createEnvironment();
                    $hpResult = \chargebee\ChargeBee\Models\HostedPage::retrieve($hostedpageid);
                    $subscrition = $hpResult->subscription();
                } catch (\chargebee\ChargeBee\Exceptions\InvalidRequestException $e) {
                    
                }
            }
        }
    $pagename = $thankyoupage == true ? 'thankyou' : 'beautyprofile';
    return $this->render($pagename, ['hpResult'=> $hpResult, 'pageid' => $pageid, 'questions' => $questions, 'questionAnswers' => $questionAnswers, 'customerId' => $customerId, 'previous' => $previous, 'next' => $next,'price'=>$price, 'currency'=>$currency, 'plantype'=>$plantype,'planname'=>$planname,'hostedpageid'=>$hostedpageid]);
  }

  protected function findModel($id) {
    if (($model = Beautyprofilepage::findOne($id)) !== null) {
      return $model;
    }
    else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
