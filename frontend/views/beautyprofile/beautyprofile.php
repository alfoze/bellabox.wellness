<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Beauty Profile');
?>


<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="row body_1 das_man" > 
        <div class="col-md-2"> </div>
           
            <div class="col-md-8">
                <div class="clearfix margin-bottom-30"></div>
                <?=$this->render('/beautyprofile/_formbeautyprofile',['pageid' => $pageid, 'questions' => $questions, 'questionAnswers' => $questionAnswers, 'customerId' => $customerId, 'previous' => $previous, 'next' => $next,'showtitle'=>true])?>
                <div class="clearfix margin-bottom-50"></div>
            </div>
            <div class="col-md-2"> </div> 
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>