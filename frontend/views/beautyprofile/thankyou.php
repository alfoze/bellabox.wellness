<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Thank You');

$price;
$currency;
$plantype;
?>
<!-- Js for facebook pixel -->
<!--Starting Rekutin Tag-->
<?= $this->render('/beautyprofile/_rekutin', ['hpResult' => $hpResult]); ?>
<!--Ending Rekutin Tag-->
<div class="row main_body">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2">

            </div>
            <div class="col-md-8">

                <?php if ($plantype != 'Normal Subscription') { ?>
                    <h2><img src="img/tick.png" />Thank you for your gift purchase!</h2>
                    <div class="clearfix margin-bottom-30"></div>
                    <p>We will be in touch shortly with details of how the lucky recipient redeems this gift to start their Wellness journey.</p>
                    <div class="clearfix margin-bottom-100"></div>
                    <?php
                } else {
                    ?>
                    <h2><img src="img/tick.png" />Welcome to Wellness! Thank you for your purchase.</h2>
                    <div class="clearfix margin-bottom-30"></div>
                   
                    <?php // $this->render('/beautyprofile/_formbeautyprofile', ['pageid' => $pageid, 'questions' => $questions, 'questionAnswers' => $questionAnswers, 'customerId' => $customerId, 'previous' => $previous, 'next' => $next, 'showtitle' => false]) ?>
                <?php } ?>
                <div class="clearfix margin-bottom-50"></div>
            </div>
            <div class="col-md-2">

            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>