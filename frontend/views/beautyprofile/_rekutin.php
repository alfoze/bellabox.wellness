<?php
if ($hpResult != null) {
    try {
        $hpResult = $hpResult->hostedPage()->content();
        //print_r($hpResult); 
        $subscription = $hpResult->subscription();
        $shippingInfo = $subscription->shippingAddress;
        $customer = $hpResult->customer();
        $discountCode = '';
        $discountAmount = 0;
        $priceBtax = $subscription->planUnitPrice / 100;
        $tax = 0;
        $invoice = $hpResult->invoice();
        $promoCode = '';
        if ($invoice != null) {
            $tax = $invoice->tax / 100;
            $planId = $subscription->planId;
            $planName = $subscription->planId;
            $discounts = $invoice->discounts;
            if (isset($discounts[0])) {
                $discountCode = $discounts[0]->entityId;
                $discountAmount = $discounts[0]->amount;
                $discountAmount = $discountAmount / 100;
            }
            if (isset($invoice->lineItems[0])) {
                $lineItem = $invoice->lineItems[0];
                $planId = $lineItem->entityId;
                $planName = $lineItem->description;
            }
            if (isset($invoice->discounts[0])) {
                $discountItem = $invoice->discounts[0];
                $promoCode = $discountItem->entityId;
            }
        }
        $priceAtax = $priceBtax - $tax;
    } catch (\chargebee\ChargeBee\Exceptions\InvalidRequestException $e) {
        return;
    }
    ?>
    <!-- Adding values to datalayer -->
    <script type="text/javascript">

        shippingAddress = {
        "country": "<?= $shippingInfo->country ?>",
                "administrative_area": "<?= $shippingInfo->state ?>",
                "sub_administrative_area": null,
                "locality": "<?= $shippingInfo->city ?>",
                "dependent_locality": "",
                "postal_code": "<?= $shippingInfo->country ?>",
                "thoroughfare": "<?= $shippingInfo->line1 ?>",
                "premise": "",
                "sub_premise": null,
                "organisation_name": "<?= $shippingInfo->company ?>",
                "name_line": "<?= $customer->firstName . " " . $customer->lastName ?>",
                "first_name": "<?= $customer->firstName ?>",
                "last_name": "<?= $customer->lastName ?>",
                "data": null
        };
        products = [
        {
        "id": "<?php echo $planId ?>",
                "sku": "<?php echo $planId ?>",
                "name": "<?php echo $planName ?>",
                "price": <?php echo $priceBtax ?>,
                "currency": "<?php echo $subscription->currencyCode ?>",
                "quantity": <?php echo $subscription->planQuantity ?>,
                "category": "Box",
                "product_type": "box_subscription",
                "photo": ""
        }
        ];
        order_data = {
        "userId": "<?php echo $subscription->customerId ?>",
                "category": "CheckoutStep",
                "checkout_step": "complete",
                "path": "",
                "event": "Completed Order",
                "order_id": "<?php echo $subscription->id ?>",
                "orderId": "<?php echo $subscription->id ?>",
                "currency": "<?php echo $subscription->currencyCode ?>",
                "order_currency": "<?php echo $subscription->currencyCode ?>",
                "product_ids": "<?php echo $planId ?>",
                "products": products,
                "total": <?php echo $priceBtax ?>,
                "revenue": <?php echo $priceAtax ?>,
    <?php if ($promoCode != '') { ?>
            "discount" :<?= $discountAmount ?>,
                    "promocode": "<?= $promoCode ?>",
                    "returnUrl": window.location.href,
    <?php } ?>
        "shipping_address": shippingAddress
        };
        integrations = {
        "All": false,
                "Google Tag Manager ": true,
                "Twitter Ads": true
        };
        visitor_info_landing = {
        "utm_campaign": "",
                "utm_source": "",
                "utm_medium": "",
                "utm_content": "",
                "request_url": "",
                "created": 0,
                "cookie_data": []
        };
        visitor_info_current = {
        "utm_campaign": "na",
                "utm_source": "na",
                "utm_medium": "na",
                "utm_content": "na",
                "cookie_data": [],
                "request_url": "",
                "created": "<?= $subscription->createdAt ?> "
        };
        dataLayer.push({
        "userId": "<?php echo $subscription->customerId ?>",
                "category": "CheckoutStep",
                "checkout_step": "complete",
                "path": "",
                "event": "Completed Order",
                "order_id": "<?php echo $subscription->id ?>",
                "orderId": "<?php echo $subscription->id ?>",
                "currency": "<?php echo $subscription->currencyCode ?>",
                "order_currency": "<?php echo $subscription->currencyCode ?>",
                "product_ids": "<?php echo $planId ?>",
                'products': products,
                "total": <?php echo $priceBtax ?>,
                "revenue": <?php echo $priceAtax ?>,
    <?php if ($promoCode != '') { ?>
            "discount" :<?= $discountAmount ?>,
                    "promocode": "<?= $promoCode ?>",
                    "returnUrl": window.location.href,
    <?php } ?>
        'shipping_address': shippingAddress,
                'order_data': order_data,
                'integrations' :integrations,
                'visitor_info_landing' : visitor_info_landing,
                'visitor_info_current' :visitor_info_current
        });

    </script>
    <!-- END values to datalayer  -->
<?php } ?>
