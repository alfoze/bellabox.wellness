<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Beautyprofilepage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="beautyprofilepage-form">

    <?php if ($showtitle == true) { ?>
      <h2>UPDATE YOUR BEAUTY PROFILE</h2>
      <?php
    }
    else {
      echo '<br>';
    }
    ?>

    <?php $form = ActiveForm::begin(['action' => ['beautyprofile/update', 'pageid' => $pageid], 'id' => 'beautyprofile_post', 'method' => 'post',]); ?>
    <?php
    foreach ($questions as $question) {

      $answers = array();
      foreach ($question->answers as $answer) {
        $answers[$answer->id] = $answer;
      }
      $answer = \yii\helpers\ArrayHelper::map($answer, 'id', 'name');
      $userAnswers = common\models\Beautyprofile::find()->select(['answer', 'answer_text', 'answer_date'])->where("question_id = '$question->id' and customer_id = '$customerId' ")->asArray()->all();

      $ua = array();
      $uaText = '';
      $uaDate = null;
      foreach ($userAnswers as $userAnswer) {
        $ua[] = $userAnswer['answer'];
        $uaText = $userAnswer['answer_text'];
        $uaDate = $userAnswer['answer_date'];
      }

      $qa = new common\models\Beautyprofile();
      $qa->answer = $ua;
      $qa->answer_text = $uaText;
      $qa->answer_date = $uaDate == null ? null : date("d/m/Y", strtotime($uaDate));
      
      $qa->type = $question->type;
      ?>
      <div class="row">
          <div class="col-md-12 margin-bottom-100">
              <p style="font-size: 17px;">  <?= $question->name ?> 
              <?php if($question->required == 1){echo '   <span style="font-weight:600;color:'.Yii::$app->params['bb_rouge'].'">*</span>';}?>
              </p>
              <?php if ($question->type == 'R' || $question->type == 'C') { ?>
                <div style="padding-bottom: 15px;text-align: center;"></div>
                  <?php } ?>
              <div style ="text-align:center">
                  <?= $form->field($qa, "[$question->id]type")->hiddenInput()->label(false); ?>
                  <?php
                  
                  if ($question->type == 'T') {
                    if($question->required == 1)
                    {
                      $qa->setScenario($qa::ANSTXT_REQUIRED);
                    }
                    echo $form->field($qa, "[$question->id]answer_text")->textInput(['maxlength' => true])->label(false);
                  }
                  elseif ($question->type == 'D') {
                    if($question->required == 1)
                    {
                      $qa->setScenario($qa::ANSDATE_REQUIRED);
                    }
                    echo $form->field($qa, "[$question->id]answer_date")->widget(kartik\date\DatePicker::classname(), [
                      'options' => ['placeholder' => 'Enter date ..'],
                      'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd/mm/yyyy'
                      ]
                    ])->label(false);
                  }
                  elseif ($question->type == 'R') {
                    if($question->required == 1)
                    {
                      $qa->setScenario($qa::ANS_REQUIRED);
                    }
                    echo $form->field($qa, "[$question->id]answer")->radioList($answers, [
                      'item' => function($index, $label, $name, $checked, $value) {

                        if ($label->image) {
                          $return = '<div class="col-md-2" style="margin-right:20px;display:inline-block;float:none; vertical-align:top;">
                            <div class="imageToggle" onclick="toggleImage(this);">';
                          if ($checked) {
                            $return .= '<input class="imageCheckbox" type="radio" " name="' . $name . '" value="' . $value . '" ' . ($checked ? 'checked' : '') . '>
                                <img class="toggleImage selectedImage" src="' . $label->image . '" />';
                          }
                          else {
                            $return .= '<input class="imageCheckbox" type="radio" " name="' . $name . '" value="' . $value . '" ' . ($checked ? 'checked' : '') . '>
                                <img class="toggleImage" src="' . $label->image . '" />';
                          }
                          //<input class="imageCheckbox" type="radio" id="'.$name.'" name="'.$name.'" />
                          //<img class="toggleImage" src="'.$label->image.'" />
                          $return .= '</div>
                            <div style="text-align:center">' . $label->name . '</div>
                            <div style="text-align:center" class="desc-text">' . $label->description . '</div>
                            </div>';
                        }
                        else {
                          $return = '<div class="col-md-2" style="margin-right:20px;display:inline-block;float:none; vertical-align:top;">
                            <div class="imageToggle" onclick="toggleImage(this);">';
                          if ($checked) {
                            $return .= '<input class="imageCheckbox" type="radio" " name="' . $name . '" value="' . $value . '" ' . ($checked ? 'checked' : '') . '>
                                <div  style="display: inline-grid;" class="round-text toggleImage selectedImage">' . $label->name . '</div>';
                          }
                          else {
                            $return .= '<input class="imageCheckbox" type="radio" " name="' . $name . '" value="' . $value . '" ' . ($checked ? 'checked' : '') . '>
                                <div style="display: inline-grid;" class="round-text toggleImage">' . $label->name . '</div>';
                          }
                          //<input class="imageCheckbox" type="radio" id="'.$name.'" name="'.$name.'" />
                          //<div  class="round-text toggleImage">' . $label->name . '</div>
                          $return .= '</div>
                          <div style="text-align:center" class="desc-text">' . $label->description . '</div>
                            </div>';

                          /* $return = '<label  class="top-level">';
                            $return .= $label->image == "" ? "" : '<img  src="' . $label->image . '" /><br>';
                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"' . ($checked ? 'checked' : '') . '>';
                            $return .= '<i></i> ';
                            $return .= '<div class="round-text">' . $label->name . '</div>';
                            $return .= '<div  class="desc-text">' . $label->description . '</div>';
                            $return .= '</label>'; */
                        }



                        return $return;
                      }
                    ])->label(false);
                  }
                  else {
                    if($question->required == 1)
                    {
                      $qa->setScenario($qa::ANS_REQUIRED);
                    }
                    echo $form->field($qa, "[$question->id]answer")->checkboxList($answers, [
                      'item' => function($index, $label, $name, $checked, $value) {


                        if ($label->image) {
                          $return = '<div class="col-md-2" style="margin-right:20px;display:inline-block;float:none; vertical-align:top;">
                            <div class="imageToggle" onclick="toggleImage(this);">';
                          if ($checked) {
                            $return .= '<input class="imageCheckbox" type="checkbox" " name="' . $name . '" value="' . $value . '" checked >
                                <img class="toggleImage selectedImage" src="' . $label->image . '" />';
                          }
                          else {
                            $return .= '<input class="imageCheckbox" type="checkbox" " name="' . $name . '" value="' . $value . '">
                                <img class="toggleImage" src="' . $label->image . '" />';
                          }
                          //<input class="imageCheckbox" type="checkbox" id="'.$name.'" name="'.$name.'" />
                          //<img class="toggleImage" src="'.$label->image.'" />
                          $return .= '</div>
                            <div  style="text-align:center">' . $label->name . '</div>
                            <div style="text-align:center" class="desc-text">' . $label->description . '</div>
                            </div>';
                        }
                        else {
                          $return = '<div class="col-md-2" style="margin-right:20px;display:inline-block;float:none; vertical-align:top;">
                            <div class="imageToggle" onclick="toggleImage(this);">';
                          if ($checked) {
                            $return .= '<input class="imageCheckbox" type="checkbox" " name="' . $name . '" value="' . $value . '" checked >
                                <div  class="round-text toggleImage selectedImage">' . $label->name . '</div>';
                          }
                          else {
                            $return .= '<input class="imageCheckbox" type="checkbox" " name="' . $name . '" value="' . $value . '" >
                                <div  class="round-text toggleImage">' . $label->name . '</div>';
                          }
                          //<input class="imageCheckbox" type="checkbox" id="'.$name.'" name="'.$name.'" />
                          //<div  class="round-text toggleImage">' . $label->name . '</div>
                          $return .= '</div>
                          <div style="text-align:center" class="desc-text">' . $label->description . '</div>
                            </div>';

                          /* $return = '<label  class="top-level" >';
                            $return .= $label->image == "" ? "" : '<img  src="'. $label->image . '" /><br>';
                            $return .= '<input type="checkbox" " name="' . $name . '" value="' . $value . '" ' . ($checked ? 'checked' : '') . '>';
                            $return .= '<i></i> ';
                            $return .= '<div  class="round-text">' . $label->name . '</div>';
                            $return .= '<div  class="desc-text">' . $label->description . '</div>';
                            $return .= '</label>'; */
                        }




                        return $return;
                      }
                    ])->label(false);
                  }
                  ?>
              </div>
          </div>
      </div>
      <?php
    }
    ?>
    <div class="form-group">

        <?php
        if ($previous > 0) {
          echo Html::submitButton(Yii::t('app', 'Back'), ['class' => 'btn-u btn-u-sm btn-u-bb_blue', 'style' => 'padding: 10px 30px 10px 30px;float:left', 'name' => "type", "value" => "b"]);
        }
        ?>

        <?php
        echo Html::submitButton(Yii::t('app', $next > 0 ? 'Continue' : 'Done'), ['class' => 'btn-u btn-u-sm btn-u-bb_rouge', 'style' => 'padding: 10px 30px 10px 30px;float:right', 'name' => "type", "value" => "c"]);
        ?>
    </div>
<?php ActiveForm::end(); ?>

</div>
<style>
    .top-level{
        border: 2px solid #00B0E8;
        padding-left: 10px;
        padding-right: 10px;
        padding-bottom: 10px;

    }
    .round-text{
        width: 140px;
        height: 140px;
        display: grid;
        align-items: center;
        vertical-align: middle;
        text-align:center;
        border: 7px solid #eee;
        border-radius: 50%;
    }
    .desc-text{
        font-size: 12px;
        font-weight: 200;
    }
    .imageCheckbox {
        display: none;
    }
    .imageToggle{
        margin-bottom: 20px;
    }
    .imageToggle .toggleImage {
        /* default/unselected image styles here */

        width: 140px;
        height: 140px;

        /* text-align: center; */
        padding: 2px;
        /* vertical-align: middle; */
        /*line-height: 120px;
            display: block;*/


    }
    .imageToggle .toggleImage:hover{
        border: 7px solid #49c2c9;
        border-radius: 50%;
    }
    .imageToggle .selectedImage {
        /* selected image styles here */
        border: 7px solid #49c2c9;
        border-radius: 50%;
    }
    .imageToggle .selectedImage:hover {
        /* selected image styles here */
        opacity: 0.5;
    }

</style>    
<script type="text/javascript">
  function toggleImage(containerElem) {

      //toggle the checkbox value
      var checkBox = containerElem.getElementsByClassName("imageCheckbox");
      var image = containerElem.getElementsByClassName("toggleImage");
      var type = $(checkBox).attr("type");
      if (type == "radio")
      {
          var div = $(checkBox).parent().parent().parent().closest('div');
          div.find(".toggleImage").each(function () {
              $(this).removeClass("selectedImage");
          });
          div.find(".imageCheckbox").each(function () {
              $(this).attr('checked', false);
          });
      }


      var check = $(checkBox).attr("checked") ? 1 : 0;
      if (check == true)
      {
          $(checkBox).attr('checked', false);
          $(image).removeClass("selectedImage");
      } else {
          $(checkBox).attr('checked', true);
          $(image).addClass("selectedImage");
      }


  }
</script>