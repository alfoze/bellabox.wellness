<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Thank You');
?>


<div class="row main_body">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2">
                
            </div>
            <div class="col-md-8">
                <h1><img src="img/tick.png" />Well Done! You've made it.</h1>
                <div class="clearfix margin-bottom-30"></div>
                <h3>You have successfully subscribe for a gift. An invitation has bee send to the gift receiver.</h3>
                
                <div class="clearfix margin-bottom-50"></div>
            </div>
            <div class="col-md-2">
                
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>