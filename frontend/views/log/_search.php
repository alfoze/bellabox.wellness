<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\LogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'option_type') ?>

    <?= $form->field($model, 'option_id') ?>

    <?= $form->field($model, 'operation') ?>

    <?= $form->field($model, 'c_date') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn-u btn-u-success']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn-u btn-u-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
