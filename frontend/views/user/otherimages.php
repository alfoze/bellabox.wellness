<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$own = false;
?>
<?php
$galaries = common\models\Gallery::findAll(["option_id" => $id, "option_type" => \common\config\Controller2::USERS]);


if (count($galaries) == 0 && $own == false)
    return;
?>
<div class="row">
    <div class="col-md-6">
        <div class = "headline margin-bottom-35"><h4><?= Yii::t('app', 'Profile Images') ?></h4></div>
    </div>
</div>
<?php
if (count($galaries) == 0)
    return;
?>

<div class="row">
    <!-- Begin Easy Block --> 
    <?php
    $counter = 0;
    foreach ($galaries as $galary) {
        $counter++;
        $oimg = Yii::$app->getUrlManager()->getBaseUrl()."/".@$galary->image_path;
    ?>
        <div class="col-md-3 col-sm-6 col-xm-12 md-margin-bottom-40" style="padding-right: 10px;padding-left: 0px;padding-bottom: 10px;padding-top: 10px;">
            <div class="thumbnails thumbnail-style" >
                <div >
                    <a href = "<?php echo $oimg ?>" rel = "gallery" class = "fancybox img-hover-v1" title = "Image">
                        <span>
                            <img class = "img-responsive" style = "min-height:160px ; max-height:160px; " src = "<?php echo $oimg ?>" class = "img_adverting imag_recent_bus" />
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
