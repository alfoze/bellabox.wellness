<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="row ">
            <div class="col-md-3">
                <?= $this->render('/site/left_menu'); ?>
            </div>
            <div class="col-md-9">
                <?= $this->render('/site/flash'); ?>
                <div class="headline">
                    <h2> <?= $this->title ?></h2>
                </div>
                <div class="unames-update">
                    <?= $this->render('_form', [ 'model' => $model,]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
