<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */

$this->title = Yii::t('app', 'My Location');
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->getAssetManager()->bundles['dosamigos\google\maps\MapAsset']['options']['language'] = Yii::$app->language ;
?>
<div class="row">
    <div class="col-md-12">
        <div class="row ">
            <div class="col-md-3">
                <?= $this->render('/site/left_menu'); ?>
            </div>
            <div class="col-md-9">
                <?= $this->render('/site/flash'); ?>
                <div class="headline">
                    <h2> <?= $this->title ?></h2>
                </div>
                <div class="unames-update">
                    <div class="unames-form">
                        <p><?= Yii::t('app', 'Drag and drop the balloon on your office location.') ?></p>
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                        <div class="row">

                            <div class="col-md-6">
                                <?= $form->field($model, 'lat')->hiddenInput()->label(false) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'lng')->hiddenInput()->label(false) ?> 
                            </div>
                        </div>

                        <?php
                        if ($model->lat == 0) {
                            $companyInfo = \common\models\Cities::findOne($model->city);
                            $latitude = $companyInfo->lat;
                            $longitude = $companyInfo->lng;
                        } else {
                            $latitude = $model->lat;
                            $longitude = $model->lng;
                        }
                        $def_zoom = 12;
                        $coord = new LatLng(['lat' => $latitude, 'lng' => $longitude]);
                        $map = new Map([
                            'center' => $coord,
                            'zoom' => $def_zoom,
                            'width' => "100%",
                            'height' => "500"
                        ]);

                        $marker = new Marker([
                            'position' => $coord,
                            'draggable' => true,
                            'title' => $model->fullname,
                        ]);
                        $map->addOverlay($marker);
                        $event = new \dosamigos\google\maps\Event(["trigger" => "dragend",
                            "js" => "document.getElementById('user-lat').value = event.latLng.lat();document.getElementById('user-lng').value = event.latLng.lng();"]);

                        $marker->addEvent($event);

                        echo $map->display();
                        ?>

                        <br>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn-u btn-u-success' : 'btn-u btn-u-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
