<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Unames */

$this->title = Yii::t('app', 'Create User Defined Names');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Defined Names'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unames-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
