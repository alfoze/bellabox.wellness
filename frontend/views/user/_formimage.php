<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Countries;
use yii\helpers\Url;
use budyaga\cropper\Widget;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */
/* @var $form yii\widgets\ActiveForm */
$colname = 'name';
Yii::setAlias('budyaga', '@vendor/budyaga');
$noimage = 10;
$rmaining = $noimage - count($g_model);
?>


<div class="unames-form">

    <div class ="headline"><h4><?= Yii::t('app', 'Profile Images') ?></h4></div>

    <div class ="row">
        <?php foreach ($g_model as $img) { ?>

            <div class="col-md-3" style="margin-bottom: 10px;">
                <img class="img-responsive" src="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/' . $img->image_path; ?>" style="max-height:100px;" />
                <br>     
                <?=
                Html::a(Yii::t('app', 'Delete'), ['/gallery/delete', 'id' => $img->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this image?'),
                        'method' => 'post',
                    ],
                ])
                ?>
                <br> 
            </div>
        <?php } ?>
    </div>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <p>
        <?= Yii::t('app', 'You can upload maximum {num} images. Maximum size of image should not be more than 1MB', ['num' => $noimage]); ?>
    </p>
    <p>
        <?= Yii::t('app', 'Please add picture of  800 X 500 px'); ?>
    </p>
    <br>
    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <?php
    if ($rmaining > 0) {
        echo \kato\DropZone::widget([

            'options' => [
                'maxFilesize' => '1',
                'maxFiles' => $rmaining,
                'acceptedFiles' => "image/jpeg,image/png,image/gif",
                'url' => Url::to(['gallery/upload', 'id' => $model->id, 'type' => 13]),
                'dictDefaultMessage' => Yii::t('app', 'Drop files here to upload'),
            ],
            'clientEvents' => [
                'complete' => "function(file){console.log(file)}",
                'removedfile' => "function(file){alert(file.name + ' is removed')}"
            ],
        ]);
    }
    ?>
    <br>

    <div class ="headline"><h4><?= Yii::t('app', 'Profile Picture') ?></h4></div>
    <div class="row">

        <div class="col-md-6">
            <p>
                <?= Yii::t('app', 'Please add picture of  80 X 80 px'); ?>
            </p>
            <?=
            $form->field($model, 'pic')->widget(Widget::className(), [
                'uploadUrl' => Url::toRoute('/user/uploadPhoto'),
                'width' => 150,
                'height' => 150,
                'cropAreaWidth' => 200,
                'cropAreaHeight' => 200,
            ])
            ?>
        </div>
        <div class="col-md-6">
            <?=
                    $form->field($model, 'b_img')->label(null, ['class' => 'btn-u btn-u-success'])
                    ->fileInput(['class' => 'sr-only', 'onchange' => 'readURL_Image(this);', 'target' => 'image_upload_preview1', 'accept' => 'image/*'])
            ?>

            <?= Yii::t('app', 'Recommended size is 900px(width) X 150px(height)'); ?>
            <img id="image_upload_preview1" src="" class="img-responsive image_upload_preview" alt="your image" style="display: none;" />
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <img id="image_upload_preview" src="" class="img-responsive image_upload_preview" alt="your image" style="display: none;" />
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <hr>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn-u btn-u-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
