<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Category;
use common\models\Subcategory;
use common\models\Businessdropdown;
use common\models\Countries;
use dosamigos\tinymce\TinyMce;

//use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model \common\models\Business */
/* @var $form yii\widgets\ActiveForm */
$noimage = 10;
$rmaining = $noimage - count($g_model);
?>
<?= $this->render('/site/flash'); ?>

<div class="business-form">
    <h3><?= $model->fullname ?></h3>
    <p>
        <?= Yii::t('app', 'You can upload maximum {num} images. Maximum size of image should not be more than 1MB', ['num' => $noimage]); ?>
    </p>
    <br>

    <div class ="row">
        <?php foreach ($g_model as $img) { ?>

            <div class="col-md-3" style="margin-bottom: 10px;">
                <img class="img-responsive" src="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/' . $img->image_path; ?>" style="max-height:100px;" />
                <br>     
                <?=
                Html::a(Yii::t('app', 'Delete'), ['/gallery/delete', 'id' => $img->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this image?'),
                        'method' => 'post',
                    ],
                ])
                ?>
                <br> 
            </div>
        <?php } ?>
    </div>


    <?php
    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
    ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'f_name')->textInput(['readonly' => 'readonly'])->label(false) ?>


    <?php
    if ($rmaining > 0) {
        echo \kato\DropZone::widget([

            'options' => [
                'maxFilesize' => '1',
                'maxFiles' => $rmaining,
                'acceptedFiles' => "image/jpeg,image/png,image/gif",
                'url' => Url::to(['gallery/upload', 'id' => $model->id, 'type' => 13]),
                'dictDefaultMessage' => Yii::t('app', 'Drop files here to upload'),
            ],
            'clientEvents' => [
                'complete' => "function(file){console.log(file)}",
                'removedfile' => "function(file){alert(file.name + ' is removed')}"
            ],
        ]);
    }
    ?>
    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn-u btn-u-success chk_bus' : 'btn-u btn-u-success chk_bus        ']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
