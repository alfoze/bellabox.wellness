<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */

$this->title = Yii::t('app', 'User');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="row ">
            <div class="col-md-3">
                <?= $this->render('/site/left_menu'); ?>
            </div>
            <div class="col-md-9">
                <?= $this->render('/site/flash'); ?>
                <div class="headline">
                    <h2> <?= $this->title ?></h2>
                </div>
                <div class="unames-view">

                    <?php
                    $bImage = $model->b_img == "" ? "images/backbanar.jpg" : $model->b_img;
                    $cImage = $model->pic == "" ? "images/user.png" : $model->pic;
                    ?>
                    <div id='cover_container' style="background:url('<?= common\config\Options::getFrontendAddress() . $bImage; ?>');">
                        <a class="btn-u pull-right" style="margin-right: 20px;margin-top: 20px;"  href="<?= Yii::$app->urlManager->createUrl(["/user/updateimage", 'id' => $model->id]) ?>"><?= Yii::t('app', 'Update Profile Pictures') ?></a>

                        <div id='info_box'>
                            <div id="profile_img"><img src="<?= common\config\Options::getFrontendAddress() . $cImage; ?>" class='avatar_img'/></div>
                            <div id="info-box" style="<?= common\config\Options::getLangDir() == 'rtl' ? 'right: 180px;' : '' ?>">
                                <div id="info-name"><h3><?= $model->fullname ?></h3></div> 
                            </div>
                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-10">
                            <?php
                            if ($model->account_type == '0') {

                                echo Html::a(Yii::t('app', 'Become Service Provider'), ['/user/serviceprovider', 'id' => $model->id], [
                                    'class' => 'btn-u btn-u-blue pull-right',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to become Service Provider?'),
                                    ],
                                ]);
                            }
                            ?>
                        </div>                             

                        <div class="col-md-2">

                            <a class="btn-u pull-right"  href="<?= Yii::$app->urlManager->createUrl(["/user/update", 'id' => $model->id]) ?>"><?= Yii::t('app', 'Update') ?></a>
                        </div>
                    </div>

                    <?php
                    if (common\config\Options::getIsServiceProvider())
                        echo $this->render('/site/_stars', ['score' => $model->rating, 'reviews' => $model->reveiws]);
                    ?>                    
                    <div class = "headline margin-bottom-5"><h4><?= $model->getAttributeLabel('notes') ?></h4></div>

                    <div class="row">
                        <div class="col-md-6">
                            <p><?= nl2br($model->notes) ?></p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'username',
                                    'email',
                                    [
                                        'attribute' => 'account_type',
                                        'value' => common\config\Options::getAccountTypeById($model->account_type)
                                    ],
                                    [
                                        'attribute' => 'is_active',
                                        'value' => common\config\Options::getYesNoById($model->is_active)
                                    ],
                                    [
                                        'attribute' => 'lang',
                                        'value' => \common\models\Languages::findOne($model->lang)->name
                                    ],
                                    [
                                        'attribute' => 'showrate',
                                        'visible' => $model->account_type == '1'
                                    ],
                                ],
                            ])
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [

                                    'mobile',
                                    'phone',
                                    'address',
                                    [
                                        'attribute' => 'city',
                                        'value' => $model->city == "" ? null : Yii::t('app', \common\models\Cities::findOne($model->city)->name)
                                    ],
                                    [
                                        'attribute' => 'country',
                                        'value' => $model->country == "" ? null : Yii::t('app', \common\models\Countries::findOne($model->country)->name)
                                    ],
                                    'device',
                                ],
                            ])
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn-u pull-right"  href="<?= Yii::$app->urlManager->createUrl(["/user/updatelocation", 'id' => $model->id]) ?>"><?= Yii::t('app', 'Update Location') ?></a>
                        </div>
                    </div>
                    <?= $this->render('/user/showmap', ['model' => $model]); ?>

                    <?= $this->render('/user/otherimages', ['id' => $model->id,'own'=>true]); ?>
                </div>
            </div>
        </div>
    </div>
</div>



<style>
    #container{
        margin:0 auto;
        width:100%;
        height: 100px;
    }

    #container-content{
        border:solid 1px #cccccc;
        padding:20px;
        width:100%;

        color:#cccccc;
    }

    #info_box{
        padding:60px 50px 0px 10px;
        border:solid 1px #dedede;
        width:538px;
        background-color:#fcfcfc;
        margin-top:88px;
    }

    #profile_img{
        background-color: #FFFFFF;
        border: 1px solid #DEDEDE;
        margin-left: 10px;
        margin-top: -100px;
        position: absolute;
        width: 86px;
        height: 86px;
        float:left;
    }

    .avatar_img{
        padding:3px;
        width: 85px;
        height: 85px;
    }

    #cover_container{
        overflow: auto;
        width: 100%;
        height:150px;

    }

    #info-box{
        margin-left:115px;
        margin-top: -50px;
        position: absolute;
    }

    #info-name{
        float:left;
        overflow:hidden;
        word-wrap: break-word;
        width:400px;
    }

    #info-content{
        margin-left:170px;
        width:290px;
    }

    #info-photos{
        text-align:center;
        font-size:11px;
        padding:5px;
        float:left;
        width:80px;
        border:solid 1px #eeeeee;
        -moz-border-radius: 3px;-webkit-border-radius: 3px;border-radius: 3px;
    }

    #info-friends{
        text-align:center;
        font-size:11px;
        padding:5px;
        margin-left:100px;
        border:solid 1px #eeeeee;
        width:80px;
        -moz-border-radius: 3px;-webkit-border-radius: 3px;border-radius: 3px;
    }
</style>