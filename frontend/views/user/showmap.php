<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

Yii::$app->getAssetManager()->bundles['dosamigos\google\maps\MapAsset']['options']['language'] = Yii::$app->language;
if (!isset($height)) {
    $height = 500;
}
?>

<?php

$showmakr = false;
if ($model->lat == 0) {
    $companyInfo = \common\models\Cities::findOne($model->city);
    if ($companyInfo != null) {
        $latitude = $companyInfo->lat;
        $longitude = $companyInfo->lng;
    } else {
        $latitude = Yii::$app->params['lat'];
        $longitude = Yii::$app->params['lng'];
    }
} else {
    $showmakr = true;
    $latitude = $model->lat;
    $longitude = $model->lng;
}

$def_zoom = 12;
$coord = new LatLng(['lat' => $latitude, 'lng' => $longitude]);
$map = new Map([
    'center' => $coord,
    'zoom' => $def_zoom,
    'width' => "100%",
    'height' => $height,
        //'language' => Yii::$app->language
        ]);

// Lets add a marker now
$marker = new Marker([
    'position' => $coord,
    'title' => $model->fullname,
        ]);
$map->addOverlay($marker);

echo $map->display();
?>