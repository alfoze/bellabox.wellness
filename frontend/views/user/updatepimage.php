<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\Business */

$this->title = Yii::t('app','Profile Images');
?>
<div class="business-update">
    <div class="row">
        <div class="col-md-3">
            <?= $this->render('/site/left_menu'); ?>  

        </div>
        <div class="col-md-9 m_top tag-box tag-box-v3 form-page">
            <div class="headline"><h2><?= $this->title ?></h2></div>
                    <?=
                    $this->render('_imgform', [
                        'model' => $model,
                        'g_model' => $g_model,
                    ])
                    ?>
        </div></div>


</div>
