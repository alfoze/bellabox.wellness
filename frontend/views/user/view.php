<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use common\models\Favoriteadd;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */

$this->title = Yii::t('app', 'View Profile');
$this->params['breadcrumbs'][] = $this->title;
$style = "margin: auto; width: 100%; padding: 0px;";
?>
<div class="row">
    <div class="col-md-12">
        <div class="row ">
            <div class="col-md-11">
                <div class="headline">
                    <h2> <?= $this->title ?></h2>
                </div>
                <?= $this->render('/site/flash'); ?>

                <div class="unames-view">

                    <?php
                    $bImage = $model->b_img == "" ? "images/backbanar.jpg" : $model->b_img;
                    $cImage = $model->pic == "" ? "images/user.png" : $model->pic;

                    $this->params['fbImage'] =  "/". $cImage;
                    $this->params['fbDesc'] = nl2br($model->notes);
                    $this->params['fbTitle'] = $model->fullname;
                    ?>
                    <div id='cover_container' style="background:url('<?= common\config\Options::getFrontendAddress() . $bImage; ?>');">

                        <div id='info_box'>
                            <div id="profile_img"><img src="<?= common\config\Options::getFrontendAddress() . $cImage; ?>" class='avatar_img'/></div>
                            <div id="info-box" style="<?= common\config\Options::getLangDir() == 'rtl' ? 'right: 180px;' : '' ?>" >
                                <div id="info-name">
                                    <h3 style= "margin-bottom: 0px;"><?= $model->fullname ?></h3>
                                </div> 

                                <div class="row">
                                    <div class="col-md-6">
                                        <?= common\config\Options::getAccountTypeById($model->account_type) ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php
                                        $tcolor = Yii::$app->params['themeColor'];
                                        $favorite = Favoriteadd::find()->where(['option_id' => $model->id, 'option_type' => \common\config\Controller2::USERS, 'user_id' => Yii::$app->user->id])->one();
                                        if ($favorite != NULL) {
                                            echo Html::a('<i class="tooltips fa fa-star pull-right" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . Yii::t('app', 'Un Favourite') . '" style="cursor:pointer;color: ' . $tcolor . ' ;"></i>', ['user/addfavorite', 'id' => $model->id]);
                                        } else {
                                            echo Html::a('<i class="tooltips fa fa-star-o pull-right" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . Yii::t('app', 'Favourite') . '" style="cursor:pointer;color: ' . $tcolor . ' ;"></i>', ['user/addfavorite', 'id' => $model->id]);
                                        }
                                        ?>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <?php
                    if (common\config\Options::getIsServiceProvider()) {
                        echo $this->render('/site/_stars', ['score' => $model->rating, 'reviews' => $model->reveiws]);
                    }

                    $city = $model->city == "" ? null : Yii::t('app', \common\models\Cities::findOne($model->city)->name);
                    $country = $model->country == "" ? null : Yii::t('app', \common\models\Countries::findOne($model->country)->name);
                    ?>
                    <div class = "headline margin-bottom-5"><h4><?= $model->getAttributeLabel('notes') ?></h4></div>

                    <div class="row">
                        <div class="col-md-6">
                            <p><?= nl2br($model->notes) ?></p>

                            <br>
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fa fa-user pull-right"></i>
                                </div>
                                <div class="col-md-3">
                                    <b>
                                        <?= Yii::t('app', 'Contact') . ':' ?>
                                    </b>
                                </div>
                                <div class="col-md-5">   
                                    <?= $model->name; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fa fa-map-marker  pull-right"></i>
                                </div>
                                <div class="col-md-3">
                                    <b>
                                        <?= Yii::t('app', 'Location') . ":" ?>
                                    </b>
                                </div>
                                <div class="col-md-5"> 
                                    <?= $model->address . ' , ' . $city . ' , ', $country; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fa fa-phone  pull-right"></i>
                                </div>
                                <div class="col-md-3">
                                    <b>
                                        <?= Yii::t('app', 'Phone') . ":" ?>
                                    </b>
                                </div>
                                <div class="col-md-5"> 
                                    <?= $model->phone; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fa fa-mobile-phone  pull-right"></i>
                                </div>
                                <div class="col-md-3">
                                    <b>
                                        <?= Yii::t('app', 'Mobile') . ":" ?>
                                    </b>
                                </div>
                                <div class="col-md-5"> 
                                    <?= $model->mobile; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fa fa-envelope  pull-right"></i>
                                </div>
                                <div class="col-md-3">
                                    <b>
                                        <?= Yii::t('app', 'Email') . ":" ?>
                                    </b>
                                </div>
                                <div class="col-md-5"> 
                                    <?= $model->email; ?>
                                </div>
                            </div>
                            <?php if ($model->account_type == '1') { ?>
                                <div class="row">
                                    <div class="col-md-1">
                                        <i class="fa fa-dollar  pull-right"></i>
                                    </div>
                                    <div class="col-md-3">
                                        <b>
                                            <?= Yii::t('app', 'Average Rate') . ":" ?>
                                        </b>
                                    </div>
                                    <div class="col-md-5"> 
                                        <?= $model->showrate; ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <br>
                        </div>
                        <div class="col-md-6">
                            <?= $this->render('/user/showmap', ['model' => $model, 'height' => 250]); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?= $this->render('/user/otherimages', ['id' => $model->id]); ?>
<?= $this->render('/business/otherbusiness', ['business' => $business, 'style' => $style]); ?>
