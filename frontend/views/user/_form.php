<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Countries;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */
/* @var $form yii\widgets\ActiveForm */
$colname = 'name';
$mobileMask2 = '';
$mobileMask = $mobileMask2 == "--UNKNOW--"?"":$mobileMask2;
?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'f_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <br>
            <?= $form->field($model, 'is_active')->checkbox() ?>
        </div>
        <div class="col-md-3">
            <?php if ($mobileMask == "") { ?>
                <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])?>
            <?php } else { ?>
                    <?= $form->field($model, 'mobile')->widget(yii\widgets\MaskedInput::className(), ['mask' => $mobileMask]) ?>
                <?php } ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'r_currency')->dropDownList(common\config\Options::getCurrencies()); ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?=
                $form->field($model, 'country')->dropDownList(\common\config\Options::getCountries(), ['prompt' => 'Select Country',
                    'onchange' => '
                        $.get( "' . Url::toRoute('/site/citylist') . '", { cid: $(this).val() } )
                            .done(function( data ) {
                                $( "#user-city" ).html( data );
                            }
                        );
                    '])
                ?>
            </div>
            <div class="col-md-3">
                <?php
                $cities = array();

                if ($model->country != "") {
                    $cities = \common\config\Options::getCities($model->country);
                }
                echo $form->field($model, 'city')->dropDownList($cities, ['prompt' => 'Select City']);
                ?>
            </div>

        </div>
        <div class="row">

            <div class="col-md-6">
                <?=
                $form->field($model, 'business_name')->textInput(['maxlength' => true]);
                ?>
            </div>

            <div class="col-md-3">
                <?php
                $pkgs = \common\config\Options::getLanguages();
                echo $form->field($model, 'lang')->dropDownList(ArrayHelper::map($pkgs, 'id', 'name'));
                ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'avg_rate')->textInput(['maxlength' => true]);
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <?=
                $form->field($model, 'notes')->textarea(['rows' => 10, 'maxlength' => true]);
                ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn-u btn-u-success' : 'btn-u btn-u-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

</div>
