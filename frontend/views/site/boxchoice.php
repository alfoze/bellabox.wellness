<?php

use yii\helpers\html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Unames */
/* @var $form yii\widgets\ActiveForm */
$plans = common\models\Plans::find()->where("active = '1'")->all();
$boxChoices = array();
foreach ($plans as $value) {
  $boxChoices[$value->name] = $value->description;
}


?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row" style="background: #2d0e2d;background-image: url(img/banar1.png);background-repeat: no-repeat;height: 550px">
    <div class="col-md-9">
    </div>
    <div class="col-md-3" style = "color:#fff; padding-top: 20px;padding-left: 30px;margin-top: 300px; background: #2d0e2d">
        
        <?= $form->field($model, 'name')->radioList($boxChoices, [
                                'item' => function($index, $label, $name, $checked, $value) {

                                    $return = '<label class="modal-radio" style="margin-bottom: 10px;">';
                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3" '.($checked?'checked':'').'>';
                                    $return .= '<i></i> ';
                                    $return .= '<span style="color: white; font-size:15px;">' . ucwords($label) . '</span>';
                                    $return .= '</label><br>';

                                    return $return;
                                }
                            ])->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Buy Now'), ['class' => 'btn-u btn-u-success']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
</div>
