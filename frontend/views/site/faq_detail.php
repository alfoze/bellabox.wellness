<?php
/* @var $this yii\web\View */
$this->title = Yii::t('app', 'FAQ');
?>


<!-- 7th row main body -->

<div class="row main_body">
    <div class="col-md-1"></div>
    <div class="col-md-10">

        <!--Main body Content -->

        <div class="row body_1">

            <div class="col-md-8">


                <!-- Browse Business For Sale By Industry -->

                <div class="row business_box" style="min-height: 400px; margin-top: 10px">
                    <div class="col-md-12">


                        <div class="row" style="padding-top: 2%">
                            <div class="col-md-2">
                                <b>Question</b>
                            </div>
                            <div class="col-md-10">


                                <a href="#" class="faq_question"><?= $f_detail->question; ?></a>
                                <br>

                            </div>
                        </div>

                        <div class="row" style="padding-top: 2%">
                            <div class="col-md-2">
                                <b>Answer</b>
                            </div>
                            <div class="col-md-10">

                                <p>
                                    <?= $f_detail->answer; ?>

                                </p>    
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <?= $this->render("/site/advertize"); ?>                
                <div class="clearfix margin-bottom-20"></div>
                <?= $this->render("/site/emailalert"); ?>
                <div class="clearfix margin-bottom-20"></div>
                <?= $this->render("/site/communityside"); ?>
                <div class="clearfix margin-bottom-20"></div>

            </div>

        </div>


    </div>
    <div class="col-md-1"></div>
</div>