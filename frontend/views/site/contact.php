<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = Yii::$app->params['name'] . ' '.'Contact Us';
?>

<div class="row">
    <div class="col-md-12">
        <div class="headline"><h2><?= Yii::t('app', 'Contact Us') ?></h2></div>
        <div class="row margin-bottom-30">
            <div class="col-md-8 mb-margin-bottom-30">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'subject') ?>
                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                <?=
                $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                  'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ])
                ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn-u btn-u-success', 'name' => 'contact-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>

            </div><!--/col-md-9-->

            <div class="col-md-4">
                <!-- Contacts -->
                <div class="headline"><h2><?= \Yii::t('app', 'General Query') ?></h2></div>
                <ul class="list-unstyled who margin-bottom-30">
                    <li><i class="fa fa-user"></i><?= $cs_model->g_name; ?></li>
                    <li><i class="fa fa-envelope"></i><?= $cs_model->g_email; ?></li>
                    <li><i class="fa fa-phone"></i><?= $cs_model->g_phone; ?></li>
                    <li><i class="fa fa-info"></i><?= $cs_model->g_info; ?></li>
                </ul>

                <div class="headline"><h2><?= \Yii::t('app', 'Advertisement Query') ?></h2></div>
                <ul class="list-unstyled who margin-bottom-30">
                    <li><i class="fa fa-user"></i><?= $cs_model->a_name; ?></li>
                    <li><i class="fa fa-envelope"></i><?= $cs_model->a_email; ?></li>
                    <li><i class="fa fa-phone"></i><?= $cs_model->a_phone; ?></li>
                    <li><i class="fa fa-info"></i><?= $cs_model->a_info; ?></li>
                </ul>

                <div class="headline"><h2><?= \Yii::t('app', 'Headquaters') ?></h2></div>
                <ul class="list-unstyled who margin-bottom-30">
                    <li><i class="fa fa-user"></i><?= $cs_model->h_name; ?></li>
                    <li><i class="fa fa-envelope"></i><?= $cs_model->h_email; ?></li>
                    <li><i class="fa fa-phone"></i><?= $cs_model->h_phone; ?></li>
                    <li><i class="fa fa-info"></i><?= $cs_model->h_info; ?></li>
                </ul>

            </div><!--/col-md-3-->
        </div><!--/row-->
    </div>
</div>

