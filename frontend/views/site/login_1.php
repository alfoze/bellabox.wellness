<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
$this->title = Yii::$app->params['name'];
?>
<div class="reg-block">

    <div class="reg-block-header">
        <h2 class="text-center"><?= Yii::t('app', 'Sign In') ?></h2>
        <ul class="social-icons text-center">
            <li><a class="rounded-x social_facebook auth-link" data-original-title="Facebook" 
                   href="<?= Yii::$app->urlManager->createUrl(['/site/auth', 'authclient' => 'facebook']) ?>"></a></li>

            <li><a class="rounded-x social_googleplus auth-link" data-original-title="Google Plus" 
                   href=" <?= Yii::$app->urlManager->createUrl(['/site/auth', 'authclient' => 'google']) ?>"></a></li>
            <li><a class="rounded-x social_twitter auth-link" data-original-title="Twitter" 
                   href=" <?= Yii::$app->urlManager->createUrl(['/site/auth', 'authclient' => 'twitter']) ?>"></a></li>

        </ul>
        <p class="text-center"><?= Yii::t('app', "Don't Have Account? Click") ?> <a class="color-green" href="<?= Yii::$app->urlManager->createUrl("/site/signup"); ?>"><?= Yii::t('app', 'Sign Up') ?> </a> <?= Yii::t('app', 'to register.') ?></p>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'login-form_1', 'options' => ['class' => 'reg-page']]);
    ?>
    <label for="exampleInputEmail1"><?= Yii::t('app', "User Name") ?></label>
    <div class="input-group margin-bottom-20">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        <?= $form->field($model, 'username')->label(false) ?>
    </div>
    <label for="exampleInputEmail1"><?= Yii::t('app', "Password") ?></label>
    <div class="input-group margin-bottom-20">
        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
        <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
    </div>
    <p id="errs" style="color:red;"></p>
    <div class="row">
        <div class="col-md-8 checkbox">
            <label><?= $form->field($model, 'rememberMe')->checkbox() ?></label>
        </div>
        <div class="col-md-4">
            <?= Html::Button(Yii::t('app', 'Login'), ['class' => 'btn-u btn-u-success login_button', 'name' => 'button', 'id' => 'loginbtn']) ?>
        </div>
    </div>

    <div style="color:#999;margin:1em 0">
        <?= Yii::t('app', "If you forgot your password you can") ?> <?= Html::a(Yii::t('app', 'reset it'), ['site/request-password-reset']) ?>.
    </div>

    <?php ActiveForm::end(); ?>
</div>
<script>

    $('.form-control').keypress(function (e) {
        if (e.which == 13) {
            $('#loginbtn').click();
            return false;    //<---- Add this line
        }
    });

    $('.login_button').click(function () {
        $('#errs').text('');
        var username = $("input[name='LoginForm[username]']", "#login-form_1").val();
        var password = $("input[name='LoginForm[password]']", "#login-form_1").val();
        $("input[name='LoginForm[username]']", "#login-form_1").css('border', ' 1px solid #ccc');
        $("input[name='LoginForm[password]']", "#login-form_1").css('border', ' 1px solid #ccc');
        if (username == '' && password == '')
        {
            $("input[name='LoginForm[username]']", "#login-form_1").css('border', '1px solid red');
            $("input[name='LoginForm[password]']", "#login-form_1").css('border', '1px solid red');
            return false;
        } else if (username == '' || username == null)
        {
            $("input[name='LoginForm[username]']", "#login-form_1").css('border', '1px solid red');
            return false;
        } else if (password == '' || password == null)
        {
            $("input[name='LoginForm[password]']", "#login-form_1").css('border', '1px solid red');
            return false;
        } else
        {

            var csrfToken = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '<?php echo yii\helpers\Url::to(['site/check_login']) ?>',
                type: 'post',
                data: $('#login-form_1').serialize(),
                success: function (data) {
                    if (data == '1')
                    {
                        location.reload();
                    } else
                    {
                        $('#errs').text('<?= \Yii::t('app', 'Incorrect username or password.'); ?>');
                        return false;
                    }
                }
            })

        }
    });
</script>
<style>
    @media all and (max-width: 500px) {
        .modal-content {width: 100% !important;  margin-left: 5%;margin-right: 5%}
    }

    @media only screen and (min-width: 700px) and (max-device-width: 1400px) 
    {
        .modal-content {width: 60% !important; margin-left: auto;margin-right: auto}
    }
    .modal-body { margin-top: -5%;}
    .close {position: relative; z-index: 1;} 
    .btn-facebook {
        background-color: #4863ae;
        border-color: #4863ae;
        color: white;
        margin-left: 2%;
        margin-right: 2%;
        padding: 1% 11% 2% 7%;
    }
    .btn-googleplus {
        background-color: #dd4a38;
        border-color: #dd4a38;
        color: white;
        margin-left: 2%;
        margin-right: 2%;
        padding: 1% 11% 2% 7%;

    }
    #loginform-username { margin-top: -2%;}

    #loginform-password { margin-top: -2%;}
    @media all and (max-width: 500px) {
        #loginform-username { margin-top: -10px;}

        #loginform-password { margin-top: -10px;}
    }
    @media only screen and (min-width: 501px) and (max-device-width: 800px) 
    {
        #loginform-username { margin-top: -3%;}

        #loginform-password { margin-top: -3%;}

    }
</style>