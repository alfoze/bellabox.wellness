<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */
//
$this->title = 'Signup';
$mobileMask = '';
?>
<div class="clearfix margin-bottom-30"></div>
<div class="site-signup">
    <div class="row">
        <div class="col-md-3"></div>

        <div class="col-md-6 business_box">
            <div class="reg-page">
                <div class="reg-header">
                    <h2 class="text-center"><?= Yii::t('app', 'Register a new account') ?></h2>
                    <p class="text-center">
                        <?= Yii::t('app', 'Already Signed Up? Click') ?>  
<!--                        <a href="#" class="showModalButton color-green" value="<?= Yii::$app->urlManager->createUrl("/site/login"); ?>">-->
                        <a href="<?= Yii::$app->urlManager->createUrl("/site/login"); ?>" class="color-green">
                            <?= Yii::t('app', 'Login'); ?>
                        </a>
                        <?= Yii::t('app', 'to login your account.') ?>
                    </p>
                </div>
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'email') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'f_name') ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'l_name') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                    <div class="col-sm-6">
                        <label><?= Yii::t('app', 'Confirm Password') ?></label>
                        <input type="password" name="c_type" id="c_type" class="form-control" value="" />
                        <p id="c_pas" style="color: #a94442;"></p>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-12">
                        <?php if ($mobileMask == "") { ?>
                            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
                            <?php
                        } else {
                            ?>
                            <?= $form->field($model, 'mobile')->widget(yii\widgets\MaskedInput::className(), ['mask' => $mobileMask]) ?>
                        <?php } ?>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        By continuing , you are agreeing to the <a href="https://bellabox.com.au/terms-and-conditions" class="color-grey">Terms and Conditions</a> and the <a href="https://bellabox.com.au/privacy-policy" class="color-grey">Privacy Policy</a>.
                        <br><br><br>
                        <?= Html::submitButton(Yii::t('app', 'CREATE ACCOUNT'), ['class' => 'button button_small', 'name' => 'signup-button', 'style' => 'width:100%;padding:10px']) ?>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

<style>
    .help-block-error {
        color: black !important;
    }
</style>