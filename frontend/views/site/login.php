<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
//
//$this->params['breadcrumbs'][] = $this->title;
$this->title = Yii::t('app', 'Login');
?>
<div class="clearfix margin-bottom-30"></div>
<div class="site-signup">

    <div class="row">

        <div class="col-md-3"></div>

        <div class="col-md-6 business_box">
            <div class="reg-page">
                <div class="reg-header">
                    <h2 class="text-center"><?= Yii::t('app', 'Login with existing account') ?></h2>
                    <p class="text-center"><?= Yii::t('app', "Don't Have Account? Click") ?> <a class="color-green" href="<?= Yii::$app->urlManager->createUrl("/site/signup"); ?>"><?= Yii::t('app', 'Sign Up') ?> </a> <?= Yii::t('app', 'to register.') ?></p>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'login-form_1', 'options' => ['class' => 'reg-page']]);
                ?>
                <label for="exampleInputEmail1"><?= Yii::t('app', "Email Address") ?></label>
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'username')->label(false) ?>
                    </div>
                </div>
                <label for="exampleInputEmail1"><?= Yii::t('app', "Password") ?></label>
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
                    </div>
                </div>
                <p id="errs" style="color:red;"></p>
                <div class="row">
                    <div class="col-md-8 checkbox">
                        <label><?= $form->field($model, 'rememberMe')->checkbox() ?></label>
                        <br>
                        <?= Yii::t('app', "") ?> <?= Html::a(Yii::t('app', 'Forgot your password?'), ['site/request-password-reset'], ['style' => 'color:#555']) ?>.

                    </div>
                    <div class="col-md-4">
                        <?= Html::submitButton(Yii::t('app', 'LOGIN'), ['class' => 'button button_small', 'style' => 'float:right']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
<div class="clearfix margin-bottom-40"></div>
<style>
    .help-block-error {
        color: black !important;
    }
</style>