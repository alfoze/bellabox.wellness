<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'About');
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    

    <div class="row" style="margin-left: -4%;">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h1><?= Yii::t('app', 'Need Help Detail') ?></h1> 
            <br>
            <br>
            <a href=""><?= $model->question ?></a>
            <hr>
            <br>
            <p>
                <?= $model->answer; ?>
            </p>
            <?php DetailView::widget([
        'model' => $model,
        'attributes' => [
            'question',
            'answer:ntext',
        ],
    ]) ?>    
        </div>
        <div class="col-md-1"></div>
    </div>

   
</div>
