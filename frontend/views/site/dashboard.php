<?php
/* @var $this yii\web\View */
$this->title = Yii::$app->params['name'] . ' ' . Yii::t('app', 'Dashboard');
$setting = common\models\Settings::findOne(1);
?>

<div style="padding-top: 15px;padding-bottom: 100px;text-align:center;background-image : url('img/banners/dash.jpg');" >
    <h4><strong>CUSTOMER & BILLING INFO</strong></h4>
    <strong>Welcome  <?= strtoupper(Yii::$app->user->identity->fullname) ?>!</strong>
</div>

<div class="row main_body">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2"> </div>
            <div class="col-md-8">
                <div class="clearfix margin-bottom-50"></div>


                <?php if ($cbAccessUrl == null) { ?>
                  <p>There is no subscription for this customer.</p>
                  <?php
                }
                else {
                  ?>
                  <div class="col-md-3"></div> 
                  <div class="col-md-9">
                      <p>Customer & Billing Info page allows you to update your customer details such as shipping address as well as your payment method details</p>
                      <a href="#" class ="button button_small" style="text-align: center" onclick="subscribeResponsePopupHandler()" >Go to my info</a>
                      <br>
                  </div>

                <?php } ?>
                <div class="clearfix margin-bottom-100"></div>
                <div class="clearfix margin-bottom-100"></div>
                <div class="clearfix margin-bottom-100"></div>
            </div>
        </div>
    </div>
</div>

​​<script src="https://js.chargebee.com/v2/chargebee.js"></script>
<script type="text/javascript">

                        function subscribeResponsePopupHandler() {
                            var chargebeeInstance = Chargebee.init({
                                site: "<?= $setting->cb_name ?>",
                                portalSession: function () {
                                    return $.ajax({
                                        url: "<?= yii\helpers\Url::to(['join/getcbsession']); ?>"
                                    });
                                }
                            });

                            var cbPortal = chargebeeInstance.createChargebeePortal();
                            cbPortal.open({
                                "close": function () {
                                    redirectCall()
                                }
                            });
                            return;



                        }
                        function redirectCall() {
                            window.location.href = "<?= Yii::$app->homeUrl ?>";

                        }
</script>
