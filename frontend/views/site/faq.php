<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'FAQ';
?>
<div class="headline"><h2><?= \Yii::t('app', 'FAQ') ?></h2></div>
<!-- 7th row main body -->
<div class="row">
    <div class="col-md-9">
        <!-- General Questions -->
        <div class="panel-group acc-v1 margin-bottom-40" id="accordion">
            <?php
            if (!empty($faq)) {
                $i = 0;
                foreach ($faq as $model):
                    $i++;
                    $f_detail = \common\models\Faq::findOne(['id' => $model->id]);
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $i; ?>">
                                    <?= $i; ?>. <?= Yii::t('app', $model->question )?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse<?= $i; ?>" class="panel-collapse collapse <?php if ($i == 1) { ?> in <?php } ?>">
                            <div class="panel-body">
                                <?= nl2br(Yii::t('app',$f_detail->answer)); ?>   
                            </div>
                        </div>
                    </div>
                    <?php
                endforeach;
            }
            ?>
        </div><!--/acc-v1-->
    </div><!--/col-md-9-->
</div>

<style>
    #accordion { margin-top: 6px;}
</style>