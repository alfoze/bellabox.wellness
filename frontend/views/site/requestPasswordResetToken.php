<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = Yii::t('app', 'Request Password Reset');
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 business_box" style="padding-left: 5%">
            <br><br>
            <h1 style="font-size:20px;"><?= Html::encode($this->title) ?></h1>
            <?php if (Yii::$app->session->getFlash('s_mesage')) { ?>
              <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?= Yii::$app->session->getFlash('s_mesage'); ?>
              </div>
            <?php } ?>
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
            <div class="row">
                <div class="col-md-6">
                    <p><?= Yii::t('app', 'Please fill out your email. A link to reset password will be sent in email.') ?></p>                    
                    <?= $form->field($model, 'email') ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'RESET PASSWORD >'), ['class' => 'btn-u btn-u-bb_rouge']) ?>
                    </div>                    
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row" style="margin-bottom:40px "></div>
</div>
<style>
    #passwordresetrequestform-email { width: 155%; border-radius: 0px}
    .help-block-error { width:142%;}
</style>