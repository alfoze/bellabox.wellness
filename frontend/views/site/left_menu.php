<?php 
use common\config\Options;
//if (Yii::$app->user->isGuest)
    return;
?>


<?php
$left_menu = array();

array_push($left_menu, ['id' => 200, 'name' => Yii::t('app', 'Customer & Billing Info'), 'url' => Yii::$app->urlManager->createUrl("/site/dashboard"), 'visible' => true]);
array_push($left_menu, ['id' => 210, 'name' => Yii::t('app', 'Subscription Operations'), 'url' => Yii::$app->urlManager->createUrl("/join/subscription"), 'visible' => true]);
array_push($left_menu, ['id' => 220, 'name' => Yii::t('app', 'Beauty Profile'), 'url' => Yii::$app->urlManager->createUrl("/join/beautyprofile"), 'visible' => true]);
array_push($left_menu, ['id' => 230, 'name' => Yii::t('app', 'Feedback'), 'url' => Yii::$app->urlManager->createUrl("/join/feedback"), 'visible' => true]);
array_push($left_menu, ['id' => 240, 'name' => Yii::t('app', 'Reward Points'), 'url' => Yii::$app->urlManager->createUrl("/join/rewardpoint"), 'visible' => true]);

?>
<div class="clearfix margin-bottom-40"></div>
<div style="height: 200px;">
    <div class="text-center" style="width:90%; position: absolute; padding-top: 120px">
        <h3 class="text-center" style="color:white"><?= strtoupper(Yii::$app->user->identity->fullname)?></h3>
    </div>
    <img src="img/dashbg.png" style="width: 100%; float: left;  ">
    
</div>
<ul class="list-group sidebar-nav-v1" id="sidebar-nav">
    <?php
    $i = 0;
    foreach ($left_menu as $left) {
        $i++;
        $class = "list-group-item list-toggle";
        $in = '';
        if ($this->context->left_menu == $left['id']) {
            $class = "list-group-item list-toggle active";
        }
        if ($left['visible'] == true) {
            echo '<li class="' . $class . '">';
            echo '<a href="'.$left['url'].'">';
            echo $left['name'];
            echo '</a>';
        }
        echo '</li>';
    }
    ?>
</ul>

<style>
    #sidebar-nav { margin-top: 5% !important;}
</style>
<div class="clearfix margin-bottom-100"></div>