<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php if (Yii::$app->session->getFlash('s_mesage')) { ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?= Yii::$app->session->getFlash('s_mesage'); ?>
    </div>
<?php } ?>
<?php if (Yii::$app->session->getFlash('s_error')) { ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?= Yii::$app->session->getFlash('s_error'); ?>
    </div>
<?php } ?>