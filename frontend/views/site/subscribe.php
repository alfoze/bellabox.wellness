<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<!-- subscribe_block -->
<div class="subscribe_block">
    <div class="wrapper">
        <h4>Subscribe to our newsletter to stay up to date</h4>
        <?php $form = ActiveForm::begin(['id' => 'bellabox-mailchimp-subscribe-form', 'action' => ['site/subsnewsletter'], 'method' => 'post', 'enableAjaxValidation' => true]); ?>
        <ul>
            <li style="padding-bottom: 0px"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder'=> "First name"])->label(false) ?></li>
            <li style="padding-bottom: 0px"><?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'placeholder'=> "Last name"])->label(false) ?></li>
            <li style="padding-bottom: 0px"><?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder'=> "e-Mail Address", 'type'=>'email'])->label(false) ?></li>
          <li><?= Html::submitButton(Yii::t('app', 'SUBSCRIBE'), ['class' => 'button ', 'name' => 'op']) ?></li>
        </ul>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<!-- /subscribe_block -->
