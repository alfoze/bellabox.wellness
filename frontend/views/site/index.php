<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\db\Query;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Home');
$setting = \common\models\Settings::findOne(1);
$homePageDivs = backend\models\Divs::find()->where("landingpage_id = '$setting->homepage'")->orderBy(['weight' => SORT_ASC])->all();
?>
<!-- <div class="clearfix margin-bottom-5"></div>-->
<?= $this->render('/site/flash'); ?>

<!--<div class="clearfix margin-bottom-5"></div>-->
<div class="shadow-wrapper" >
</div>

<?php
foreach ($homePageDivs as $homePageDiv) {
  $pageContent = backend\models\Pages::findOne($homePageDiv->page_id);
  if ($pageContent != null) {
    echo $pageContent->html;
  }
}
?>

<?=
$this->render("/site/subscribe", ['model'=>$newsmodel]);
?>