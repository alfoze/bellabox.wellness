<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = ' Need Help';
//$this->params['breadcrumbs'][] = $this->title;
?>

<!--<div class="site-about">
    

    <div class="row" style="margin-left: -4%;">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h1 style="border-bottom: 2px solid #f58320;"><?= Yii::t('app', 'Need Help') ?><span style="float: right;"></span></h1>
            <br>
            <br>
<?php
$i = 0;
foreach ($models as $model) {
  $i++;
  ?>
  <?= $model->id . '.'; ?>
  <?=
  Html::a($model->question, ['site/view', 'id' => $model->id], ['class' => 'add_title']
  )
  ?>
                            <br>
                          
                            <br>
                            <p>
  <?php
  $string = $model->answer;
  $string = strip_tags($string);

  if (strlen($string) > 250) {

    // truncate string
    $stringCut = substr($string, 0, 250);

    // make sure it ends in a word so assassinate doesn't become ass...
    $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '..';
  }
  echo $string;
  ?>
                            </p>
                            <hr>
<?php } ?>
<?php
echo LinkPager::widget([
  'pagination' => $pages,
]);
?>
        </div>
        <div class="col-md-1"></div>
    </div>

   
</div>-->
<div class="headline"><h2><?= Yii::t('app', 'Need Help') ?></h2></div>
<div class="row">
    <div class="col-md-9">
        <!-- General Questions -->

        <div class="panel-group acc-v1 margin-bottom-40" id="accordion">
            <?php
            if (!empty($models)) {
              $i = 0;
              foreach ($models as $model):
                $i++;
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $i; ?>">
                                <?= $i; ?>. <?= $model->question ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse<?= $i; ?>" class="panel-collapse collapse <?php if ($i == 1) { ?> in <?php } ?>">
                        <div class="panel-body">

                            <?= $model->answer; ?>
                        </div>
                    </div>
                </div>
                <?php
              endforeach;
            }
            ?>

        </div><!--/acc-v1-->
        <!-- End General Questions -->

    </div><!--/col-md-9-->

    <div class="col-md-3">
        <?= $this->render("/site/advertize"); ?>                
        <div class="clearfix margin-bottom-20"></div>
        <?= $this->render("/site/emailalert"); ?>
        <div class="clearfix margin-bottom-20"></div>
        <?= $this->render("/site/communityside"); ?>
        <div class="clearfix margin-bottom-20"></div>
    </div><!--/col-md-3-->
</div>

<style>
    #accordion { margin-top: 6px;}

</style>
