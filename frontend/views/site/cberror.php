<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'ChargeBee Error';
?>

<div class="text-center">
    <h3>
      Error while connecting with ChargeBee
    </h3>
</div>