<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */
//
$this->title = 'Reset password';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-request-password-reset">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 business_box" style="padding-left: 5%">
            <br><br>
            <h1 style="font-size:20px;"><?= Html::encode($this->title) ?></h1>
            <?php if (Yii::$app->session->getFlash('s_mesage')) { ?>
              <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?= Yii::$app->session->getFlash('s_mesage'); ?>
              </div>
            <?php } ?>
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
            <div class="row">
                <div class="col-md-6">
                    <p><?= Yii::t('app', 'Please choose your new password') ?></p>                    
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'CHANGE PASSWORD >'), ['class' => 'btn-u btn-u-bb_rouge']) ?>
                    </div>                    
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row" style="margin-bottom:40px "></div>
</div>