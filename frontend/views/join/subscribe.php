<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Subscribe Box');
$setting = common\models\Settings::findOne(1);
?>
<div style="padding-top: 15px;padding-bottom: 100px;text-align:center;background-image : url('img/banners/dash.jpg');" >
    <h4>SUBSCRIBING : <?= $planName ?></h4>
    <h5><?= strtoupper(Yii::$app->user->identity->fullname) ?></h5>
</div>

<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="row body_1 das_man" > 

            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="clearfix margin-bottom-50"></div>

                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <p>Very close now! We will now direct you to our payment page to confirm your order and get your payment details.</p>
                    <br>
                    <a href="#" class ="btn-u btn-u-bb_rouge" style="text-align: center" onclick="subscribeResponsePopupHandler()">Let's go</a>
                    <br>
                </div>



                <div class="clearfix margin-bottom-100"></div>
                <div class="clearfix margin-bottom-100"></div>
                <div class="clearfix margin-bottom-100"></div>
            </div>
            <div class="col-md-2"></div>

        </div>
        <div class="clearfix margin-bottom-50"></div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>

<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="max-width: 540px;">
            <div class="modal-header">
                <h4 class="modal-title text-center">
                    Payment Information
                </h4>
            </div>
            <!--add custom attribute data-cb-modal-body="body" to modal body -->
            <div class="modal-body"  data-cb-modal-body="body" style="padding-left: 0px;padding-right: 0px;">
            </div>
        </div> 
    </div>
</div> 

<?php
$urlArray = ['join/gethpp', 'planId' => $planId];
if ($coupon != null) {
    $urlArray['coupon'] = $coupon;
}
if ($addon != null) {
    $urlArray['addon'] = $addon;
}
if ($giftfname != null) {
    $urlArray['giftfname'] = $giftfname;
    $urlArray['giftlname'] = $giftlname;
    $urlArray['giftemail'] = $giftemail;
}
if ($utm_campaign != null) {
    $urlArray['utm_campaign'] = $utm_campaign;
}
if ($utm_source != null) {
    $urlArray['utm_source'] = $utm_source;
}
if ($utm_medium != null) {
    $urlArray['utm_medium'] = $utm_medium;
}
if ($utm_content != null) {
    $urlArray['utm_content'] = $utm_content;
}
?>
​​<script src="https://js.chargebee.com/v2/chargebee.js"></script>
<script type="text/javascript">
    function subscribeResponsePopupHandler() {
        var chargebeeInstance = Chargebee.init({site: "<?= $setting->cb_name ?>"});
        chargebeeInstance.openCheckout({
            hostedPage: function () {
                return $.ajax({url: "<?= yii\helpers\Url::to($urlArray); ?>"});
            },
            loaded: function () {},
            success: function (hostedPageId) {
                redirectCall(hostedPageId);
            },
            close: function () {
                redirectfailCall();
            }
        });
    }
    function redirectCall(hostedPageId) {
        window.location.href = " <?= yii\helpers\Url::to(['beautyprofile/thankyou', 'price' => $price, 'currency' => $currency, 'plantype' => $plantype, 'planname' => $plancbName, 'hostedpageid' => '']) ?>" + hostedPageId;
    }
    function redirectfailCall() {
        window.location.href = "<?= yii\helpers\Url::to(Yii::$app->request->referrer ?: Yii::$app->homeUrl) ?>";
    }
</script>
