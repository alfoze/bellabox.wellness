<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Reward Point');
?>

<div style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
    <h4>REWARD POINTS</h4>
</div>
<div style="background-color:<?=Yii::$app->params['bb_lgrey']?>;padding-top: 40px;padding-bottom: 40px;">
    <div class="row">
        <div class="col-md-1 col-xs-1"></div>
        <div class="col-md-10 col-xs-10">
            <div class="row body_1 das_man" > 
                <div class="col-md-2"> </div>
                <div class="col-md-8">
                   <h5><?= strtoupper(Yii::$app->user->identity->fullname)?>,</h5> You've Got  <br>
                   <h1 style="color:<?=Yii::$app->params['bb_blue']?>;font-size: 40px;"><?= $totalpoint ?> Points</h1>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-xs-1"></div>
    </div>
</div>
<div class="row main_body">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2 col-xs-1"> </div>
            <div class="col-md-8 col-xs-10">
                
                <br>
                <?php if (count($rewardPoints) == 0) { ?>
                  <p>There is no subscription for this customer.</p>
                  <div class="clearfix margin-bottom-100"></div>
                  <?php
                }
                else {
                  ?>
                  <h3>Points History</h3>
                  <table class="table" style="text-align:center">
                      <tr style="background-color:<?=Yii::$app->params['bb_lgrey']?>">
                          <td width="25%"><b>POINTS</b></td>
                          <td width="25%"><b>REASON</b></td>
                          <td width="25%"><b>DATE</b></td>
                          <td width="25%"><b>STATUS</b></td>
                      </tr>
                      <?php
                      foreach ($rewardPoints as $rewardPoint) {
                        ?>
                        <tr>
                            <td style="color:<?=Yii::$app->params['bb_blue']?>"><strong><?= $rewardPoint->points ?></strong></td>
                            <td><?= $rewardPoint->reason ?></td>
                            <td><?= date_format(date_create($rewardPoint->create_date), 'd/m/Y'); ?></td>
                            <td><?= $rewardPoint->status == 'U' ? 'Un Approved' : 'Approved' ?></td>
                        </tr>

                      <?php } ?>
                  </table>
                <div style="width: 100%;text-align: center;margin-bottom:25px;margin-top:25px;">
                  <label>Point Redemption coming soon!</label>
                  </div>
                <?php } ?>

            </div>
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>