<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Feedback Box');
?>

<div style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
<h4>FEEDBACK</h4>
</div>
<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2"> </div>
            <div class="col-md-8">
                <div class="clearfix margin-bottom-50"></div>
                <h5>Welcome <?= strtoupper(Yii::$app->user->identity->fullname)?>!</h5><br>
                <div class="clearfix margin-bottom-20" style="border-bottom: 1px solid <?=Yii::$app->params['bb_lgrey']?>;"></div>
               
                <?php if (count($allocations) == 0) { ?>

                  <p>There is nothing to rate.</p>
                  <?php 
                  if(!Yii::$app->user->isGuest){
                    if(Yii::$app->user->identity->is_admin == true){
                      echo "<p>If you are an admin please configue the typeform-url and typeform code in related box variant in admin.</p>";
                    }
                  }
                  ?>
                  <div class="clearfix margin-bottom-100"></div>
                  <div class="clearfix margin-bottom-100"></div>
                  <?php
                }
                else {
                    echo"
                    <p>Tell us what you think about the boxes and samples you've received so that we can serve you better!.</p>
                    <p>If you believe your delivery has gone missing, don't hesitate to <a href='mailto:info@bellabox.com.au' class='color-grey'>contact us</a>.</p>";
                    echo'<br>
                    <div class="row">';
                  foreach ($allocations as $allocation) {
                      if($allocation['main'] == true)
                      {?>
                        <div class="col-md-12" style="text-align:center;padding:10px;margin-bottom:10px;border: 1px solid <?=Yii::$app->params['bb_lgrey']?>;">
                            
                            <img src="<?= $allocation['image'] ?>" style="width: 100%;" class="img-responsive"/>
                            <h4><?= $allocation['name'] ?></h4> 
                            <a href ="<?= $allocation['rated'] == true ? '#' : $allocation['rate_url'] ?>" style="padding:10px;width:100%; <?= $allocation['rated'] == true ?'cursor: not-allowed':''?>" class="btn-u btn-u-sm  <?= $allocation['rated'] == true ? 'btn-u-bb_grey' : 'btn-u-bb_rouge' ?>" ><?=$allocation['ratedText']?></a>
                            
                        </div>

                <?php } 
                      else
                      {?>
                        <div class="col-md-4" style="text-align:center;padding:10px;margin-bottom:10px;border: 1px solid <?=Yii::$app->params['bb_lgrey']?>;">
                            <div style="min-height:180px">
                            <img src="<?= $allocation['image'] ?>" style ="max-width:150px; max-height:180px"/>
                            </div>
                            <div style="min-height:40px">
                            <h5><?= $allocation['name'] ?></h5>
                            </div>
                            <a href ="<?= $allocation['rated'] == true ? '#' : $allocation['rate_url'] ?>" style="width:100%;padding:10px; <?= $allocation['rated'] == true ?'cursor: not-allowed':''?>" class="btn-u btn-u-sm  <?= $allocation['rated'] == true ? 'btn-u-bb_grey' : 'btn-u-bb_rouge' ?>" ><?=$allocation['ratedText']?></a>
                           
                        </div>
                      <?php } 
                  }
                  echo "</div>";
                } ?>
            </div>
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>