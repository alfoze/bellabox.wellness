<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Beauty Profile');
?>

<div style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
    <h4>WTW BEAUTY PROFILE</h4>
</div>
<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2"> </div>
            <div class="col-md-8">
                <div class="clearfix margin-bottom-50"></div>
              
                <h5>Welcome <?= strtoupper(Yii::$app->user->identity->fullname)?>!</h5><br>
                <div class="clearfix margin-bottom-20" style="border-bottom: 1px solid <?=Yii::$app->params['bb_lgrey']?>;"></div>
                <p>Your Wellness journey has started, stay tuned for more. <//p>
                <p style="display:none">With your beauty profile, we do our best to keep your boxes relevant and exciting! You can find out more about how your beauty profile and bellabox works here.</p>
                <div class="clearfix margin-bottom-15"></div>
                <div class="row" style="margin-left:0px;display: none">
                    <?php
                    if (count($beautyData) == 0) {
                      ?>
                      <p>There is no subscription for this customer.</p>
                      <?php
                    }
                    else {

                      foreach ($beautyData as $key => $beautyPage) { 
                        if($key%2 == 0)
                        {
                            echo '<div class="row btp_desktop">';
                        }
                        
                        ?>
                        <div class="col-lg-6 col-md-6 col-xs-12" style="border: 1px solid lightgray;margin-top:10px;display: table-cell;">
                            <div class="row" style="margin-bottom: 20px;border-bottom: 1px solid lightgray;">
                                
                                <div class="col-md-9 col-xs-9" style="padding-top:10px;padding-bottom:10px; ">
                                    <h5><b><?= strtoupper($beautyPage['name']) ?></b> <span class="pull-right" ></h5> 
                                </div>
                                <div class="col-md-3 col-xs-3" style="background:#FAFAFA;text-align:center;padding-top:10px;padding-bottom:10px;">
                                    <h5><a style="color:#aaa;" href="<?= yii\helpers\Url::to(['/beautyprofile/update', 'pageid' => $beautyPage['id']]) ?>">Edit</a></span></h5>
                                </div>
                            </div>
                            
                            <div>

                                <?php
                                if (count($beautyPage['questions']) > 0) {
                                  foreach ($beautyPage['questions'] as $beautyquestion) {
                                    //print_r($beautyquestion);
                                    ?>
                                    <div class="margin-top-20"><b><?= $beautyquestion['question'] ?></b></div>
                                    <?php if ($beautyquestion['haveImage'] == true) { ?>

                                      <div style="font-size:15px">
                                          <?php
                                          foreach ($beautyquestion['answer'] as $bans) {
                                            echo '<img src="' . $bans['image'] . '" style = "width:80px"/>';
                                            echo ' ' . $bans['name'];
                                            echo "<br>";
                                          }
                                          ?>
                                      </div>
                                      <?php
                                    }
                                    else {
                                      ?>
                                      <div style="font-size:15px">
                                          <?php
                                          $showans = '';
                                          foreach ($beautyquestion['answer'] as $bans) {
                                            $showans .= $showans == "" ? '' : " / ";
                                            $showans .= $bans['name'];
                                          }
                                          echo $showans;
                                          ?>
                                      </div>
                                    <?php } ?>
                                   
                                    <?php
                                  }
                                }
                                ?>

                                <?php if ($beautyPage['complete'] == false || count($beautyPage['questions']) == 0) { ?>
                                  <div style="background-color: #ffe9a7;color: #555555;padding: 20px; margin-bottom: 15px;" ><p>Looks like you haven't completed this section. Click "Edit" to finish it up!</p></div>
                                  <?php
                                }
                                ?>
                                <div class="clearfix margin-bottom-20"></div>
                            </div>
                        </div>
                        <?php
                        if($key%2 != 0)
                        {
                            echo '</div>';
                        }
                      }
                    }
                    ?>
                </div>
                <div class="clearfix margin-bottom-50"></div>
            </div>
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
    .btp_desktop{display: flex;}
    @media screen and (max-width: 800px) {
        .btp_desktop{display: block;}
}
</style>