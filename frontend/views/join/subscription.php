<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Subscription');
?>
<div style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
    <h4>SUBSCRIPTION OPERATION</h4>
</div>

<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2"> </div>
            <div class="col-md-8">
                <div class="clearfix margin-bottom-50"></div>
                <h5>Welcome <?= strtoupper(Yii::$app->user->identity->fullname) ?>!</h5><br>
                <div class="clearfix margin-bottom-20" style="border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;"></div>
                <div class="row">
                    <?php
                    if (count($result) == 0) {
                      echo "<p>" . $errorText . "</p>";
                      echo '<div class="clearfix margin-bottom-100"></div>';
                    }
                    else {
                      foreach ($result as $resultum) {
                        $billDate = $resultum['nextBill'];
                        $allowChange = $resultum['allowChange'];
                        $planId = $resultum['planid'];
                        $planType = $resultum['planType'];
                        $boxType = $resultum['boxtype'];
                        $period = $resultum['renewesMonths'] > 1 ? $resultum['renewed'] . 's' : $resultum['renewed'];
                        $renewedAt = $resultum['renewesMonths'] . ' ' . ucfirst($period);
                        $status = ucfirst(str_replace('_', ' ', $resultum['status']));
                        ?>
                        <div class="col-md-6">
                            <div style="border :1px solid <?= Yii::$app->params['bb_lgrey'] ?>;margin:5px;">
                                <div class="margin-bottom-10" style="background-color:<?= Yii::$app->params['bb_lgrey'] ?>;padding: 10px;text-align:center;color:<?= Yii::$app->params['bb_blue'] ?>">
                                    <h5 style="color:<?= Yii::$app->params['bb_blue'] ?>"><?= $resultum['name'] ?></h5>
                                </div>

                                <table class="table" style="padding:10px">
                                    <tr>
                                        <td class="col-md-6 col-xs-5"  style="border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;">Subscription Id : </td>
                                        <td class="col-md-6 col-xs-7" style="text-align:right;border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><?= $resultum['id'] ?></td>
                                    </tr>
                                    <?php if ($planType != 'Send Gift Subscription') { ?>
                                      <tr>    
                                          <td class="col-md-6 col-xs-5"  style="border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;">Renewed : </td>
                                          <td class="col-md-6 col-xs-7" style="text-align:right;border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><?= $renewedAt ?></td>
                                      </tr>
                                    <?php } ?>
                                    <?php if ($planType != 'Receive Gift Subscription') { ?>
                                      <tr>     
                                          <td class="col-md-6 col-xs-5"  style="border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><?= $resultum['currency'] ?></td>
                                          <td class="col-md-6 col-xs-7" style="text-align:right;border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><?= $resultum['price'] / 100 ?></td>
                                      </tr>
                                    <?php } ?>
                                    <?php if ($planType != 'Send Gift Subscription' && $planType != 'Receive Gift Subscription') { ?>
                                      <tr>     
                                          <td class="col-md-6 col-xs-5"  style="border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;">Next Billed At : </td>
                                          <td class="col-md-6 col-xs-7" style="text-align:right;border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><?= $billDate ?></td>
                                      </tr>
                                    <?php } ?>
                                    <tr> 
                                        <td class="col-md-6 col-xs-5"  style="border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;">Status : </td>
                                        <td class="col-md-6 col-xs-7" style="text-align:right;border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><b><?= $status ?></b></td>
                                    </tr>
                                    <?php if ($resultum['ReceipientName'] != '') { ?>
                                      <tr> 
                                          <td class="col-md-6 col-xs-5"  style="border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><?= $planType == 'Send Gift Subscription' ? 'Recipient Name' : 'Sender Name' ?>: </td>
                                          <td class="col-md-6 col-xs-7" style="text-align:right;border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><b><?= $resultum['ReceipientName'] ?></b></td>
                                      </tr>
                                    <?php } ?>
                                    <?php if ($resultum['ReceipientEmail'] != '') { ?>
                                      <tr> 
                                          <td class="col-md-6 col-xs-5"  style="border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><?= $planType == 'Send Gift Subscription' ? 'Recipient Email' : 'Sender Email' ?> : </td>
                                          <td class="col-md-6 col-xs-7" style="text-align:right;border-bottom: 1px solid <?= Yii::$app->params['bb_lgrey'] ?>;border-top: 0px;"><b><?= $resultum['ReceipientEmail'] ?></b></td>
                                      </tr>
                                    <?php } ?>
                                </table>

                                <div class="clearfix margin-bottom-20"></div>
                                <div class="row" style="padding:10px">
                                    <?php if ($resultum['status'] == 'redeem') { ?>
                                      <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:5px;display:table"><a style="width:100%;text-align:center;padding:10px;" class="btn-u btn-u-sm btn-u-bb_blue"  href="<?php echo yii\helpers\Url::to(['/join/redemesub', 'subscriptionId' => $resultum['id'], 'stub' => $rs]); ?>" >Redeem Subscription</a></div>
                                      <!--<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:46px;display:table"></div>-->
                                      <?php
                                    }
                                    elseif ($resultum['status'] == 'non_renewing') {
                                      if ($planType == 'Send Gift Subscription') {
                                        ?>
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:5px;display:table"><a style="width:100%;text-align:center;padding:10px;" class="btn-u btn-u-sm btn-u-bb_grey" href="#" ><?= $resultum['linkedstatus'] ?></a></div>
                                        <!--<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:46px;display:table"></div>-->
                                        <?php
                                      }
                                      else {
                                        ?>
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:46px;display:table"></div>
                                        <!--<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:46px;display:table"></div>--> 
                                        <?php
                                      }
                                    }
                                    elseif ($resultum['status'] == 'cancelled') {
                                      ?>
                                      <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:5px;display:table"><a style="width:100%;text-align:center;padding:10px;" class="btn-u btn-u-sm btn-u-bb_blue"  href="<?php echo yii\helpers\Url::to(['/join/reactivatesub', 'subscriptionId' => $resultum['id'], 'stub' => $rs]); ?>" >Reactivate Subscription</a></div>
                                         <?php
                                    }
                                    elseif ($resultum['status'] == 'paused') {
                                      ?>
                                     <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom:5px;display:table"><a style="width:100%;text-align:center;padding:10px;" class="btn-u btn-u-sm btn-u-bb_blue" onclick="return confirm('Are you sure you want to resume the subscription?')" href="<?php echo yii\helpers\Url::to(['/join/resumesub', 'subscriptionId' => $resultum['id'], 'stub' => $rs]); ?>">Resume</a></div>
                                      <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom:5px;display:table"><a style="width:100%;text-align:center;padding:10px;" class="btn-u btn-u-sm btn-u-bb_grey"  href="<?php echo yii\helpers\Url::to(['/join/cancelsub', 'subscriptionId' => $resultum['id'], 'pname' => $resultum['name'], 'stub' => $rs]); ?>">Cancel</a></div>
                                      <?php
                                    }
                                    else {
                                      ?>
                                      <?php
                                      $optin = \common\models\Optin::findOne($boxType);
                                      if ($optin != null) {
                                        if ($optin->active == '1') {
                                          $showText = $optin->text_to_show;

                                          $url = $optin->type_form_code;
                                          $typeformCode = trim(str_replace("https://bellabox.typeform.com/to/", '', $url));
                                          if (strlen($typeformCode) > 0) {
                                            $userId = Yii::$app->user->identity->id;
                                            $cbCustomerId = Yii::$app->user->identity->cb_customer_id;
                                            $name  = Yii::$app->user->identity->name;
                                            $email  = Yii::$app->user->identity->email;
                                            $subRef = $resultum['id'];
                                            $postfix = "?user_id=$userId&sub_id=$subRef&name=$name&email=$email&country=NZ&sub_ref=$cbCustomerId&sur_id=$typeformCode";
                                            $url = $url . $postfix;
                                            $optinhistory = \common\models\Optinhistory::find()->where("typeform_id = '$typeformCode' and sub_id = '$subRef' ")->one();
                                            $class = 'btn-u btn-u-bb_rouge';
                                            $target = "_blank";
                                            if ($optinhistory != null) {
                                              $showText = $optin->text_chosen;
                                              $url = '#';
                                              $class = 'btn-u btn-u-sm btn-u-bb_grey isDisabled';
                                              $target = "_self";
                                            }
                                            ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:5px;display:table">
                                                <a style="width:100%;text-align:center;padding:10px;" class="<?= $class ?>"  href="<?= $url ?>" target="<?= $target?>"><?= $showText ?></a>
                                            </div>
                                            <?php
                                          }
                                        }
                                      }
                                      ?>
                                      <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom:5px;display:table"><a style="width:100%;text-align:center;padding:10px;" class="btn-u btn-u-sm btn-u-bb_blue"  href="<?php echo yii\helpers\Url::to(['/join/pausesub', 'subscriptionId' => $resultum['id'], 'pname' => $resultum['name'], 'stub' => $rs]); ?>">Pause</a></div>
                                      <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom:5px;display:table"><a style="width:100%;text-align:center;padding:10px;" class="btn-u btn-u-sm btn-u-bb_grey"  href="<?php echo yii\helpers\Url::to(['/join/cancelsub', 'subscriptionId' => $resultum['id'], 'pname' => $resultum['name'], 'stub' => $rs]); ?>">Cancel</a></div>

                                    <?php } ?>
                                </div>
                                <div class="clearfix margin-bottom-20"></div>
                            </div>
                        </div>
                        <?php
                      }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
    .isDisabled {
        color: currentColor;
        cursor: not-allowed;
        opacity: 0.5;
        text-decoration: none;
    }
</style>