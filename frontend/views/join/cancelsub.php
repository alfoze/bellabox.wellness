<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Cancel Subscription');
?>

<div style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
<h4>CANCLE SUBSCRIPTION</h4>
</div>
<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2"> </div>
            <div class="col-md-8">
                <div class="clearfix margin-bottom-50"></div>
                <h5><?= strtoupper(Yii::$app->user->identity->fullname)?></h5><br>
                <div class="clearfix margin-bottom-20" style="border-bottom: 1px solid <?=Yii::$app->params['bb_lgrey']?>;"></div>
                
                <h4>ARE YOU SURE YOU WANT TO CANCEL <?= strtoupper($pname)?> </h4>
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'subscriptionid')->hiddenInput()->label(false)?>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'reason')->dropDownList(ArrayHelper::map(common\models\Reasons::findAll(["type" => 'C']), 'name', 'name'), ['prompt' => 'Choose a reason..']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Cancel Subscription'), ['class' => 'btn-u btn-u-sm btn-u-bb_rouge','style'=>'padding:10px;']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                <div class="clearfix margin-bottom-40"></div>
                <hr>

            </div>
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>
