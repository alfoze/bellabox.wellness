<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Change Subscription');
?>

<div style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
<h4>CHANGE SUBSCRIPTION</h4>
</div>
<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="col-md-2"> </div>
            <div class="col-md-8">
                <div class="clearfix margin-bottom-50"></div>
                <h5><?= strtoupper(Yii::$app->user->identity->fullname)?></h5><br>
                <div class="clearfix margin-bottom-20" style="border-bottom: 1px solid <?=Yii::$app->params['bb_lgrey']?>;"></div>
                <h4>Current Subscription: <?=$model->fromsubscription?></h4>
                 <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'fromsubscriptionid')->hiddenInput()->label(false)?>
                
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'tosubscriptionid')->dropDownList(ArrayHelper::map($palnList, 'id', 'name'), ['prompt' => 'Choose a paln..']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Change Subscription'), ['class' => 'btn-u btn-u-sm btn-u-bb_rouge','style'=>'padding:10px;']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                <div class="clearfix margin-bottom-40"></div>
                
            </div>
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>
