<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Gift Receiver Information');
?>

<div style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
<h4>GIFT RECEIVER INFORMATION</h4>
</div>
<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-4"> </div>
            <div class="col-md-4">
                <div class="clearfix margin-bottom-50"></div>
                <h4>Please fill in information about the gift receiver.</h4>
                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'fname')->textInput(['maxlength' => true]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'lname')->textInput(['maxlength' => true]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Continue >'), ['class' => 'btn-u btn-u-sm btn-u-bb_rouge','style'=>'padding:10px;']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                <div class="clearfix margin-bottom-40"></div>
                <hr>

            </div>
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>
