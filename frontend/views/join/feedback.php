<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Feedback');
?>

<div style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
<h4>FEEDBACK</h4>
</div>
<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2"> </div>
            <div class="col-md-8">
                <div class="clearfix margin-bottom-50"></div>
                <h5>Welcome <?= strtoupper(Yii::$app->user->identity->fullname)?>!</h5><br>
                <div class="clearfix margin-bottom-20" style="border-bottom: 1px solid <?=Yii::$app->params['bb_lgrey']?>;"></div>
               
                <?php if (count($allocations) == 0) { ?>

                  <p>Contact us at hello@bellabox.com.au if you have any question or require any assistance.</p>
                  <div class="clearfix margin-bottom-100"></div>
                  <?php
                }
                else {
                    echo"
                    <p>Tell us what you think about Welcome to Wellness and its contents, so that we can serve you better.</p>
                    <p>If you believe your delivery has gone missing, don't hesitate to <a href='mailto:info@bellabox.com.au' class='color-grey'>contact us</a>.</p>";
                    echo"<br><h4>Received boxes</h4><br>";
                  foreach ($allocations as $allocation) {
                    ?>
                    <div class="row margin-bottom-20" style="padding:10px;border: 1px solid <?=Yii::$app->params['bb_lgrey']?>;">
                        <div class="col-md-4">
                            <img src="<?= $allocation['image'] ?>" style ="width:150px"/>
                        </div>  
                        <div class="col-md-8">
                            <h4><?= $allocation['name'] ?></h4>
                            <?php if($allocation['rated'] == true){ echo "<h5>You've rated this Box.</h5><br><br>";                           }
                            else{ echo "<h5>You have not rated this Box yet.</h5><br><br>";}?>
                                
                            <a href ="<?= $allocation['rated'] == true ? '#' : $allocation['rate_url'] ?>" style="padding:10px; <?= $allocation['rated'] == true ?'cursor: not-allowed':''?>" class="btn-u btn-u-sm  <?= $allocation['rated'] == true ? 'btn-u-bb_grey' : 'btn-u-bb_rouge' ?>" >Rate the box and the samples</a>
                        </div>
                    </div>


                  <?php }
                } ?>
            </div>
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>