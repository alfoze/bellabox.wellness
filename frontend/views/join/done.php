<?php

use common\models\Email;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Thank You');
?>


<div class="row main_body">
    <div class="col-md-1 col-xs-1"></div>
    <div class="col-md-10 col-xs-10">
        <div class="row body_1 das_man" > 
            <div class="col-md-2">
                
            </div>
            <div class="col-md-8">
                <div class="clearfix margin-bottom-50"></div>
                <h1>UPDATE YOU BEAUTY PROFILE</h1>
                <div class="clearfix margin-bottom-30"></div>
                <h3 class="text-center">DONE!</h3>
                <p class="text-center">If you want to check it or edit your beauty profile, you can do that in your dashboard.</p>
                <div class="clearfix margin-bottom-50"></div>
                <div style="text-align: center;">
                <a style="text-align:center;padding:10px;" onClick="fbq('track', 'Beautyprofile');" class="btn-u btn-u-sm btn-u-bb_rouge" class="text-center" href="<?= yii\helpers\Url::to(['join/beautyprofile'])?>">TO MY DASHBOARD > </a>
                </div>
                <div class="clearfix margin-bottom-100"></div>
            </div>
            <div class="col-md-2">
                
            </div>
        </div>
    </div>
    <div class="col-md-1 col-xs-1"></div>
</div>
<style>
    .li_style { margin-top:4%;}
</style>