<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Countries;

/* @var $this yii\web\View */
/* @var $model \common\models\Business */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="business-form">

  <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>


    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'f_name')->textInput() ?>

    <?= $form->field($model, 'l_name')->textInput() ?>

    <?= $form->field($model, 'address')->textInput() ?>
    <?=
    $form->field($model, 'country')->dropDownList(common\config\Options::getCountries(), ['prompt' => Yii::t('app', 'Select Country'),
        'onchange' => '
                        $.get( "' . Url::toRoute('/site/citylist') . '", { cid: $(this).val() } )
                            .done(function( data ) {
                                $( "#user-city" ).html( data );
                            }
                        );
                    '])
   ?>
    <?php
    $cities = array();
    if ($model->country != "") {
        $cities = common\config\Options::getCities($model->country);
    }
    echo $form->field($model, 'city')->dropDownList($cities);
    ?>
    
    <?= $form->field($model, 'pic')->fileInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-u' : 'btn-u btn-u-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
