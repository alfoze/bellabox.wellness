<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\Business */

$this->title = Yii::t('app', 'Update Account Information');
?>
<div class="business-update">

    <div class="row">
        <div class="col-md-3">
            <?= $this->render('/site/left_menu'); ?>

        </div>
        <div class="col-md-9 business_box tag-box tag-box-v3 form-page" style="margin-top: 1%;">
            <div class="headline">
                <h3><?= Yii::t('app', 'Update Your Account Information')?></h3>
            </div>
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>
            </di></div>


    </div>
</div>