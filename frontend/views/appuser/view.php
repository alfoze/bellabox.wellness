<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Query;
$connection = \Yii::$app->db;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\Business */

$this->title = $model->add_title.' ';
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Businesses'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="business-view">

    

    <p>
        <?php //Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn-u btn-u-success']) ?>
        <?php //  Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
           // 'class' => 'btn btn-danger',
           // 'data' => [
           //     'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
          //      'method' => 'post',
         //   ],
      //  ]) ?>
    </p>

   

</div>
<div class="business-index">

<!-- 7th row main body -->

<div class="row main_body">
<div class="col-md-1"></div>
<div class="col-md-10">

<!--Main body Content -->

<div class="row body_1">

<div class="col-md-8">

<!-- Business Sale List Detail -->

<div class="row">
<div class="col-md-12">
    <h1><?= Html::encode($model->add_title) ?></h1>
    <br >
    <b>
        <?php 
        $command = $connection->createCommand('SELECT name FROM es_cities WHERE id="'.$model->city.'"');
        $city = $command->queryColumn();

        $command_1 = $connection->createCommand('SELECT name FROM es_countries WHERE id="'.$model->country.'"');
        $country = $command_1->queryColumn();

          echo $model->address.','.@$city[0].','.@$country[0];
        ?>
    </b>
    <!--<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/images/business_sale_1.png" class="img_adverting" />
-->
</div>

</div>
<br>
<br>
<div class="row">
    <div class="col-md-6"><b>Price:</b>$<?= $model->price; ?></div>
    <div class="col-md-6"><a href="#" class="btn-u btn-u-success">Get Financed</a></div>
    
</div>

<div class="row" style="margin-top: 2%;">
    <?= $model->description; ?>   
</div>

<div class="row" style="margin-top: 4%;">
    <div class="col-md-3"><b>Optional Information:</b></div>
    <div class="col-md-9">
        
        <table class="table table-bordered">
                    <tbody>
                    <tr>
                      <td><b>Category</b></td>
                      <td>
                          <?php 
                        $command_3 = $connection->createCommand('SELECT name FROM es_category WHERE id="'.$model->category_id.'"');
                        $category = $command_3->queryColumn();
                        echo $category[0];
                          ?>
                      </td>
                      <td>
                          <b>Employee</b>
                      </td>
                      <td><?= $model->employess; ?></td>
                    </tr>

                                        <tr>
                      <td><b>Establish Year</b></td>
                      <td>
                          <?= $model->established_year; ?>
                      </td>
                      <td>
                          <b>Gross Revenue</b>
                      </td>
                      <td>
                          <?= $model->gross_revenue; ?>
                      </td>
                    </tr>
                                        <tr>
                      <td><b>Real State Included</b></td>
                      <td>
                          <?php 
                          if($model->real_estate==1)
                          {
                              echo 'Yes';
                          }
                          else 
                          {
                              echo 'No';
                          }
                          ?>
                      </td>
                      <td>
                          <b>Training</b>
                      </td>
                      <td>
                           <?php 
                          if($model->training==1)
                          {
                              echo 'Yes';
                          }
                          else 
                          {
                              echo 'No';
                          }
                          ?>
                      </td>
                  
                    
                  </tbody></table>
        
    </div>
    
</div>

<div class="row" style="margin-top: 2%;">
     <div class="col-md-3"><b>Reason Of Sealing:</b></div>
     <div class="col-md-9">
         <p>
             <?= $model->reason_of_selling;  ?>
         </p>  
     </div>
</div>

<div class="row" style="margin-top: 2%;">
     <div class="col-md-3"><b>Facilities:</b></div>
     <div class="col-md-9">
          <?= $model->facilities;  ?>
     </div>
</div>

<div class="row" style="margin-top: 2%;">
     <div class="col-md-3"><b>Web Site:</b></div>
     <div class="col-md-9">
          <?= $model->website;  ?>
     </div>
</div>
<?php if(!empty($model->video_link)){ ?>
<div class="row" style=" margin-top: 3%;">
    <div class="col-md-3"></div>
    <div class="col-md-9">
        <iframe src="<?= $model->video_link; ?>" style="width: 100%; height: 315px;" frameborder="0" allowfullscreen></iframe>
    </div>
    
</div>

<?php } ?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
?>

<div class="row">
    <div class="col-md-12">
        <br >
        
        <br>
         <?php  if (Yii::$app->user->isGuest) {
            echo     Html::a('<b>Contact Seller</b>', 
        ['site/login'], 
        ['class' => 'btn btn-warning btn-lg list_btn']);
            } else { ?>
        <?= Html::button('Contact Seller', ['value' => Url::to(['requestedbuy/create', 'id'=>$model->id]), 'title' => 'Contact Seller', 'class' => 'showModalButton btn btn-warning btn-lg']); ?>  
            <?php } ?>
    </div>
    
</div>


</div>
<div class="col-md-4">
    
    
    <div class="business_box" style="margin-top:3%;" >
        <span class="btn btn-green img_adverting recent_discussion ">Contact Seller</span>
  
        <div class="tab-pane active" id="settings">
                    <form class="form-horizontal contact_sel">
                      <div class="form-group">
                        <label for="inputName" class="col-sm-3 control-label">Buyer Name</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="inputName" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-3 control-label">Buyer Email</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputName" class="col-sm-3 control-label">Buyer Phone</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="inputName" placeholder="phone">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputExperience" class="col-sm-3 control-label">Message</label>
                        <div class="col-sm-9">
                          <textarea class="form-control" id="inputExperience" placeholder="message"></textarea>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn-u btn-u-success">Submit</button>
                        </div>
                      </div>
                        
                        <div class="form-group">
                        <label for="inputExperience" class="col-sm-4 control-label">Seller Name</label>
                        <div class="col-sm-8">
                            <b>
                                <?= $model->contact_name; ?>
                            </b>
                        </div>
                      </div>
                      
                          <div class="form-group">
                        <label for="inputExperience" class="col-sm-4 control-label">Seller Email</label>
                        <div class="col-sm-8">
                            <b>
                               <?= $model->contact_email; ?>
                            </b>
                        </div>
                      </div>
                        
                          <div class="form-group">
                        <label for="inputExperience" class="col-sm-4 control-label">Seller Phone</label>
                        <div class="col-sm-8">
                            <b>
                             <?= $model->contact_phone; ?>
                            </b>
                        </div>
                      </div>
                        
                    </form>
                  </div>
    </div>
<img src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/images/advertising_right.PNG" class="img_adverting" style="margin-top:4%;" />

<div class="business_box" style="margin-top:4%;">
  
    <span class="btn btn-green img_adverting recent_discussion ">Email Alerts</span>
    <br>
    <br >
    <a class="btn-u btn-u-success btn-lg right_box_btn" >Get Email Alerts</a>
    
</div>

<div class="business_box" style="margin-top:4%;">
  
    <span class="btn btn-green img_adverting recent_discussion ">Community</span>
    <br>
    <br >
    <a class="btn-u btn-u-success btn-lg right_box_btn" >Looking For Business</a>
    
</div>

<div class="business_box" style="margin-top:4%;">
  
    <span class="btn btn-green img_adverting recent_discussion ">Similer Business</span>

    <br >
    <img class="similar_businnes_list" src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/images/recent_business3.PNG"  /><a class="img_adverting" style="margin-left: 3%;" >Business for sale in sohag </a>
    
    <br >
    <img class="similar_businnes_list" src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/images/recent_business3.PNG"  />
    <a class="img_adverting" style="margin-left: 3%;" >Business for sale in fayoum </a>
    <br >
    <img class="similar_businnes_list" src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/images/recent_business3.PNG"  />
    <a class="img_adverting" style="margin-left: 3%;" >Business for sale in minya </a>
    <br >
    <img class="similar_businnes_list" src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/images/recent_business3.PNG"  />
    <a class="img_adverting" style="margin-left: 3%;" >Business for sale in ismailia </a>
    <br >
    <img class="similar_businnes_list" src="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/images/recent_business3.PNG"  />
    <a class="img_adverting" style="margin-left: 3%;" >Business for sale in zagazig </a>
    
    
</div>


</div>

</div>


</div>
<div class="col-md-1"></div>
</div>

</div>