<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \common\models\BusinessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Businesses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="business-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Business'), ['create'], ['class' => 'btn-u btn-u-success']) ?>
    </p>
<?php
//    foreach ($dataProvider->models as $model) {
//    " {$model->add_title}";
//}

?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'common\config\SerialColumn2'],

            'id',
            'add_title',
            'description:ntext',
            'price',
            'address',
            // 'city',
            // 'country',
            // 'contact_name',
            // 'contact_phone',
            // 'contact_email:email',
            // 'category_id',
            // 'employess',
            // 'established_year',
            // 'website',
            // 'business_type',
            // 'gross_revenue',
            // 'real_estate',
            // 'training',
            // 'reason_of_selling:ntext',
            // 'facilities',
            // 'keyword',
            // 'video_link',
            // 'user_id',
            // 'status',
            // 'c_timestampt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
