<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\Business */

$this->title = Yii::t('app', 'Create Business');

?>
<div class="business-create">
    <div class="row">
        <div class="col-md-3">
            <div class="business_box" style="margin-top:4%;">
                <span class="btn btn-green img_adverting recent_discussion "><?= Yii::t('app', 'Sellings')?></span>
                <ul class="li_style">
                    <li><a href="#" style="margin-top:2% !important;"><?= Yii::t('app', 'My Listings')?></a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl("business/create"); ?>" style="margin-top:2% !important;"><?= Yii::t('app', 'Add New Listing')?></a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl("franchise/create"); ?>" style="margin-top:2% !important;"><?= Yii::t('app', 'Add New Listing Franchise')?></a></li>
                </ul>
            </div>

            <div class="business_box" style="margin-top:4%;">
                <span class="btn btn-green img_adverting recent_discussion "><?= Yii::t('app', 'Community')?></span>
                <ul class="li_style">
                    <li><a href="#" style="margin-top:2% !important;"><?= Yii::t('app', 'Community')?></a></li>
                    <li><a href="#" style="margin-top:2% !important;"><?= Yii::t('app', 'Add New Question')?></a></li>
                </ul>
            </div>

            <div class="business_box" style="margin-top:4%;">
                <span class="btn btn-green img_adverting recent_discussion "><?= Yii::t('app', 'My Account')?></span>
                <ul class="li_style">
                    <li><a href="#" style="margin-top:2% !important;"><?= Yii::t('app', 'Edit My Account')?></a></li>
                    <li><a href="#" style="margin-top:2% !important;"><?= Yii::t('app', 'Edit Email Preference')?></a></li>
                </ul>
            </div>


        </div>
        <div class="col-md-9 business_box" style="margin-top: 1%;">
            <h1><?= Html::encode($this->title) ?></h1>

            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>
        </div>

    </div>
</div>
