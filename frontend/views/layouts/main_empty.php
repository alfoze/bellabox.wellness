<?php

use yii\helpers\Html;
use common\config\Options;
use frontend\assets\AppAsset;
use yii\widgets\Breadcrumbs;
use frontend\widgets\Alert;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use common\models\Settings;
use kartik\social\GoogleAnalytics;
use backend\models\Pages;

/* @var $this \yii\web\View */
/* @var $content string */

$asset = frontend\assets\AppAsset::register($this);
$baseUrl = $asset->baseUrl;
$r = new yii\web\Request();
$settings = Settings::findOne(1);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="<?= Yii::$app->language ?>" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="<?= Yii::$app->language ?>" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html dir= "" lang="<?= Yii::$app->language ?>"> <!--<![endif]--> 

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Each month, bellabox subscribers receive a beauty box delivered to their doorstep.">
        <meta name="author" content="">

        <?php
        if (isset($this->params['fbImage'])) {
            $output = preg_replace('#[^0-9a-z- ]#', '', strtolower($this->params['fbDesc']));
            ?>
            <meta property="og:image" content="<?= $r->getHostInfo() . $this->params['fbImage'] ?>" />
            <meta property="og:title" content="<?= $this->params['fbTitle'] . " | " . \Yii::$app->params['name'] ?>" />
            <meta property="og:description" content="<?= $output ?>" />
        <?php } ?>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title . ($this->title != '' ? ' | ' : '') . Yii::t('app', \Yii::$app->params['name'])) ?></title>
        <?php $this->head() ?>
        <?php header('Cache-Control: max-age=900'); ?>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
<!--        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54eb06a15e038123" async="async"></script>-->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

        <!-- Google Tag Manager -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!-- End Google Tag Manager -->
</head>
<body>
    <?php $this->beginBody() ?>
    <!-- Added to handel affliated tags -->
    <script async src="https://t.cfjump.com/tag/12632"></script>
    <div class="wrapper">

        <div class="" style="padding-top: 2px;">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div><!--/End Wrapepr-->
    <?php $this->endBody() ?>

    <script>
            $(window).load(function () {
                //              $(".loader-wrapper").fadeOut("slow");
            });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            LayerSlider.initLayerSlider();
            OwlCarousel.initOwlCarousel();
            RevolutionSlider.initRSfullWidth();
            FancyBox.initFancybox();
        });
    </script>
</body>
</html>
<?php $this->endPage() ?>