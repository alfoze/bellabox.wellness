<?php

use yii\helpers\Html;
use common\config\Options;
use frontend\assets\AppAsset;
use yii\widgets\Breadcrumbs;
use frontend\widgets\Alert;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use common\models\Settings;
use kartik\social\GoogleAnalytics;
use backend\models\Pages;

/* @var $this \yii\web\View */
/* @var $content string */

$frontpage = false;
if (Yii::$app->controller->action->id == 'index' && Yii::$app->controller->id == 'site') {
    $asset = frontend\assets\AppAssetFront::register($this);
    $frontpage = true;
} else {
    $asset = frontend\assets\AppAsset::register($this);
}
$baseUrl = $asset->baseUrl;
$r = new yii\web\Request();
$settings = Settings::findOne(1);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="<?= Yii::$app->language ?>" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="<?= Yii::$app->language ?>" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html dir= "" lang="<?= Yii::$app->language ?>"> <!--<![endif]--> 

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Each month, bellabox subscribers receive a beauty box delivered to their doorstep.">
        <meta name="author" content="">

        <?php
        if (isset($this->params['fbImage'])) {
            $output = preg_replace('#[^0-9a-z- ]#', '', strtolower($this->params['fbDesc']));
            ?>
            <meta property="og:image" content="<?= $r->getHostInfo() . $this->params['fbImage'] ?>" />
            <meta property="og:title" content="<?= $this->params['fbTitle'] . " | " . \Yii::$app->params['name'] ?>" />
            <meta property="og:description" content="<?= $output ?>" />
        <?php } ?>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title . ($this->title != '' ? ' | ' : '') . Yii::t('app', \Yii::$app->params['name'])) ?></title>
        <?php $this->head() ?>
        <?php header('Cache-Control: max-age=900'); ?>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54eb06a15e038123" async="async">
        </script>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <!-- Google Tag Manager -->

    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','<?= Yii::$app->params['gtm_tag']; ?>');
    </script>
<!-- End Google Tag Manager —>

</head>
<body>
    <?php $this->beginBody() ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=<?= Yii::$app->params['gtm_tag']; ?>" height="0" width="0" style="display:none;visibility:hidden">
        </iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php
    $dash_style = "padding-top:7px";
    $dash_class = "";
    $showInMenu = false;
    $getStartedClass = 'button button_small';
    ?>
    <!-- Added to handel affliated tags -->
    <script async src="https://t.cfjump.com/tag/12632"></script>   

    <header class="header">
        <div class="wrapper">
            <a class="logo" href="<?= Yii::$app->params['main-site']; ?>">Bellabox</a>
            <ul>
                <?php if (Yii::$app->user->isGuest) { ?>
                    <li><a href="<?= Yii::$app->urlManager->createUrl("/site/login"); ?>">LOGIN</a></li>
                    <?php
                } else {
                    if ($this->context->mainMenu == 200) {
                        $dash_class = 'button button_small';
                        $dash_style = '';
                        $getStartedClass = '';
                    }
                    ?>

                    <li class="dropdown">
                        <a class="<?= $dash_class ?>" onclick="return true;"> DASHBOARD </a>
                        <div class="dropdown-content">
                            <a class="<?= $this->context->submenu == 210 ? 'active-submenu' : '' ?>"  href = "<?= Yii::$app->urlManager->createUrl("/site/dashboard") ?>">Customer & Billing Info<br></a>
                            <a class="<?= $this->context->submenu == 220 ? 'active-submenu' : '' ?>" href = "<?= Yii::$app->urlManager->createUrl("/join/subscription") ?>">Subscription Operation<br></a>
                            <!--<a class="<?php  //$this->context->submenu == 230 ? 'active-submenu' : '' ?>" href = "<?php // Yii::$app->urlManager->createUrl("/join/beautyprofile") ?>">Beauty Profile <br></a>-->
                            <a class="<?= $this->context->submenu == 240 ? 'active-submenu' : '' ?>" href = "<?= Yii::$app->urlManager->createUrl("/join/feedback") ?>">Feedback <br></a>
                        </div>
                    </li>
                    <li >
                        <a href = "<?= Yii::$app->urlManager->createUrl("/site/logout"); ?>" >
                            LOGOUT
                        </a>
                    </li>
                <?php } ?>
                <li>
                    <a <?php echo  $frontpage == true ?' data-slide=".get_started"':'href="'.Yii::$app->urlManager->createUrl("/site/index").'#getstarted"' ?> class="<?php echo $getStartedClass ?>" href="#"> GET STARTED</a>
                </li>
            </ul>
        </div>
    </header>
    <div id="loader"></div>
    <div style="overflow-x:hidden">
        <!-- === End Header === -->
        <div class = "row" >
            <div class = "col-md-12">
                <div class = "pull-right">
                    <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'options' => ['class' => 'breadcrumb', 'style' => 'margin-bottom: 2px;padding-top: 2px;padding-bottom: 2px;']
                    ])
                    ?>
                </div>
            </div>
            <!--<div style="background: #46c3d2;height: 3px"></div>-->
        </div>

        <div class="" style="padding-top: 2px;">
            <!-- Bootstrap model popup -->
            <?php
            Modal::begin([
                'headerOptions' => ['id' => 'modalHeader', 'style' => 'border-bottom:0px'],
                'id' => 'modal',
                'size' => 'modal-lg',
                //keeps from closing modal with esc key or by clicking out of the modal.
                // user must click cancel or X to close
                'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
            ]);
            echo "<div id='modalContent'></div>";
            yii\bootstrap\Modal::end();
            ?>
            <?= Alert::widget() ?>

            <?= $content ?>
        </div>
        <?php
        $footer = '';
        $footer = Pages::findOne($settings->footer_id);
        echo $footer->html;
        ?>

    </div><!--/End Wrapepr-->
    <?php $this->endBody()
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
<?= $frontpage == true ? '' : 'FancyBox.initFancybox();' ?>
        });
    </script>
</body>
</html>
<?php $this->endPage() ?>
<?php

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>