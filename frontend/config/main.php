<?php

$params = array_merge(
    //this is comment
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;

return [
  'id' => 'app-frontend',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'controllerNamespace' => 'frontend\controllers',
  'aliases' => [
    'chargebee' => '@vendor/chargebee/chargebee',
  ],
  'components' => [
    'assetManager' => [
      'bundles' => [
        
        'dosamigos\google\maps\MapAsset' => [
          'options' => [
            'language' => 'ar',
          //'key' => 'this_is_my_key',
          ]
        ],
        'yii\web\JqueryAsset' => [
          'js' => [
            'jquery.min.js'=> '/plugins/jquery/jquery.min.js'
          ]
        ],
        'yii\bootstrap\BootstrapAsset' => [
          'css' => [
            'bootstrap.css' => '/plugins/bootstrap/css/bootstrap.min.css'
          ]
        ],
        'yii\bootstrap\BootstrapPluginAsset' => [
          'js' => [
            'bootstrap.js' => '/plugins/bootstrap/js/bootstrap.min.js'
          ]
        ]
      ]
    ],
    'user' => [
      'identityClass' => 'common\models\User',
      'enableAutoLogin' => true,
    ],
    'authClientCollection' => [
      'class' => 'yii\authclient\Collection',
      'clients' => [
        'facebook' => [
          'class' => 'yii\authclient\clients\Facebook',
          'clientId' => '558100807723562',
          'clientSecret' => 'c18858a614ef880a18dfe508161e1a91',
        ],
        'google' => [
          'class' => 'yii\authclient\clients\GoogleOAuth',
          'clientId' => '386789857838-t2sp0hg9debrv8scg9ddbid9qbg6gjtb.apps.googleusercontent.com',
          'clientSecret' => 'X62YtTVGcIqbhkDxutCjjS_z',
        ],
        'twitter' => [
          'class' => 'yii\authclient\clients\Twitter',
          'attributeParams' => [
            'include_email' => 'true'
          ],
          'consumerKey' => 'FBCiGG1Va3N7lFwZLW00FYesi',
          'consumerSecret' => 'Th8wU8rg19LX9llzLqqwACaoHrS17dXc8VmLTIKANAcHs7EQIm',
        ],
      ],
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
          [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
  ],
  'params' => $params,
];
