<?php
return [
    'adminEmail' => 'admin@bellabox.com.au',
    'bb_blue' => '#46c3d2',
    'bb_rouge' => '#ed1555',
    'bb_morningsky' => '#ceeae9',
    'bb_complexion' => '#fcdbd9',
    'bb_midnight' => '#11165e',
    'bb_sun' => '#fff372',
    'bb_natural' => '#00a84f',
    'bb_lgrey' => '#F6F6F6',
    'bb_grey' => '#7E7E7E',
];
