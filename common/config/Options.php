<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\config;

use common\config\Controller2;
use Yii;
use \yii\helpers\ArrayHelper;

/**
 * Description of Options
 *
 * @author Yousaf
 */
class Options {

  private static $_enabled = array();
  private static $_unames = array();
  private static $_unumbers = array();
  private static $_languages = array();
  private static $_languagesList = array();
  private static $_user = null;

  public static function getFrontendAddress() {
    return Yii::$app->params['frontendaddress'];
  }

  public static function getDate($date) {
    if (Yii::$app->language == 'ar' || Yii::$app->language == 'ur') {
      $fmonths = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
      if (Yii::$app->language == 'ar') {
        $rmonths = array("يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر");
      }
      else {
        $rmonths = array("جنوری", "فروری", "مارچ", "اپریل", "مئ", "جون", "جولائی", "اگست", "ستمبر", "اکتوبر", "نومبر", "دسمبر");
      }
      $date = str_replace($fmonths, $rmonths, $date);
      $find = array("Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri");
      if (Yii::$app->language == 'ar') {
        $replace = array("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
      }
      else {
        $replace = array("ہفتہ", "اتوار", "پیر", "منگل", "بدھ", "جمعرات", "جمعہ");
      }
      $date = str_replace($find, $replace, $date);


      if (Yii::$app->language == 'ar') {
        $find = array("AM", "PM");
        $replace = array("صباحا", "مساءا");
        $date = str_replace($find, $replace, $date);
      }


      if (Yii::$app->language == 'ar') {
        $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $eastern_arabic_symbols = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
        $date = str_replace($standard, $eastern_arabic_symbols, $date);
      }
    }
    return $date;
  }

  public static function getSortImages() {
    $sortImage = [];
    $sortImage[''] = "/images/nosort.png";
    $sortImage['ASC'] = "/images/asc.png";
    $sortImage['DESC'] = "/images/des.png";
    return $sortImage;
  }

  public static function renderSummary($dataprovider, $pagination, $summary = null, $pad = "") {
    $count = count($dataprovider);
    $TotalCount = (int) $pagination->totalCount;
    if ($count <= 0) {
      return '';
    }
    $summaryOptions = ['class' => 'summary'];
    $tag = ArrayHelper::remove($summaryOptions, 'tag', 'div');
    if ($pagination !== false) {
      $totalCount = $TotalCount;
      $begin = $pagination->getPage() * $pagination->pageSize + 1;
      $end = $begin + $count - 1;
      if ($begin > $end) {
        $begin = $end;
      }
      $page = $pagination->getPage() + 1;
      $pageCount = $pagination->pageCount;
      if (($summaryContent = $summary) === null) {
        return \yii\helpers\Html::tag($tag, Yii::t('app', '<b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{result} other{results}}', [
                  'begin' => $begin,
                  'end' => $end,
                  'count' => $count,
                  'totalCount' => $totalCount,
                  'page' => $page,
                  'pageCount' => $pageCount,
                ]) . ($pad == "" ? "" : Yii::t('app', " for ") . $pad), $summaryOptions);
      }
    }
    else {
      $begin = $page = $pageCount = 1;
      $end = $totalCount = $count;
      if (($summaryContent = $summary) === null) {
        return \yii\helpers\Html::tag($tag, Yii::t('yii', 'Total <b>{count, number}</b> {count, plural, one{item} other{items}}.', [
                  'begin' => $begin,
                  'end' => $end,
                  'count' => $count,
                  'totalCount' => $totalCount,
                  'page' => $page,
                  'pageCount' => $pageCount,
                ]), $summaryOptions);
      }
    }

    return Yii::$app->getI18n()->format($summaryContent, [
          'begin' => $begin,
          'end' => $end,
          'count' => $count,
          'totalCount' => $totalCount,
          'page' => $page,
          'pageCount' => $pageCount,
            ], Yii::$app->language);
  }

  public static function getYesNo() {
    $rtn = [['id' => 1, 'name' => Yii::t('app', 'Yes')], ['id' => 0, 'name' => Yii::t('app', 'No')]];
    return ArrayHelper::map($rtn, 'id', 'name');
    ;
  }

  public static function getMonthNames() {
    $rtn = array();
    for ($i = 1; $i <= 12; $i++) {
      $monthName = self::getMonthNameById($i);
      $rtn[] = ['id' => $i, 'name' => $monthName];
    }
    return ArrayHelper::map($rtn, 'id', 'name');
  }

  public static function getMonthNameById($month) {
    $dateObj = \DateTime::createFromFormat('!m', $month);
    return $dateObj->format('F');
  }

  public static function getCountries(){
    $rtn = [['id' => 'AU', 'name' => Yii::t('app', 'Australia')],['id' => 'NZ', 'name' => Yii::t('app', 'New Zealand')]];
    return ArrayHelper::map($rtn, 'id', 'name');
  }
  public static function getFieldTypes() {
    $rtn = [['id' => 'C', 'name' => Yii::t('app', 'Check Boxes')], ['id' => 'R', 'name' => Yii::t('app', 'Radio Buttons')], ['id' => 'T', 'name' => Yii::t('app', 'Text Field')], ['id' => 'D', 'name' => Yii::t('app', 'Date Field')]];
    return ArrayHelper::map($rtn, 'id', 'name');
    ;
  }

  public static function getFieldTypeByID($id) {
    $rtn = ['C' => Yii::t('app', 'Check Boxes'), 'R' => Yii::t('app', 'Radio Buttons'), 'T' => Yii::t('app', 'Text Field'), 'D' => Yii::t('app', 'Date Field')];
    return $rtn[$id]
    ;
  }
  public static function getQType() {
    $rtn = [['id' => '1', 'name' => Yii::t('app', 'Yes')], ['id' => '0', 'name' => Yii::t('app', 'No')]];
    return ArrayHelper::map($rtn, 'id', 'name');
    ;
  }

  public static function getQTypeByID($id) {
    $rtn = ['1' => Yii::t('app', 'Yes'), '0' => Yii::t('app', 'No')];
    return $rtn[$id]
    ;
  }

  public static function getBoxTypes() {
    $rtn = [['id' => 'women', 'name' => Yii::t('app', 'Women')], ['id' => 'men', 'name' => Yii::t('app', 'Men')], ['id' => 'baby', 'name' => Yii::t('app', 'Baby')], ['id' => 'kstrt', 'name' => Yii::t('app', 'Kick Start')]];
    return ArrayHelper::map($rtn, 'id', 'name');
    ;
  }

  public static function getBoxTypeByID($id) {
    $rtn = ['women' => Yii::t('app', 'Women'), 'men' => Yii::t('app', 'Men'), 'baby' => Yii::t('app', 'Baby'), 'kstrt' => Yii::t('app', 'Kick Start')];
    return $rtn[$id]
    ;
  }

  public static function getColorById($id) {
    if ($id == null) {
      return null;
    }
    $brand = \common\models\Colors::findOne($id);
    if ($brand != null) {
      return $brand->name;
    }
    return '-Unknown-';
  }

  public static function getPageLayouts() {
    return ArrayHelper::map(\backend\models\Landingpages::find()->all(), 'id', 'name');
  }

  public static function getPageLayoutById($id) {
    if ($id == null) {
      return null;
    }
    $brand = \backend\models\Landingpages::findOne($id);
    if ($brand != null) {
      return $brand->name;
    }
    return '-Unknown-';
  }
  
   public static function getPageContents() {
    return ArrayHelper::map(\backend\models\Pages::find()->all(), 'id', 'name');
  }

  public static function getPageContentById($id) {
    if ($id == null) {
      return null;
    }
    $brand = \backend\models\Pages::findOne($id);
    if ($brand != null) {
      return $brand->name;
    }
    return '-Unknown-';
  }
  
  public static function getBeautyPages() {
    return ArrayHelper::map(\common\models\Beautyprofilepage::find()->all(), 'id', 'name');
  }

  public static function getBeautyPageById($id) {
    if ($id == null) {
      return null;
    }
    $brand = \common\models\Beautyprofilepage::findOne($id);
    if ($brand != null) {
      return $brand->name;
    }
    return '-Unknown-';
  }

  public static function getBrandNameById($id) {
    if ($id == null) {
      return null;
    }
    $brand = \common\models\Brand::findOne($id);
    if ($brand != null) {
      return $brand->name;
    }
    return '-Unknown-';
  }

  public static function getProductTypes() {
    $rtn = [['id' => '1', 'name' => Yii::t('app', 'Box Subscription')],
        ['id' => '2', 'name' => Yii::t('app', 'GWP')],
        ['id' => '3', 'name' => Yii::t('app', 'Product')],
        ['id' => '4', 'name' => Yii::t('app', 'Sample')],
        ['id' => '5', 'name' => Yii::t('app', 'Subscription Gift')]];
    return ArrayHelper::map($rtn, 'id', 'name');
  }

  public static function getProductTypeByID($id) {
    $rtn = ['1' => 'Box Subscription', '2' => 'GWP', '3' => 'Product', '4' => 'Sample', '5' => 'Subscription Gift'];
    return $rtn[$id];
  }

  public static function getYesNoById($id) {
    return Yii::t('app', ($id == '1' ? 'Yes' : 'No'));
  }

  public static function getStatus2() {
    $rtn = [['id' => 'Active'], ['id' => 'Inactive']];
    return ArrayHelper::map($rtn, 'id', 'id');
    ;
  }

  public static function getStatus() {
    $rtn = [['id' => 1, 'name' => Yii::t('app', 'Active')], ['id' => 0, 'name' => Yii::t('app', 'Inactive')]];
    return ArrayHelper::map($rtn, 'id', 'name');
    ;
  }
  
  public static function getStatusId($id) {
    return Yii::t('app', ($id == '1' ? 'Active' : 'Inactive'));
  }

  public static function getBpDivSize() {
    $rtn = [['id' => 1, 'name' => Yii::t('app', 'Small')], ['id' => 0, 'name' => Yii::t('app', 'Large')]];
    return ArrayHelper::map($rtn, 'id', 'name');
    ;
  }

  public static function getBpDivSizeId($id) {
    return Yii::t('app', ($id == '1' ? 'Small' : 'Large'));
  }


  private static function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key => $row) {
      $sort_col[$key] = $row[$col];
    }
    array_multisort($sort_col, $dir, $arr);
  }

  public static function getUsers() {
    $countries = \common\models\User::find()->all();
    return ArrayHelper::map($countries, 'id', 'fullname');
  }

  public static function getEnabled($option) {
    if (self::$_enabled == null || count(self::$_enabled) == 0) {
      $arr = \common\models\Enabled::find()->all();
      foreach ($arr as $value) {
        self::$_enabled[$value->name] = $value->enabled == 1 ? true : false;
      }
    }
    return isset(self::$_enabled[$option]) ? self::$_enabled[$option] : false;
  }

  public static function getUNames($option) {
    $return = "--UNKNOW--";
    if ($option == 'updateSuccess') {
      $return = "Record updated successfully";
    }
    elseif ($option == 'createSuccess') {
      $return = "Record saved successfully";
    }
    return $return;
  }

  public static $OptionName = array(
    array('id' => Controller2::LOGIN, 'name' => 'Login', 'pname' => 'Login', 'policy' => 'N', 'log' => 'Y',),
    //    array('id' => Controller2::REPORT_BUG, 'name' => 'Report Issue / Error', 'pname' => 'Report Issue / Error', 'policy' => 'N'),
    array('id' => Controller2::LOGOUT, 'name' => 'Logout', 'pname' => 'Logout', 'policy' => 'N', 'log' => 'Y',),
    array('id' => Controller2::CHANGE_PASS, 'name' => 'Change Password', 'pname' => 'Change Password', 'policy' => 'N', 'log' => 'Y',),
    array('id' => Controller2::ATTACHMENTS, 'name' => 'Attachment', 'pname' => 'Attachments', 'policy' => 'N'), 'log' => 'N',
    array('id' => Controller2::USERS, 'name' => 'User', 'pname' => 'Users', 'policy' => 'Y', 'qpolicy' => 'Y', 'log' => 'Y',
      array(array('id' => 1300, 'name' => 'View', 'denied' => '0'),
        array('id' => 1301, 'name' => 'Create', 'denied' => '0'),
        array('id' => 1302, 'name' => 'Update', 'denied' => '0'),
        array('id' => 1303, 'name' => 'Delete', 'denied' => '0'),
        array('id' => 1304, 'name' => 'Admin', 'denied' => '0'))),
    array('id' => Controller2::ROLE, 'name' => 'Role', 'pname' => 'Roles', 'policy' => 'Y', 'qpolicy' => 'Y', 'log' => 'Y',
      array(array('id' => 1500, 'name' => 'View', 'denied' => '0'),
        array('id' => 1501, 'name' => 'Create', 'denied' => '0'),
        array('id' => 1502, 'name' => 'Update', 'denied' => '0'),
        array('id' => 1503, 'name' => 'Delete', 'denied' => '0'),
        array('id' => 1504, 'name' => 'Admin', 'denied' => '0'))),
    array('id' => Controller2::LOG, 'name' => 'Log', 'pname' => 'Log', 'policy' => 'Y', 'log' => 'N',
      array(array('id' => 1403, 'name' => 'Delete', 'denied' => '0'),
        array('id' => 1404, 'name' => 'Admin', 'denied' => '0'))),
    array('id' => Controller2::PRODUCTS, 'name' => 'Product', 'pname' => 'Products', 'policy' => 'N'),
    array('id' => Controller2::BOXS, 'name' => 'Box Variant', 'pname' => 'Box Variants', 'policy' => 'N'),
    array('id' => Controller2::PAGE_LAYOUT, 'name' => 'Page Layout Design', 'pname' => 'Page Layout Designs', 'policy' => 'N'),
    array('id' => Controller2::PAGE_CONTENTS, 'name' => 'Page Content Design', 'pname' => 'Page Content Designs', 'policy' => 'N'),
    array('id' => Controller2::GENERATE_ALLOCATION, 'name' => 'Generate Allocation List', 'pname' => 'Generate Allocation List', 'policy' => 'N'),
    array('id' => Controller2::UPDATE_SHIPMENT, 'name' => 'Update Shipment Information', 'pname' => 'Update Shipment Information', 'policy' => 'N'),
    array('id' => Controller2::BEAUTYPROFILE_PAGE, 'name' => 'Beauty Profle Page', 'pname' => 'Beauty Profle Pages', 'policy' => 'N'),
    array('id' => Controller2::BEAUTYPROFILE_QUESTION, 'name' => 'Beauty Profle Question', 'pname' => 'Beauty Profle Questions', 'policy' => 'N'),
    array('id' => Controller2::BEAUTYPROFILE_ANSWER, 'name' => 'Beauty Profle Answer', 'pname' => 'Beauty Profle Answers', 'policy' => 'N'),
    array('id' => Controller2::NEWSLETTER_EMAILS, 'name' => 'Newsletter Email', 'pname' => 'Newsletter Emails', 'policy' => 'N'),
    array('id' => Controller2::UPDATE_ADDRESS_CB, 'name' => 'Update Addresses', 'pname' => 'Update Addresses', 'policy' => 'N'),
  );

  public static function getActionOption($id) {
    $ActionOptionName = "";

    foreach (Options::$OptionName as $item) {
      if ($item['policy'] == "Y") {
        foreach ($item as $rows) {
          if (is_array($rows)) {
            foreach ($rows as $value) {
              if ($value["id"] == $id) {
                $ActionOptionName = Yii::t('app', $value["name"]);
                break;
              }
            }
          }
        }
      }
    }

    return $ActionOptionName;
  }

  public static function getPolicyOptions() {

    $OptionNames = array();
    $count = 0;
    foreach (Options::$OptionName as $item) {
      $count ++;
      if ($item['policy'] == "Y") {
        $OptionNames[$count] = $item;
      }
    }

    return $OptionNames;
  }

  public static function getQoutaPolicyOptions() {

    $OptionNames = array();
    $count = 0;
    foreach (Options::$OptionName as $item) {
      $count ++;
      $pol = 'N';
      if (isset($item['qpolicy']))
        $pol = $item['qpolicy'];
      if ($pol == "Y") {
        $OptionNames[$count] = $item;
      }
    }

    return $OptionNames;
  }

  public static function getOptionName($id) {
    $OptionName = "";

    foreach (Options::$OptionName as $item) {
      if (isset($item['id']) && $item['id'] == $id) {
        $OptionName = Yii::t('app', $item['name']);
        break;
      }
    }
    return $OptionName;
  }

  public static function getMaintainLog($id) {
    $MaintinLog = false;
    foreach (Options::$OptionName as $item) {
      if (isset($item['id']) && $item['id'] == $id) {
        if (isset($item['log']))
          $MaintinLog = $item['log'] == 'Y' ? true : false;
        break;
      }
    }
    return $MaintinLog;
  }

  public static function getOptionPName($id) {
    $OptionName = "";
    foreach (Options::$OptionName as $item) {
      if (isset($item['id']) && $item['id'] == $id) {
        $OptionName = Yii::t('app', $item['pname']);
        break;
      }
    }
    return $OptionName;
  }

  public static function getOptionById($id) {
    $Option = null;
    foreach (Options::$OptionName as $item) {
      if (isset($item['id']) && $item['id'] == $id) {
        $item['name'] = Yii::t('app', $item['name']);
        $Option = $item;
        break;
      }
    }
    return $Option;
  }

  public static function isRights($user_id, $option_id) {

    if (Yii::app()->user->isAdmin()) {
      return true;
    }
    $option = MenuRights::model()->findAllByAttributes(
        array('user_id' => $user_id, 'menu_id' => $option_id));

    if (!isset($option)) {
      return false;
    }

    if (count($option) < 1) {
      return false;
    }
    else {
      return true;
    }
  }

}
