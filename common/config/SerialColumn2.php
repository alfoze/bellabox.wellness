<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\config;

use yii\grid\SerialColumn;

class SerialColumn2 extends SerialColumn {

    public function renderDataCell($model, $key, $index) {
        return Options::convertNumberToArabic(parent::renderDataCell($model, $key, $index));
    }
}