<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class FileForm2 extends Model
{
    public $file;
    public $listdate;
    public $par1;
    public $par2;
    public $par3;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file','listdate'], 'required'],
            [['file'], 'file'],
            [['par1','par2','par3','listdate'], 'safe'],
        ];
    }
    public function attributeLabels() {
    return [
      'listdate' => Yii::t('app', 'Allocation List Date'),
      
    ];
  }
}