<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reward_point".
 *
 * @property integer $id
 * @property string $reason
 * @property string $reason_code
 * @property integer $points
 * @property string $status
 * @property string $create_date
 * @property string $expiry_date
 * @property string $expired
 * @property string $customer_id
 */
class Rewardpoint extends \common\config\ActiveRecord2
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reward_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reason', 'reason_code', 'points', 'customer_id'], 'required'],
            [['points'], 'integer'],
            [['create_date', 'expiry_date'], 'safe'],
            [['reason'], 'string', 'max' => 100],
            [['reason_code', 'status', 'expired'], 'string', 'max' => 1],
            [['customer_id'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'reason' => Yii::t('app', 'Reason'),
            'reason_code' => Yii::t('app', 'Reason Code'),
            'points' => Yii::t('app', 'Points'),
            'status' => Yii::t('app', 'Status'),
            'create_date' => Yii::t('app', 'Create Date'),
            'expiry_date' => Yii::t('app', 'Expiry Date'),
            'expired' => Yii::t('app', 'Expired'),
            'customer_id' => Yii::t('app', 'Customer ID'),
        ];
    }
}
