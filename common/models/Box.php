<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "box".
 *
 * @property integer $id
 * @property string $name
 * @property string $variant_name
 * @property string $type
 * @property integer $month
 * @property string $year
 * @property string $image
 * @property string $samples
 * @property string $status
 * @property string $typeform_url
 * @property string $typeform_code
 */
class Box extends \common\config\ActiveRecord2 {

  public $PID = 101;
  public $oldImage = '';

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'box';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['name', 'variant_name', 'type', 'month', 'year', 'status'], 'required'],
        [['month'], 'integer'],
        [['name'], 'string', 'max' => 200],
        [['variant_name'], 'string', 'max' => 100],
        [['type'], 'string', 'max' => 5],
        [['year'], 'string', 'max' => 4],
        [['image'], 'string', 'max' => 400],
        [['samples'], 'string', 'max' => 1000],
        [['status'], 'string', 'max' => 1],
        [['typeform_url'], 'string', 'max' => 255],
        [['typeform_code'], 'string', 'max' => 50]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => Yii::t('app', 'ID'),
      'name' => Yii::t('app', 'Name'),
      'variant_name' => Yii::t('app', 'Variant Name'),
      'type' => Yii::t('app', 'Box Type'),
      'month' => Yii::t('app', 'Month'),
      'year' => Yii::t('app', 'Year'),
      'image' => Yii::t('app', 'Image'),
      'samples' => Yii::t('app', 'Samples'),
      'status' => Yii::t('app', 'Status'),
      'typeform_url' => Yii::t('app', 'Typeform Url'),
      'typeform_code' => Yii::t('app', 'Typeform Code'),
    ];
  }

  public function afterFind() {
    $this->oldImage = $this->image;
    return parent::afterFind();
  }

}
