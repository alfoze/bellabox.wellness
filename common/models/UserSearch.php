<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * CommunitySearch represents the model behind the search form about `common\models\Community`.
 */
class UserSearch extends User {

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['id'], 'integer'],
        [['username', 'mobile', 'c_timestamp', 'cb_customer_id', 'is_admin', 'company_name', 'email', 'f_name', 'l_name'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios() {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params, $pagination = true, $cbuser = false) {
    $query = User::find();

    $aDP['query'] = $query;
    if ($pagination == false) {
      $aDP['pagination'] = false;
    }
    $dataProvider = new ActiveDataProvider($aDP);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'is_admin' => $this->is_admin,
    ]);

    $dateOperator = $this->getOperatorLocal($this->c_timestamp);
    $value = str_replace($dateOperator, '', $this->c_timestamp);
    $query->andFilterWhere([$dateOperator, 'c_timestamp', $value]);

    $query->andFilterWhere(['like', 'username', $this->username])
        ->andFilterWhere(['like', 'f_name', $this->f_name])
        ->andFilterWhere(['like', 'l_name', $this->l_name])
        ->andFilterWhere(['like', 'email', $this->email])
        ->andFilterWhere(['like', 'mobile', $this->mobile])
        ->andFilterWhere(['like', 'cb_customer_id', $this->cb_customer_id]);
    
    if ($cbuser == true) {
      $query->andWhere(['not', ['cb_customer_id' => null]]);
    }

    return $dataProvider;
  }

  private function getOperatorLocal($qryString) {
    switch ($qryString) {
      case strpos($qryString, '>=') === 0:
        $operator = '>=';
        break;
      case strpos($qryString, '>') === 0:
        $operator = '>';
        break;
      case strpos($qryString, '<=') === 0:
        $operator = '<=';
        break;
      case strpos($qryString, '<') === 0:
        $operator = '<';
        break;
      default:
        $operator = 'like';
        break;
    }
    return $operator;
  }

}
