<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "optin_history".
 *
 * @property integer $id
 * @property string $sub_id
 * @property string $typeform_id
 * @property string $created_when
 */
class Optinhistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'optin_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_id', 'typeform_id'], 'required'],
            [['created_when'], 'safe'],
            [['sub_id', 'typeform_id'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sub_id' => 'Sub ID',
            'typeform_id' => 'Typeform ID',
            'created_when' => 'Created When',
        ];
    }
}
