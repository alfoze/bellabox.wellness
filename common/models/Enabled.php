<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "es_enabled".
 *
 * @property string $name
 * @property string $enabled
 * @property string $show
 */
class Enabled extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'es_enabled';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 20],
            [['enabled'], 'string', 'max' => 1],
            [['show'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'enabled' => Yii::t('app', 'Enabled'),
            'enabled' => Yii::t('app', 'show'),
        ];
    }
}
