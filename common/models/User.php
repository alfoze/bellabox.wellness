<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $f_name
 * @property string $l_name
 * @property string $is_admin
 * @property string $is_active
 * @property string $company_name
 * @property string $mobile
 * @property string $cb_customer_id
 * @property string $c_timestamp
 * @property string $tmp_pass
 */
class User extends ActiveRecord implements IdentityInterface {

  const STATUS_DELETED = 0;
  const STATUS_ACTIVE = 10;

  public $fullname = "";
  public $name = "";
  public $rate = "";
  private $isNew = false;

  public $password1 ='';
  public $password2 ='';
  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'es_user';
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      TimestampBehavior::className(),
    ];
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        ['status', 'default', 'value' => self::STATUS_ACTIVE],
        ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        [['company_name'], 'string', 'max' => 255],
        [['cb_customer_id'], 'string', 'max' => 100],
        [['mobile'], 'string', 'max' => 15],
        [['tmp_pass'], 'string', 'max' => 100],
        [['mobile', 'f_name', 'email'], 'required'],
    ];
  }

  public function attributeLabels() {
    return [
      'username' => Yii::t('app', 'User Name'),
      'f_name' => Yii::t('app', 'First Name'),
      'l_name' => Yii::t('app', 'Last Name'),
      'is_admin' => Yii::t('app', 'Admin'),
      'is_active' => Yii::t('app', 'Active'),
      'email' => Yii::t('app', 'Email'),
      'mobile' => Yii::t('app', 'Contact Number'),
      'notes' => Yii::t('app', 'Overview'),
      'company_name' => Yii::t('app', 'Company Name'),
      'c_timestamp' => Yii::t('app', 'Registration Date'),
      'creationdate' => Yii::t('app', 'Registration Date'),
      'cb_customer_id' => Yii::t('app', 'Chargebee Customer ID'),
      'tmp_pass' => 'Temporary Password',
      'password1' => 'Password',
      'password2' => 'Confirm Password',
    ];
  }

  public function getCreationdate() {
    return date('d/m/Y H:m', $this->created_at);
  }

  /**
   * @inheritdoc
   */
  public static function findIdentity($id) {
    return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
  }

  /**
   * @inheritdoc
   */
  public static function findIdentityByAccessToken($token, $type = null) {
    return static::findOne(['auth_key' => $token]);
  }

  /**
   * Finds user by username
   *
   * @param string $username
   * @return static|null
   */
  public static function findByUsername($username) {
    return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
  }

  /**
   * Finds user by password reset token
   *
   * @param string $token password reset token
   * @return static|null
   */
  public static function findByPasswordResetToken($token) {
    if (!static::isPasswordResetTokenValid($token)) {
      return null;
    }

    return static::findOne([
          'password_reset_token' => $token,
          'status' => self::STATUS_ACTIVE,
    ]);
  }

  /**
   * Finds out if password reset token is valid
   *
   * @param string $token password reset token
   * @return boolean
   */
  public static function isPasswordResetTokenValid($token) {
    if (empty($token)) {
      return false;
    }
    $expire = Yii::$app->params['user.passwordResetTokenExpire'];
    $parts = explode('_', $token);
    $timestamp = (int) end($parts);
    return $timestamp + $expire >= time();
  }

  /**
   * @inheritdoc
   */
  public function getIsAdmin() {
    return $this->is_admin;
  }

  public function getIsActive() {
    return $this->is_active;
  }

  public function getId() {
    return $this->getPrimaryKey();
  }

  /**
   * @inheritdoc
   */
  public function getAuthKey() {
    return $this->auth_key;
  }

  /**
   * @inheritdoc
   */
  public function validateAuthKey($authKey) {
    return $this->getAuthKey() === $authKey;
  }

  /**
   * Validates password
   *
   * @param string $password password to validate
   * @return boolean if password provided is valid for current user
   */
  public function validatePassword($password) {
    return Yii::$app->security->validatePassword($password, $this->password_hash);
  }

  /**
   * Generates password hash from password and sets it to the model
   *
   * @param string $password
   */
  public function setPassword($password) {
    $this->password_hash = Yii::$app->security->generatePasswordHash($password);
  }

  /**
   * Generates "remember me" authentication key
   */
  public function generateAuthKey() {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }

  /**
   * Generates new password reset token
   */
  public function generatePasswordResetToken() {
    $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
  }

  /**
   * Removes password reset token
   */
  public function removePasswordResetToken() {
    $this->password_reset_token = null;
  }

  public function afterFind() {
    $this->fullname = $this->f_name . ' ' . $this->l_name;
    $this->name = $this->f_name . ' ' . $this->l_name;
    parent::afterFind();
  }

  public function beforeSave($insert) {
    if ($this->isNewRecord) {
      $this->isNew = true;
    }
    return parent::beforeSave($insert);
  }

  public function afterSave($insert, $changedAttributes) {
    if ($this->isNew) {
//            $this->sendMail($this->email, $this->f_name);
    }
    return parent::afterSave($insert, $changedAttributes);
  }

  public function sendMail($to, $name) {
//    $rp = "NO MAIL";
//    $id = "signup";
//    $subject = "Congratulations on joining IService";
//    $emailtemplate = "";
//    $email = \common\models\Emailtemplate::findOne($id);
//    if ($email != null) {
//      if ($email->active == "1") {
//        $emailtemplate = $email->content;
//        $emailtemplate = str_replace('[USER_NAME]', $name, $emailtemplate);
//        $message = '<html><head><title>-</title></head><body>' . $emailtemplate . ' </body></html>';
//
//        $rp = @\Yii::$app->mailer->compose()
//                ->setFrom([\Yii::$app->params['adminEmail'] => 'IService'])
//                ->setTo($to)
//                ->setSubject($subject)
//                ->setHtmlBody($emailtemplate)
//                ->send();
//      }
//    }
    return;
  }

}
