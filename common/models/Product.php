<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $fullname
 * @property string $type
 * @property string $sku
 * @property string $price
 * @property string $org_price
 * @property string $status
 * @property string $image
 * @property string $color
 * @property string $size
 * @property integer $brand
 * @property string $typeform_url
 * @property string $typeform_code
 */
class Product extends \common\config\ActiveRecord2 {

  public $PID = 100;
  public $oldImage = '';

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'product';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['name', 'type', 'sku', 'status'], 'required'],
        [['price', 'org_price'], 'number'],
        [['brand'], 'integer'],
        [['name', 'fullname'], 'string', 'max' => 200],
        [['type'], 'string', 'max' => 10],
        [['sku'], 'string', 'max' => 20],
        [['status'], 'string', 'max' => 1],
        [['image','typeform_url'], 'string', 'max' => 255],
        [['typeform_code'], 'string', 'max' => 50],
        [['color', 'size'], 'string', 'max' => 100]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => Yii::t('app', 'ID'),
      'name' => Yii::t('app', 'Name'),
      'fullname' => Yii::t('app', 'Full Name'),
      'type' => Yii::t('app', 'Type'),
      'sku' => Yii::t('app', 'SKU'),
      'price' => Yii::t('app', 'Price'),
      'org_price' => Yii::t('app', 'Original Price'),
      'status' => Yii::t('app', 'Status'),
      'image' => Yii::t('app', 'Image'),
      'color' => Yii::t('app', 'Color'),
      'size' => Yii::t('app', 'Size'),
      'brand' => Yii::t('app', 'Brand'),
      'typeform_url' => Yii::t('app', 'Typeform Url'),
      'typeform_code' => Yii::t('app', 'Typeform Code'),
    ];
  }

  public function afterFind() {
    $this->oldImage = $this->image;
    return parent::afterFind();
  }

}
