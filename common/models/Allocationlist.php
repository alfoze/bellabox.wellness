<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "allocation_list".
 *
 * @property integer $id
 * @property string $name
 * @property integer $allocated
 * @property string $create_date
 */
class Allocationlist extends \common\config\ActiveRecord2
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'allocation_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'allocated'], 'required'],
            [['allocated'], 'integer'],
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'allocated' => Yii::t('app', 'Allocated'),
            'create_date' => Yii::t('app', 'Create Date'),
        ];
    }
}
