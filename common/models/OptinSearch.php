<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Optin;

/**
 * OptinSearch represents the model behind the search form about `\common\models\Optin`.
 */
class OptinSearch extends Optin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'text_to_show', 'active', 'type_form_code', 'text_chosen'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Optin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'text_to_show', $this->text_to_show])
            ->andFilterWhere(['like', 'active', $this->active])
            ->andFilterWhere(['like', 'type_form_code', $this->type_form_code])
            ->andFilterWhere(['like', 'text_chosen', $this->text_chosen]);

        return $dataProvider;
    }
}
