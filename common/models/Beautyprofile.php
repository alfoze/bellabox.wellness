<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "beautyprofile".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $answer_text
 * @property string $answer_date
 * @property integer $page_id
 * @property integer $question_id
 * @property integer $answer
 */
class Beautyprofile extends \common\config\ActiveRecord2 {

  public $type;
  const ANS_REQUIRED = 'ans_required';
  const ANSTXT_REQUIRED = 'anstxt_required';
  const ANSDATE_REQUIRED = 'ansdate_required';

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'beautyprofile';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['customer_id', 'page_id', 'question_id'], 'required'],
        [['page_id', 'question_id', 'answer'], 'integer'],
        [['customer_id', 'answer_text'], 'string', 'max' => 100],
        [['type'], 'string', 'max' => 1],
        [['answer_date'], 'safe'],
        [['answer'], 'required', 'on' => self::ANS_REQUIRED],
        [['answer_text'], 'required', 'on' => self::ANSTXT_REQUIRED],
        [['answer_date'], 'required', 'on' => self::ANSDATE_REQUIRED],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => Yii::t('app', 'ID'),
      'customer_id' => Yii::t('app', 'Customer ID'),
      'page_id' => Yii::t('app', 'Page ID'),
      'question_id' => Yii::t('app', 'Question ID'),
      'answer' => Yii::t('app', 'Answer'),
      'answer_text' => Yii::t('app', 'Answer'),
      'answer_date' => Yii::t('app', 'Answer'),
    ];
  }

  public function beforeSave($insert) {
    if ($this->answer_date != null) {
      $this->answer_date = str_replace('/', '-', $this->answer_date);
      $this->answer_date = date('Y-m-d', strtotime($this->answer_date));
    }
    return parent::beforeSave($insert);
  }

  public function afterFind() {
    if ($this->answer_date != null) {
      $this->answer_date = date("d/m/Y", strtotime($this->answer_date));
    }
    return parent::afterFind();
  }

}
