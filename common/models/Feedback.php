<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property string $survey_id
 * @property string $customer_email
 * @property string $customer_id
 * @property string $created_when
 * @property string $box_id
 */
class Feedback extends \common\config\ActiveRecord2
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['survey_id', 'customer_email', 'customer_id'], 'required'],
            [['created_when'], 'safe'],
            [['box_id'], 'integer'],
            [['survey_id', 'customer_id'], 'string', 'max' => 50],
            [['customer_email'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'survey_id' => Yii::t('app', 'Survey ID'),   
            'box_id' => Yii::t('app', 'Box ID'),
            'customer_email' => Yii::t('app', 'Customer Email'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'created_when' => Yii::t('app', 'Created When'),
        ];
    }
}
