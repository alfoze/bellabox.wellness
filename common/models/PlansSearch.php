<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Plans;

/**
 * PlansSearch represents the model behind the search form about `\common\models\Plans`.
 */
class PlansSearch extends Plans {

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['id'], 'integer'],
        [['name', 'description', 'cb_name', 'cb_url', 'active', 'gift'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios() {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params) {
    $query = Plans::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
    ]);

    $query->andFilterWhere(['like', 'name', $this->name])
        ->andFilterWhere(['like', 'description', $this->description])
        ->andFilterWhere(['like', 'cb_name', $this->cb_name])
        ->andFilterWhere(['like', 'cb_url', $this->cb_url])
        ->andFilterWhere(['like', 'gift', $this->gift])
        ->andFilterWhere(['like', 'active', $this->active]);

    return $dataProvider;
  }

}
