<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "beautyprofilepage".
 *
 * @property integer $id
 * @property string $name
 * @property integer $weight
 * @property integer $div_size
 * @property string $active
 */
class Beautyprofilepage extends \common\config\ActiveRecord2
{
    public $PID = 104;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'beautyprofilepage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'weight'], 'required'],
            [['weight','div_size'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['active'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'weight' => Yii::t('app', 'Sort Order'),
            'div_size' => Yii::t('app', 'Div Size'),
            'active' => Yii::t('app', 'Status'),
        ];
    }
}
