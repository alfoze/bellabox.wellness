<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "es_operations".
 *
 * @property integer $id
 * @property integer $option_id
 * @property string $operation
 * @property string $o_time
 * @property integer $o_by
 * @property string $o_msg
 */
class Operations extends \common\config\ActiveRecord2
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'es_operations';
    }

    public $toUser = null;
    public $type = null;
    public $code = null;
    public $title = null;
    public $params = null;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'o_by'], 'integer'],
            [['o_time'], 'safe'],
            [['operation'], 'string', 'max' => 45],
            [['o_msg'], 'string', 'max' => 160]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'option_id' => Yii::t('app', 'Option ID'),
            'operation' => Yii::t('app', 'Operation'),
            'o_time' => Yii::t('app', 'O Time'),
            'o_by' => Yii::t('app', 'O By'),
            'o_msg' => Yii::t('app', 'O Msg'),
        ];
    }
    
    public function getFrom() {
        $city = User::findOne($this->o_by);
        if ($city != null)
            return Yii::t('app', $city->fullname);
        else
            return "";
    }
    
    public function beforeSave($insert) {
        
        if($this->toUser != null){
            $this->params = \common\config\Options::sendCloudMessage($this->toUser, $this->title, $this->o_msg,$this->code,null, $this->type, $this->option_id, $this->o_by,$this->getFrom());
        }
        return parent::beforeSave($insert);
    }
}
