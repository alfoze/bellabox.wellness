<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "es_faq".
 *
 * @property integer $id
 * @property string $question
 * @property string $question_ar
 * @property string $answer
 * @property string $answer_ar
 * @property string $c_timestamp
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'es_faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question', 'answer'], 'required'],
            [['answer','answer_ar'], 'string'],
            [['question', 'question_ar'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'question' => Yii::t('app', 'Question'),
            'answer' => Yii::t('app', 'Answer'),
            'c_timestamp' => Yii::t('app', 'Timestamp'),
            'question_ar' => Yii::t('app', 'Question in Arabic'),
            'answer_ar' => Yii::t('app', 'Answer in Arabic'),
        ];
    }
}
