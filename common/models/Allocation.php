<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "allocation".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $subscription_id
 * @property string $order_id
 * @property string $box_name
 * @property string $mail_ref
 * @property string $created_when
 * @property integer $batch_id
 * @property integer $box_id
 */
class Allocation extends \common\config\ActiveRecord2 {

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'allocation';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['subscription_id', 'order_id', 'box_name', 'mail_ref', 'batch_id', 'box_id'], 'required'],
        [['created_when'], 'safe'],
        [['batch_id', 'box_id'], 'integer'],
        [['customer_id', 'subscription_id', 'order_id', 'mail_ref'], 'string', 'max' => 50],
        [['box_name'], 'string', 'max' => 100]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => Yii::t('app', 'ID'),
      'customer_id' => Yii::t('app', 'Customer ID'),
      'subscription_id' => Yii::t('app', 'Subscription ID'),
      'order_id' => Yii::t('app', 'Order ID'),
      'box_name' => Yii::t('app', 'Box Name'),
      'mail_ref' => Yii::t('app', 'Mail Ref'),
      'created_when' => Yii::t('app', 'Created When'),
      'batch_id' => Yii::t('app', 'Batch ID'),
      'box_id' => Yii::t('app', 'Box ID'),
    ];
  }

}
