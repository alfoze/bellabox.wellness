<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Box;

/**
 * BoxSearch represents the model behind the search form about `common\models\Box`.
 */
class BoxSearch extends Box
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'month'], 'integer'],
            [['name', 'variant_name', 'type', 'year', 'image', 'samples', 'status', 'typeform_url', 'typeform_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Box::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'month' => $this->month,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'variant_name', $this->variant_name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'year', $this->year])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'samples', $this->samples])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'typeform_url', $this->typeform_url])
            ->andFilterWhere(['like', 'typeform_code', $this->typeform_code]);

        return $dataProvider;
    }
}
