<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contents".
 *
 * @property integer $id
 * @property string $name
 * @property string $path
 */
class Contents extends \yii\db\ActiveRecord {

  public $oldImage;

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'contents';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['name', 'path'], 'required'],
        [['name', 'path'], 'string', 'max' => 200],
      
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'name' => 'Name',
      'path' => 'Path',
      'url' => 'Url',
    ];
  }

  public function afterFind() {
    $this->oldImage = $this->path;
    return parent::afterFind();
  }

  public function getUrl(){
    return \common\config\Options::getFrontendAddress().$this->path;
  }
}
