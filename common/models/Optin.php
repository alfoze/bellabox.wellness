<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "optin".
 *
 * @property string $id
 * @property string $text_to_show
 * @property string $active
 * @property string $type_form_code
 * @property string $text_chosen
 */
class Optin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'optin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'text_to_show', 'text_chosen'], 'required'],
            [['id'], 'string', 'max' => 10],
            [['text_to_show', 'text_chosen'], 'string', 'max' => 100],
            [['active'], 'string', 'max' => 1],
            [['type_form_code'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Box Type',
            'text_to_show' => 'Text to Show',
            'active' => 'Status',
            'type_form_code' => 'Typeform Code',
            'text_chosen' => 'Text for Already Chosen',
        ];
    }
    public function getActivated(){
      return $this->active == '1' ?'Active':'Inactive';
    }
}
