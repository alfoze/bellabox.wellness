<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use \common\models\Log;

/**
 * LogSearch represents the model behind the search form about `\common\models\Log`.
 */
class LogSearch extends Log
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'option_type', 'option_id', 'user_id'], 'integer'],
            [['operation', 'c_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Log::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'option_type' => $this->option_type,
            'option_id' => $this->option_id,
            'c_date' => $this->c_date,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'operation', $this->operation]);

        return $dataProvider;
    }
}
