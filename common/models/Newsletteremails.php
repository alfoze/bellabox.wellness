<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "newsletter_emails".
 *
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $last_name
 * 
 * @property integer $ctimestamp 
 */
class Newsletteremails extends \common\config\ActiveRecord2 {

    public $PID = 107;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'newsletter_emails';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['email', 'name'], 'required'],
            [['ctimestamp'], 'string'],
            [['email'], 'email'],
            [['email'], 'uniqueemail'],
            [['email'], 'string', 'max' => 255],
            [['name', 'last_name'], 'string', 'max' => 100],
        ];
    }

    public function uniqueemail($attribute, $params) {
        $checkuser = \common\models\User::find()->where(['username' => $this->email])->one();
        $checkSubscribers = \common\models\Newsletteremails::find()->where(['email' => $this->email])->one();
        if ($checkuser != null || $checkSubscribers != null) {
            $this->addError($attribute, Yii::t('email', 'Email "' . $this->email . '" has already been taken.'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'First Name',
            'last_name' => 'Last Name',
            'ctimestamp' => 'Registration Date',
        ];
    }
}