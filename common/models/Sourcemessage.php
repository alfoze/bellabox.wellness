<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "source_message".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 */
class Sourcemessage extends \common\config\ActiveRecord2 {

    /**
     * @inheritdoc
     */
    public $tmessage = array();
    public $cat = 'app';
    public $tempmsg;

    public static function tableName() {
        return 'source_message';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 32],
            [['tmessage'], 'safe'],
        ];
    }

    public function getTranslation() {
        $s = Message::find()->where(['id' => $this->id])->all();
        $rtn = array();
        foreach ($s as $value) {
            $rtn[] = $value->language.": ". $value->translation;
        }
        return $rtn;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'message' => Yii::t('app', 'Message'),
            'tmessage' => Yii::t('app', 'Translated Message'),
            'translation' => Yii::t('app', 'Translated Message'),
        ];
    }

    public function beforeSave($insert) {
        $this->category = $this->cat;
        $this->tempmsg = $this->tmessage;
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes) {
        foreach ($this->tempmsg as $key => $value) {
            if ($value != "") {
                $tmodel = \common\models\Message::findOne(['language' => $key, 'id' => $this->id]);
                if ($tmodel == null) {
                    $tmodel = new \common\models\Message();
                }
                $tmodel->id = $this->id;
                $tmodel->language = $key;
                $tmodel->translation = $value;
                $tmodel->save();
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }

}
