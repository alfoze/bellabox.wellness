<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "beautyprofileanswer".
 *
 * @property integer $id
 * @property integer $question_id
 * @property string $name
 * @property string $image
 * @property string $description
 */
class Beautyprofileanswer extends \common\config\ActiveRecord2 {

  public $PID = 106;
  public $oldImage = '';
  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'beautyprofileanswer';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['name'], 'required'],
        [['question_id'], 'integer'],
        [['name', 'description'], 'string', 'max' => 100],
        [['image','oldImage'], 'safe'],
        [['image'], 'file', 'extensions' => 'jpg, gif, png', 'maxSize' => 1024 * 1024 * 2], //max size is 2mb
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => Yii::t('app', 'ID'),
      'question_id' => Yii::t('app', 'Question ID'),
      'name' => Yii::t('app', 'Name'),
      'image' => Yii::t('app', 'Image'),
      'description' => Yii::t('app', 'Description'),
    ];
  }

  public function afterFind() {
    $this->oldImage = $this->image;
    return parent::afterFind();
  }
}
