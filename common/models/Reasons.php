<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reasons".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 */
class Reasons extends \common\config\ActiveRecord2
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reasons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['name'], 'string', 'max' => 99],
            [['type'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
