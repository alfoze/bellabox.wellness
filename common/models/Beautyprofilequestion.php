<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "beautyprofilequestion".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $name
 * @property integer $weight
 * @property string $active
 * @property string $type
 * @property string $required
 * @property Beautyprofileanswer[] $answers
 */
class Beautyprofilequestion extends \common\config\ActiveRecord2
{
    public $PID = 105;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'beautyprofilequestion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'name', 'weight'], 'required'],
            [['page_id', 'weight'], 'integer'],
            [['name'], 'string', 'max' => 500],
            [['active', 'type','required'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_id' => Yii::t('app', 'Page'),
            'name' => Yii::t('app', 'Name'),
            'weight' => Yii::t('app', 'Sort Order'),
            'active' => Yii::t('app', 'Active'),
            'type' => Yii::t('app', 'Type'),
            'required' => Yii::t('app', 'Required'),
        ];
    }
    
    public function getAnswers(){
      return Beautyprofileanswer::find()->where("question_id = '$this->id'")->all();
    }
}
