<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "es_settings".
 *
 * @property integer $id
 * @property string $name
 * @property string $cb_name
 * @property string $cb_api
 * @property string $site_logo
 * @property integer $header_id
 * @property integer $footer_id
 * @property integer $billing_day
 * @property integer $homepage
 * @property integer $giftpage
 * @property string $standalone_css
 */
class Settings extends \yii\db\ActiveRecord {

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'es_settings';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['name', 'cb_name', 'cb_api', 'site_logo'], 'required'],
        [['header_id', 'homepage', 'footer_id', 'billing_day', 'giftpage'], 'integer'],
        [['name', 'cb_name'], 'string', 'max' => 50],
        [['cb_api'], 'string', 'max' => 200],
        [['site_logo'], 'string', 'max' => 255],
        [['standalone_css'], 'string'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'name' => 'Name',
      'cb_name' => 'ChargeBee Domain Name',
      'cb_api' => 'ChargeBee API Key',
      'site_logo' => 'Site Logo',
      'header_id' => 'Frontend Page Header',
      'footer_id' => 'Frontend Page Footer',
      'billing_day' => 'Billing Day',
      'homepage' => 'Home Page',
      'giftpage' => 'Gift Page',
      'standalone_css' => 'BellaBox.au Style CSS'
    ];
  }

}
