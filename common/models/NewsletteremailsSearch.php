<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Newsletteremails;

/**
 * NewsletteremailsSearch represents the model behind the search form about `common\models\Newsletteremails`.
 */
class NewsletteremailsSearch extends Newsletteremails {

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['id'], 'integer'],
        [['email', 'ctimestamp', 'name','last_name'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios() {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params,$pagination = true) {
    $query = Newsletteremails::find();

    // add conditions that should always apply here
    $aDP['query'] =  $query;
    if ($pagination == false){
      $aDP['pagination'] =  false;
    }
    $dataProvider = new ActiveDataProvider($aDP);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id, 
    ]);

    $dateOperator = $this->getOperator($this->ctimestamp);
    $value = str_replace($dateOperator, '', $this->ctimestamp);
    $query->andFilterWhere([$dateOperator, 'ctimestamp', $value]);

    $query->andFilterWhere(['like', 'email', $this->email]);
    $query->andFilterWhere(['like', 'name', $this->name]);
    $query->andFilterWhere(['like', 'last_name', $this->last_name]);

    return $dataProvider;
  }

}
