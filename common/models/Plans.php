<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plans".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $cb_name
 * @property string $cb_url
 * @property string $active
 * @property string $gift
 */
class Plans extends \common\config\ActiveRecord2 {

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'plans';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        [['name', 'description', 'cb_name', 'cb_url'], 'required'],
        [['name'], 'string', 'max' => 20],
        [['description'], 'string', 'max' => 200],
        [['cb_name'], 'string', 'max' => 50],
        [['cb_url'], 'string', 'max' => 500],
        [['active', 'gift'], 'string', 'max' => 1]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => Yii::t('app', 'ID'),
      'name' => Yii::t('app', 'Name'),
      'description' => Yii::t('app', 'Description'),
      'cb_name' => Yii::t('app', 'ChargeBee Name'),
      'cb_url' => Yii::t('app', 'ChargeBee Url'),
      'active' => Yii::t('app', 'Active'),
      'gift' => 'Is Gift'
    ];
  }

}
