<?php

namespace common\models;

use Yii;
use common\config\ActiveRecord2;

/**
 * This is the model class for table "{{%es_log}}".
 *
 * @property integer $id
 * @property integer $option_type
 * @property integer $option_id
 * @property string $operation
 * @property string $c_date
 * @property integer $user_id
 * @property string $ip
 * 
 */
class Log extends ActiveRecord2
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'es_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_type', 'option_id', 'user_id'], 'integer'],
            [['c_date'], 'safe'],
            [['operation'], 'string', 'max' => 10],
            [['ip'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'option_type' => Yii::t('app', 'Option Type'),
            'option_id' => Yii::t('app', 'Option ID'),
            'operation' => Yii::t('app', 'Operation'),
            'c_date' => Yii::t('app', 'Creation Date'),
            'user_id' => Yii::t('app', 'User ID'),
            'ip' => Yii::t('app', 'IP Adrress'),
        ];
    }
}
