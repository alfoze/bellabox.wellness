/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cssMin = require('gulp-css');
var gzip = require('gulp-gzip');
var gutil = require('gulp-util');
var filelog = require('gulp-filelog');

tasks = [];
cssFiles = [
    //['./plugins/bootstrap/css/bootstrap.css', 'bootstrap.min.css', './plugins/bootstrap/css'],
    //['./css/headers/header-v3.css', 'header-v3.min.css', './css/headers'],
    //['./css/footers/footer-v4.css', 'footer-v4.min.css', './css/footers'],
    //['./plugins/animate.css', 'animate.min.css', './plugins'],
    //['./plugins/line-icons/line-icons.css', 'line-icons.min.css', './plugins/line-icons'],
    //['./plugins/font-awesome/css/font-awesome.css', 'font-awesome.min.css', './plugins/font-awesome/css'],
    //['./plugins/flexslider/flexslider.css', 'flexslider.min.css', './plugins/flexslider'],
    //['./plugins/fancybox/source/jquery.fancybox.css', 'jquery.fancybox.min.css', './plugins/fancybox/source'],
    //['./plugins/revolution-slider/rs-plugin/css/settings.css', 'settings.min.css', './plugins/revolution-slider/rs-plugin/css'],
    //['./css/owl.carousel.css', 'owl.carousel.min.css', './css'],
    //['./plugins/sky-forms-pro/skyforms/css/sky-forms.css', 'sky-forms.min.css', './plugins/sky-forms-pro/skyforms/css'],
    //['./plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css', 'custom-sky-forms.min.css', './plugins/sky-forms-pro/skyforms/custom'],
    //["./plugins/owl-carousel/owl-carousel/owl.carousel.css", 'owl.carousel.min.css', './plugins/owl-carousel/owl-carousel'],
    //["./plugins/layer-slider/layerslider/css/layerslider.css", 'layerslider.min.css', './plugins/layer-slider/layerslider/css'],
   // ['./css/pages/page_search.css', 'page_search.min.css', './css/pages'],
   // ['./css/pages/page_clients.css', 'page_clients.min.css', './css/pages'],
   // ['./css/pages/shortcode_timeline1.css', 'shortcode_timeline1.min.css', './css/pages'],
   // ['./css/pages/profile.css', 'profile.min.css', './css/pages'],
    ['./css/style.css', 'style.min.css', './css'],
   // ['./css/theme-colors/blue.css', 'blue.min.css', './css/theme-colors'],
   // ['./css/custom.css', 'custom.min.css', './css'],
   // ['./css/app.css', 'app.min.css', './css']
   //['./plugins/bootstrap/css/bootstrap.min.css', 'bootstrap.min.css', './plugins/bootstrap/css']
];

for (var i = 0; i < cssFiles.length; i++) {
    file = cssFiles[i][0];
    name = cssFiles[i][1];
    dest = cssFiles[i][2];
    taks1 = 'css' + i;
    taks2 = 'csszip' + i;

    (function (i) {
        gulp.task(taks1, function () {
            return gulp.src(file)
                    .pipe(concat(name))
                    .pipe(cssMin())
                    .pipe(filelog('Minify'))
                    .pipe(gulp.dest(dest));
        });

    })(i);

    tasks.push(taks1);
    gulp.task(taks2, function () {
        return gulp.src(file)
                .pipe(concat(name))
                .pipe(cssMin())
                .pipe(gzip())
                .pipe(filelog('gZip'))
                .pipe(gulp.dest(dest));

    });
    tasks.push(taks2);
}
gulp.task('default', tasks);


jsFiles = [
//        ['./plugins/jquery/jquery-migrate.js', 'jquery-migrate.min.js', './plugins/jquery'],
//    ['./js/owl.carousel.min.js', 'owl.carousel.min.js', './js/owl'],
//    ['./plugins/flexslider/jquery.flexslider.js', 'jquery.flexslider.min.js', './plugins/flexslider'],
//    ['./plugins/back-to-top.js', 'back-to-top.min.js', './plugins'],
//    ['./plugins/smoothScroll.js', 'smoothScroll.min.js', './plugins'],
//    ['./plugins/layer-slider/layerslider/js/greensock.js', 'greensock.min.js', './plugins/layer-slider/layerslider/js'],
//    ['./plugins/layer-slider/layerslider/js/layerslider.transitions.js', 'layerslider.transitions.min.js', './plugins/layer-slider/layerslider/js'],
//    ['./plugins/layer-slider/layerslider/js/layerslider.kreaturamedia.jquery.js', 'layerslider.kreaturamedia.jquery.min.js', './plugins/layer-slider/layerslider/js'],
//    ['./js/plugins/layer-slider.js', 'layer-slider.min.js', './js/plugins'],
//    ['./plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.js', 'jquery.themepunch.tools.min.js', './plugins/revolution-slider/rs-plugin/js'],
//    ['./plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.js', 'jquery.themepunch.revolution.min.js', './plugins/revolution-slider/rs-plugin/js'],
//    ['./plugins/jquery.parallax.js', 'jquery.parallax.min.js', './plugins'],
//    ['./plugins/counter/waypoints.js', 'waypoints.min.js', './plugins/counter'],
//    ['./plugins/counter/jquery.counterup.js', 'jquery.counterup.min.js', './plugins/counter'],
//    ['./plugins/fancybox/source/jquery.fancybox.pack.js', 'jquery.fancybox.pack.min.js', './plugins/fancybox/source'],
//    ['./js/custom.js', 'custom.min.js', './js'],
//    ['./js/app.js', 'app.min.js', './js'],
//    ['./js/plugins/fancy-box.js', 'fancy-box.min.js', './js/plugins'],
//    ["./js/plugins/revolution-slider.js", 'revolution-slider.min.js', './js/plugins'],
//    ['./js/plugins/owl-carousel.js', 'owl-carousel.min.js', './js/plugins']
//      ['./plugins/jquery/jquery.min.js', 'jquery.min.js', './plugins/jquery'],
//      ['./plugins/bootstrap/js/bootstrap.min.js', 'bootstrap.min.js', './plugins/bootstrap/js'],
];

for (var i = 0; i < jsFiles.length; i++) {
    file = jsFiles[i][0];
    name = jsFiles[i][1];
    dest = jsFiles[i][2];
    taks1 = 'js' + i;
    taks2 = 'jszip' + i;

    (function (i) {
        gulp.task(taks1, function () {
            return gulp.src(file)
                    .pipe(concat(name))
                    .pipe(uglify())
                    .pipe(filelog('Minify'))
                    .pipe(gulp.dest(dest));
        });

    })(i);

    tasks.push(taks1);
    gulp.task(taks2, function () {
        return gulp.src(file)
                .pipe(concat(name))
                .pipe(uglify())
                .pipe(gzip())
                .pipe(filelog('gZip'))
                .pipe(gulp.dest(dest));

    });
    tasks.push(taks2);
}
gulp.task('default', tasks);
