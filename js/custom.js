/* Write here your custom javascript codes */
var base_url = '/eshtry/frontend/web';

(function ($) {

    $(document).on('click', '.showModalButton', function () {
        //check if the modal is open. if it's open just reload content not whole modal
        //also this allows you to nest buttons inside of modals to reload the content it is in
        //the if else are intentionally separated instead of put into a function to get the 
        //button since it is using a class not an #id so there are many of them and we need
        //to ensure we get the right button and content. 
        //if modal isn't open; open it and load content
        $('#modal').modal('show')
                .find('#modalContent')
                .load($(this).attr('value'));
        //dynamiclly set the header for the modal
        // document.getElementById('modalHeaderTitle').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';

    });
    // Data background cover
    $('*[data-cover]').each(function () {
        var coverTarget = $(this).attr('data-cover');
        var coverimage = $(this).find(coverTarget).attr('src');
        $(this).css('background-image', 'url(' + coverimage + ')');
    });

    // Data slide
    $('*[data-slide]').on('click', function () {
        var slideTarget = $(this).attr('data-slide');
        $('html, body').animate({
            scrollTop: $(slideTarget).offset().top
        }, 1000);
        return false;
    });

    // Slider
    $('.witb_slider ul').bxSlider({
        mode: 'fade',
        pager: false,
        adaptiveHeight: true
    });

    // Get started mobile
    $('.get_started ol > li h3 .trigger').on('click', function () {
        $(this).parent().parent().toggleClass('active');
        $(this).parent().next('div').slideToggle();
        return false;
    });

    // Show/hide input value
    $('input[type="text"], input[type="password"], input[type="email"]').each(function () {
        var valtxt = $(this).attr('value');
        $(this).focus(function () {
            if ($(this).val() == valtxt) {
                $(this).val('');
            }
        });
        $(this).blur(function () {
            if ($(this).val() == '') {
                $(this).val(valtxt);
            }
        });
    });
    $("textarea").focus(function () {
        if (this.value === this.defaultValue) {
            this.value = '';
        }
    }).blur(function () {
        if (this.value === '') {
            this.value = this.defaultValue;
        }
    });

})(jQuery);


function readURL_signup(input) {
    $('#signup_upload_preview').show();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#signup_upload_preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#signupform-pic").change(function () {
    readURL_signup(this);

});