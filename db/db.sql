-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2017 at 10:23 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bellanz`
--

-- --------------------------------------------------------

--
-- Table structure for table `allocation`
--

CREATE TABLE `allocation` (
  `id` int(11) NOT NULL,
  `customer_id` varchar(50) DEFAULT NULL,
  `subscription_id` varchar(50) NOT NULL,
  `box_name` varchar(100) NOT NULL,
  `mail_ref` varchar(50) NOT NULL,
  `created_when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `batch_id` int(11) NOT NULL,
  `box_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `allocation`
--

INSERT INTO `allocation` (`id`, `customer_id`, `subscription_id`, `box_name`, `mail_ref`, `created_when`, `batch_id`, `box_id`) VALUES
(2, 'Hr551BIQX9frnY5YxB', '', 'WomenOctober17A', 'm.zulkifal@gmail.com', '2017-10-16 14:23:01', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `allocation_list`
--

CREATE TABLE `allocation_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `allocated` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `beautyprofile`
--

CREATE TABLE `beautyprofile` (
  `id` int(11) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beautyprofile`
--

INSERT INTO `beautyprofile` (`id`, `customer_id`, `page_id`, `question_id`, `answer`) VALUES
(245, 'Hr551BIQX9frnY5YxB', 1, 1, 4),
(228, 'Hr551BIQX9frnY5YxB', 2, 6, 31),
(227, 'Hr551BIQX9frnY5YxB', 2, 6, 30),
(226, 'Hr551BIQX9frnY5YxB', 2, 6, 29),
(225, 'Hr551BIQX9frnY5YxB', 2, 6, 27),
(224, 'Hr551BIQX9frnY5YxB', 2, 6, 26),
(223, 'Hr551BIQX9frnY5YxB', 2, 6, 25),
(222, 'Hr551BIQX9frnY5YxB', 2, 5, 23),
(221, 'Hr551BIQX9frnY5YxB', 2, 4, 20),
(220, 'Hr551BIQX9frnY5YxB', 2, 4, 18),
(219, 'Hr551BIQX9frnY5YxB', 2, 4, 16),
(218, 'Hr551BIQX9frnY5YxB', 2, 4, 14),
(217, 'Hr551BIQX9frnY5YxB', 2, 3, 12),
(216, 'Hr551BIQX9frnY5YxB', 2, 2, 8),
(244, 'Hr551BIQX9frnY5YxB', 1, 1, 2),
(229, 'Hr551BIQX9frnY5YxB', 0, 2, 8),
(230, 'Hr551BIQX9frnY5YxB', 0, 3, 12),
(231, 'Hr551BIQX9frnY5YxB', 0, 4, 14),
(232, 'Hr551BIQX9frnY5YxB', 0, 4, 16),
(233, 'Hr551BIQX9frnY5YxB', 0, 4, 18),
(234, 'Hr551BIQX9frnY5YxB', 0, 4, 20),
(235, 'Hr551BIQX9frnY5YxB', 0, 5, 23),
(236, 'Hr551BIQX9frnY5YxB', 0, 6, 25),
(237, 'Hr551BIQX9frnY5YxB', 0, 6, 26),
(238, 'Hr551BIQX9frnY5YxB', 0, 6, 27),
(239, 'Hr551BIQX9frnY5YxB', 0, 6, 29),
(240, 'Hr551BIQX9frnY5YxB', 0, 6, 30),
(241, 'Hr551BIQX9frnY5YxB', 0, 6, 31);

-- --------------------------------------------------------

--
-- Table structure for table `beautyprofileanswer`
--

CREATE TABLE `beautyprofileanswer` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beautyprofileanswer`
--

INSERT INTO `beautyprofileanswer` (`id`, `question_id`, `name`, `image`, `description`) VALUES
(1, 1, 'Classic & Chic', 'img/beautyprofile/0001.png', ''),
(2, 1, 'Girly & Feminine', 'img/beautyprofile/0002.png', ''),
(3, 1, 'All about the natural look', 'img/beautyprofile/0003.png', ''),
(4, 1, 'Trend-based & Fashion Forward', 'img/beautyprofile/0004.png', ''),
(5, 1, 'Glamorous & Seductive', 'img/beautyprofile/0005.png', ''),
(6, 1, 'Low-maintenance & Carefree', 'img/beautyprofile/0006.png', ''),
(7, 2, 'Light', 'img/beautyprofile/a0001.png', 'Peaches & Cream, fair'),
(8, 2, 'Medium', 'img/beautyprofile/a0002.png', 'Mid-tones, tan & olive'),
(9, 2, 'Dark', 'img/beautyprofile/a0003.png', 'Deep to dark brown'),
(10, 3, 'Never/ Not Interested', NULL, ''),
(11, 3, 'Never/ Keen to Try', NULL, ''),
(12, 3, 'Regularly', NULL, ''),
(13, 3, 'Occasionally', NULL, ''),
(14, 4, 'Wax', NULL, ''),
(15, 4, 'Laser hair removal', NULL, ''),
(16, 4, 'Razor/ Shaving', NULL, ''),
(17, 4, 'Epilator', NULL, ''),
(18, 4, 'Threading', NULL, ''),
(19, 4, 'Hair removal cream', NULL, ''),
(20, 4, 'I don\'t remove my hair', NULL, ''),
(21, 5, 'Dry', NULL, ''),
(22, 5, 'Oily', NULL, ''),
(23, 5, 'Combination', NULL, ''),
(24, 5, 'Normal', NULL, ''),
(25, 6, 'Acen', NULL, ''),
(26, 6, 'Lines / Wrinkles', NULL, ''),
(27, 6, 'Pigmentation / Uneven tone', NULL, ''),
(28, 6, 'Sensitive Skin', NULL, ''),
(29, 6, 'Redness', NULL, ''),
(30, 6, 'Skin colour too pale', NULL, ''),
(31, 6, 'Skin colour too dark', NULL, ''),
(32, 6, 'Dark Circles / Puffy Eyes', NULL, ''),
(33, 6, 'Sun Protection', NULL, ''),
(34, 6, 'Body Odour', NULL, ''),
(35, 6, 'Stratch Marks', NULL, ''),
(36, 6, 'Cellulite', NULL, ''),
(37, 6, 'Thinning hair and damaged hair', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `beautyprofilepage`
--

CREATE TABLE `beautyprofilepage` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `weight` int(11) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beautyprofilepage`
--

INSERT INTO `beautyprofilepage` (`id`, `name`, `weight`, `active`) VALUES
(1, 'Beauty Style', 10, '1'),
(2, 'Skin', 20, '1'),
(3, 'Hair', 30, '1'),
(4, 'Beauty Indulgence', 40, '1'),
(5, 'Beauty Products', 50, '1'),
(6, 'About Me', 60, '1');

-- --------------------------------------------------------

--
-- Table structure for table `beautyprofilequestion`
--

CREATE TABLE `beautyprofilequestion` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `weight` int(11) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT '1',
  `type` varchar(1) NOT NULL DEFAULT 'C'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beautyprofilequestion`
--

INSERT INTO `beautyprofilequestion` (`id`, `page_id`, `name`, `weight`, `active`, `type`) VALUES
(1, 1, 'How would you describe your personal beauty style?', 10, '1', 'C'),
(2, 2, 'Which of the following best describes your skin colouring?', 10, '1', 'R'),
(3, 2, 'How often do you use self-tanning product?', 20, '1', 'R'),
(4, 2, 'Which types of hair removal do you use?', 30, '1', 'C'),
(5, 2, 'Which best describes your skin type?', 40, '1', 'R'),
(6, 2, 'What are your key beauty concerns', 50, '1', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `box`
--

CREATE TABLE `box` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `variant_name` varchar(100) NOT NULL,
  `type` varchar(5) NOT NULL,
  `month` int(11) NOT NULL,
  `year` varchar(4) NOT NULL,
  `image` varchar(400) NOT NULL,
  `samples` varchar(1000) NOT NULL,
  `status` varchar(1) NOT NULL,
  `typeform_url` varchar(255) NOT NULL,
  `typeform_code` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `box`
--

INSERT INTO `box` (`id`, `name`, `variant_name`, `type`, `month`, `year`, `image`, `samples`, `status`, `typeform_url`, `typeform_code`) VALUES
(1, 'Beauty Tricks & Treats', 'WomenOctober17A', 'women', 10, '2017', 'img/box_img_oct_1.jpg', 'BURT-OIL-SAMPLE\r\nTRPZ-MOUS-SAMPLE\r\nKRTS-ELXR-SAMPLE\r\nNATU-SOAP-SAMPLE\r\nMOCC-KIT-SAMPLE', '1', 'https://zulkifl1.typeform.com/to/bdvRWT', 'bdvRWT');

-- --------------------------------------------------------

--
-- Table structure for table `es_contactus`
--

CREATE TABLE `es_contactus` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `c_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `es_faq`
--

CREATE TABLE `es_faq` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `c_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `question_ar` varchar(255) DEFAULT NULL,
  `answer_ar` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `es_logs`
--

CREATE TABLE `es_logs` (
  `id` int(11) NOT NULL,
  `option_type` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `c_date` timestamp NULL DEFAULT NULL,
  `operation` varchar(10) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `es_settings`
--

CREATE TABLE `es_settings` (
  `id` int(11) NOT NULL,
  `g_name` varchar(155) DEFAULT NULL,
  `g_email` varchar(45) DEFAULT NULL,
  `g_phone` varchar(45) DEFAULT NULL,
  `g_info` text,
  `a_name` varchar(45) DEFAULT NULL,
  `a_email` varchar(45) DEFAULT NULL,
  `a_phone` varchar(45) DEFAULT NULL,
  `a_info` text,
  `h_name` varchar(45) DEFAULT NULL,
  `h_email` varchar(45) DEFAULT NULL,
  `h_phone` varchar(45) DEFAULT NULL,
  `h_info` text,
  `c_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `def_country` int(11) DEFAULT NULL,
  `site_logo` varchar(255) NOT NULL,
  `ar_site_logo` varchar(255) NOT NULL,
  `homepage_text` varchar(200) NOT NULL,
  `home_text_1` varchar(200) NOT NULL,
  `error_message` text NOT NULL,
  `publish_message` text NOT NULL,
  `def_language` varchar(6) DEFAULT 'en',
  `request_status` varchar(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `es_settings`
--

INSERT INTO `es_settings` (`id`, `g_name`, `g_email`, `g_phone`, `g_info`, `a_name`, `a_email`, `a_phone`, `a_info`, `h_name`, `h_email`, `h_phone`, `h_info`, `c_timestamp`, `def_country`, `site_logo`, `ar_site_logo`, `homepage_text`, `home_text_1`, `error_message`, `publish_message`, `def_language`, `request_status`) VALUES
(1, 'john', 'example@yahoo.com', '021-887666', 'Manager', 'micke', 'mick@gmail.com', '087-765642345', 'Test', 'mani', 'mani@hotmail.com', '09-76657', 'headquater', NULL, 160, '/uploads/573636cb22588maxresdefault.jpg', '/eshtry/frontend/web/images/5682601f1b6b3ar_final_logo.png', 'We help foodies to find different type of cuisines quickly and conveniently on a single platform', 'Making it most trusted food portal', '', '', 'en', '1');

-- --------------------------------------------------------

--
-- Table structure for table `es_user`
--

CREATE TABLE `es_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `f_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_name` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_admin` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_active` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cb_customer_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `es_user`
--

INSERT INTO `es_user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `f_name`, `l_name`, `c_timestamp`, `is_admin`, `is_active`, `company_name`, `mobile`, `cb_customer_id`) VALUES
(1, 'admin', 'Vi1TKuWixfBm5gpRchSfypE15mppp4Hy', '$2y$13$Lu7yJJYLuP7wj4SGxXYxw.KJJYZmnfzSnADXKPBVE0nk8brxclWya', 'ldcKrbgnBYty9s4DUstaIJeYwiXcBZ_O_1463647037', 'anwaarnuml@gmail.com', 10, 1445238113, 1472070257, 'Zulkifl', 'Muhammad', NULL, '1', '1', '', '030005465123', 'Hr551BIQX9frnY5YxB'),
(4, 'wer', 'pYlMpXgqqtGA6i36QOt5EAY8Kdlh53eR', '$2y$13$u.ZZCVgmEenGcSoyaNkNeOhFnFdELdiRqAjV63Kp7jmbgMjEsTy1C', '4Ctc4dpdI_m_dA50JEOGCsD_pDSgBbxY_1468307956', 'm.anwaar@yahoo.com', 10, 1445415586, 1480922892, 'Ali', '', NULL, '0', '1', '', '0556 546 546', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `survey_id` varchar(50) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_id` varchar(50) NOT NULL,
  `created_when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(200) NOT NULL,
  `cb_name` varchar(50) NOT NULL,
  `cb_url` varchar(500) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT '1',
  `gift` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name`, `description`, `cb_name`, `cb_url`, `active`, `gift`) VALUES
(1, 'monthly', 'Monthly NZ$ 24.95 Cancel Any Time', 'bellabox-nz-test', 'https://bellabox-test.chargebee.com/hosted_pages/plans/bellabox-nz-test', '1', '0'),
(2, 'quarterly', 'Quarterly NZ$ 70', 'bellabox-nz-quarter-test', 'https://bellabox-test.chargebee.com/hosted_pages/plans/bellabox-nz-quarter-test', '1', '0'),
(3, 'gift-3month', 'Gift - 3 Months', 'bellabox-nz-3month-gift-test', 'https://bellabox-test.chargebee.com/hosted_pages/plans/bellabox-nz-3month-gift-test', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `type` varchar(10) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `org_price` decimal(10,0) NOT NULL,
  `status` varchar(1) NOT NULL,
  `image` varchar(255) NOT NULL,
  `color` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `brand` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reasons`
--

CREATE TABLE `reasons` (
  `id` int(11) NOT NULL,
  `name` varchar(99) NOT NULL,
  `type` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reasons`
--

INSERT INTO `reasons` (`id`, `name`, `type`) VALUES
(1, 'I just want a break', 'P'),
(2, 'Travelling or on vacation', 'P'),
(3, 'I can\'t afford it for now', 'P'),
(4, 'Haven\'t used my current products', 'P'),
(5, 'Moving houses/address', 'P'),
(6, 'I have now received and trialed enough products, thank you!', 'C'),
(7, 'I\'m only leaving for a while and might be back soon!', 'C'),
(8, 'My beauty profile is not reflected in my boxes', 'C'),
(9, 'There isn\'t enough variety of products month to month', 'C'),
(10, 'I don\'t like enough of the products I have sampled to stay', 'C'),
(11, 'I wanted to try bellabox for one month only', 'C'),
(12, 'I had too many postage problems', 'C'),
(13, 'I had too many payment problems', 'C'),
(14, 'I had too many missing product/faulty/broken item problems', 'C'),
(15, 'I can\'t afford it anymore', 'C'),
(16, 'I\'m not satisfied by the customer service', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `reward_point`
--

CREATE TABLE `reward_point` (
  `id` int(11) NOT NULL,
  `reason` varchar(100) NOT NULL,
  `reason_code` varchar(1) NOT NULL,
  `points` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'U',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiry_date` datetime DEFAULT NULL,
  `expired` varchar(1) NOT NULL DEFAULT '0',
  `customer_id` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reward_point`
--

INSERT INTO `reward_point` (`id`, `reason`, `reason_code`, `points`, `status`, `create_date`, `expiry_date`, `expired`, `customer_id`) VALUES
(1, 'Points awarded for purchasing.', 'P', 17, 'A', '2017-10-16 19:24:30', '2108-10-17 00:00:00', '0', 'Hr551BIQX9frnY5YxB');

-- --------------------------------------------------------

--
-- Table structure for table `source_message`
--

CREATE TABLE `source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(32) DEFAULT NULL,
  `message` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allocation`
--
ALTER TABLE `allocation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `allocation_list`
--
ALTER TABLE `allocation_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beautyprofile`
--
ALTER TABLE `beautyprofile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beautyprofileanswer`
--
ALTER TABLE `beautyprofileanswer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beautyprofilepage`
--
ALTER TABLE `beautyprofilepage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beautyprofilequestion`
--
ALTER TABLE `beautyprofilequestion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `box`
--
ALTER TABLE `box`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `es_contactus`
--
ALTER TABLE `es_contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `es_faq`
--
ALTER TABLE `es_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `es_logs`
--
ALTER TABLE `es_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_1013_idx` (`user_id`);

--
-- Indexes for table `es_settings`
--
ALTER TABLE `es_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `es_user`
--
ALTER TABLE `es_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`,`language`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reasons`
--
ALTER TABLE `reasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reward_point`
--
ALTER TABLE `reward_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `source_message`
--
ALTER TABLE `source_message`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allocation`
--
ALTER TABLE `allocation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `allocation_list`
--
ALTER TABLE `allocation_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `beautyprofile`
--
ALTER TABLE `beautyprofile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;
--
-- AUTO_INCREMENT for table `beautyprofileanswer`
--
ALTER TABLE `beautyprofileanswer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `beautyprofilepage`
--
ALTER TABLE `beautyprofilepage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `beautyprofilequestion`
--
ALTER TABLE `beautyprofilequestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `box`
--
ALTER TABLE `box`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `es_contactus`
--
ALTER TABLE `es_contactus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `es_faq`
--
ALTER TABLE `es_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `es_logs`
--
ALTER TABLE `es_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `es_settings`
--
ALTER TABLE `es_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `es_user`
--
ALTER TABLE `es_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reasons`
--
ALTER TABLE `reasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `reward_point`
--
ALTER TABLE `reward_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `source_message`
--
ALTER TABLE `source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `es_logs`
--
ALTER TABLE `es_logs`
  ADD CONSTRAINT `fk_1013` FOREIGN KEY (`user_id`) REFERENCES `es_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
