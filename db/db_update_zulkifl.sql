/* On 2017-10-19 14:00 by Zulkifl */
ALTER TABLE `es_user` CHANGE `company_name` `company_name` VARCHAR(255)  NULL;


/* On 2017-10-23 00:00 by Zulkifl */

ALTER TABLE `allocation` ADD `order_id` VARCHAR(50) NOT NULL AFTER `subscription_id`;

/* On 2017-10-23 15:24 by Zulkifl */


CREATE TABLE `brand` (`id` int(11) NOT NULL,`name` varchar(100) NOT NULL );
ALTER TABLE `brand` ADD PRIMARY KEY (`id`);
ALTER TABLE `brand`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `colors` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`));
/* On 2017-10-24 11:15 By Zulkifl */

ALTER TABLE `product` CHANGE `fullname` `fullname` VARCHAR(200) NULL, CHANGE `price` `price` DECIMAL(10) NULL, CHANGE `org_price` `org_price` DECIMAL(10) NULL, CHANGE `image` `image` VARCHAR(255) NULL, CHANGE `color` `color` VARCHAR(100) NULL, CHANGE `size` `size` VARCHAR(100) NULL, CHANGE `brand` `brand` INT(11) NULL;
ALTER TABLE `box` CHANGE `image` `image` VARCHAR(400)   NULL, CHANGE `samples` `samples` VARCHAR(1000)   NULL, CHANGE `typeform_url` `typeform_url` VARCHAR(255)   NULL, CHANGE `typeform_code` `typeform_code` VARCHAR(50)   NULL;
ALTER TABLE `beautyprofileanswer` CHANGE `description` `description` VARCHAR(100)  NULL;
ALTER TABLE `es_settings`  DROP `g_name`,  DROP `g_email`,  DROP `g_phone`,  DROP `g_info`,  DROP `a_name`,  DROP `a_email`,  DROP `a_phone`,  DROP `a_info`,  DROP `h_name`,  DROP `h_email`,  DROP `h_phone`,  DROP `h_info`,  DROP `c_timestamp`,  DROP `def_country`,  DROP `ar_site_logo`,  DROP `homepage_text`,  DROP `home_text_1`,  DROP `error_message`,  DROP `publish_message`,  DROP `def_language`,  DROP `request_status`;
CREATE TABLE `contents` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(200) NOT NULL , `path` VARCHAR(200) NOT NULL ,  PRIMARY KEY (`id`));
ALTER TABLE `es_settings` ADD `name` VARCHAR(50) NOT NULL AFTER `id`;
ALTER TABLE `es_settings` ADD `cb_name` VARCHAR(50) NOT NULL AFTER `name`;
ALTER TABLE `es_settings` ADD `cb_api` VARCHAR(200) NOT NULL AFTER `cb_name`;

/* On 2017-10-26 2:08 by Zulkifl */

ALTER TABLE `es_settings` ADD `billing_day` INT NOT NULL AFTER `footer_id`;
ALTER TABLE `es_settings` CHANGE `billing_day` `billing_day` INT(11) NOT NULL DEFAULT '7';

/* On 2017-10-31 2:22 by Zulkifl */

ALTER TABLE `beautyprofile` ADD `answer_text` VARCHAR(100) NULL AFTER `answer`, ADD `answer_date` DATE NULL ;
ALTER TABLE `beautyprofile` CHANGE `answer` `answer` INT(11) NULL;
ALTER TABLE `es_settings` ADD `homepage` INT NULL AFTER `billing_day`;
ALTER TABLE `es_user` ADD `tmp_pass` VARCHAR(100) NULL AFTER `cb_customer_id`;
ALTER TABLE `newsletter_emails` ADD `ctimestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `email`;

/* On 2018-01-04 10:50 by Zulkifl */

ALTER TABLE `es_settings` ADD `giftpage` INT NULL AFTER `homepage`;
/* On 2019-05-02 10:50 by Zulkifl */
ALTER TABLE `newsletter_emails` ADD `name` VARCHAR(100) NOT NULL AFTER `ctimestamp`, ADD `last_name` VARCHAR(100) NULL AFTER `name`;