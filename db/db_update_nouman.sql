/* On 2017-10-18 15:00 by Nouman */
DROP TABLE IF EXISTS divs;
DROP TABLE IF EXISTS landingpages;
DROP TABLE IF EXISTS pages;
CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `page_header` varchar(255) NOT NULL,
  `page_html` longtext,
  `page_status` enum('inactive','active') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `pages`  ADD PRIMARY KEY (`page_id`);
ALTER TABLE `pages` MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
CREATE TABLE `landingpages` (
  `landingpages_id` int(11) NOT NULL,
  `landingpages_name` varchar(100) NOT NULL,
  `landingpages_header` varchar(100) NOT NULL,
  `landingpages_created_on` datetime NOT NULL,
  `landingpages_created_by` int(11) NOT NULL,
  `landingpages_updated_on` datetime DEFAULT NULL,
  `landingpages_status` enum('Inactive','Active') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `landingpages` ADD PRIMARY KEY (`landingpages_id`);
ALTER TABLE `landingpages` MODIFY `landingpages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
CREATE TABLE `divs` (
  `div_id` int(11) NOT NULL,
  `div_name` varchar(100) NOT NULL,
  `div_weight` int(11) NOT NULL,
  `pages_page_id` int(11) NOT NULL,
  `landingpages_id` int(11) DEFAULT NULL,
  `div_status` enum('inactive','active') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `divs` ADD PRIMARY KEY (`div_id`), ADD KEY `pages_page_id` (`pages_page_id`), ADD KEY `landingpages_id` (`landingpages_id`);
ALTER TABLE `divs` MODIFY `div_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
ALTER TABLE `divs` ADD CONSTRAINT `landingpage_page_id` FOREIGN KEY (`landingpages_id`) REFERENCES `landingpages` (`landingpages_id`),
  ADD CONSTRAINT `pages_page_id` FOREIGN KEY (`pages_page_id`) REFERENCES `pages` (`page_id`);

/*-----------------------------------------------------------------------------------------------------------------*/
/* On 2017-10-19 05:50 PM by Nouman */
DROP TABLE IF EXISTS divs;
DROP TABLE IF EXISTS landingpages;
DROP TABLE IF EXISTS pages;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `html` longtext,
  `status` enum('Inactive','Active') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `pages` ADD PRIMARY KEY (`id`);
ALTER TABLE `pages` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

CREATE TABLE `landingpages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `header` varchar(100) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('Inactive','Active') NOT NULL,
  `can_delete` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `landingpages` ADD PRIMARY KEY (`id`);
ALTER TABLE `landingpages` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

CREATE TABLE `divs` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `weight` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `landingpage_id` int(11) DEFAULT NULL,
  `status` enum('Inactive','Active') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `divs` ADD PRIMARY KEY (`id`), ADD KEY `pages_page_id` (`page_id`), ADD KEY `landingpages_id` (`landingpage_id`);
ALTER TABLE `divs` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
ALTER TABLE `divs`
  ADD CONSTRAINT `landingpage_page_id` FOREIGN KEY (`landingpage_id`) REFERENCES `landingpages` (`id`),
  ADD CONSTRAINT `pages_page_id` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`);
/*-----------------------------------------------------------------------------------------------------------------*/
/* On 2017-10-19 05:50 PM by Nouman */
ALTER TABLE `pages` ADD `can_delete` INT NULL AFTER `status`;
ALTER TABLE `es_settings` ADD `header_id` INT NULL AFTER `request_status`, ADD `footer_id` INT NULL AFTER `header_id`;

/*-----------------------------------------------------------------------------------------------------------------*/
/* On 2017-11-01 11:36 PM by Nouman */
ALTER TABLE  `beautyprofilepage` ADD  `div_size` VARCHAR( 1 ) NULL DEFAULT '1' AFTER  `weight` ;
/*-----------------------------------------------------------------------------------------------------------------*/
/* On 2017-11-02 05:16 PM by Nouman */
ALTER TABLE `beautyprofilequestion` ADD `required` VARCHAR(1) NOT NULL DEFAULT '0' AFTER `type`;
/*-----------------------------------------------------------------------------------------------------------------*/
/* On 2017-11-16 10:50 PM by Nouman */
ALTER TABLE `product` ADD `typeform_url` VARCHAR(255) NULL AFTER `brand`, ADD `typeform_code` VARCHAR(50) NULL AFTER `typeform_url`;
/*-----------------------------------------------------------------------------------------------------------------*/
/* On 2017-11-20 12:54 PM by Nouman */
ALTER TABLE `feedback` ADD `box_id` INT NULL AFTER `survey_id`;
/*-----------------------------------------------------------------------------------------------------------------*/
/* On 2017-11-21 03:25 PM by Nouman */
CREATE TABLE `bellanz`.`newsletter_emails`
(
   `id` INT(11) NOT NULL AUTO_INCREMENT , 
   `email` VARCHAR(255) NOT NULL , 
   PRIMARY KEY (`id`)
);
/*-----------------------------------------------------------------------------------------------------------------*/